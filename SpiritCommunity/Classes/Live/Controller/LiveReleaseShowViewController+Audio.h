//
//  LiveReleaseShowViewController+Audio.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController.h"

@interface LiveReleaseShowViewController (Audio)

-(void)audioWithInit;

- (void)changeVoiceType:(QAVVoiceType)type ;

@end

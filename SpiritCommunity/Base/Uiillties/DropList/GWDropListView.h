//
//  GWDropListView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWDropListView : UIView

-(void)showInViewController:(UIViewController *)controller frame:(CGRect)listFrame listArr:(NSArray *)listArr block:(void(^)(NSString *chooseInfo))block;

@end

//
//  ShenheLiveSIngleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/10.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol ShenheLiveSIngleModel <NSObject>

@end

@interface ShenheLiveSIngleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *url;
@property (nonatomic,copy)NSString *des;
@property (nonatomic,copy)NSString *img;

@end

//
//  LiveUIHouseInfoView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveUIHouseInfoView.h"

@interface LiveUIHouseInfoView()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *zanLabel;
@property (nonatomic,assign)NSInteger zanCount;

@end

@implementation LiveUIHouseInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    if (!self.titleLabel){
        self.titleLabel = [GWViewTool createLabelFont:@"提示" textColor:@"白"];
        self.titleLabel.textAlignment = NSTextAlignmentRight;
        self.titleLabel.text = @"";
        self.titleLabel.shadowOffset = CGSizeMake(1, 1);
        self.titleLabel.shadowColor = [UIColor lightGrayColor];
        self.titleLabel.shadowOffset = CGSizeMake(.4f, .5f);
        self.titleLabel.frame = CGRectMake(0, LCFloat(7), self.size_width - LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
        [self addSubview:self.titleLabel];
    }
    if (!self.timeLabel){
        self.timeLabel = [GWViewTool createLabelFont:@"提示" textColor:@"白"];
        self.timeLabel.textAlignment = NSTextAlignmentRight;
        self.timeLabel.text = [NSDate getCurrentTime];
        self.timeLabel.shadowColor = [UIColor lightGrayColor];
        self.timeLabel.shadowOffset = CGSizeMake(.4f, .5f);
        self.timeLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(7), self.size_width - LCFloat(11), [NSString contentofHeightWithFont:self.timeLabel.font]);
        [self addSubview:self.timeLabel];
    }
    if (!self.zanLabel){
        self.zanLabel = [GWViewTool createLabelFont:@"提示" textColor:@"白"];
        self.zanLabel.textAlignment = NSTextAlignmentRight;
        self.zanLabel.text = [NSDate getCurrentTime];
        self.zanLabel.shadowColor = [UIColor lightGrayColor];
        self.zanLabel.shadowOffset = CGSizeMake(.4f, .5f);
        self.zanLabel.frame = CGRectMake(0, CGRectGetMaxY(self.timeLabel.frame) + LCFloat(7), self.size_width - LCFloat(11), [NSString contentofHeightWithFont:self.zanLabel.font]);
        [self addSubview:self.zanLabel];
    }
    
}

-(void)setTransferLiveSingleModel:(HomeLiveSingleModel *)transferLiveSingleModel{
    _transferLiveSingleModel = transferLiveSingleModel;
    self.titleLabel.text = [NSString stringWithFormat:@"房间号：%@",transferLiveSingleModel.group_id];
    self.zanLabel.text = [NSString stringWithFormat:@"%li赞",transferLiveSingleModel.count];
}


-(void)settingZan:(NSInteger)zan{
    self.zanCount = zan;
    self.zanLabel.text = [NSString stringWithFormat:@"%li赞",self.zanCount];
}

-(void)addZan{
    self.zanCount += 1;
    self.zanLabel.text = [NSString stringWithFormat:@"%li赞",self.zanCount];
}

+(CGFloat)calculationHeight{
    CGFloat height = 0;
    height += LCFloat(7);
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    height += LCFloat(7);
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    height += LCFloat(7);
    return height;
}
@end

//
//  HomeRootGoodsListMainSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/9.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol HomeRootGoodsListMainSingleModel <NSObject>

@end

@interface HomeRootGoodsListMainSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)NSTimeInterval create_time;         /**< 创建时间*/
@property (nonatomic,assign)CGFloat discount_price;
@property (nonatomic,copy)NSString *img_path;
@property (nonatomic,assign)CGFloat original_price;
@property (nonatomic,assign)NSInteger stock;
@property (nonatomic,copy)NSString *sub_title;
@property (nonatomic,copy)NSString *title;

@end

//
//  PDWXPayHandle.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"
#import "ChongZhiSingleModelSimple.h"
#define kWXPay_AppID      @"wxab8268b11e980213"
#define kWXPay_AppDesc    @"BeeTV"


@protocol WXApiManagerDelegate <NSObject>

@optional

- (void)managerDidRecvGetMessageReq:(GetMessageFromWXReq *)request;

- (void)managerDidRecvShowMessageReq:(ShowMessageFromWXReq *)request;

- (void)managerDidRecvLaunchFromWXReq:(LaunchFromWXReq *)request;

- (void)managerDidRecvMessageResponse:(SendMessageToWXResp *)response;

- (void)managerDidRecvAuthResponse:(SendAuthResp *)response;

- (void)managerDidRecvAddCardResponse:(AddCardToWXCardPackageResp *)response;

@end


@interface PDWXPayHandle : NSObject<WXApiDelegate>

+ (instancetype)sharedManager;
@property (nonatomic, assign) id<WXApiManagerDelegate> delegate;                // 代理
@property (nonatomic,copy)NSString *orderNumber;                                // 订单号
@property (nonatomic,assign)NSInteger coin;                                     // 充值的金币
@property (nonatomic, copy) NSString *notifyUrl;                                // 通知的网页

+ (void)regiset;                                                                // 注册
+ (id)payHandle;
- (void)sendPayRequestWithOrder:(ChongZhiSingleModelSimple *)order;
+(BOOL)handleOpenURL:(NSURL *)url delegate:(id<WXApiDelegate>) delegate;
@end


//
//  GWStupSQLiteManager.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWStupSQLiteManager.h"
#import <sqlite3.h>
#import "GWMessageSingleModel.h"
#define sqliteName @"mydatabase.sqlite"
#define sqlite_message @"Message"

@implementation GWStupSQLiteManager

+(void)steupSqlite{
    // 1. 寻找当前程序锁在沙盒下的Document
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 2.取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 3.附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:sqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(!ifFind) {
        NSLog(@"沙盒数据库文件不存在，需要复制!");
        // 把包里面的数据库文件 复制到沙盒的Documents 文件夹
        NSString * srcPath= [[NSBundle mainBundle]pathForResource:sqliteName ofType:nil];
        
        // 复制操作
        [fm copyItemAtPath:srcPath toPath:path error:nil];
    }
}


//
//+(NSMutableArray *)getListWithTableName{
//    NSMutableArray *list = [NSMutableArray array];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:sqliteName];
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    BOOL find = [fileManager fileExistsAtPath:path];
//    if (find){          // 如果找到
//        sqlite3 *database;
//        if(sqlite3_open([path UTF8String], &database) != SQLITE_OK) {         // 判断数据库是否打开
//            sqlite3_close(database);
//            [[UIAlertView alertViewWithTitle:@"错误" message:@"打开数据库文件失败" buttonTitles:@[@"确定"] callBlock:NULL] show];
//        } else {                // 3.准备sql语句
//            sqlite3_stmt *statement;
//            
//            // 用“limit 0,5”代替“top 5”
//            NSMutableString *query = [NSMutableString stringWithString:@"select "];
//            [query appendString:@" identify,messageType,title,content,direct,createTime,selected,msgImg"];
//            [query appendFormat:@" from %@ order by createTime desc",sqlite_message];
//            
//            // NSLog(@"query = %@",query);
//            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
//                while(sqlite3_step(statement) == SQLITE_ROW) {
//                    GWMessageSingleModel *messageSingleModel = [[GWMessageSingleModel alloc]init];
//                    // 1. id
//                    char *identifyTemp = (char *) sqlite3_column_text(statement,0);
//                    if (identifyTemp != NULL){
//                        messageSingleModel.identify = [NSString stringWithUTF8String:identifyTemp];
//                    } else {
//                        messageSingleModel.identify = @"1";
//                    }
//                    
//                    // 2. messageType
//                    messageSingleModel.messageType = (int)sqlite3_column_int(statement, 1);
//                    // 3. title
//                    char *titleTemp = (char *) sqlite3_column_text(statement,2);
//                    if (titleTemp != NULL){
//                        messageSingleModel.title = [NSString stringWithUTF8String:titleTemp];
//                    } else {
//                        messageSingleModel.title = @"";
//                    }
//                    // 4. content
//                    char *contentTemp = (char *) sqlite3_column_text(statement,3);
//                    if (contentTemp != NULL){
//                        messageSingleModel.content = [NSString stringWithUTF8String:contentTemp];
//                    } else {
//                        messageSingleModel.content = @"";
//                    }
//                    // 5. direct
//                    char *directTemp = (char *) sqlite3_column_text(statement,4);
//                    if (directTemp != NULL){
//                        messageSingleModel.direct = [NSString stringWithUTF8String:directTemp];
//                    } else {
//                        messageSingleModel.direct = @"";
//                    }
//                    // 6.time
//                    messageSingleModel.time = (int) sqlite3_column_text(statement,5);
//                    // 7. selected
//                    messageSingleModel.selected = (int)sqlite3_column_text(statement,6);
//                    
//                    // 8. msgImg
//                    char *msgImgTemp = (char *) sqlite3_column_text(statement,7);
//                    if (msgImgTemp != NULL){
//                        messageSingleModel.msgImg = [NSString stringWithUTF8String:msgImgTemp];
//                    } else {
//                        messageSingleModel.msgImg = @"";
//                    }
//                    
//                    [list addObject:messageSingleModel];
//                }
//            }
//            // 7.释放sql文资源
//            sqlite3_finalize(statement);
//            // 8.关闭iPhone上的sqlite3的数据库
//            sqlite3_close(database);
//        }
//    }
//    return list;
//}
//
//
#pragma mark - 插入数据库操作
+(BOOL)insertIntoSQLiteWithMusic:(NSString *)url file:(NSString *)file{

    BOOL result = false;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:sqliteName];
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if (ifFind){
        sqlite3 *database;
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK) {
            sqlite3_close(database);
            [[UIAlertView alertViewWithTitle:@"错误" message:@"打开数据库文件失败" buttonTitles:@[@"确定"] callBlock:NULL] show];
        } else {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"insert into %@",@"Music"];
            [sql appendFormat:@"(url,file)values('%@','%@')",url,file];
            
            char *errorMsg;
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK) {
                result=false;
                NSLog(@"error to exec %@",sql);
            } else {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;
}





+(NSString *)getFileWithURL:(NSString *)url {
    NSString *file = @"";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:sqliteName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL find = [fileManager fileExistsAtPath:path];
    if (find){          // 如果找到
        sqlite3 *database;
        if(sqlite3_open([path UTF8String], &database) != SQLITE_OK) {         // 判断数据库是否打开
            sqlite3_close(database);
            [[UIAlertView alertViewWithTitle:@"错误" message:@"打开数据库文件失败" buttonTitles:@[@"确定"] callBlock:NULL] show];
        } else {                // 3.准备sql语句
            sqlite3_stmt *statement;

            // 用“limit 0,5”代替“top 5”
            NSMutableString *query = [NSMutableString stringWithString:@"select "];
            [query appendString:@" file"];
            [query appendFormat:@" from %@ ",@"Music"];

            // NSLog(@"query = %@",query);
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    // 1. id
                    char *identifyTemp = (char *) sqlite3_column_text(statement,0);
                    if (identifyTemp != NULL){
                        file = [NSString stringWithUTF8String:identifyTemp];
                    } else {
                    
                    }
                }
            }
            // 7.释放sql文资源
            sqlite3_finalize(statement);
            // 8.关闭iPhone上的sqlite3的数据库
            sqlite3_close(database);
        }
    }
    return file;
}







#pragma mark - 消息
+(NSMutableArray *)getListWithMessageTableName{
    NSMutableArray *list = [NSMutableArray array];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:sqliteName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL find = [fileManager fileExistsAtPath:path];
    if (find){          // 如果找到
        sqlite3 *database;
        if(sqlite3_open([path UTF8String], &database) != SQLITE_OK) {         // 判断数据库是否打开
            sqlite3_close(database);
            [[UIAlertView alertViewWithTitle:@"错误" message:@"打开数据库文件失败" buttonTitles:@[@"确定"] callBlock:NULL] show];
        } else {                // 3.准备sql语句
            sqlite3_stmt *statement;
            
            // 用“limit 0,5”代替“top 5”
            NSMutableString *query = [NSMutableString stringWithString:@"select "];
            [query appendString:@" identify,messageType,title,content,direct,createTime,selected,msgImg"];
            [query appendFormat:@" from %@ order by createTime desc",sqlite_message];
            
            // NSLog(@"query = %@",query);
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    GWMessageSingleModel *messageSingleModel = [[GWMessageSingleModel alloc]init];
                    // 1. id
                    char *identifyTemp = (char *) sqlite3_column_text(statement,0);
                    if (identifyTemp != NULL){
                        messageSingleModel.identify = [NSString stringWithUTF8String:identifyTemp];
                    } else {
                        messageSingleModel.identify = @"1";
                    }
                    
                    // 2. messageType
                    messageSingleModel.messageType = (int)sqlite3_column_int(statement, 1);
                    // 3. title
                    char *titleTemp = (char *) sqlite3_column_text(statement,2);
                    if (titleTemp != NULL){
                        messageSingleModel.title = [NSString stringWithUTF8String:titleTemp];
                    } else {
                        messageSingleModel.title = @"";
                    }
                    // 4. content
                    char *contentTemp = (char *) sqlite3_column_text(statement,3);
                    if (contentTemp != NULL){
                        messageSingleModel.content = [NSString stringWithUTF8String:contentTemp];
                    } else {
                        messageSingleModel.content = @"";
                    }
                    // 5. direct
                    char *directTemp = (char *) sqlite3_column_text(statement,4);
                    if (directTemp != NULL){
                        messageSingleModel.direct = [NSString stringWithUTF8String:directTemp];
                    } else {
                        messageSingleModel.direct = @"";
                    }
                    // 6.time
                    messageSingleModel.time = (int) sqlite3_column_text(statement,5);
                    // 7. selected
                    messageSingleModel.selected = (int)sqlite3_column_text(statement,6);
                    
                    // 8. msgImg
                    char *msgImgTemp = (char *) sqlite3_column_text(statement,7);
                    if (msgImgTemp != NULL){
                        messageSingleModel.msgImg = [NSString stringWithUTF8String:msgImgTemp];
                    } else {
                        messageSingleModel.msgImg = @"";
                    }
                    
                    [list addObject:messageSingleModel];
                }
            }
            // 7.释放sql文资源
            sqlite3_finalize(statement);
            // 8.关闭iPhone上的sqlite3的数据库
            sqlite3_close(database);
        }
    }
    return list;
}


#pragma mark - 插入数据库操作
+(BOOL)insertIntoSQLiteWithMessageModel:(GWMessageSingleModel *)messageModel{
    if (messageModel == nil){
        return false;
    }
    BOOL result = false;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:sqliteName];
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if (ifFind){
        sqlite3 *database;
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK) {
            sqlite3_close(database);
            [[UIAlertView alertViewWithTitle:@"错误" message:@"打开数据库文件失败" buttonTitles:@[@"确定"] callBlock:NULL] show];
        } else {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"insert into %@",sqlite_message];
            [sql appendFormat:@"(identify,messageType,title,content,direct,createTime,msgImg)values('%@','%li','%@','%@','%@','%li','%@')",messageModel.identify,(long)messageModel.messageType,messageModel.title,messageModel.content,messageModel.direct,(long)messageModel.time,messageModel.msgImg];
            
            char *errorMsg;
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK) {
                result=false;
                NSLog(@"error to exec %@",sql);
            } else {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;
}


@end

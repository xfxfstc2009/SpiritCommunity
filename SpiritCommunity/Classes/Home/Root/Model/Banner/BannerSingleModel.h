//
//  BannerSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/18.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol BannerSingleModel <NSObject>


@end

@interface BannerSingleModel : FetchModel

@property (nonatomic,copy)NSString *banner;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *info;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,assign)NSInteger type;

@end

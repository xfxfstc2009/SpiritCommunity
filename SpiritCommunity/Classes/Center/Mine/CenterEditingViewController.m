//
//  CenterEditingViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/18.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "CenterEditingViewController.h"
#import "GWDropListView.h"
#import "GWAssetsLibraryViewController.h"
#import "NickNameViewController.h"
#import "CenterLiveEditingViewController.h"
#import "SettingRootViewController.h"

static char editSuccessManagerkey;
@interface CenterEditingViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSArray *infoArr;
@property (nonatomic,strong)UITableView *infoTableView;
@property (nonatomic,strong)GWDropListView *dropList;

@end

@implementation CenterEditingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)pageSetting{
    self.barMainTitle = @"编辑";

    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"center_setting_icon"] barHltImage:[UIImage imageNamed:@"center_setting_icon"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        SettingRootViewController *settingViewController = [[SettingRootViewController alloc]init];
        [strongSelf.navigationController pushViewController:settingViewController animated:YES];
    }];
}

-(void)arrayWithInit{
    if ([AccountModel sharedAccountModel].isShenhe){
        self.infoArr = @[@[@"头像"],@[@"昵称",@"性别",@"年龄"]];
    } else {
        self.infoArr = @[@[@"头像"],@[@"昵称",@"性别",@"年龄"],@[@"直播配置"]];
    }
}

-(void)createTableView{
    if (!self.infoTableView){
        self.infoTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.infoTableView.dataSource = self;
        self.infoTableView.delegate = self;
        [self.view addSubview:self.infoTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.infoArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.infoArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0 ){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWAvatarTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWAvatarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferAvatarUrl = [AccountModel sharedAccountModel].loginModel.avatar;
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = [[self.infoArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowOne.transferHasArrow = YES;
        if (indexPath.section == 1){
            if (indexPath.row == 0){            // 昵称
                cellWithRowOne.transferDesc = [AccountModel sharedAccountModel].loginModel.nick;
            } else if (indexPath.row == 1){     // 性别
                cellWithRowOne.transferDesc =  [AccountModel sharedAccountModel].loginModel.sex == 1?@"男":@"女";
            } else if (indexPath.row == 2){     // 年龄
                cellWithRowOne.transferDesc = [NSString stringWithFormat:@"%li", (long)[AccountModel sharedAccountModel].loginModel.age];
            }
        } else if (indexPath.section == 2){
            if ([AccountModel sharedAccountModel].loginModel.live_img.length && [AccountModel sharedAccountModel].loginModel.live_title.length){
                cellWithRowOne.transferDesc = @"已配置";
            } else {
                cellWithRowOne.transferDesc = @"未配置";
            }
        }
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0){                    // 更换头像
        GWAssetsLibraryViewController *assetLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [assetLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf uploadAvatar:[selectedImgArr lastObject]];
        }];
        [self.navigationController pushViewController:assetLibraryViewController animated:YES];
    } else if (indexPath.section == 1){
        if (indexPath.row == 1){
            if (!self.dropList){
                self.dropList = [[GWDropListView alloc]init];
            }
            GWNormalTableViewCell *cell = (GWNormalTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            CGRect convertFrame = [tableView convertRect:cell.frame toView:self.view.window];
            __weak typeof(self)weakSelf = self;
            [self.dropList showInViewController:self frame:convertFrame listArr:@[@"男",@"女"] block:^(NSString *chooseInfo) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if ([chooseInfo isEqualToString:@"男"]){
                    [strongSelf sendRequestToUpdateInfoWithType:UserEditTypeSex info:@"1"];
                } else if ([chooseInfo isEqualToString:@"女"]){
                    [strongSelf sendRequestToUpdateInfoWithType:UserEditTypeSex info:@"2"];
                }
            }];
            UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
            [keywindow addSubview:self.dropList];
        } else if (indexPath.row == 0 || indexPath.row == 2){
            NickNameViewController *nickNameViewController = [[NickNameViewController alloc]init];
            if(indexPath.row == 0){
                nickNameViewController.transferEditType = UserEditTypeNickName;
            } else if (indexPath.row == 2){
                nickNameViewController.transferEditType = UserEditTypeAge;
            }
            __weak typeof(self)weakSelf = self;
            [nickNameViewController actionBlock:^(UserEditType transferEditType) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (transferEditType == UserEditTypeNickName){
                    [strongSelf.infoTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                } else if (transferEditType == UserEditTypeAge){
                    [strongSelf.infoTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            }];
            [self.navigationController pushViewController:nickNameViewController animated:YES];
        }
    } else if (indexPath.section == 2){
        CenterLiveEditingViewController *centerLiveViewController = [[CenterLiveEditingViewController alloc]init];
        [self.navigationController pushViewController:centerLiveViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    
    UILabel *titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    titleLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width, LCFloat(20));
    [headerView addSubview:titleLabel];
    if (section == 0){
        titleLabel.text = @"头像设置";
    } else if (section == 1){
        titleLabel.text = @"用户信息";
    } else if (section == 2){
        titleLabel.text = @"直播信息";
    }
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return [GWAvatarTableViewCell calculationCellHeight];
    } else {
        return LCFloat(44);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.infoTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.infoArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.infoArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }

}


-(void)editSuccessManager:(void(^)())block{
    objc_setAssociatedObject(self, &editSuccessManagerkey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


#pragma mark - sendRequestToUpdateInfo
-(void)sendRequestToUpdateInfoWithType:(UserEditType)type info:(NSString *)info{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if(type == UserEditTypeAvatar){             // 修改头像
        [params setValue:info forKey:@"avatar"];
    } else if (type == UserEditTypeAge){        // 年龄
        [params setValue:info forKey:@"age"];
    } else if(type == UserEditTypeSex){         // 性别
        [params setValue:info forKey:@"gender"];
    } else if (type == UserEditTypeNickName){   // 昵称
        [params setValue:info forKey:@"nick"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:user_eidt requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if(type == UserEditTypeAvatar){             // 修改头像
                [StatusBarManager statusBarHidenWithText:@"头像修改成功"];
                [AccountModel sharedAccountModel].loginModel.avatar = info;
                [strongSelf.infoTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            } else if (type == UserEditTypeAge){        // 年龄
                [StatusBarManager statusBarHidenWithText:@"年龄修改成功"];
                [AccountModel sharedAccountModel].loginModel.age = [info integerValue];
            } else if(type == UserEditTypeSex){         // 性别
                [StatusBarManager statusBarHidenWithText:@"性别修改成功"];
                [AccountModel sharedAccountModel].loginModel.sex = [info integerValue];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
                [strongSelf.infoTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            } else if (type == UserEditTypeNickName){   // 昵称
                [StatusBarManager statusBarHidenWithText:@"昵称修改成功"];
            }
            [[PDMainTabbarViewController sharedController].centerViewController uploadUserInfo];
        }
    }];
}


#pragma mark - 上传头像
-(void)uploadAvatar:(UIImage *)image{
    __weak typeof(self)weakSelf = self;
    [[AliOSSManager sharedOssManager] uploadFileWithObjctype:ImageUsingTypeUser name:@"" objImg:image block:^(NSString *fileUrl) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToUpdateInfoWithType:UserEditTypeAvatar info:fileUrl];
    } progress:^(CGFloat progress) {
        if (!weakSelf){
            return ;
        }
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"图片正在上传中%.2f",progress]];
    }];
}

@end

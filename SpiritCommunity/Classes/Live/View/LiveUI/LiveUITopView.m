//
//  LiveUITopView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveUITopView.h"

static char actionClickWithLiveTopBlockKey;
@interface LiveUITopView()
@property (nonatomic,strong)PDImageView *avatarbgView;              /**< 头像的背景*/
@property (nonatomic,strong)PDImageView *avatarImgView;             /**< 头像*/
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)UIButton *linkButton;

@end

@implementation LiveUITopView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createAvatarView];
    }
    return self;
}

#pragma mark - 创建头部的view
-(void)createAvatarView{
    self.avatarbgView = [[PDImageView alloc]init];
    self.avatarbgView.backgroundColor = [UIColor clearColor];
    self.avatarbgView.image = [Tool stretchImageWithName:@"window_show_background"];
    self.avatarbgView.frame = self.bounds;
    self.avatarbgView.clipsToBounds = YES;
    self.avatarbgView.layer.cornerRadius = MIN(self.avatarbgView.size_width, self.avatarbgView.size_height) / 2.;
    [self addSubview:self.avatarbgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor blackColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(5), LCFloat(5), (self.avatarbgView.size_height - 2 * LCFloat(5)), (self.avatarbgView.size_height - 2 * LCFloat(5)));
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_height / 2.;
    self.avatarImgView.clipsToBounds = YES;
    [self.avatarbgView addSubview:self.avatarImgView];
    
    self.nickNameLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    self.nickNameLabel.backgroundColor = [UIColor clearColor];
    self.nickNameLabel.text = @"";
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(5), LCFloat(5), self.avatarbgView.size_width - self.avatarImgView.size_width - 2 * LCFloat(11) - LCFloat(15), [NSString contentofHeightWithFont:self.nickNameLabel.font]);
    [self.avatarbgView addSubview:self.nickNameLabel];
    
    // 观众人数
    self.countLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.text = @"0观众";
    self.countLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, self.avatarbgView.size_height - LCFloat(5) - [NSString contentofHeightWithFont:self.countLabel.font], self.nickNameLabel.size_width, [NSString contentofHeightWithFont:self.countLabel.font]);
    [self.avatarbgView addSubview:self.countLabel];


    self.linkButton = [[UIButton alloc]init];
    self.linkButton.frame = self.avatarbgView.bounds;
    self.avatarbgView.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithLiveTopBlockKey);
        if (block){
            block();
        }
    }];
    [self.avatarbgView addSubview:self.linkButton];
}

-(void)setTransferLiveSingleModel:(HomeLiveSingleModel *)transferLiveSingleModel{
    _transferLiveSingleModel = transferLiveSingleModel;
    // 头像
    [self.avatarImgView uploadImageWithURL:transferLiveSingleModel.userInfo.avatar placeholder:nil callback:NULL];
    // 2. 昵称
    self.nickNameLabel.text = transferLiveSingleModel.userInfo.nick;
    // count
    self.countLabel.text = [NSString stringWithFormat:@"%li观众",transferLiveSingleModel.count];
}

-(void)changeCustomerCount:(NSInteger)count{
    self.countLabel.text = [NSString stringWithFormat:@"%li观众",count];
}

-(void)actionClickWithLiveTopBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithLiveTopBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);

}

@end

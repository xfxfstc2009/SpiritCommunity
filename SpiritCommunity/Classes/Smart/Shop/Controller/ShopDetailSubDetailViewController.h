//
//  ShopDetailSubDetailViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/1.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "ProductDetailRootModel.h"
@interface ShopDetailSubDetailViewController : AbstractViewController

@property (nonatomic,strong)ProductDetailRootModel *transferDetailModel;

-(void)pageLoad;

@end

//
//  OrderPayViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/11.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "OrderListSingleModel.h"
#import "ProductDetailRootModel.h"

@interface OrderPayViewController : AbstractViewController
@property (nonatomic,strong)OrderListSingleModel *transferSingleModel;              // 传递过来的订单信息

// 购物车选中的商品
@property (nonatomic,strong)NSArray *transferCartArr;

@property (nonatomic,strong)ProductDetailRootModel *transferProductDetailModel;
@end

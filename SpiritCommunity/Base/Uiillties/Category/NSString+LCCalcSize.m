//
//  NSString+LCCalcSize.m
//  LCZC
//
//  Created by SmartMin on 15/10/3.
//  Copyright © 2015年 SmartMin. All rights reserved.
//

#import "NSString+LCCalcSize.h"
#import "MD5Manager.h"
@implementation NSString (LCCalcSize)


- (CGSize)sizeWithCalcFont:(UIFont *)font{
    
    CGSize calcSize = CGSizeZero;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    if (IS_IOS7_LATER) {
        calcSize = [self sizeWithAttributes:@{NSFontAttributeName:font}];
    }else{
        calcSize = [self sizeWithFont:font];
    }
#pragma clang diagnostic pop
    return calcSize;
}

- (CGSize)sizeWithCalcFont:(UIFont *)font constrainedToSize:(CGSize)size{
    
    CGSize calcSize = CGSizeZero;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    if (IS_IOS7_LATER) {
        
        calcSize = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    }else{
        calcSize = [self sizeWithFont:font constrainedToSize:size];
    }
#pragma clang diagnostic pop
    return calcSize;
}

+(CGFloat)contentofHeightWithFont:(UIFont *)font{
    if (!font){
        return LCFloat(20);
    }
    CGSize contentOfHeight = [@"计算" sizeWithCalcFont:font constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)];
    return contentOfHeight.height;
}

+(CGSize)makeSizeWithLabel:(UILabel *)label{
    if (!label.text.length){
        return CGSizeMake(0, 0);
    }
    CGSize contentOfSize = [label.text sizeWithCalcFont:label.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:label.font])];
    return CGSizeMake(contentOfSize.width, [NSString contentofHeightWithFont:label.font]);
}

- (NSString *)md5String
{
    return [MD5Manager md5:self];
}


@end

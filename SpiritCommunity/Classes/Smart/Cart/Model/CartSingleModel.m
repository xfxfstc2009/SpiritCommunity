//
//  CartSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/22.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CartSingleModel.h"

@implementation CartSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end

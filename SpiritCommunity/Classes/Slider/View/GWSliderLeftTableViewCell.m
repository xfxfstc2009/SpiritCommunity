
//
//  GWSliderLeftTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/15.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWSliderLeftTableViewCell.h"

@interface GWSliderLeftTableViewCell()

@property (nonatomic,strong) UILabel*nicknameLabel;

@end

@implementation GWSliderLeftTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImageView = [[PDImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    self.iconImageView.clipsToBounds = YES;
    
    [self addSubview:self.iconImageView];
    
    // 2. 创建nickLabel
    self.nicknameLabel = [[UILabel alloc]init];
    self.nicknameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17];
    self.nicknameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.nicknameLabel];
    
//    // 3. 创建二维码
//    self.scanningButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.scanningButton setBackgroundImage:[UIImage imageNamed:@"icon_erweima"] forState:UIControlStateNormal];
//    [self addSubview:self.scanningButton];
}

-(void)setTransferCellHeiht:(CGFloat)transferCellHeiht{
    _transferCellHeiht = transferCellHeiht;
}

-(void)setTransferLoginModel:(AccountModel *)transferLoginModel{
    // 1. 头像
    self.iconImageView.frame = CGRectMake(LCFloat(11), LCFloat(5), (self.transferCellHeiht - 2 * LCFloat(13)), (self.transferCellHeiht - 2 * LCFloat(13)));
    self.iconImageView.layer.cornerRadius = self.iconImageView.size_height / 2.;
    // 2. 昵称
    self.nicknameLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + LCFloat(15), CGRectGetMaxY(self.iconImageView.frame) - LCFloat(40), LCFloat(200), LCFloat(40));
    
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){        // 已经登录
        [self.iconImageView uploadImageWithURL:[AccountModel sharedAccountModel].loginModel.avatar  placeholder:nil callback:NULL];
        
        if ([AccountModel sharedAccountModel].loginModel.ID.length && [AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeNor){
            if ([AccountModel sharedAccountModel].loginModel.phone.length){
                self.nicknameLabel.text = [NSString stringWithFormat:@"精灵%@",[AccountModel sharedAccountModel].loginModel.phone];
            } else if ([AccountModel sharedAccountModel].loginModel.ID.length){
                self.nicknameLabel.text = [NSString stringWithFormat:@"精灵%@",[AccountModel sharedAccountModel].loginModel.ID];
            }
        }
    } else {    // 没有登录
        self.iconImageView.image = [Tool imageByComposingImage: [UIImage imageNamed:@"icon_login_avatar"] withMaskImage:[UIImage imageNamed:@"round"]];
        
        self.nicknameLabel.text = @"登录/注册";
    }
}


#pragma mark - 登录方法
-(void)loginManager{
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){        // 已经登录
        [self.iconImageView uploadImageWithURL:[AccountModel sharedAccountModel].loginModel.avatar  placeholder:nil callback:NULL];
        
        self.nicknameLabel.text = [NSString stringWithFormat:@"%@",[AccountModel sharedAccountModel].loginModel.nick];
        
        if ([AccountModel sharedAccountModel].loginModel.ID.length && [AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeNor){
            if ([AccountModel sharedAccountModel].loginModel.union_number.length){
                self.nicknameLabel.text = [NSString stringWithFormat:@"精灵%@",[AccountModel sharedAccountModel].loginModel.union_number];
            } else if ([AccountModel sharedAccountModel].loginModel.ID.length){
                self.nicknameLabel.text = [NSString stringWithFormat:@"精灵%@",[AccountModel sharedAccountModel].loginModel.ID];
            }
        }
    } else {    // 没有登录
        self.iconImageView.image = [Tool imageByComposingImage: [UIImage imageNamed:@"icon_login_avatar"] withMaskImage:[UIImage imageNamed:@"round"]];
        
        self.nicknameLabel.text = @"登录/注册";
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(80);
}
@end

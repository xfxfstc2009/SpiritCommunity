//
//  GWWeatherTodayViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/8.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"
#import "GWWeatherCurrentModel.h"

@protocol GWWeatherTodayViewControllerDelegate <NSObject>


@end

@interface GWWeatherTodayViewController : AbstractViewController

@property (nonatomic,strong)UITableView *todayWeatherTableView;
@property (nonatomic,strong)GWWeatherCurrentModel *weatherCurrentModel;
@property (nonatomic,weak)id<GWWeatherTodayViewControllerDelegate> delegate;

@end

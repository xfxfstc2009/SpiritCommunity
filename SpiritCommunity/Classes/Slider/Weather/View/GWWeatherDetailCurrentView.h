//
//  GWWeatherDetailCurrentView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/14.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWWeatherCurrentModel.h"
@interface GWWeatherDetailCurrentView : UIView

-(instancetype)initWithFrame:(CGRect)frame withModel:(GWWeatherCurrentModel *)model;

@end

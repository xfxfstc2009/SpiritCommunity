//
//  GWBannerTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWBannerTableViewCell.h"
#import "PDScrollView.h"
static char bannerActionClickBlockKey;
@interface GWBannerTableViewCell()


@end

@implementation GWBannerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    if (!self.headerScrollView){
        self.headerScrollView = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(200))];
        self.headerScrollView.backgroundColor = BACKGROUND_VIEW_COLOR;

        [self addSubview:self.headerScrollView];
        __weak typeof(self)weakSelf = self;
        [self.headerScrollView bannerImgTapManagerWithInfoblock:^(GWScrollViewSingleModel *model) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(GWScrollViewSingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &bannerActionClickBlockKey);
            if(block){
                block(model);
            }
        }];
    }
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight {
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferImgArr:(NSArray *)transferImgArr{
    _transferImgArr = transferImgArr;
    if (!self.headerScrollView.transferImArr.count){
        NSMutableArray *tempScrollMutableArr = [NSMutableArray array];
        for (int i = 0 ; i < self.transferImgArr.count;i++){
            GWScrollViewSingleModel *scrollViewSingleModel = [[GWScrollViewSingleModel alloc]init];
            scrollViewSingleModel.img = [transferImgArr objectAtIndex:i];
            [tempScrollMutableArr addObject:scrollViewSingleModel];
        }
        self.headerScrollView.transferImArr = tempScrollMutableArr;
    }
}

-(void)setTransferImgModelArr:(NSArray *)transferImgModelArr{
    _transferImgModelArr = transferImgModelArr;
    if (!self.headerScrollView.transferImArr.count){
        NSMutableArray *tempScrollMutableArr = [NSMutableArray array];
        for (int i = 0 ; i < transferImgModelArr.count;i++){
            HomeNormalSingleGoodsModel *singleModel = [transferImgModelArr objectAtIndex:i];

            GWScrollViewSingleModel *scrollViewSingleModel = [[GWScrollViewSingleModel alloc]init];
            scrollViewSingleModel.img = singleModel.imgUrl;
            scrollViewSingleModel.type = 1;
            scrollViewSingleModel.info = singleModel.linkUrl;
            [tempScrollMutableArr addObject:scrollViewSingleModel];
        }
        self.headerScrollView.transferImArr = tempScrollMutableArr;
    }
}

-(void)bannerActionClickBlock:(void(^)(GWScrollViewSingleModel *singleModel))block{
    objc_setAssociatedObject(self, &bannerActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}




-(void)remove{
    [self.headerScrollView removeFromSuperview];
    self.headerScrollView = nil;
}


+(CGFloat) calculationCellHeight{
    return LCFloat(200);
}
@end

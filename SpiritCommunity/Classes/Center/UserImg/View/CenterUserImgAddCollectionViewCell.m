//
//  CenterUserImgAddCollectionViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/20.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "CenterUserImgAddCollectionViewCell.h"

@interface CenterUserImgAddCollectionViewCell()
@property (nonatomic,strong)PDImageView *addImgView;

@end

@implementation CenterUserImgAddCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.addImgView = [[PDImageView alloc]init];
    self.addImgView.backgroundColor = [UIColor clearColor];
    self.addImgView.image = [UIImage imageNamed:@"center_img_add_icon"];
    self.addImgView.frame = CGRectMake((self.size_width - LCFloat(60)) / 2., (self.size_height - LCFloat(60)) / 2., LCFloat(60), LCFloat(60));
    [self addSubview:self.addImgView];
}



@end

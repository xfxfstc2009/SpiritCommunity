//
//  LiveTopMoneyView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveTopMoneyView.h"

@interface LiveTopMoneyView()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *giftLabel;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *myMoneyLabel;
@property (nonatomic,strong)UIView *lineView;

@end

@implementation LiveTopMoneyView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.fixedLabel = [GWViewTool createLabelFont:@"提示" textColor:@"蓝"];
    self.fixedLabel.font = [self.fixedLabel.font boldFont];
    [self addSubview:self.fixedLabel];

    //
    self.nameLabel = [GWViewTool createLabelFont:@"提示" textColor:@"红高亮"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    [self addSubview:self.nameLabel];

    self.giftLabel = [GWViewTool createLabelFont:@"提示" textColor:@"蓝"];
    self.giftLabel.font = [self.giftLabel.font boldFont];
    [self addSubview:self.giftLabel];

    // price
    self.priceLabel = [GWViewTool createLabelFont:@"提示" textColor:@"红"];
    self.priceLabel.font = [self.priceLabel.font boldFont];
    [self addSubview:self.priceLabel];

    // money
    self.myMoneyLabel = [GWViewTool createLabelFont:@"提示" textColor:@"快递"];
    self.myMoneyLabel.font = [self.myMoneyLabel.font boldFont];
    [self addSubview:self.myMoneyLabel];

    // 充值按钮
    self.chongzhiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.chongzhiButton setTitle:@"充值" forState:UIControlStateNormal];
    self.chongzhiButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.chongzhiButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.chongzhiButton.layer.cornerRadius = LCFloat(3);
    self.chongzhiButton.backgroundColor = [UIColor colorWithCustomerName:@"蓝"];
    CGSize chongzhiSize = [@"充值" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.size_height)];
    self.chongzhiButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(7) - chongzhiSize.width - 2 * LCFloat(15), LCFloat(7), chongzhiSize.width + 2 * LCFloat(11), self.size_height - 2 * LCFloat(7));

    [self addSubview:self.chongzhiButton];

    self.fixedLabel.text = @"送给";
    CGSize fixedSize = [Tool makeSizeWithLabel:self.fixedLabel];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, fixedSize.width, self.size_height);

    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self addSubview:self.lineView];
    self.lineView.frame = CGRectMake(0, self.size_height - 1, kScreenBounds.size.width, .5f);
}

-(void)setTransferName:(NSString *)transferName{
    _transferName = transferName;
    // name
    self.nameLabel.text = self.transferName;
    CGSize nameSize = [Tool makeSizeWithLabel:self.nameLabel];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(2), 0, nameSize.width, self.size_height);
}

-(void)setTransferSingleModel:(LiveUIBottomViewSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;

    // 2. gift
    self.giftLabel.text = transferSingleModel.giftSingleModel.name;
    CGSize giftSize = [Tool makeSizeWithLabel:self.giftLabel];
    self.giftLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + LCFloat(2), 0, giftSize.width, self.size_height);

    // price
    self.priceLabel.text = [NSString stringWithFormat:@"%li 金币",transferSingleModel.giftSingleModel.price];
    CGSize priceSize = [Tool makeSizeWithLabel:self.priceLabel];
    self.priceLabel.frame = CGRectMake(CGRectGetMaxX(self.giftLabel.frame) + LCFloat(11), 0, priceSize.width, self.size_height);

}


-(void)setMyAccountMoney:(NSInteger)myAccountMoney{
    _myAccountMoney = myAccountMoney;
    self.myMoneyLabel.text = [NSString stringWithFormat:@"%li金币 >",myAccountMoney];
    CGSize moneySize = [Tool makeSizeWithLabel:self.myMoneyLabel];
    self.myMoneyLabel.frame = CGRectMake(self.chongzhiButton.orgin_x - LCFloat(11) - moneySize.width, 0, moneySize.width, self.size_height);
    
}


@end

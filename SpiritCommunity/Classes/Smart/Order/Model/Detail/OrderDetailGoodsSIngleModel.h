//
//  OrderDetailGoodsSIngleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol OrderDetailGoodsSIngleModel <NSObject>

@end

@interface OrderDetailGoodsSIngleModel : FetchModel

@property (nonatomic,assign)NSInteger count;
@property (nonatomic,copy)NSString *good_id;
@property (nonatomic,copy)NSString *good_name;
@property (nonatomic,copy)NSString *img_path;
@property (nonatomic,assign)CGFloat min_price;
@property (nonatomic,copy)NSString *sku;


@end

//
//  FindShenheProductAddViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/31.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"

@interface FindShenheProductAddViewController : AbstractViewController

-(void)actionClickBlockProduct:(void(^)())block;

@end

//
//  LiveReleaseShowViewController+AVListener.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/29.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController.h"

@interface MyTapGesture : UITapGestureRecognizer

@property (nonatomic, copy) NSString *codeId;
@end


@interface LiveReleaseShowViewController (AVListener)<ILVLiveAVListener>

@end

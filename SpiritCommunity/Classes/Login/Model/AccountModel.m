//
//  AccountModel.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AccountModel.h"
#import "AliChatManager.h"

@implementation AccountModel

+(instancetype)sharedAccountModel{
    static AccountModel *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[AccountModel alloc] init];
    });
    return _sharedAccountModel;
}



#pragma mark - 退出登录方法
-(void)logoutManagerWithBlock:(void(^)())block{
    [AccountModel sharedAccountModel].loginModel = nil;
    [AccountModel sharedAccountModel].thirdUserModel = nil;
    [AccountModel sharedAccountModel].thirdLoginType = PDThirdLoginTypeNor;
    [StatusBarManager statusBarHidenWithText:@"正在登出IM"];
    [Tool userDefaultDelegtaeWithKey:LoginType];
    [Tool userDefaultDelegtaeWithKey:LoginKey];
    [[AliChatManager sharedInstance]aliLogoutWithBlock:^{
        [StatusBarManager statusBarHidenWithText:@"IM登出成功"];
        if (block){
            block();
        }
    }];
    [[PDMainTabbarViewController sharedController].centerViewController logoutManager];
    PDSliderRootViewController *sliderController = (PDSliderRootViewController *)[RESideMenu shareInstance].leftMenuViewController;
    [sliderController logoutSuccess];
    
}

// 自有账号登录
-(void)loginManagerWithMyAccount:(NSString *)account callBack:(void(^)(BOOL successed))block{
    [self loginManagerType:[AccountModel sharedAccountModel].thirdLoginType account:account callBack:^(BOOL successed) {
        if (block){
            block(successed);
        }
    }];
}


// 三方登录
-(void)loginManagerWithCallBack:(void(^)(BOOL successed))block{
    [self loginManagerType:[AccountModel sharedAccountModel].thirdLoginType account:[AccountModel sharedAccountModel].thirdUserModel.unionid callBack:^(BOOL successed) {
        if (block){
            block(successed);
        }
    }];
}


#pragma mark - 登录方法
-(void)loginManagerType:(PDThirdLoginType)loginType account:(NSString *)account callBack:(void(^)(BOOL successed))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (loginType == PDThirdLoginTypeNor){      // 手机登录
        [params setObject:account forKey:@"phone"];
        [params setObject:@(loginType) forKey:@"login_type"];
    } else {
        if ([AccountModel sharedAccountModel].thirdUserModel.headimgurl.length){
            [params setObject:[AccountModel sharedAccountModel].thirdUserModel.headimgurl forKey:@"avatar"];
        }
        if ([AccountModel sharedAccountModel].thirdUserModel.nickname.length){
            [params setObject:[AccountModel sharedAccountModel].thirdUserModel.nickname forKey:@"nick"];
        }
        [params setObject:@([AccountModel sharedAccountModel].thirdUserModel.sex) forKey:@"sex"];
        if (account.length){
            [params setObject:account forKey:@"union_id"];
        }
        [params setObject:@(loginType) forKey:@"login_type"];
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:login requestParams:params responseObjectClass:[LoginNativeSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            LoginNativeSingleModel *loginModel = (LoginNativeSingleModel *)responseObject;
            [AccountModel sharedAccountModel].loginModel = loginModel;
            
            // 1. 进行保存登录类型
            [Tool userDefaulteWithKey:LoginType Obj:[NSString stringWithFormat:@"%li",(long)loginType]];
            // 2. 进行保存登录key
            if([AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeQQ){
                [StatusBarManager statusBarHidenWithText:@"QQ登录成功"];
            } else if ([AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeWechat){
                [StatusBarManager statusBarHidenWithText:@"微信登录成功"];
            } else if ([AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeNor){
                [StatusBarManager statusBarHidenWithText:@"登录成功"];
            }
            [Tool userDefaulteWithKey:LoginKey Obj:account];
            
            // 【登录阿里IM】
            [[AliChatManager sharedInstance] loginWithUserName:loginModel.im_id password:@"beetv" preloginedBlock:^{
                [StatusBarManager statusBarHidenWithText:@"IM 预登录成功"];
            } successBlock:^{
                [StatusBarManager statusBarHidenWithText:@"IM 登录成功"];
            } failedBlock:^(NSError *error) {
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"IM 预登录失败 %@",error.localizedDescription]];
            }];
            
            // 登录腾讯直播
            [[AccountModel sharedAccountModel] sendRequestToTXCloudWithBlock:^(BOOL successed) {
                if (!weakSelf){
                    return ;
                }
                if (successed){
//                    [StatusBarManager statusBarHidenWithText:@"腾讯云 登录成功"];
                }
            }];

            // 进行连接socket
            [strongSelf linkLongLink];
            // 更新用户地址
            [strongSelf uploadUserLocationManager];
            // 登录一模狗
            [strongSelf smartToAuthManagerWithBlock:^(BOOL successed) {
                if (isSucceeded){
                    [strongSelf loginManagerSuccessBlock:NULL];
                }
            }];
            
            // 进行返回
            if (block){
                block(isSucceeded);
            }
        }
    }];
}


#pragma mark - login TX Cloud
-(void)sendRequestToTXCloudWithBlock:(void(^)(BOOL successed))block{
    NSString *sig = @"";
    if ([AccountModel sharedAccountModel].loginModel.user_sig.length){
        sig = [AccountModel sharedAccountModel].loginModel.user_sig;
    }
    NSString *userId = @"";
    if ([AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeNor){
        userId = [AccountModel sharedAccountModel].loginModel.phone;
    } else {
        userId = [AccountModel sharedAccountModel].loginModel.union_id;
    }
    
    // 直播登录
    [LiveRootManager liveLoginManager:userId pwd:sig];
}

#pragma mark - 自动登录
-(void)autoLoginWithBlock:(void(^)(BOOL successed))block{
    NSString *loginType = [Tool userDefaultGetWithKey:LoginType];
    NSString *loginKey = [Tool userDefaultGetWithKey:LoginKey];
    if (loginType.length && loginKey.length){
        PDThirdLoginType thirdLoginType = (PDThirdLoginType)[loginType integerValue];
        __weak typeof(self)weakSelf = self;
        [AccountModel sharedAccountModel].thirdLoginType = thirdLoginType;

        [self loginManagerType:thirdLoginType account:loginKey callBack:^(BOOL successed) {
            if (!weakSelf){
                return ;
            }
            if (block){
                block(successed);
            }
        }];
    }
}

#pragma mark - 提交用户的地址信息
-(void)uploadUserLocationManager{
    if ([AccountModel sharedAccountModel].userLocation){
        [self sendRequestToUplocationInfo];
    } else {
        __weak typeof(self)weakSelf = self;
        [[PDCurrentLocationManager sharedLocation] getCurrentLocationManager:^(CGFloat lat, CGFloat lng, AMapAddressComponent *addressComponent) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [AccountModel sharedAccountModel].lat = lat;
            [AccountModel sharedAccountModel].lng = lng;
            [AccountModel sharedAccountModel].userLocation = addressComponent;
            [strongSelf sendRequestToUplocationInfo];
        }];
    }
}

-(void)sendRequestToUplocationInfo{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([AccountModel sharedAccountModel].userLocation.province.length){
        [params setObject:[AccountModel sharedAccountModel].userLocation.province forKey:@"province"];
    }
    if ([AccountModel sharedAccountModel].userLocation.city.length){
        [params setObject:[AccountModel sharedAccountModel].userLocation.city forKey:@"city"];
    }
    if ([AccountModel sharedAccountModel].userLocation.district.length){
        [params setObject:[AccountModel sharedAccountModel].userLocation.district forKey:@"area"];
    }
    if ([AccountModel sharedAccountModel].lat != 0){
        [params setObject:@([AccountModel sharedAccountModel].lat) forKey:@"lat"];
    }
    if ([AccountModel sharedAccountModel].lng != 0){
        [params setObject:@([AccountModel sharedAccountModel].lng) forKey:@"lng"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:userLocation requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"用户更新当前地址"];
        }
    }];
}


- (BOOL)hasLoggedIn{
    return ([AccountModel sharedAccountModel].loginModel.ID.length ? YES : NO);
}

#pragma mark - 链接长连接
-(void)linkLongLink{
    [NetworkAdapter sharedAdapter].loginWebSocketConnection = [[WebSocketConnection alloc]init];
    [[NetworkAdapter sharedAdapter].loginWebSocketConnection  webSocketConnectWithHost:socket_Host port:[socket_port integerValue]];
}




// action
#pragma mark - 登录方法
-(void)loginManagerSuccessBlock:(void(^)(BOOL issuccess))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"osType":@"ios",@"phoneNum":@"15669969617",@"password":@"qqqqqq"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:_login requestParams:params responseObjectClass:[AccountModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            AccountModel *accountModel = (AccountModel *)responseObject;
            [AccountModel sharedAccountModel].userId = accountModel.userId;
            [AccountModel sharedAccountModel].userToken = accountModel.userToken;

            // 3. token
            [Tool userDefaulteWithKey:AUTO_Login_Token Obj:accountModel.userToken];

            // 3.
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
}

-(void)smartToAuthManagerWithBlock:(void(^)(BOOL successed))block{
    __weak typeof(self)weakSelf = self;
    if ([AccountModel sharedAccountModel].authToken.length){
        block(YES);
        return;
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:_oauth requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [AccountModel sharedAccountModel].authToken = [responseObject objectForKey:@"token"];
            if (block){
                block(YES);
            }
        }
    }];
}


-(NSString *)userId{
    return @"42875";
}


-(void)reloadAllAccountInfo{
    PDSliderRootViewController *sliderRootViewController = (PDSliderRootViewController *)[RESideMenu shareInstance].leftMenuViewController;
    [sliderRootViewController logoutSuccess];
    
    [[PDMainTabbarViewController sharedController].centerViewController logoutManager];
}

@end

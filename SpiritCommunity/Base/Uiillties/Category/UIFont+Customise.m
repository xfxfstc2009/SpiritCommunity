//
//  UIFont+Customise.m
//  LaiCai
//
//  Created by SmartMin on 15/8/13.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UIFont+Customise.h"

@implementation UIFont (Customise)

+ (UIFont *)fontWithCustomerSizeName:(NSString *)fontName{
    CGFloat customerFontSize = 0.0;
    if ([fontName isEqualToString:@"标题"]){
        customerFontSize = 18;
    } else if ([fontName isEqualToString:@"副标题"]){
        customerFontSize = 16.;
    } else if ([fontName isEqualToString:@"正文"]){
        customerFontSize = 15.;
    } else if ([fontName isEqualToString:@"小正文"]){
        customerFontSize = 14.;
    } else if ([fontName isEqualToString:@"提示"]){
        customerFontSize = 13.;
    } else if ([fontName isEqualToString:@"小提示"]){
        customerFontSize = 12.;
    }
    
    if (kScreenBounds.size.width == 320) {
        return [UIFont systemFontOfSize:LCFloat(customerFontSize + 2)];
    }
    return [UIFont fontWithName:@"HelveticaNeue-Thin" size:LCFloat(customerFontSize)];
}

+ (UIFont *)systemFontOfCustomeSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Thin" size:LCFontSize(fontSize)];
}

extern CGFloat LCFontSize(CGFloat pointSize) {
    if (kScreenBounds.size.width == 320) {
        return LCFloat(pointSize + 2);
    }
    return LCFloat(pointSize);
}

- (UIFont *)boldFont {
    CGFloat pointSize = [self pointSize];
    return [UIFont boldSystemFontOfSize:pointSize];
}



+ (UIFont *)HYQiHeiWithFontSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"HYQiHei-BEJF" size:size];
}

#pragma mark - System font.

+ (UIFont *)AppleSDGothicNeoThinWithFontSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"AppleSDGothicNeo-Thin" size:size];
}

+ (UIFont *)AvenirWithFontSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"Avenir" size:size];
}

+ (UIFont *)AvenirLightWithFontSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"Avenir-Light" size:size];
}

+ (UIFont *)HeitiSCWithFontSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"Heiti SC" size:size];
}

+ (UIFont *)HelveticaNeueFontSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"HelveticaNeue" size:size];
}

+ (UIFont *)HelveticaNeueBoldFontSize:(CGFloat)size {
    
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
}

#pragma mark - 开场动画
+ (UIFont *)giganticTitleFontSize:(CGFloat)size{
    return  [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
}

@end

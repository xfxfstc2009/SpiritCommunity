//
//  ChongZhiSingleModelSimple.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/17.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface ChongZhiSingleModelSimple : FetchModel

@property (nonatomic,copy)NSString *productName;
@property (nonatomic,copy)NSString *out_trade_no;       // 订单
@property (nonatomic,copy)NSString *total_fee;          // 总价
@property (nonatomic,copy)NSString *prepayId;           // 预生成微信订单
@property (nonatomic,copy)NSString *notify_url;         // 通知回调
@property (nonatomic,copy)NSString *sign;               // sign
@end

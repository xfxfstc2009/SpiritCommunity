//
//  CenterRoundView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/9.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CenterRoundView.h"

@interface CenterRoundView()
@property (nonatomic,strong)PDImageView *iconImageView;
@property (nonatomic,strong)UILabel *signLabel;
@property (nonatomic,copy)NSString *title;

@end

@implementation CenterRoundView

-(instancetype)initWithFrame:(CGRect)frame withTitle:(NSString *)title{
    self = [super initWithFrame:frame];
    if (self){
        _title = title;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImageView = [[PDImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    self.iconImageView.image = [UIImage imageNamed:@"jjr_ic_vip"];
    self.iconImageView.frame = CGRectMake(0, 0, 11, 11);
    [self addSubview:self.iconImageView];
    
    self.signLabel = [[UILabel alloc]init];
    self.signLabel.backgroundColor = [UIColor clearColor];
    self.signLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.signLabel.textColor = [UIColor colorWithCustomerName:@"橙"];
    self.signLabel.text = self.title;
    [self addSubview:self.signLabel];
    
    CGSize signSize = [self.signLabel.text sizeWithCalcFont:self.signLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.signLabel.font])];
    self.signLabel.frame= CGRectMake(CGRectGetMaxX(self.iconImageView.frame) , 0, signSize.width, [NSString contentofHeightWithFont:self.signLabel.font]);
    
    // 重新计算
    self.iconImageView.frame = CGRectMake(LCFloat(2), LCFloat(2), [NSString contentofHeightWithFont:self.signLabel.font],  [NSString contentofHeightWithFont:self.signLabel.font]);
    self.signLabel.orgin_x = CGRectGetMaxX(self.iconImageView.frame);
    self.signLabel.orgin_y = LCFloat(2);
    
    CGFloat width = self.iconImageView.size_width + signSize.width + 2 * LCFloat(2) + LCFloat(2);
    self.bounds = CGRectMake(0, 0, width, [NSString contentofHeightWithFont:self.signLabel.font] + 2 * LCFloat(2));
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = ([NSString contentofHeightWithFont:self.signLabel.font] + 2 * LCFloat(2)) / 2.;
    self.clipsToBounds = YES;
}

-(void)changeGender:(NSInteger)gender{
    if (gender == 1){
        self.iconImageView.image = [UIImage imageNamed:@"icon_user_gender_b"];
    } else {
        self.iconImageView.image = [UIImage imageNamed:@"icon_user_gender_g"];
    }
}

-(void)changeInfo:(NSString *)info{
    self.signLabel.text = info;
}
@end

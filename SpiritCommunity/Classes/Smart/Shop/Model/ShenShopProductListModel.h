//
//  ShenShopProductListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/30.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol ShenShopProductListSingleModel <NSObject>

@end

@interface ShenShopProductListSingleModel : FetchModel

@property (nonatomic,copy)NSString *appointment;
@property (nonatomic,copy)NSString *appointment_node;
@property (nonatomic,copy)NSString *area;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,assign)CGFloat discount_price;
@property (nonatomic,copy)NSString *goodstype;
@property (nonatomic,copy)NSString *goodstype3;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *img_path;
@property (nonatomic,copy)NSString *original_price;
@property (nonatomic,copy)NSString *range;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *stock;
@property (nonatomic,copy)NSString *sub_title;
@property (nonatomic,copy)NSString *supplier;
@property (nonatomic,copy)NSString *title;

@end

@interface ShenShopProductListModel : FetchModel

@property (nonatomic,strong)NSArray<ShenShopProductListSingleModel> *list;

@end

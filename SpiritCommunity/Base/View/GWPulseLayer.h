//
//  GWPulseLayer.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/17.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface GWPulseLayer : CALayer

@property (nonatomic, assign) CGFloat radius;                   // default:60pt
@property (nonatomic, assign) NSTimeInterval animationDuration; // default:3s
@property (nonatomic, assign) NSTimeInterval pulseInterval; // default is 0s

@end

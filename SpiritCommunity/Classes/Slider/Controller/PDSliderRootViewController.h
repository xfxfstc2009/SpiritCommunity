//
//  PDSliderRootViewController.h
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/23.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import "AbstractViewController.h"
#import "GWSliderLeftTableViewCell.h"
#import "PDSliderTableViewCell.h"


@interface PDSliderRootViewController : AbstractViewController

@property (nonatomic,strong)UITableView *sliderTableView;                   // 侧边栏

-(void)updateList:(NSArray *)sliderList;

-(void)loginSuccess;
-(void)logoutSuccess;

#pragma mark - 更新天气
-(void)reloadWeatherManager;

@end

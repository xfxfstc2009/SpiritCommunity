//
//  CartSingleTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartSingleModel.h"
#import "OrderListGoodsSingleModel.h"
#import "ShopDetailViewController.h"

@class ShopDetailViewController;
typedef NS_ENUM(NSInteger,CartSingleTableViewCellType) {
    CartSingleTableViewCellTypeCart = 1,
    CartSingleTableViewCellTypeOrder = 2,
};


@interface CartSingleTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)CartSingleModel *transferCartSingleModel;

@property (nonatomic,assign)CartSingleTableViewCellType transferType;
@property (nonatomic,assign)productType transferOrderPageType;

// 订单
@property (nonatomic,strong)OrderListGoodsSingleModel *transferGoodsSingleModel;


+(CGFloat)calculationCellHeight;
-(void)actionClickWithDelete:(void(^)())block;

-(void)selectedButtonStatus:(BOOL)status;

-(void)actionClickWithCheck:(void(^)(BOOL checked))block;

-(void)changeNumberOfCount:(void(^)(NSInteger count))block;
@end

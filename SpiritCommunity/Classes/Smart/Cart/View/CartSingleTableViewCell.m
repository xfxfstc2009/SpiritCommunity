//
//  CartSingleTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CartSingleTableViewCell.h"


static char deleteKey;
static char checkKey;
static char changeNumberOfCountKey;
@interface CartSingleTableViewCell()
@property (nonatomic,strong)PDImageView *checkImgView;              /**< */
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *proImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *skuLabel;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)UIButton *deleteButton;
@property (nonatomic,strong)GWNumberChooseView *numberView;
@property (nonatomic,strong)UIButton *checkButton;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *jifenLabel;
@end


@implementation CartSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
        // 1. 创建选择按钮
    self.checkImgView = [[PDImageView alloc]init];
    self.checkImgView.backgroundColor = [UIColor clearColor];
    self.checkImgView.frame = CGRectMake(LCFloat(5), 0, LCFloat(18), LCFloat(18));
    [self addSubview:self.checkImgView];
    
    //2. 创建背景view
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.clipsToBounds = YES;
    self.convertView.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
    self.convertView.layer.borderWidth = .5f;
    [self addSubview:self.convertView];
    
    self.proImgView = [[PDImageView alloc]init];
    self.proImgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.proImgView];
    
    // 3. 创建价格
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.priceLabel.adjustsFontSizeToFitWidth = YES;
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.priceLabel];
    
    // 4. 创建数量
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.textAlignment = NSTextAlignmentRight;
    self.countLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.countLabel];
    
    // 5. 创建名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.nameLabel];

    // 6. 创建sku
    self.skuLabel = [[UILabel alloc]init];
    self.skuLabel.backgroundColor = [UIColor clearColor];
    self.skuLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self addSubview:self.skuLabel];
    
    // 7. 创建数量
    __weak typeof(self)weakSelf = self;
    self.numberView = [[GWNumberChooseView alloc]initWithFrame:CGRectMake(0, 0, LCFloat(75), LCFloat(20)) numberBlock:^(NSInteger number) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.transferOrderPageType == productTypeJifenDuobao){
            void(^block)(NSInteger count) = objc_getAssociatedObject(strongSelf, &changeNumberOfCountKey);
            if (block){
                block(number);
            }
        } else {
            [strongSelf sendRequestToUpdateProductCount:strongSelf.transferCartSingleModel count:number];
        }
    }];
    [self addSubview:self.numberView];
    
    // 8. 创建删除按钮
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setImage:[UIImage imageNamed:@"cart_delte"] forState:UIControlStateNormal];
    [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    self.deleteButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12.];
    [self.deleteButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)()= objc_getAssociatedObject(strongSelf, &deleteKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.deleteButton];
    
    // 9. 选择按钮
    self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.checkButton.backgroundColor = [UIColor clearColor];
    [self.checkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BOOL ischecked) = objc_getAssociatedObject(strongSelf, &checkKey);
        if (block){
            block(YES);
        }
    }];
    [self addSubview:self.checkButton];
    
    // 10.创建数量label
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.numberLabel];
    
    // 11. 积分label
    self.jifenLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"棕黑"];
    self.jifenLabel.text = @"100积分";
    [self addSubview:self.jifenLabel];
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferCartSingleModel:(CartSingleModel *)transferCartSingleModel{
    _transferCartSingleModel = transferCartSingleModel;
    // 1. 选择
    self.checkImgView.frame = CGRectMake(LCFloat(11), 0, LCFloat(18), LCFloat(18));
    self.checkImgView.image = [UIImage imageNamed:@"dizhi_xuanzhong"];
    self.checkImgView.center_y = self.transferCellHeight / 2.;
    
    // 2.边框
    self.convertView.frame = CGRectMake(CGRectGetMaxX(self.checkImgView.frame) + LCFloat(11), LCFloat(11), (self.transferCellHeight - 2 * LCFloat(11)), (self.transferCellHeight - 2 * LCFloat(11)));
    
    if(self.transferType == CartSingleTableViewCellTypeCart){
        self.convertView.orgin_x = CGRectGetMaxX(self.checkImgView.frame) + LCFloat(11);
    } else if (self.transferType == CartSingleTableViewCellTypeOrder){
        self.convertView.orgin_x = LCFloat(11);
    }
    
    // 3. 创建图片
    __weak typeof(self)weakSelf = self;
    [self.proImgView uploadImageWithURL:self.transferCartSingleModel.goodsImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool setTransferSingleAsset:image imgView:strongSelf.proImgView convertView:strongSelf.convertView];
        [strongSelf.proImgView sizeToFit];
    }];

    // 4.创建价格
    if (self.transferCartSingleModel.goodsMoney == 0){
        self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",self.transferCartSingleModel.goodsMoney];
    } else {
        self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",self.transferCartSingleModel.goodsMoney];
    }
    
    NSString *priceStr = [NSString stringWithFormat:@"%@10000.00",@"￥"];
    CGSize priceSize = [priceStr sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    self.priceLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - priceSize.width, LCFloat(11), priceSize.width, [NSString contentofHeightWithFont:self.priceLabel.font]);
    
    // 2. 商品名字
    CGFloat width = kScreenBounds.size.width - LCFloat(11) - priceSize.width - LCFloat(11) - CGRectGetMaxX(self.convertView.frame) - LCFloat(11);
    self.nameLabel.text = transferCartSingleModel.goodsName;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (nameSize.height >= 2 * [NSString contentofHeightWithFont:self.nameLabel.font]){
        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), LCFloat(11), width,  2 * [NSString contentofHeightWithFont:self.nameLabel.font]);
        self.nameLabel.numberOfLines = 2;
    } else {
        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), LCFloat(11), width,  1 * [NSString contentofHeightWithFont:self.nameLabel.font]);
        self.nameLabel.numberOfLines = 1;
    }
    
    // 3. 赏金sku
    self.skuLabel.text = self.transferCartSingleModel.shippingWay;
    CGSize skuSize = [self.skuLabel.text sizeWithCalcFont:self.skuLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (skuSize.height >= 2 * [NSString contentofHeightWithFont:self.skuLabel.font]){
        self.skuLabel.frame = CGRectMake(self.nameLabel.orgin_x, LCFloat(11) * 2 + 2 * [NSString contentofHeightWithFont:self.nameLabel.font], width, 2 * [NSString contentofHeightWithFont:self.skuLabel.font]);
        self.skuLabel.numberOfLines = 2;
    } else {
        self.skuLabel.frame = CGRectMake(self.nameLabel.orgin_x, LCFloat(11) * 2 + 2 * [NSString contentofHeightWithFont:self.nameLabel.font], width, 1 * [NSString contentofHeightWithFont:self.skuLabel.font]);
        self.skuLabel.numberOfLines = 1;
    }
    
    // 5. 创建
    self.numberView.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.convertView.frame) - self.numberView.size_height, self.numberView.size_width, self.numberView.size_height);
    
    // 6. 创建删除按钮
    CGSize deleteSize = [@"删除" sizeWithCalcFont:[UIFont systemFontOfCustomeSize:12.] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12.]])];
    CGFloat btnWidth = deleteSize.width + LCFloat(13);
    
    self.deleteButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - btnWidth, 0, btnWidth, self.numberView.size_height);
    self.deleteButton.center_y = self.numberView.center_y;
    
    // 7.
    self.numberView.currentNumber = transferCartSingleModel.goodsNum;
    
    // 8.
    self.checkButton.frame = CGRectMake(0, 0, self.convertView.orgin_x, self.transferCellHeight);
    
    // 9.创建数量
    self.numberLabel.text = [NSString stringWithFormat:@"X%li",(long)self.transferCartSingleModel.goodsNum];
    CGSize numberSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.numberLabel.font])];
    self.numberLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - numberSize.width, self.skuLabel.orgin_y, numberSize.width, [NSString contentofHeightWithFont:self.numberLabel.font]);
    
    // 10.积分
    CGSize jifenSize = [self.jifenLabel.text sizeWithCalcFont:self.jifenLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width, [NSString contentofHeightWithFont:self.jifenLabel.font])];
    self.jifenLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), CGRectGetMaxY(self.convertView.frame) - [NSString contentofHeightWithFont:self.jifenLabel.font], jifenSize.width, [NSString contentofHeightWithFont:self.jifenLabel.font]);
    
}

-(void)setTransferType:(CartSingleTableViewCellType)transferType{
    _transferType = transferType;
}

-(void)setTransferGoodsSingleModel:(OrderListGoodsSingleModel *)transferGoodsSingleModel{
    _transferGoodsSingleModel = transferGoodsSingleModel;
    
    // 2.边框
    self.convertView.frame = CGRectMake(CGRectGetMaxX(self.checkImgView.frame) + LCFloat(11), LCFloat(11), (self.transferCellHeight - 2 * LCFloat(11)), (self.transferCellHeight - 2 * LCFloat(11)));
    self.convertView.orgin_x = LCFloat(11);
    
    // 3. 创建图片
    __weak typeof(self)weakSelf = self;
    [self.proImgView uploadImageWithURL:transferGoodsSingleModel.goodsImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool setTransferSingleAsset:image imgView:strongSelf.proImgView convertView:strongSelf.convertView];
        [strongSelf.proImgView sizeToFit];
    }];
    
    // 4.创建价格
    
    self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",transferGoodsSingleModel.goodMoney];
    NSString *priceStr = [NSString stringWithFormat:@"%@10000.00",@"￥"];
    CGSize priceSize = [priceStr sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    self.priceLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - priceSize.width, LCFloat(11), priceSize.width, [NSString contentofHeightWithFont:self.priceLabel.font]);
    
    // 2. 商品名字
    CGFloat width = kScreenBounds.size.width - LCFloat(11) - priceSize.width - LCFloat(11) - CGRectGetMaxX(self.convertView.frame) - LCFloat(11);
    self.nameLabel.text = transferGoodsSingleModel.goodName;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (nameSize.height >= 2 * [NSString contentofHeightWithFont:self.nameLabel.font]){
        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), LCFloat(11), width,  2 * [NSString contentofHeightWithFont:self.nameLabel.font]);
        self.nameLabel.numberOfLines = 2;
    } else {
        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), LCFloat(11), width,  1 * [NSString contentofHeightWithFont:self.nameLabel.font]);
        self.nameLabel.numberOfLines = 1;
    }
    
    // 3. 赏金sku
    self.skuLabel.text = @"sku";
    CGSize skuSize = [self.skuLabel.text sizeWithCalcFont:self.skuLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (skuSize.height >= 2 * [NSString contentofHeightWithFont:self.skuLabel.font]){
        self.skuLabel.frame = CGRectMake(self.nameLabel.orgin_x, LCFloat(11) * 2 + 2 * [NSString contentofHeightWithFont:self.nameLabel.font], width, 2 * [NSString contentofHeightWithFont:self.skuLabel.font]);
        self.skuLabel.numberOfLines = 2;
    } else {
        self.skuLabel.frame = CGRectMake(self.nameLabel.orgin_x, LCFloat(11) * 2 + 2 * [NSString contentofHeightWithFont:self.nameLabel.font], width, 1 * [NSString contentofHeightWithFont:self.skuLabel.font]);
        self.skuLabel.numberOfLines = 1;
    }
    
    
    // 9.创建数量
    self.numberLabel.text = [NSString stringWithFormat:@"X%li",(long)transferGoodsSingleModel.goodsNum];
    CGSize numberSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.numberLabel.font])];
    self.numberLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - numberSize.width, self.skuLabel.orgin_y, numberSize.width, [NSString contentofHeightWithFont:self.numberLabel.font]);
    
    self.numberView.hidden = YES;
}

-(void)setTransferOrderPageType:(productType)transferOrderPageType{
    _transferOrderPageType = transferOrderPageType;
    if (transferOrderPageType == productTypeJifenDuobao){           // 积分夺宝
        self.jifenLabel.hidden = YES;
        self.priceLabel.hidden = NO;
        self.priceLabel.text = @"￥1";
        self.numberLabel.hidden = NO;
        self.numberView.hidden = NO;
        self.numberView.orgin_x = kScreenBounds.size.width - LCFloat(11) - self.numberView.size_width;
        self.numberView.center_y = self.jifenLabel.center_y;
        
        
        
    } else if (transferOrderPageType == productTypeJifenShangcheng){        // 积分商城
        self.jifenLabel.hidden = NO;
        self.priceLabel.hidden = YES;
        self.numberLabel.hidden = NO;
        self.numberView.hidden = YES;
        self.jifenLabel.text = [NSString stringWithFormat:@"%li积分",(long)self.transferCartSingleModel.goodsMoney];
        CGSize jifenSize = [self.jifenLabel.text sizeWithCalcFont:self.jifenLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width, [NSString contentofHeightWithFont:self.jifenLabel.font])];
        self.jifenLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), CGRectGetMaxY(self.convertView.frame) - [NSString contentofHeightWithFont:self.jifenLabel.font], jifenSize.width, [NSString contentofHeightWithFont:self.jifenLabel.font]);
    } else if(transferOrderPageType == productTypeNormal){          // 普通商品
        self.jifenLabel.hidden = YES;
        if (self.transferType == CartSingleTableViewCellTypeOrder){
            self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",self.transferGoodsSingleModel.goodMoney];
        } else {
            if (self.transferCartSingleModel.goodsMoney == 0){
                self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",self.transferCartSingleModel.goodsMoney];
            } else {
                self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",self.transferCartSingleModel.goodsMoney];
            }
        }
    }
    [self cartCellStup];
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    cellHeight += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(20);
    cellHeight += LCFloat(11);
    return MAX(LCFloat(90), cellHeight);
}

-(void)selectedButtonStatus:(BOOL)status{
    __weak typeof(self)weakSelf = self;
    if (status == YES){
        [Tool clickZanWithView:self.checkImgView block:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.checkImgView.image = [UIImage imageNamed:@"dizhi_xuanzhong"];
        }];
    } else {
        self.checkImgView.image = [UIImage imageNamed:@"dizhi_weixuanzhong"];
    }
}


-(void)actionClickWithDelete:(void(^)())block{
    objc_setAssociatedObject(self, &deleteKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

-(void)actionClickWithCheck:(void(^)(BOOL checked))block{
    objc_setAssociatedObject(self, &checkKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 修改商品数量
-(void)sendRequestToUpdateProductCount:(CartSingleModel*)cartSingleModel count:(NSInteger)count{
    __weak typeof(self)weakSelf = self;
    if (!cartSingleModel.goodsId.length){

    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:cartSingleModel.goodsId forKey:@"goodId"];
    [params setObject:@(cartSingleModel.goodsNum) forKey:@"goodNum"];
    [params setObject:@"0" forKey:@"goodSkuId"];

    [[NetworkAdapter sharedAdapter] fetchWithPath:@"ymgInterface/mobile/shop/item/editCart" requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSucceeded){
            cartSingleModel.goodsNum = count;
            void(^block)(NSInteger count) = objc_getAssociatedObject(strongSelf, &changeNumberOfCountKey);
            if (block){
                block(count);
            }

        }
    }];
}

-(void)changeNumberOfCount:(void(^)(NSInteger count))block{
    objc_setAssociatedObject(self, &changeNumberOfCountKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void)cartCellStup{
    if(self.transferType == CartSingleTableViewCellTypeCart){
        self.checkButton.hidden= NO;
        self.checkImgView.hidden = NO;
        self.numberView.hidden = NO;
        self.deleteButton.hidden = NO;
        self.numberLabel.hidden = YES;
        self.jifenLabel.hidden = YES;
    } else if (self.transferType == CartSingleTableViewCellTypeOrder){
        self.checkButton.hidden= YES;
        self.checkImgView.hidden = YES;
        self.numberView.hidden = YES;
        self.deleteButton.hidden = YES;
        self.numberLabel.hidden = NO;
        self.jifenLabel.hidden = NO;
        
        if (self.transferOrderPageType == productTypeJifenDuobao){
            self.numberView.hidden = NO;
            self.jifenLabel.hidden = YES;
            self.numberLabel.hidden = YES;
        }
    }
    
}
@end

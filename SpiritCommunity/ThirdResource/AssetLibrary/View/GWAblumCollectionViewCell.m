//
//  GWAblumCollectionViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/18.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAblumCollectionViewCell.h"
#import "GWAssetsSnapshotStackView.h"
@interface GWAblumCollectionViewCell()
@property (nonatomic, strong)GWAssetsSnapshotStackView *snapshotStackView;
@property (nonatomic,strong)UILabel *ablumNameLabel;
@property (nonatomic,strong)UILabel *numberOfAblumLabel;

@end

@implementation GWAblumCollectionViewCell


-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self createView];
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = YES;
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.snapshotStackView = [[GWAssetsSnapshotStackView alloc] init];
    self.snapshotStackView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.width);
    self.snapshotStackView.autoresizesSubviews = YES;
    self.snapshotStackView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.snapshotStackView.backgroundColor = [UIColor colorWithWhite:1.000 alpha:1.000];
    self.snapshotStackView.clearsContextBeforeDrawing = YES;
    self.snapshotStackView.backgroundColor = [UIColor clearColor];
    self.snapshotStackView.clipsToBounds = NO;
    self.snapshotStackView.contentMode = UIViewContentModeScaleAspectFit;
    self.snapshotStackView.hidden = NO;
    self.snapshotStackView.multipleTouchEnabled = NO;
    self.snapshotStackView.opaque = YES;
    self.snapshotStackView.tag = 0;
    self.snapshotStackView.userInteractionEnabled = YES;
    self.snapshotStackView.contentMode = UIViewContentModeRedraw;
    self.snapshotStackView.displayAsStack = YES;
    [self addSubview:self.snapshotStackView];
    
    // 2. 创建number
    self.numberOfAblumLabel = [[UILabel alloc]init];
    self.numberOfAblumLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.numberOfAblumLabel.textColor = [UIColor lightGrayColor];
    self.numberOfAblumLabel.shadowColor = [UIColor grayColor];
    self.numberOfAblumLabel.shadowOffset = CGSizeMake(.3f, .3f);
    self.numberOfAblumLabel.textAlignment = NSTextAlignmentRight;
    self.numberOfAblumLabel.frame = CGRectMake(0, CGRectGetMaxY(self.snapshotStackView.frame) - LCFloat(30) - [NSString contentofHeightWithFont:self.numberOfAblumLabel.font], self.bounds.size.width - LCFloat(20), [NSString contentofHeightWithFont:self.numberOfAblumLabel.font]);
    [self addSubview:self.numberOfAblumLabel];
    
    
    // 2. 创建label
    self.ablumNameLabel = [[UILabel alloc]init];
    self.ablumNameLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.ablumNameLabel.shadowOffset = CGSizeMake(.3f, .3f);
    self.ablumNameLabel.textColor = [UIColor lightGrayColor];
    self.ablumNameLabel.shadowColor = [UIColor grayColor];
    self.ablumNameLabel.textAlignment = NSTextAlignmentRight;
    self.ablumNameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.numberOfAblumLabel.frame), self.numberOfAblumLabel.size_width, [NSString contentofHeightWithFont:self.ablumNameLabel.font]);
    [self addSubview:self.ablumNameLabel];
    

}

+(CGSize)calculationCellSize {
    return CGSizeMake((kScreenBounds.size.width - 3 * 10) / 2., (kScreenBounds.size.width - 3 * 10) / 2.);
}

-(void)setTransferImage:(UIImage *)transferImage{
    self.snapshotStackView.image = transferImage;
}

-(void)setNumberOfAblum:(NSString *)numberOfAblum{
    self.numberOfAblumLabel.text = numberOfAblum;
}

-(void)setAblumName:(NSString *)ablumName{
    self.ablumNameLabel.text = ablumName;
}

@end

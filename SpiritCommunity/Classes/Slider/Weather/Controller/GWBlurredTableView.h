//
//  GWBlurredTableView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWBlurredTableView : UITableView

@property (strong, nonatomic) UIImage *backgroundImage;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (strong, nonatomic) NSMutableArray *frames;
@property (assign, nonatomic) NSInteger framesCount;
@property (assign, nonatomic) CGFloat compressionQuality;
@property (assign, nonatomic) CGFloat roundingValue;
@property (strong, nonatomic) UIColor *blurTintColor;
@property (assign, nonatomic) BOOL animateTintAlpha;
@property (assign, nonatomic) CGFloat startTintAlpha;
@property (assign, nonatomic) CGFloat endTintAlpha;
@property (assign, nonatomic) BOOL rendering;
@property (assign, nonatomic) BOOL animateOnLoad;
@property (assign, nonatomic) CGFloat animationDuration;
@end

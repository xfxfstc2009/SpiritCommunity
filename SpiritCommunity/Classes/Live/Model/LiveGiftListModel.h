//
//  LiveGiftListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/3.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "LiveGiftSingleModel.h"

@interface LiveGiftListModel : FetchModel

@property (nonatomic,strong)NSArray<LiveGiftSingleModel> *list;

@end

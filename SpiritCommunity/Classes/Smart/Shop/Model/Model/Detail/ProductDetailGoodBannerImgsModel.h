//
//  ProductDetailGoodBannerImgsModel.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/9.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol ProductDetailGoodBannerImgsModel <NSObject>

@end


@interface ProductDetailGoodBannerImgsModel : FetchModel

@property (nonatomic,assign)NSTimeInterval createTime;
@property (nonatomic,copy)NSString *goodsId;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *imgUrl;

@end

//
//  OrderDetailPriceDetailListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "OrderDetailPriceDetailSingleModel.h"

@interface OrderDetailPriceDetailListModel : FetchModel

@property (nonatomic,strong)NSArray<OrderDetailPriceDetailSingleModel> *infoList;

@end

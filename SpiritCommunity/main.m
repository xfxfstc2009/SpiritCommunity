//
//  main.m
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/21.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

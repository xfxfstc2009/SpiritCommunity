//
//  LiveUIHouseInfoView.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//
// 【房间详情的view】
#import <UIKit/UIKit.h>
#import "HomeLiveSingleModel.h"

@interface LiveUIHouseInfoView : UIView

@property (nonatomic,strong)HomeLiveSingleModel *transferLiveSingleModel;   /**< 传递过去的直播Model*/

+(CGFloat)calculationHeight;

-(void)settingZan:(NSInteger)zan;

-(void)addZan;

@end

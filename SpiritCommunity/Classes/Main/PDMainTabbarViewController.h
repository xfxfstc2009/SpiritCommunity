//
//  PDMainTabbarViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTabBarController.h"

#import "MainCenterMenuViewController.h"                // 个人中心
#import "MessageRootViewController.h"                   // 消息
#import "FindRootViewController.h"                      // 发现
#import "HomeRootViewController.h"                      // 首页

@interface PDMainTabbarViewController : ESTabBarController


@property (nonatomic,strong)MainCenterMenuViewController *centerViewController;

@property (nonatomic,assign)CGFloat tabBarHeight;

+(instancetype)sharedController;

-(void)shenheManager;

@end

//
//  WebSocketConnection.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "WebSocketConnection.h"


@interface WebSocketConnection()<SRWebSocketDelegate>

@end

@implementation WebSocketConnection

-(instancetype)init{
    self = [super init];
    if (self){
        self.webSocket.delegate = nil;
        [self.webSocket close];
    }
    return self;
}

#pragma mark - 链接到服务器
- (void)webSocketConnectWithHost:(NSString *)hostName port:(NSInteger)port{
    NSString *requestURL = [NSString stringWithFormat:@"ws://%@:%li/",hostName,(long)port];
    self.webSocket = [[SRWebSocket alloc]initWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:requestURL]]];
    self.webSocket.delegate = self;
    [self.webSocket open];
}

#pragma mark 断开服务器
- (void)webSocketDisconnect{
    self.webSocket.delegate = nil;
    [self.webSocket close];
    self.webSocket = nil;
}

#pragma mark SRWebSocketDelegate
-(void)webSocketDidOpen:(SRWebSocket *)webSocket{
    PDSocketLog(@"【webSocket已链接上】");
    [StatusBarManager statusBarHidenWithText:@"webSocket已连接"];
    if (_delegate && [_delegate respondsToSelector:@selector(webSocketDidConnectedWithSocket:)]) {
        [_delegate webSocketDidConnectedWithSocket:webSocket];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    PDSocketLog(@":( Websorcket Failed With Error %@", error);
    
    self.webSocket = nil;
}


#pragma mark - 返回数据
- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    if (_delegate && [_delegate respondsToSelector:@selector(webSocket:didReceiveData:)]) {
        [_delegate webSocket:webSocket didReceiveData:message];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean; {
    PDSocketLog(@"WebSocket closed【关闭】");
    _webSocket = nil;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload; {
    PDSocketLog(@"Websocket received pong");
}


#pragma mark - 发送信息
- (void)webSocketWriteData:(NSString *)info{
    [self.webSocket send:info];
}

@end

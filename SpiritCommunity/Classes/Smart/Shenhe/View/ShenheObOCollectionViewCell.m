//
//  ShenheObOCollectionViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/10.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "ShenheObOCollectionViewCell.h"

@interface ShenheObOCollectionViewCell()
@property (nonatomic,strong)PDImageView *guanzhuBgView;
@property (nonatomic,strong)PDImageView *guanzhuIcon;
@property (nonatomic,strong)UILabel *guanzhuLabel;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *areaLabel;
@property (nonatomic,strong)PDImageView *img;

@end

@implementation ShenheObOCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. img
    self.img = [[PDImageView alloc]init];
    self.img.backgroundColor = [UIColor clearColor];
    [self addSubview:self.img];
    self.img.frame = self.bounds;
    self.img.layer.cornerRadius = LCFloat(3);
    self.img.clipsToBounds = YES;

    // 2. 创建关注数量
    self.guanzhuBgView = [[PDImageView alloc]init];
    self.guanzhuBgView.backgroundColor = [UIColor clearColor];
    self.guanzhuBgView.image = [Tool stretchImageWithName:@"home_live_guanzhu_icon"];
    [self addSubview:self.guanzhuBgView];

    // 3. 关注icon
    self.guanzhuIcon = [[PDImageView alloc] init];
    self.guanzhuIcon.backgroundColor = [UIColor clearColor];
    self.guanzhuIcon.image = [UIImage imageNamed:@"home_onebyone_guanzhu_icon"];
    [self.guanzhuBgView addSubview:self.guanzhuIcon];

    self.guanzhuLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"粉"];
    self.guanzhuLabel.adjustsFontSizeToFitWidth = YES;
    [self.guanzhuBgView addSubview:self.guanzhuLabel];

    // 4. name
    self.nickNameLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    [self addSubview:self.nickNameLabel];

    // 5. 创建地区
    self.areaLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    [self addSubview:self.areaLabel];
}

-(void)setTransferSingleModel:(ShenheLiveSIngleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    if (transferSingleModel.img.length){
        [self.img uploadImageWithURL:transferSingleModel.img placeholder:nil callback:NULL];
    } else {
       self.img.image = [Tool getPlayerThumbnailImageWithURL:transferSingleModel.url];
    }

    // 2. 昵称
    self.nickNameLabel.text = transferSingleModel.title;
    self.nickNameLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.img.frame) - LCFloat(11) - [NSString contentofHeightWithFont:self.nickNameLabel.font], self.size_width * .7f, [NSString contentofHeightWithFont:self.nickNameLabel.font]);
}


@end

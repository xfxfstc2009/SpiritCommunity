//
//  CenterOrderRootSingleView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CenterOrderRootSingleView.h"

static char actionKey;
@interface CenterOrderRootSingleView()
@property (nonatomic,strong)PDImageView *iconImgView;
@property(nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel*numberLabel;
@property (nonatomic,strong)UIButton *actionButton;


@end

@implementation CenterOrderRootSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    // 2.创建view
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    // 3.创建数量
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor colorWithCustomerName:@"棕"];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.textColor = [UIColor whiteColor];
    self.numberLabel.font = [[UIFont systemFontOfCustomeSize:11.]boldFont];
    [self addSubview:self.numberLabel];
    
    // 4. actionBlock
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    self.actionButton.frame = self.bounds;
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionKey);
        if(block){
            block();
        }
    }];
}

-(void)setImage:(UIImage *)image{
    _image = image;
}

-(void)setTitle:(NSString *)title{
    _title = title;
    
    CGFloat margin = (self.size_height - LCFloat(18) - [NSString contentofHeightWithFont:self.titleLabel.font]) / 3.;;
    self.iconImgView.frame = CGRectMake(0,margin , LCFloat(18), LCFloat(18));
    
    if (self.tranferType == CenterOrderRootSingleViewTypeHome){
        margin = (self.size_height - LCFloat(34) - [NSString contentofHeightWithFont:self.titleLabel.font]) / 3.;;
        self.iconImgView.frame = CGRectMake(0,margin , LCFloat(34), LCFloat(34));

    }
    
    self.iconImgView.center_x = self.size_width / 2.;
    self.iconImgView.image = self.image;
    
    // 2.
    self.titleLabel.text = title;
    self.titleLabel.frame =CGRectMake(0, CGRectGetMaxY(self.iconImgView.frame) + margin, self.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    // 3. 数量
}

-(void)setNumber:(NSInteger)number{
    _number = number;
    
    self.numberLabel.text = [NSString stringWithFormat:@"%li",(long)number];
    CGSize numberSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.numberLabel.font])];
    CGFloat width = numberSize.width + 2 * LCFloat(3);
    self.numberLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) - width * .4f, self.iconImgView.orgin_y - [NSString contentofHeightWithFont:self.numberLabel.font] * .4f,width, [NSString contentofHeightWithFont:self.numberLabel.font]);
    self.numberLabel.layer.cornerRadius = MIN(self.numberLabel.size_height, self.numberLabel.size_width) / 2.;
    self.numberLabel.clipsToBounds = YES;
    
    if (number == 0){
        self.numberLabel.hidden = YES;
    } else {
        self.numberLabel.hidden = NO;
    }
}


-(void)actionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end

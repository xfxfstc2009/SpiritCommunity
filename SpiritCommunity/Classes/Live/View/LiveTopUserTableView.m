//
//  LiveTopUserTableView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/29.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveTopUserTableView.h"

static CGFloat kCellWidth = 80;
static CGFloat kCellHeight = 40;

@interface LiveTopUserTableView()<UITableViewDelegate, UITableViewDataSource>

@end

@implementation LiveTopUserTableView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.bounds = CGRectMake(0, 0, self.size_height, self.size_width);
    tableView.center = CGPointMake(CGRectGetMidX(self.frame), 64 + kCellHeight / 2);
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:tableView];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CELL"];
        cell.transform = CGAffineTransformMakeRotation(M_PI_2);

        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(kCellWidth - 1, 0, 1, kCellWidth)];
        separatorView.backgroundColor = [UIColor grayColor];
        [cell.contentView addSubview:separatorView];

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kCellWidth, kCellHeight)];
        label.textAlignment = NSTextAlignmentCenter;
        label.tag = 1;
        [cell.contentView addSubview:label];
    }

    return cell;
}

#pragma mark - UITableviewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kCellWidth;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    UILabel *label = [cell.contentView viewWithTag:1];
    label.text = [@(indexPath.row + 1) stringValue];
}

@end

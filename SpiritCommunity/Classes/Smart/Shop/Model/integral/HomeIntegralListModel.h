//
//  HomeIntegralListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "HomeIntegralSingleModel.h"
@interface HomeIntegralListModel : FetchModel

@property (nonatomic,strong)NSArray<HomeIntegralSingleModel>*infoList;

@end

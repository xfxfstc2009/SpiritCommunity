//
//  LiveUIBottomView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/29.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveUIBottomView.h"


static char liveBottomUIViewClickBlockKey;
@interface LiveUIBottomView(){
    
}
@property (nonatomic,strong)UIView *bottomView;             /**< 底部的view*/
@property (nonatomic,strong)NSMutableArray *bottomDataArr;  /**< 底部的view的数组*/
@property (nonatomic,assign)CGFloat margin;
@end

@implementation LiveUIBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.bottomView.frame = self.bounds;
    [self addSubview:self.bottomView];
    
    // 创建底部的view
    self.bottomDataArr = [NSMutableArray array];
}

-(void)setTransferShowType:(LiveShowType)transferShowType{
    _transferShowType = transferShowType;
    if (self.bottomDataArr.count){
        [self.bottomDataArr removeAllObjects];
    }
    
    if (self.transferShowType == LiveShowTypeAnchor){           // 主播
        for (int i = 0 ; i < 6; i++){
            LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
            if (i == 0){                                    // 【聊天】
                liveUIBottomViewSingleModel.buttonImg = @"live_detail_chat";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionChat;
            } else if (i == 1){                             // 【分享】
                liveUIBottomViewSingleModel.buttonImg = @"live_detail_share";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionShare;
            } else if (i == 2){                             // 【美颜】
                liveUIBottomViewSingleModel.buttonImg = @"";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionNone;
            } else if (i == 3){                             // 【更换相机】
                liveUIBottomViewSingleModel.buttonImg = @"";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionNone;
            } else if (i == 4){                             // 【语音】
                liveUIBottomViewSingleModel.buttonImg = @"";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionNone;
            } else if (i == 5){                             // 【更多】
                liveUIBottomViewSingleModel.buttonImg = @"live_detail_more";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionMore;
            }
            [self.bottomDataArr addObject:liveUIBottomViewSingleModel];
        }
    } else if (self.transferShowType == LiveShowTypeAudience){  // 观众
        for (int i = 0 ;i<5;i++){
             LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
            if (i == 0){                                        // 【聊天】
                liveUIBottomViewSingleModel.buttonImg = @"live_detail_chat";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionChat;
            } else if (i == 1){                                 // 【分享】
                liveUIBottomViewSingleModel.buttonImg = @"";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionNone;
            } else if (i == 2){
                liveUIBottomViewSingleModel.buttonImg = @"";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionNone;
            } else if (i == 3){                                 // 【礼物】
                liveUIBottomViewSingleModel.buttonImg = @"live_detail_gift";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionGift;
            } else if (i == 4){                                 // 【点赞】
                liveUIBottomViewSingleModel.buttonImg = @"live_detail_aixin_icon";
                liveUIBottomViewSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionClickZan;
            }
            [self.bottomDataArr addObject:liveUIBottomViewSingleModel];
        }
    }
    if (self.bottomView.subviews.count){
        [self.bottomView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    __weak typeof(self)weakSelf = self;
    CGFloat size_width = LCFloat(30);
    CGFloat size_height = LCFloat(30);
    CGFloat origin_y = (self.bottomView.size_height - LCFloat(30)) / 2.;
    CGFloat margin = (kScreenBounds.size.width - size_height * self.bottomDataArr.count) / (self.bottomDataArr.count + 1);
    for (int i = 0 ; i < self.bottomDataArr.count;i++){
        LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel = [self.bottomDataArr objectAtIndex:i];
        CGFloat origin_x = margin + i * (margin + size_width);
        UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        actionButton.frame = CGRectMake(origin_x, origin_y, size_width, size_height);
        [actionButton setImage:[UIImage imageNamed:liveUIBottomViewSingleModel.buttonImg] forState:UIControlStateNormal];
        [actionButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
           void(^block)(LiveUIBottomViewSingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &liveBottomUIViewClickBlockKey);
            if (block){
                block(liveUIBottomViewSingleModel);
            }
        }];
        if (liveUIBottomViewSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionClickZan){
            self.clickZanButton = actionButton;
        }
        
        [self.bottomView addSubview:actionButton];
    }
}

-(void)liveBottomUIViewClickBlock:(void(^)(LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel))block{
    objc_setAssociatedObject(self, &liveBottomUIViewClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end

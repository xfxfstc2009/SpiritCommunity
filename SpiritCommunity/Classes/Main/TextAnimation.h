//
//  TextAnimation.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextAnimation : UIView

-(instancetype)initWithFrame:(CGRect)frame  title:(NSString *)title;

@property (nonatomic,assign)BOOL hasDismiss;

-(void)showAnimationWithFinishBlock:(void(^)())finish;

@end

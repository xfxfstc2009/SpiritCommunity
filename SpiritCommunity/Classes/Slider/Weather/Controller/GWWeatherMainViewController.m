//
//  GWWeatherMainViewController.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/7.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWWeatherMainViewController.h"
#import "GWWeatherCurrentViewController.h"
#import "GWWeatherTodayViewController.h"
#import "GWWeatherSingleModel.h"
#import "GWWeatherHistoryViewController.h"

@interface GWWeatherMainViewController()<GWWeatherTodayViewControllerDelegate>{
    GWWeatherCurrentModel *weatherCurrentModel;
}
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UIButton *openDrawerButton;                         /**< 按钮*/
@property (nonatomic,strong)GWWeatherTodayViewController *todayController;      /**< 今天控制器*/
@property (nonatomic,strong)GWWeatherHistoryViewController *historyController;  /**< 历史控制器*/
@end

@implementation GWWeatherMainViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createMainScrollView];
    [self sendRequestToGetWeatherInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.openDrawerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openDrawerButton.backgroundColor = [UIColor clearColor];
    self.openDrawerButton.userInteractionEnabled = YES;
    [self.openDrawerButton setImage:[UIImage imageNamed:@"icon_home_slider"] forState:UIControlStateNormal];
    self.openDrawerButton.frame = CGRectMake(LCFloat(25), LCFloat(30), 30, 30);
    __weak typeof(self)weakSelf = self;
    [weakSelf.openDrawerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
//        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
    [self.view addSubview:self.openDrawerButton];
    
}

#pragma mark -CreateView
-(void)createMainScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.openDrawerButton.frame) + LCFloat(11), kScreenBounds.size.width, kScreenBounds.size.height - (CGRectGetMaxY(self.openDrawerButton.frame) + LCFloat(11)))];
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, 2 * self.mainScrollView.size_height);
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.bounces = NO;
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.mainScrollView];
    }
    [self createTodayController];
    [self createHistoryController];
}

#pragma mark - 创建今天
-(void)createTodayController{
    self.todayController = [[GWWeatherTodayViewController alloc] init];
    self.todayController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.mainScrollView.size_height);
    self.todayController.delegate = self;
    [self.mainScrollView addSubview:self.todayController.view];
    self.todayController.view.backgroundColor = [UIColor clearColor];
    [self addChildViewController:self.todayController];
}

#pragma mark - 创建历史
-(void)createHistoryController{
    self.historyController = [[GWWeatherHistoryViewController alloc] init];
    self.historyController.view.frame = CGRectMake(0, self.mainScrollView.size_height, kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.historyController.view];
    self.historyController.view.backgroundColor = [UIColor clearColor];
    [self addChildViewController:self.historyController];
}

#pragma mark - Interface
-(void)sendRequestToGetWeatherInfo{
    GWWeatherSingleModel *fetchModel = [[GWWeatherSingleModel alloc]init];
    fetchModel.requestParams = @{@"city":@"杭州",@"province":@"浙江"};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithGetShareAPIPath:@"http://apicloud.mob.com/v1/weather/query" completionHandler:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){                       // 1.保存数据
            strongSelf->weatherCurrentModel =(GWWeatherCurrentModel *)[fetchModel.data lastObject];
            
            if (strongSelf.historyController.historyMutableArr){
                strongSelf.historyController.historyMutableArr = [NSMutableArray array];
            }
            
            strongSelf.todayController.weatherCurrentModel = strongSelf->weatherCurrentModel;
            [strongSelf.todayController.todayWeatherTableView reloadData];
            
            [strongSelf.historyController.historyMutableArr addObjectsFromArray:strongSelf->weatherCurrentModel.future];
            [strongSelf.historyController.historyTableView reloadData];
        }
    }];
}
@end

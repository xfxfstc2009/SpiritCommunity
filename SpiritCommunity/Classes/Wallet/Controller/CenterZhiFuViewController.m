//
//  CenterZhiFuViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/8.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "CenterZhiFuViewController.h"
#import "CenterMoneyZhifuPayBindingModel.h"

static char actionBidingSuccessedManagerkey;
@interface CenterZhiFuViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *inputTextField;
}
@property (nonatomic,strong)UITableView *aliPayTableView;
@property (nonatomic,strong)NSArray *aliPayArr;
@end

@implementation CenterZhiFuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self pageSetting];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"绑定支付宝账号";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.aliPayArr = @[@[@"支付宝账号"],@[@"确认"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.aliPayTableView){
        self.aliPayTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.aliPayTableView.dataSource = self;
        self.aliPayTableView.delegate = self;
        [self.view addSubview:self.aliPayTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.aliPayArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.aliPayArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if(indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferPlaceholeder = @"请输入你的支付宝账号";
        cellWithRowOne.transferTitle = @"账号";
        inputTextField = cellWithRowOne.inputTextField;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            GWButtonTableViewCell *cell = (GWButtonTableViewCell *)[strongSelf.aliPayTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            if (info.length){
                [cell setButtonStatus:YES];
            } else {
                [cell setButtonStatus:NO];
            }
        }];
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"确认";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf actionClickManager];
        }];
        return cellWithRowTwo;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0){
        NSString *str = @"为了您账户安全，提现账户一旦绑定即无法解绑，如需帮助，请联系客服";

        CGSize titleSize = [str sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
        return titleSize.height + 2 * LCFloat(11);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    footerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(50));

    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    titleLabel.text = @"为了您账户安全，提现账户一旦绑定即无法解绑，如需帮助，请联系客服";
    titleLabel.numberOfLines = 0;
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), titleSize.height);
    [footerView addSubview:titleLabel];
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(30);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section < 1){
        [cell addSeparatorLineWithType:SeparatorTypeBottom];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return LCFloat(44);
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

#pragma mark - 创建支付宝
-(void)actionClickManager{
    if (!inputTextField.text.length){
        [PDHUD showText:@"请输入账号!"];
        return;
    }

    NSString *err = [NSString stringWithFormat:@"提示：当前绑定账号为【%@】绑定后如需修改，请联系客服",inputTextField.text];
    __weak typeof(self)weakSelf = self;
    [[UIAlertView alertViewWithTitle:@"再次确认" message:err buttonTitles:@[@"确定",@"再考虑考虑"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){
            [strongSelf sendRequestTobinding];
        }
    }]show];
}

-(void)sendRequestTobinding{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"accountId":inputTextField.text};
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_zhifuPay requestParams:params responseObjectClass:[CenterMoneyZhifuPayBindingModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            CenterMoneyZhifuPayBindingModel *singleModel = (CenterMoneyZhifuPayBindingModel *)responseObject;
            if (singleModel.zhifuStatus){
                [AccountModel sharedAccountModel].loginModel.zhifupay = singleModel.zhifuAccount;
                [StatusBarManager statusBarHidenWithText:@"绑定成功"];
            } else {
                [StatusBarManager statusBarHidenWithText:@"绑定失败"];
            }
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionBidingSuccessedManagerkey);
            if (block){
                block();
            }
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

-(void)actionBidingSuccessedManager:(void(^)())block{
    objc_setAssociatedObject(self, &actionBidingSuccessedManagerkey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end

//
//  OrderSegmentSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface OrderSegmentSingleModel : FetchModel

@property (nonatomic,copy)NSString *itemId;

@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *tableIndex;




@end

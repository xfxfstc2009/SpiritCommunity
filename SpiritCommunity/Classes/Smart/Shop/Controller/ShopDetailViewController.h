//
//  ShopDetailViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/1.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,productType) {
    productTypeNormal = 1,                      /**< 普通商品*/
    productTypeJifenDuobao = 3,                 /**< 积分夺宝*/
    productTypeJifenShangcheng = 2,             /**< 积分商城*/
};

@interface ShopDetailViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferProductId;
@property (nonatomic,assign)productType transferProductType;

@property (nonatomic,assign)NSTimeInterval transferEndTime;

@end

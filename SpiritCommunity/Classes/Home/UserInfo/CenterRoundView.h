//
//  CenterRoundView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/9.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 个人中心圆角的view 
#import <UIKit/UIKit.h>

@interface CenterRoundView : UIView


-(instancetype)initWithFrame:(CGRect)frame withTitle:(NSString *)title;
-(void)changeInfo:(NSString *)info;
-(void)changeGender:(NSInteger)gender;
@end

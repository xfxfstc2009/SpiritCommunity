//
//  ChatSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/5.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface ChatSingleModel : FetchModel

@property (nonatomic,copy)NSString *avatarUrl;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *personId;
@property (nonatomic,copy)NSString *aDisplayName;
@property (nonatomic,strong)UIImage *avatarImg;
@property (nonatomic,copy)NSString *desc;
@property (nonatomic,strong)NSDate *time;
@property (nonatomic,assign)NSInteger numberOfCount;
@property (nonatomic,copy)NSString *conversationId;

@end

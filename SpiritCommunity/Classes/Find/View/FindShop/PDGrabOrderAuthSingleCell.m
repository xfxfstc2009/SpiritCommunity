//
//  PDGrabOrderAuthSingleCell.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/21.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDGrabOrderAuthSingleCell.h"

@interface PDGrabOrderAuthSingleCell()
@property (nonatomic,strong)PDImageView*bgImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nameLabel;

@end

@implementation PDGrabOrderAuthSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgImgView];

    self.nameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.nameLabel.text = @"卖家信息";
    [self addSubview:self.nameLabel];

    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame= CGRectMake(kScreenBounds.size.width - LCFloat(38) - LCFloat(65), 0, LCFloat(65), LCFloat(65));
    [self addSubview:self.avatarImgView];
}

-(void)setTransferAuthModel:(GWShopShopSingleModel *)transferAuthModel{
    _transferAuthModel = transferAuthModel;

    //1. bg
    self.bgImgView.frame = CGRectMake(LCFloat(12), 0, kScreenBounds.size.width - 2 * LCFloat(12), self.transferCellHeight);
    self.bgImgView.image = [Tool stretchImageWithName:@"icon_grabOrder_auth_bg"];

    // 2.
    if (transferAuthModel.userInfo.avatar.length){
            [self.avatarImgView uploadImageWithAvatarURL:transferAuthModel.userInfo.avatar placeholder:[UIImage imageNamed:@"center_header_nor_icon"] callback:NULL];
    } else {
        self.avatarImgView.image = [UIImage imageNamed:@"center_header_nor_icon"];
    }

    self.avatarImgView.center_y = self.transferCellHeight / 2.;

    // title
    self.nameLabel.frame = CGRectMake(LCFloat(38), 0, 80, [NSString contentofHeightWithFont:self.nameLabel.font]);
    self.nameLabel.center_y = self.transferCellHeight / 2.;
}


+(CGFloat)calculationCellHeight{
    return LCFloat(118);
}

@end

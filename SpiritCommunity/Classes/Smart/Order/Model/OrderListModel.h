//
//  OrderListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "OrderListSingleModel.h"

@interface OrderListModel : FetchModel

@property (nonatomic,strong)NSArray<OrderListSingleModel> *infoList;

@end

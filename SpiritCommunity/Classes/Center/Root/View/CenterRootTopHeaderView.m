//
//  CenterRootTopHeaderView.m
//  17Live
//
//  Created by 裴烨烽 on 2018/1/20.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "CenterRootTopHeaderView.h"
#import "CenterRootCustomerSingleView.h"

static char actionClickWithHeaderKey;
static char actionClickBlockKey;
@interface CenterRootTopHeaderView()
@property (nonatomic,strong)UIView *alphaView;                            /**< 上部的萌层*/
@property (nonatomic,strong)PDImageView *avatarImgView;                   /**< 头像*/
@property (nonatomic,strong)UIButton *avatarButton;                       /**< 头像点击*/
@property (nonatomic,strong)UILabel *nameLabel;                           /**< 用户名字*/
@property (nonatomic,strong)CenterRootCustomerSingleView *leftView;       /**< 左侧的view*/
@property (nonatomic,strong)CenterRootCustomerSingleView *rightView;      /**< 右侧的view*/

@end

@implementation CenterRootTopHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - pageSetting
-(void)createView{

    // 1. 创建萌层
    self.alphaView = [[UIView alloc]init];
    self.alphaView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.alphaView.layer.shadowOffset = CGSizeMake(0.9f, 0.9f);
    self.alphaView.layer.shadowOpacity = .9f;
    self.alphaView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.alphaView.frame = CGRectMake(0, kCenterTopHeaderAvatar / 2., kScreenBounds.size.width, self.size_height - kCenterTopHeaderAvatar / 2.);

    // 2. 头像背景
    self.avatarBgView = [[UIView alloc]init];
    self.avatarBgView.frame = CGRectMake((kScreenBounds.size.width - kCenterTopHeaderAvatar) / 2., 0,kCenterTopHeaderAvatar, kCenterTopHeaderAvatar);
    self.avatarBgView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.avatarBgView.layer.cornerRadius = self.avatarBgView.size_height / 2.;
    self.avatarBgView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.avatarBgView.layer.shadowOffset = CGSizeMake(0.9f, 0.9f);
    self.avatarBgView.layer.shadowOpacity = .9f;
    [self addSubview:self.avatarBgView];
    [self addSubview:self.alphaView];

    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.frame = CGRectMake(self.avatarBgView.orgin_x, self.avatarBgView.orgin_y, self.avatarBgView.size_width, self.avatarBgView.size_height);
    [self.avatarImgView uploadImageWithAvatarURL:[AccountModel sharedAccountModel].loginModel.avatar placeholder:nil callback:NULL];
    [self addSubview:self.avatarImgView];

    // 3. 创建名字
    self.nameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.font = [self.nameLabel.font boldFont];
    self.nameLabel.frame = CGRectMake((kScreenBounds.size.width - kCenterTopHeaderAvatar) / 2., CGRectGetMaxY(self.avatarBgView.frame) + LCFloat(7), kCenterTopHeaderAvatar, LCFloat(20));

    [self addSubview:self.nameLabel];

    // 4. 创建按钮
    self.avatarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.avatarButton.frame = self.avatarBgView.frame;
    __weak typeof(self)weakSelf = self;
    [self.avatarButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithHeaderKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.avatarButton];

    // 5.粉丝
    CGFloat margin = LCFloat(20);
    CGFloat size = (kScreenBounds.size.width - 4 * margin - kCenterTopHeaderAvatar) / 2.;

    CenterCustomerSingleModel *recordSingleModel = [[CenterCustomerSingleModel alloc]init];
    if ([AccountModel sharedAccountModel].isShenhe){
        recordSingleModel.fixedStr = @"关注";
        recordSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.link_count];
    } else {
        recordSingleModel.fixedStr = @"关注/粉丝";
        recordSingleModel.dymicStr = [NSString stringWithFormat:@"%li/%li",(long)[AccountModel sharedAccountModel].loginModel.link_count,(long)[AccountModel sharedAccountModel].loginModel.fans_count];
    }


    self.leftView = [[CenterRootCustomerSingleView alloc]initWithFrame:CGRectMake(margin, self.alphaView.orgin_y, size, self.alphaView.size_height)];
    self.leftView.transferCustomerSingleMode = recordSingleModel;
    self.leftView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.leftView];

    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.frame = self.leftView.bounds;
    [leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(CenterCustomerSingleModel *recordSingleModel) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(recordSingleModel);
        }
    }];
    [self.leftView addSubview:leftButton];

    // 6. 金币
    CenterCustomerSingleModel *goldSingleModel = [[CenterCustomerSingleModel alloc]init];

    if ([AccountModel sharedAccountModel].isShenhe){
        goldSingleModel.fixedStr = @"粉丝";
        goldSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.fans_count];
    } else {
        goldSingleModel.fixedStr = @"我的金币";
        goldSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.money];
    }

    self.rightView = [[CenterRootCustomerSingleView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - margin - size, self.alphaView.orgin_y, size, self.alphaView.size_height)];
    self.rightView.transferCustomerSingleMode = goldSingleModel;
    self.rightView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.rightView];


    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = self.rightView.bounds;
    [rightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(CenterCustomerSingleModel *recordSingleModel) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(goldSingleModel);
        }
    }];
    [self.rightView addSubview:rightButton];
}

#pragma mark - Set
-(void)setTransferAccountModel:(AccountModel *)transferAccountModel{
    _transferAccountModel = transferAccountModel;

    // 1. 头像
    if (transferAccountModel.loginModel.avatar.length){
        [self.avatarImgView uploadImageWithAvatarURL:transferAccountModel.loginModel.avatar placeholder:nil callback:NULL];
    } else {
        self.avatarImgView.image = [UIImage imageNamed:@"icon_avatar_normal"];
    }

    // 2. 名字
    if (transferAccountModel.loginModel.nick.length){
        self.nameLabel.text = transferAccountModel.loginModel.nick;
    } else {
        if ([AccountModel sharedAccountModel].loginModel.ID.length && [AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeNor){
            if ([AccountModel sharedAccountModel].loginModel.union_number.length){
                self.nameLabel.text = [NSString stringWithFormat:@"精灵%@",[AccountModel sharedAccountModel].loginModel.union_number];
            } else if ([AccountModel sharedAccountModel].loginModel.ID.length){
                self.nameLabel.text = [NSString stringWithFormat:@"精灵%@",[AccountModel sharedAccountModel].loginModel.ID];
            }
        } else {
            self.nameLabel.text = @"注册/登录";
        }
    }
    
    // 6. 金币
    CenterCustomerSingleModel *goldSingleModel = [[CenterCustomerSingleModel alloc]init];
    
    //        if ([AccountModel sharedAccountModel].isShenhe){
    //            goldSingleModel.fixedStr = @"粉丝";
    //            goldSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.fans_count];
    //        } else {
    goldSingleModel.fixedStr = @"我的金币";
    goldSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.money];
    //        }
    self.rightView.transferCustomerSingleMode = goldSingleModel;
    
    
    CenterCustomerSingleModel *recordSingleModel = [[CenterCustomerSingleModel alloc]init];
    //        if ([AccountModel sharedAccountModel].isShenhe){
    //            recordSingleModel.fixedStr = @"关注";
    //            recordSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.link_count];
    //        } else {
    recordSingleModel.fixedStr = @"关注/粉丝";
    recordSingleModel.dymicStr = [NSString stringWithFormat:@"%li/%li",(long)[AccountModel sharedAccountModel].loginModel.link_count,(long)[AccountModel sharedAccountModel].loginModel.fans_count];
    //        }
    self.leftView.transferCustomerSingleMode = recordSingleModel;
}









#pragma mark - Action
// 1. 点击头像
-(void)actionClickWithHeader:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithHeaderKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

// 2. 点击用户信息
-(void)actionClickBlock:(void(^)(CenterCustomerSingleModel *recordSingleModel))block{
    objc_setAssociatedObject(self, &actionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

// 3. 登录之后的动画
-(void)loginAnimationActionManager{
    self.transferAccountModel = [AccountModel sharedAccountModel];
}

+(CGFloat)calculationCellHeight{
    return kCenterTopHeader + LCFloat(15);
}
@end

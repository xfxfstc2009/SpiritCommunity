//
//  LiveErrorStatusSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface LiveErrorStatusSingleModel : FetchModel

@property (nonatomic,assign)NSTimeInterval end_time;
@property (nonatomic,copy)NSString *houseId;
@property (nonatomic,assign)NSTimeInterval start_time;


@end

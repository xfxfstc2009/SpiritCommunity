//
//  ImgSelectorViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/11.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterUserImgsCollectionViewCell.h"

@interface ImgSelectorViewController : UIViewController

-(void)showInView:(UIViewController *)viewController imgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentIndex cell:(CenterUserImgsCollectionViewCell *)cell;

- (void)dismissFromView:(UIViewController *)viewController;

@end

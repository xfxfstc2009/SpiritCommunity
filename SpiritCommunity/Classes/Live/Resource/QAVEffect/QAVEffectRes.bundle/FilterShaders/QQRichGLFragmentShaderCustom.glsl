//
//  QQRichGLFragmentShaderCustom.glsl
//  QQMSFContact
//
//  Created by hodxiang on 16/7/27.
//
//

precision highp float;
varying vec2 canvasCoordinate;
varying vec2 textureCoordinate;
uniform sampler2D inputImageTexture;
uniform sampler2D inputImageTexture1;
uniform sampler2D inputImageTexture2;
uniform sampler2D inputImageTexture3;
uniform sampler2D inputImageTexture4;
uniform sampler2D inputImageTexture5;
uniform sampler2D inputImageTexture6;
uniform sampler2D inputImageTexture7;
uniform sampler2D inputImageTexture8;
uniform sampler2D inputImageTexture9;
uniform vec2 canvasSize;
uniform vec2 faceDetectImageSize;
uniform float facePoints[180];
uniform int faceActionType;
uniform float frameDuration;
uniform float elementDurations[50];

void main(void) {
    vec4 texColor = texture2D(inputImageTexture, canvasCoordinate);
    gl_FragColor = texColor;
}

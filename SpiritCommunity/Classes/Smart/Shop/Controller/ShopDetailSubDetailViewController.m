//
//  ShopDetailSubDetailViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2018/2/1.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "ShopDetailSubDetailViewController.h"
#import "ProductDetailBannerTableViewCell.h"
#import "ProductDetailPriceTableViewCell.h"
#import "ProductDetailProductNameTableViewCell.h"
#import "ProductDetailProductYouhuiTableViewCell.h"
#import "ProductDetailDuobaoInfoCell.h"
#import "ProductEvaluaTableViewCell.h"
#import "ProductDetailSub2TableViewCell.h"

@interface ShopDetailSubDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *productDetailTableView;
@property (nonatomic,strong)NSMutableArray *productMutableArr;
@end

@implementation ShopDetailSubDetailViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];

}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"商品详情";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.productMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if(!self.productDetailTableView){
        self.productDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.productDetailTableView.dataSource = self;
        self.productDetailTableView.delegate = self;
        [self.view addSubview:self.productDetailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.productMutableArr.count;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.productMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;

    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner" sourceArr:self.productMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ProductDetailBannerTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if(!cellWithRowOne){
            cellWithRowOne = [[ProductDetailBannerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferProductDetailModel = self.transferDetailModel;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"价格" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"价格" sourceArr:self.productMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        ProductDetailPriceTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if(!cellWithRowTwo){
            cellWithRowTwo = [[ProductDetailPriceTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo. transferProductDetailModel = self.transferDetailModel;

        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品详情" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"商品详情" sourceArr:self.productMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        ProductDetailProductNameTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowThr){
            cellWithRowThr = [[ProductDetailProductNameTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferProductDetailModel = self.transferDetailModel;
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"优惠" sourceArr:self.productMutableArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        ProductDetailProductYouhuiTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if(!cellWithRowFour){
            cellWithRowFour = [[ProductDetailProductYouhuiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowFour;
    } else if(indexPath.section == [self cellIndexPathSectionWithcellData:@"评价标题" sourceArr:self.productMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"评价标题" sourceArr:self.productMutableArr]){
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            GWNormalTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if(!cellWithRowFiv){
                cellWithRowFiv = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
//            cellWithRowFiv.transferTitle = [NSString stringWithFormat:@"%@(%@,%@)",@"评价",self.transferCommentListModel.sorceinfo.count,self.transferCommentListModel.sorceinfo.star];
            cellWithRowFiv.transferHasArrow = YES;
            return cellWithRowFiv;
        } else {
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            ProductEvaluaTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if(!cellWithRowFiv){
                cellWithRowFiv = [[ProductEvaluaTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellheight = cellHeight;
            cellWithRowFiv.transferCommentDataSingleModel = [[self.productMutableArr objectAtIndex:indexPath.section ] objectAtIndex:indexPath.row];
            return cellWithRowFiv;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"积分详情" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"积分详情" sourceArr:self.productMutableArr]){
        static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
        ProductDetailDuobaoInfoCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
        if (!cellWithRowSev){
            cellWithRowSev = [[ProductDetailDuobaoInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
            cellWithRowSev.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowSev.transferCellHeight = cellHeight;
        cellWithRowSev.transferProductDetailRootModel = self.transferDetailModel;
        return cellWithRowSev;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"图片" sourceArr:self.productMutableArr]){
        static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
        ProductDetailSub2TableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
        if (!cellWithRowSev){
            cellWithRowSev = [[ProductDetailSub2TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
            cellWithRowSev.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowSev.transferImg = self.transferDetailModel.goodBaseInfo.goodDetailImg;
        __weak typeof(self)weakSelf = self;
        [cellWithRowSev actionBlockHeight:^(CGFloat imgHeihgt) {
            if (!weakSelf){
                return ;
            }
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
        return cellWithRowSev;
    }

    else {
        static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
        UITableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
        if(!cellWithRowSex){
            cellWithRowSex = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
            cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowSex;
    }

}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == [self cellIndexPathSectionWithcellData:@"评价标题" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"评价标题" sourceArr:self.productMutableArr]){
//        ProductEvaluateViewController *evaluateViewController = [[ProductEvaluateViewController alloc]init];
//        evaluateViewController.transferProductId = self.transferProductId;
//        [self.navigationController pushViewController:evaluateViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner" sourceArr:self.productMutableArr]){
        return [ProductDetailBannerTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"价格" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"价格" sourceArr:self.productMutableArr]){
        return [ProductDetailPriceTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品详情" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"商品详情" sourceArr:self.productMutableArr]){
        return [ProductDetailProductNameTableViewCell calculationCellHeight:self.transferDetailModel];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"优惠" sourceArr:self.productMutableArr]){
        return [ProductDetailProductYouhuiTableViewCell calculationCellheight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"评价标题" sourceArr:self.productMutableArr]){
        if (indexPath.row == 0){
            return LCFloat(44);
        } else {
            return [ProductEvaluaTableViewCell calculationCellHeight:[[self.productMutableArr objectAtIndex:indexPath.section ] objectAtIndex:indexPath.row]];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"积分详情" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"积分详情" sourceArr:self.productMutableArr]){
        return [ProductDetailDuobaoInfoCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.productMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"图片" sourceArr:self.productMutableArr]){
        return 100;
    } else {
        return 10;
    }
}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *headerView = [[UIView alloc]init];
//    headerView.backgroundColor = [UIColor clearColor];
//    return headerView;
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (section != 0){
//        return LCFloat(15);
//    }
//    return 0;
//}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.productDetailTableView) {
        NSArray *sectionOfArr = [self.productMutableArr objectAtIndex:indexPath.section];
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
            [cell addSeparatorLineWithType:separatorType];return;
        } else if ([indexPath row] == sectionOfArr.count - 1) {
            separatorType  = SeparatorTypeBottom;
            [cell addSeparatorLineWithType:separatorType];return;
        } else {

        }
        if (sectionOfArr.count == 1) {
            separatorType  = SeparatorTypeSingle;
            [cell addSeparatorLineWithType:separatorType];return;
        }
    }
}

-(void)pageLoad{
    [self.productMutableArr addObjectsFromArray:@[@[@"banner",@"商品详情",@"价格",@"优惠"]]];
    [self.productDetailTableView reloadData];
}

@end

//
//  ProductDetailPriceSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "ProductDetailPriceSingleModel.h"

@implementation ProductDetailPriceSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end

//
//  UserMoneyTransferViewController.m
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/3/9.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import "UserMoneyTransferViewController.h"
#import "PDTransferAccountsHeaderCell.h"

@interface UserMoneyTransferViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    UITextField *amountTextField;               /**< 金额*/
    UITextField *beizhuTextField;               /**< 备注*/
}
@property (nonatomic,strong)UITableView *zhuanzhangTableView;
@property (nonatomic,strong)NSArray *zhuanzhangArr;
@end

@implementation UserMoneyTransferViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"金币转账";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.zhuanzhangArr = @[@[@"头部",@"金额",@"备注"],@[@"确认赠送"]];
}

-(void)createTableView{
    self.zhuanzhangTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
    self.zhuanzhangTableView.dataSource = self;
    self.zhuanzhangTableView.delegate = self;
    [self.view addSubview:self.zhuanzhangTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.zhuanzhangArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.zhuanzhangArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellheight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if(indexPath.section == 0 && indexPath.row == 0){         // 头部
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDTransferAccountsHeaderCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDTransferAccountsHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        }
        cellWithRowOne.transferCellHeight = cellheight;
        cellWithRowOne.transferSearchSingleModel = self.transferSearchSingleModel;
        
        return cellWithRowOne;
    } else if ((indexPath.section == 0 && indexPath.row == 1) || (indexPath.section == 0 && indexPath.row == 2)){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellheight;
        if (indexPath.row == 1){
            cellWithRowTwo.transferTitle = @"金额";
            cellWithRowTwo.transferPlaceholeder = @"请输入要输入金额";
            amountTextField = cellWithRowTwo.inputTextField;
            amountTextField.delegate = self;
            amountTextField.keyboardType = UIKeyboardTypeDecimalPad;
        } else if (indexPath.row == 2){
            cellWithRowTwo.transferTitle = @"备注";
            cellWithRowTwo.transferPlaceholeder = @"请输入要输入备注（20字以内）";
            beizhuTextField = cellWithRowTwo.inputTextField;
            beizhuTextField.delegate = self;
            beizhuTextField.keyboardType = UIKeyboardTypeDefault;
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf adjustWithBtnStatus];
        }];
        
        return cellWithRowTwo;
        
    } else if (indexPath.section == 1 && indexPath.row == 0){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellheight;
        cellWithRowThr.transferTitle = @"确认赠送";
        [cellWithRowThr setButtonStatus:NO];
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf releaseTextField];
            [strongSelf sendRequestToZhuanzhangWithId:strongSelf.transferSearchSingleModel.ID amount:strongSelf->amountTextField.text remark:strongSelf->beizhuTextField.text];
        }];
        return cellWithRowThr;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        return LCFloat(160);
    } else{
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return LCFloat(60);
    } else {
        return 0;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.zhuanzhangTableView) {
        if (indexPath.section == 0){
            if(indexPath.row != 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                if ( [indexPath row] == 0) {
                    separatorType = SeparatorTypeHead;
                } else if ([indexPath row] == [[self.zhuanzhangArr objectAtIndex:indexPath.section] count] - 1) {
                    separatorType = SeparatorTypeBottom;
                } else {
                    separatorType = SeparatorTypeMiddle;
                }
                if ([[self.zhuanzhangArr objectAtIndex:indexPath.section] count] == 1) {
                    separatorType = SeparatorTypeSingle;
                }
                [cell addSeparatorLineWithType:separatorType];
            }
        }
    }
}

#pragma mark - IntefaceManager
-(void)sendRequestToZhuanzhangWithId:(NSString *)memberId amount:(NSString *)amount remark:(NSString *)remark{
    __weak typeof(self)weakSelf = self;

    NSDictionary *params = @{@"toUserId":memberId,@"coin":amount,@"remark":remark};

    [[NetworkAdapter sharedAdapter] fetchWithPath:center_moneyTransfer requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"转账成功" message:[NSString stringWithFormat:@"您已成功向【%@】转账金币【%@】",strongSelf.transferSearchSingleModel.nick,amount]  buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }]show];
        }
    }];
}

-(void)setTransferSearchSingleModel:(OtherSearchSingleModel *)transferSearchSingleModel{
    _transferSearchSingleModel = transferSearchSingleModel;
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (textField == amountTextField){
        return YES;
    } else if (textField == beizhuTextField){
        if (toBeString.length > 20){
            textField.text = [toBeString substringToIndex:20];
            return NO;
        }
    }
    
    return YES;
}

-(void)adjustWithBtnStatus{
    GWButtonTableViewCell *btnCell = (GWButtonTableViewCell *)[self.zhuanzhangTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if(amountTextField.text.length && beizhuTextField.text.length){
        [btnCell setButtonStatus:YES];
    } else {
        [btnCell setButtonStatus:NO];
    }
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    [self releaseTextField];
}

-(void)releaseTextField{
    if ([amountTextField isFirstResponder]){
        [amountTextField resignFirstResponder];
    } else if ([beizhuTextField isFirstResponder]){
        [beizhuTextField resignFirstResponder];
    }
}

@end

//
//  CenterRootCustomerSingleView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/6.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CenterRootCustomerSingleView.h"

@interface CenterRootCustomerSingleView()
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *fixedLabel;

@end

@implementation CenterRootCustomerSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.font = [[UIFont fontWithCustomerSizeName:@"小正文"] boldFont];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.numberLabel];
    
    // 2. 创建fixedLabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview:self.fixedLabel];
}


-(void)setTransferCustomerSingleMode:(CenterCustomerSingleModel *)transferCustomerSingleMode{
    transferCustomerSingleMode = transferCustomerSingleMode;
    self.numberLabel.text = transferCustomerSingleMode.dymicStr;
    
    self.fixedLabel.text = transferCustomerSingleMode.fixedStr;
    
    CGFloat numberSize = [NSString contentofHeightWithFont:self.numberLabel.font];
    CGFloat fixedSize = [NSString contentofHeightWithFont:self.fixedLabel.font];
    CGFloat margin = (self.size_height - numberSize - fixedSize) / 3.;
    
    self.numberLabel.frame = CGRectMake(0, CGRectGetMaxY(self.numberLabel.frame ) + LCFloat(20), self.bounds.size.width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    self.fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.numberLabel.frame) + margin, self.size_width, fixedSize);
    
    self.numberLabel.orgin_y = margin;
    self.fixedLabel.orgin_y = CGRectGetMaxY(self.numberLabel.frame) + margin;
}
@end

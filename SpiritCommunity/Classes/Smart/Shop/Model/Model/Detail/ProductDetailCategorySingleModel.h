//
//  ProductDetailCategorySingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol ProductDetailCategorySingleModel <NSObject>

@end

@interface ProductDetailCategorySingleModel : FetchModel


@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *Vaule;


@end

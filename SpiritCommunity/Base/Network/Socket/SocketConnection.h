//
//  SocketConnection.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/23.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "URLConstance.h"

typedef NS_ENUM(NSInteger,SocketOffline) {
    SocketOfflineByServer,          /**< 服务器掉线，默认0*/
    SocketOfflineByUser,            /**< 用户主动cut*/
};

@protocol PDSocketConnectionDelegate <NSObject>

@optional
- (void)didDisconnectWithError:(NSError *)error;
- (void)didConnectToHost:(NSString *)host port:(UInt16)port;
- (void)didReceiveData:(NSData *)data tag:(long)tag;

@end

@interface SocketConnection : NSObject
@property (nonatomic,copy)NSString *host;
@property (nonatomic,assign)NSInteger port;

@property (nonatomic,weak)id<PDSocketConnectionDelegate> delegate;

-(void)connectWithHost:(NSString *)hostName port:(NSInteger)port;                       /**< 连接到服务器*/
- (void)disconnect;                                                                     /**< 断开服务*/

- (BOOL)isConnected;                                                                    /**< 判断是否链接*/
- (void)readDataWithTimeout:(NSTimeInterval)timeout tag:(long)tag;                      /**< 设置timeout时间*/
- (void)writeData:(NSData *)data timeout:(NSTimeInterval)timeout tag:(long)tag;         /**< 传输数据*/

@end

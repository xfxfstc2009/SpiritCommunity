//
//  GWSuggestionViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/12.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSuggestionViewController.h"

@interface GWSuggestionViewController()<UITextViewDelegate>
@property (nonatomic,strong)UITextView *adviceTextView;                 /**< 建议textView*/
@property (nonatomic,strong)UILabel *textlimitLabel;                    /**< 控制文字长度的Label*/
@property (nonatomic,strong)UIButton *sendButton;                       /**< 提交按钮*/

@end

@implementation GWSuggestionViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"意见反馈";
}

#pragma mark - createView
-(void)createView{
    // 建议textView
    self.adviceTextView = [[UITextView alloc]init];
    self.adviceTextView.delegate = self;
    self.adviceTextView.textColor = [UIColor blackColor];
    self.adviceTextView.returnKeyType = UIReturnKeyDefault;
    self.adviceTextView.keyboardType = UIKeyboardTypeDefault;
    self.adviceTextView.backgroundColor = [UIColor whiteColor];
    self.adviceTextView.scrollEnabled = YES;
    self.adviceTextView.limitMax = 300;
    self.adviceTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.adviceTextView.frame = CGRectMake(LCFloat(16),LCFloat(20), kScreenBounds.size.width - 2 * LCFloat(16), LCFloat(203));
    [self.view addSubview:self.adviceTextView];
    self.adviceTextView.placeholder = @"输入你的建议";
    __weak typeof(self)weakSelf = self;
    [weakSelf.adviceTextView textViewDidChangeWithBlock:^(NSInteger currentCount) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf adjustWithButtonAndPlaceholderWithCount:currentCount];
    }];
    
    
    // 提示
    UILabel *promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(LCFloat(16), CGRectGetMaxY(self.adviceTextView.frame), self.view.frame.size.width-LCFloat(32), 50)];
    promptLabel.text = @"请详细描述您遇到的问题，我们会进行整改";
    promptLabel.textColor = [UIColor grayColor];
    promptLabel.numberOfLines = 0;
    promptLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.view addSubview:promptLabel];
    
    
    // 确定发送按钮
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sendButton setTitle:@"提交" forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.sendButton.layer.cornerRadius = LCFloat(5);
    self.sendButton.frame = CGRectMake(LCFloat(16), CGRectGetMaxY(promptLabel.frame) + LCFloat(25), kScreenBounds.size.width - 2 * LCFloat(16), LCFloat(44));
    self.sendButton.enabled = NO;
    [self.sendButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendReqeustToSuggest];
    }];
    [self.view addSubview: self.sendButton];
  
    // 添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backTapAction:)];
    [self.view addGestureRecognizer:tap];
}

#pragma mark - 键盘回收
- (void)backTapAction:(UITapGestureRecognizer *)sender {
    [self.adviceTextView resignFirstResponder];
}


#pragma mark 验证
-(void)adjustWithButtonAndPlaceholderWithCount:(NSInteger)count{
    if (count == 0){
        self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
        self.sendButton.enabled = NO;
    } else {
        self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        self.sendButton.enabled = YES;
    }
}

#pragma mark - 接口
-(void)sendReqeustToSuggest{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:suggestion requestParams:@{@"suggestion":self.adviceTextView.text} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"非常感谢您的建议，我们将更加努力" message:NULL buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }]show];
        }
    }];
}

@end

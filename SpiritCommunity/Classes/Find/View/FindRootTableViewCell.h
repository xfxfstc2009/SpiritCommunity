//
//  FindRootTableViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindRootListModel.h"

@interface FindRootTableViewCell : PDBaseTableViewCell

- (void)startAnimationWithDelay:(CGFloat)delayTime;

@property (nonatomic,strong)FindRootSingleModel *transferSingleModel;
@property (nonatomic,assign)NSInteger transferIndexPath;


+(CGFloat)calculationCellHeight:(FindRootSingleModel *)transferSingleModel;

@end

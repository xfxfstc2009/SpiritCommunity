//
//  CenterRootCustomerSingleView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/6.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterCustomerSingleModel.h"
@interface CenterRootCustomerSingleView : UIView

@property (nonatomic,strong)CenterCustomerSingleModel *transferCustomerSingleMode;
@end

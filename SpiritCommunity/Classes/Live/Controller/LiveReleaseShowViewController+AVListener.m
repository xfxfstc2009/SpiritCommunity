//
//  LiveReleaseShowViewController+AVListener.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/29.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController+AVListener.h"
#import "UserViewManager.h"

@implementation LiveReleaseShowViewController (AVListener)

#pragma mark - 用户修改当前的播放模式
- (void)onUserUpdateInfo:(ILVLiveAVEvent)event users:(NSArray *)users {
    if (event == ILVLIVE_AVEVENT_CAMERA_ON){            // 打开摄像头
        [self onVideoType:QAVVIDEO_SRC_TYPE_CAMERA users:users];
    } else if (event == ILVLIVE_AVEVENT_CAMERA_OFF){    // 关闭摄像头
        [self offVideoType:QAVVIDEO_SRC_TYPE_CAMERA users:users];
    } else if (event == ILVLIVE_AVEVENT_SCREEN_ON){     // 打开屏幕分享
        [self onVideoType:QAVVIDEO_SRC_TYPE_SCREEN users:users];
    } else if (event == ILVLIVE_AVEVENT_SCREEN_OFF){    // 关闭屏幕分享
        [self offVideoType:QAVVIDEO_SRC_TYPE_SCREEN users:users];
    } else if (event == ILVLIVE_AVEVENT_MEDIA_ON){      // 播放视频文件
        [self onVideoType:QAVVIDEO_SRC_TYPE_MEDIA users:users];
    } else if (event == ILVLIVE_AVEVENT_MEDIA_OFF){     // 停止播放视频文件
        [self offVideoType:QAVVIDEO_SRC_TYPE_MEDIA users:users];
    }
}

// 创建房间以后，就执行的回调
- (void)onFirstFrameRecved:(int)width height:(int)height identifier:(NSString *)identifier srcType:(avVideoSrcType)srcType; {
    
    [TXLiveManager sharedAccountModel].transferBeautyValue = .5f;
    [TXLiveManager sharedAccountModel].transferWhiteValue = .5f;
    
    //选中当前方案,默认ILiveSDK美颜包              //首帧来时，设置美颜
    NSString *beautyScheme = [Tool userDefaultGetWithKey:kBeautyScheme];
    if (!(beautyScheme && beautyScheme.length > 0)) {
        [Tool userDefaulteWithKey:kBeautyScheme Obj:kILiveBeauty];
        beautyScheme = kILiveBeauty;
    }
    // 设置当前的美颜&美白的度数
    if ([beautyScheme isEqualToString:kILiveBeauty]) {      //TILFilterSDK美颜效果
        [self.tilFilter setBeautyLevel:[TXLiveManager sharedAccountModel].transferWhiteValue * 10];
        [self.tilFilter setWhitenessLevel:[TXLiveManager sharedAccountModel].transferBeautyValue * 10];
    }
    
    if ([beautyScheme isEqualToString:kQAVSDKBeauty]) {     // QAVSDK美颜效果【插件美颜】
        QAVContext *context = [[ILiveSDK getInstance] getAVContext];
        if (context && context.videoCtrl) {
            [context.videoCtrl inputBeautyParam:[TXLiveManager sharedAccountModel].transferBeautyValue * 10];
        }
    }
    QAVContext *context = [[ILiveSDK getInstance] getAVContext];
}


- (void)onVideoType:(avVideoSrcType)type users:(NSArray *)users {
    for (NSString *user in users) {
        ILiveRenderView *renderView = [[UserViewManager shareInstance] addRenderView:user srcType:type];
        if ([UserViewManager shareInstance].total != 0)//小画面添加点击事件。大画面不加。
        {
            renderView.userInteractionEnabled = YES;
            MyTapGesture *tap = [[MyTapGesture alloc] initWithTarget:self action:@selector(onSwitchToMain:)];
            tap.numberOfTapsRequired = 1;
            tap.codeId = [UserViewManager codeUser:user type:type];
            [renderView addGestureRecognizer:tap];
        }
        renderView.isRotate = NO;
    }
    
    NSArray *renderViews = [[TILLiveManager getInstance] getAllAVRenderViews];
    if (renderViews.count > 0)
    {
        self.noCameraDatatalabel.hidden = YES;
    }
    else
    {
        self.noCameraDatatalabel.hidden = NO;
        self.isCameraEvent = NO;
    }
    self.isCameraEvent = YES;
}

- (void)offVideoType:(avVideoSrcType)type users:(NSArray *)users
{
    for (NSString *user in users)
    {
        //如果移除的画面是大画面，则屏幕上只会显示一个或几个小画面，为了美化，将最后剩下小画面中的一个显示成大画面
        NSString *codeUserId = [UserViewManager codeUser:user type:type];
        if ([codeUserId isEqualToString:[UserViewManager shareInstance].mainCodeUserId] && [UserViewManager shareInstance].total > 0)
        {
            //将主播的画面切到大画面
            MyTapGesture *tap = [[MyTapGesture alloc] init];
            avVideoSrcType uidType = [[UserViewManager shareInstance] getUserType:@"beetv_1"];
            tap.codeId = [UserViewManager codeUser:@"beetv_1" type:uidType];
            [self onSwitchToMain:tap];
        }
        [[UserViewManager shareInstance] removeRenderView:user srcType:type];
    }
    [[UserViewManager shareInstance] refreshViews];
    
    NSArray *renderViews = [[TILLiveManager getInstance] getAllAVRenderViews];
    if (renderViews.count > 0)
    {
        self.isCameraEvent = YES;
        self.noCameraDatatalabel.hidden = YES;
    }
    else
    {
        self.isCameraEvent = NO;
        self.noCameraDatatalabel.hidden = NO;
    }
}

- (void)onSwitchToMain:(MyTapGesture *)gesture
{
    NSString *codeId = gesture.codeId;
    NSDictionary *userDic = [UserViewManager decodeUser:codeId];
    if (userDic)
    {
        gesture.codeId = [UserViewManager shareInstance].mainCodeUserId;
        [[UserViewManager shareInstance] switchToMainView:codeId];
        //大小界面切换之后，判断主窗口上是主播，连麦用户，还是普通观众，对底部的功能按钮做对应的变换
        [self relayoutBottom];
    }
}

- (void)relayoutBottom {
    
}




@end



@implementation MyTapGesture
@end

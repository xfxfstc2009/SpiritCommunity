//
//  OrderRootRowTwoTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "OrderRootRowTwoTableViewCell.h"

static char tapKey;
@interface OrderRootRowTwoTableViewCell()
@property(nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UIButton *button1;
@property (nonatomic,strong)UIButton *button2;

@end

@implementation OrderRootRowTwoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    //1.名字
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [self addSubview:self.priceLabel];
    
    // 2. 创建按钮
    self.button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button1.backgroundColor = [UIColor colorWithCustomerName:@"绿"];
    self.button1.layer.cornerRadius = LCFloat(4);
    self.button1.clipsToBounds = YES;
    [self.button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:self.button1];
    [self.button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.button1 buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(OrderListSingleModel *transferSingleModel,NSString *title) = objc_getAssociatedObject(strongSelf, &tapKey);
        if (block){
            block(strongSelf.transferSingleModel,self.button2.currentTitle);
        }
    }];
    self.button1.titleLabel.adjustsFontSizeToFitWidth = YES;

    
    self.button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button2.backgroundColor = [UIColor colorWithCustomerName:@"绿"];
    self.button2.layer.cornerRadius = LCFloat(4);
    self.button2.clipsToBounds = YES;
    [self.button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:self.button2];
    [self.button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.button2 buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(OrderListSingleModel *transferSingleModel,NSString *title) = objc_getAssociatedObject(strongSelf, &tapKey);
        if (block){
            block(strongSelf.transferSingleModel,self.button2.currentTitle);
        }
    }];
    self.button2.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    self.button1.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.button2.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.button1.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.button2.titleLabel.adjustsFontSizeToFitWidth = YES;
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferSingleModel:(OrderListSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // 1. 名字
    self.priceLabel.text = [NSString stringWithFormat:@"%@%@%.2f",@"实付款",@"￥",transferSingleModel.payMoney];
    CGSize priceSize = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.priceLabel.frame = CGRectMake(LCFloat(11), 0, priceSize.width, self.transferCellHeight);
    
    // 2.button
    self.button2.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(80), LCFloat(7), LCFloat(80), (self.transferCellHeight - 2 * LCFloat(7)));
    self.button1.frame = CGRectMake(self.button2.orgin_x-LCFloat(11) -LCFloat(80), LCFloat(7), LCFloat(80), self.button2.size_height);
    if (transferSingleModel.orderStatus == 0){           // 【代付款】
        [self.button2 setTitle:@"立即支付" forState:UIControlStateNormal];
        self.button1.hidden = YES;
        self.button2.hidden = NO;
    } else if (transferSingleModel.orderStatus == 1){        // 待发货
        self.button1.hidden = YES;
        self.button2.hidden = NO;
        
       [self.button2 setTitle:@"确认发货" forState:UIControlStateNormal];
    } else if(transferSingleModel.orderStatus == 2){          // 待收货
        self.button1.hidden = YES;
        self.button2.hidden = NO;

        [self.button1 setTitle:@"查看物流" forState:UIControlStateNormal];
        [self.button2 setTitle:@"确认收货" forState:UIControlStateNormal];
    } else if (transferSingleModel.orderStatus == 3){         // 待评价
        self.button1.hidden = YES;
        self.button2.hidden = NO;
        [self.button2 setTitle:@"立即评价" forState:UIControlStateNormal];
    } else if(transferSingleModel.orderStatus == 6){         // 退款退货
        self.button1.hidden = YES;
        self.button2.hidden = YES;
        [self.button2 setTitle:@"追踪退款退货" forState:UIControlStateNormal];
    } else if (transferSingleModel.orderStatus == 1){
        self.button1.hidden = YES;
        self.button2.hidden = YES;
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}

-(void)actionClickWithBlock:(void(^)(OrderListSingleModel *transferSingleModel,NSString *title))block{
    objc_setAssociatedObject(self, &tapKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end

//
//  HomeGoodsNormalSingleView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/15.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeNormalSingleGoodsModel.h"

@interface HomeGoodsNormalSingleView : UIView

@property (nonatomic,strong) HomeNormalSingleGoodsModel *transferHomeNormalGoodsModel;

-(void)actionClickBlock:(void(^)(HomeNormalSingleGoodsModel *transferHomeNormalGoodsModel))block;

+(CGSize)calculationSize;
@end

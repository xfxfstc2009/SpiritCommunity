//
//  PDSliderTableViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

@interface PDSliderTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;             /**< 传递上来的cell高度*/
@property (nonatomic,copy)NSString *transferTitle;                  /**< 传递的标题*/


+(CGFloat)calculationCellHeight;

@end

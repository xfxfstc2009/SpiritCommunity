//
//  MoneyFlowListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/8.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "MoneyFlowSIngleModel.h"

@interface MoneyFlowListModel : FetchModel

@property (nonatomic,strong)NSArray<MoneyFlowSIngleModel> *list;

@end

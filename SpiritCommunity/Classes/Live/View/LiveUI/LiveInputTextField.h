//
//  LiveInputTextField.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/1.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveUIActionView.h"

@interface LiveInputTextField : UIView

@property (nonatomic,strong)UITextField *inputTextField;            /***< 输入内容*/
-(instancetype)initWithFrame:(CGRect)frame actionChangeBlock:(void(^)(UITextField *inputTextField))block btnClick:(void(^)(UITextField *inputTextField))block;

-(void)actionClickBlockSendMsg:(NSString *)info;

-(void)cleanInfo;


@property (nonatomic,strong)LiveUIActionView *transferActionView;


@end

//
//  CenterUserImgsViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "CenterUserImgsViewController.h"
#import "CenterUserImgsCollectionViewCell.h"
#import "CenterUserImgSingleModel.h"
#import "CenterUserImgAddCollectionViewCell.h"
#import "CenterUserImgsAddViewController.h"
#import "ImgSelectorViewController.h"

static char collectionCellContentOfHeightBlockKey;
@interface CenterUserImgsViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)NSMutableArray *imgsMutableArr;
@property (nonatomic,strong)NSMutableArray *imgsMainArr;

@end

@implementation CenterUserImgsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createCollectionView];
    [self sendRequestToGetInfo];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.imgsMutableArr = [NSMutableArray array];
    [self.imgsMutableArr addObject:@"添加图片"];
    self.imgsMainArr = [NSMutableArray array];
}

#pragma mark - 用户图片
-(void)createCollectionView{
    if (!self.imgsCollectionView){
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        self.imgsCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.imgsCollectionView.dataSource = self;
        self.imgsCollectionView.delegate = self;
        self.imgsCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

        self.imgsCollectionView.showsVerticalScrollIndicator = YES;
        self.imgsCollectionView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.imgsCollectionView];
        [self.imgsCollectionView registerClass:[CenterUserImgsCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentify"];
        [self.imgsCollectionView registerClass:[CenterUserImgAddCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentify1"];
    }

    __weak typeof(self)weakSelf = self;
    [self.imgsCollectionView appendingPullToRefreshHandler:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(scrollViewDelegateManager:andDrop:)]){
            [strongSelf.delegate scrollViewDelegateManager:strongSelf.imgsCollectionView andDrop:YES];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [strongSelf.imgsCollectionView stopPullToRefresh];
        });
        [strongSelf sendRequestToGetInfo];
    }];

    [self.imgsCollectionView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imgsMutableArr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        CenterUserImgAddCollectionViewCell *cellWithRowOne = (CenterUserImgAddCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentify1" forIndexPath:indexPath];
        
        return cellWithRowOne;
    } else {
        CenterUserImgsCollectionViewCell *cell = (CenterUserImgsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentify"  forIndexPath:indexPath];
        cell.transferCellImg = [self.imgsMutableArr objectAtIndex:indexPath.row];
        return cell;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat assetMargin = LCFloat(10);
    return CGSizeMake((kScreenBounds.size.width - 4 * assetMargin) / 3., (kScreenBounds.size.width - 4 * assetMargin) / 3.);
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,LCFloat(7),0,LCFloat(7));
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,LCFloat(20)};
    return size;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size = {kScreenBounds.size.width,LCFloat(10)};
    return size;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        __weak typeof(self)weakSelf = self;

        [self authorizeWithCompletionHandler:^(BOOL successed) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf addImgManager];
        }];
    } else {
        ImgSelectorViewController *imgSelected = [[ImgSelectorViewController alloc]init];
        CenterUserImgsCollectionViewCell * cell = (CenterUserImgsCollectionViewCell *)[self.imgsCollectionView cellForItemAtIndexPath:indexPath];
        [imgSelected showInView:self.parentViewController imgArr:self.imgsMainArr currentIndex:indexPath.row - 1 cell:cell];
    }
}

-(void)addImgManager{
    CenterUserImgsAddViewController *addImgViewController = [[CenterUserImgsAddViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [addImgViewController actionClickBlock:^(NSArray *imgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;

        [strongSelf.imgsMutableArr addObjectsFromArray:imgArr];
        [strongSelf.imgsMainArr addObjectsFromArray:imgArr];
        [strongSelf.imgsCollectionView reloadData];
    }];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:addImgViewController];
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
}

#pragma mark - 获取图片信息
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page":@(self.imgsCollectionView.currentPage),@"size":@"20"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:assetList requestParams:params responseObjectClass:[CenterUserImgSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            CenterUserImgSingleModel *singleModel = (CenterUserImgSingleModel *)responseObject;
            if (strongSelf.imgsCollectionView.isXiaLa){
                [strongSelf.imgsMutableArr removeAllObjects];
                [strongSelf.imgsMainArr removeAllObjects];
                [strongSelf.imgsMutableArr addObject:@"添加图片"];
            }
            [strongSelf.imgsMainArr addObjectsFromArray:singleModel.imgs];
            [strongSelf.imgsMutableArr addObjectsFromArray:singleModel.imgs];
            [strongSelf.imgsCollectionView reloadData];
            [strongSelf.imgsCollectionView stopFinishScrollingRefresh];
            [strongSelf.imgsCollectionView stopPullToRefresh];

            void(^block)(CGFloat height) = objc_getAssociatedObject(strongSelf, &collectionCellContentOfHeightBlockKey);
            if (block){
                block(strongSelf.imgsCollectionView.contentSize.height);
            }
        }
    }];
}


#pragma mark - 点击图片方法



-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (self.delegate &&[self.delegate respondsToSelector:@selector(scrollViewDelegateManager:andDrop:)]){
//        [self.delegate scrollViewDelegateManager:scrollView andDrop:NO];
//    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (self.delegate &&[self.delegate respondsToSelector:@selector(lotteryscrollViewWillBeginDragging:)]){
        [self.delegate lotteryscrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate; {
    if (self.delegate &&[self.delegate respondsToSelector:@selector(lotteryscrollViewDidEndDragging:)]){
        [self.delegate lotteryscrollViewDidEndDragging:scrollView];
    }
}

-(void)collectionCellContentOfHeightBlock:(void(^)(CGFloat height))block{
    objc_setAssociatedObject(self, &collectionCellContentOfHeightBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)reloadImgsManagae{
    self.imgsCollectionView.currentPage = 1;
    [self sendRequestToGetInfo];
}
@end

//
//  NetworkAdapter.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

// 网络适配器
#import "NetworkAdapter.h"
#import "NetworkEngine.h"
#import "SBJSON.h"


@implementation NetworkAdapter

+(instancetype)sharedAdapter{
    static NetworkAdapter *_sharedAdapter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAdapter = [[NetworkAdapter alloc] init];
    });
    return _sharedAdapter;
}

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block{
    __weak typeof(self)weakSelf = self;

    NetworkEngine *networkEngine;

    if([path isEqualToString:txLogin]){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@",TX_Host]]];
    } else if ([path hasPrefix:@"ymgInterface"]){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",@"api.yimugo.com:8080"]]];
        __weak typeof(self)weakSelf = self;
        [self fetchWithShop:networkEngine path:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
            if (!weakSelf){
                return ;
            }
            if (isSucceeded){
                if ([responseObject isKindOfClass:[NSDictionary class]]){           // 字典类别
                    if ([path hasPrefix:@"api"]){
                        FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:responseObject];
                        block(YES,responseModelObject,nil);
                        return;
                    }

                    // 判断是否请求成功
                    NSNumber *errorNumber = [responseObject objectForKey:@"status"];
                    if (errorNumber.integerValue == 1){           // 请求成功
                        if (responseObjectClass == nil){
                            NSDictionary *dic = [responseObject objectForKey:@"data"];
                            block(YES,dic,nil);
                            return;
                        }
                        NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];
                        id info = [responseObject objectForKey:@"data"];
                        if ([info isKindOfClass:[NSDictionary class]]){
                            tempDic = info;
                        } else if ([info isKindOfClass:[NSArray class]]){
                            [tempDic setObject:info forKey:@"infoList"];
                        }

                        FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:tempDic];
                        block(YES,responseModelObject,nil);

                    } else {                                        // 业务错误
                        NSString *backInfo = @"";
                        NSString *errorInfo = [responseObject objectForKey:@"errorMsg"];
                        NSString *error = [responseObject objectForKey:@"tips"];
                        if (errorNumber.integerValue == 2 && [error isEqualToString:@"请先登录"]){         // 去登录

                            if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(pandaNotLoginNeedLoginManager)]){
                                [[NetworkAdapter sharedAdapter].delegate pandaNotLoginNeedLoginManager];
                            }
                            return;
                        }

                        if (!errorInfo.length){
                            if (error.length){
                                backInfo = error;
                            } else {
                                backInfo = @"系统错误，请联系管理员";
                            }
                        } else {
                            backInfo = errorInfo;
                        }
                        NSDictionary *dict = @{NSLocalizedDescriptionKey: backInfo};
                        NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorNumber.integerValue userInfo:dict];
                        block(NO,responseObject,bizError);
                        if ([backInfo hasPrefix:@"请不要重复收藏"]){
                            return;
                        }
                        if ([path isEqualToString:_orderList]){
                            return;
                        }
                        [[UIAlertView alertViewWithTitle:@"系统出错" message:backInfo buttonTitles:@[@"确定"] callBlock:NULL]show];

                        return;
                    }
                }
            }
        }];
        return;
    } else {
        if (JAVA_Port.length){
            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@",JAVA_Host,JAVA_Port]]];
        } else {
            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",JAVA_Host]]];
        }
    }
    
    [networkEngine fetchWithPath:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            [PDHUD dismissManager];
            if ([responseObject isKindOfClass:[NSDictionary class]]){           // 字典类别
                // 判断是否请求成功
                NSNumber *errorNumber = [responseObject objectForKey:@"errCode"];
                if (errorNumber.integerValue == 200){           // 请求成功
                    if (responseObjectClass == nil){
                        NSDictionary *dic = [responseObject objectForKey:@"data"];
                        block(YES,dic,nil);
                        return;
                    }
                    
                    FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
                    block(YES,responseModelObject,nil);
                    
                } else if (errorNumber.integerValue == 401){            // 重新登录
                   
                    if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(pandaNotLoginNeedLoginManager)]){
                        [[NetworkAdapter sharedAdapter].delegate pandaNotLoginNeedLoginManager];
                    }
                } else {                                        // 业务错误
                    [StatusBarManager statusBarHidenWithText:@"服务器好像出了点问题"];
                    id errInfo = [responseObject objectForKey:@"errMsg"];
                    if ([errInfo isKindOfClass:[NSString class]]){
                        NSString *errorInfo = [responseObject objectForKey:@"errMsg"];
                        if (!errorInfo.length){
                            errorInfo = @"系统错误，请联系管理员";
                        }
                        NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
                        NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorNumber.integerValue userInfo:dict];
                        [PDHUD showHUDError:errorInfo];
                        
                        block(NO,responseObject,bizError);
                        return;
                    } else {
                        return;
                    }
                }
            }
        }
    }];
}

-(void)fetchWithShop:(NetworkEngine *)networkEngine path:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block{
    [networkEngine fetchWithShopPath:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:block];
}


#pragma mark - 长链接
#pragma mark - WebSocket 
// 【1.webSocket 连接】
-(void)webSocketConnection:(WebSocketConnection *)webSocketConnection host:(NSString *)host port:(NSInteger)port{
    if (webSocketConnection){
        [self webSocketCloseConnectionWithSoceket:webSocketConnection];
    }
    webSocketConnection = [[WebSocketConnection alloc]init];
    webSocketConnection.delegate = self;
    [webSocketConnection webSocketConnectWithHost:host port:port];
}

// 【2.断开长链接【WebSocket】】
-(void)webSocketCloseConnectionWithSoceket:(WebSocketConnection *)webSocketConnection{
    if (webSocketConnection){
        webSocketConnection.delegate = nil;
        [webSocketConnection webSocketDisconnect];
        webSocketConnection = nil;
    }
}

 // 【3.填写信息WebSocket】
-(void)webSocketFetchModelWithPHPRequestParams:(NSDictionary *)requestParams socket:(WebSocketConnection *)webSocketConnection{
    NSString *jsonParams = [self dictionaryToJson:requestParams];
    NSString *appendingParams = [NSString stringWithFormat:@"\r\n"];
    NSString *dataStr = [NSString stringWithFormat:@"%@%@",jsonParams,appendingParams];
    
    [webSocketConnection webSocketWriteData:dataStr];
}

//【4.WebSocket 内容信息返回】
//-(void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message{
//#ifdef DEBUG
//    NSLog(@"RESPONSE 【WebSocket】 :%@", message );
//    if (isSocketLogAlert){
//        [[UIAlertView alertViewWithTitle:@"socket Log" message:message buttonTitles:@[@"确定"] callBlock:NULL]show];
//    }
//#endif
//    
//    if (_delegate && [_delegate respondsToSelector:@selector(webSocketDidBackData:)]){
//        [_delegate webSocketDidBackData:message];
//    }
//}




#pragma mark - 【SOCKET】
-(void)socketCloseConnectionWithSocket:(SocketConnection *)socketConnection{
    if (socketConnection){
        socketConnection.delegate = nil;
        [socketConnection disconnect];
        socketConnection = nil;
    }
}

#pragma mark 【写信息】
-(void)socketFetchModelWithJavaRequestParams:(NSDictionary *)requestParams socket:(SocketConnection *)socketConnection{
    NSString *jsonParams = [self dictionaryToJson:requestParams];
    
    NSData *data1 = [jsonParams dataUsingEncoding:NSUTF8StringEncoding];
    
    NSInteger jsonParamsLength = jsonParams.length;
    
    char str[4];
    str[0] = (char) ((jsonParamsLength >> 24) & 0xFF);
    str[1] = (char) ((jsonParamsLength >> 16)& 0xFF);
    str[2] = (char) ((jsonParamsLength >> 8)&0xFF);
    str[3] = (char) (jsonParamsLength & 0xFF);
    
    NSData *data2 = [NSData dataWithBytes:str length:4];
    NSMutableData *data3 = [NSMutableData dataWithData:data2];
    [data3 appendData:data1];
    
    [socketConnection writeData:data3 timeout:4 tag:socketConnection.port];
}



#pragma mark 写信息JAVA
-(void)didReceiveData:(NSData *)data tag:(long)tag{
#ifdef DEBUG
    NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"RESPONSE Socket :%@", result);
    if (isSocketLogAlert){
        [[UIAlertView alertViewWithTitle:@"socket Log" message:result buttonTitles:@[@"确定"] callBlock:NULL]show];
    }
#endif
    if (_delegate && [_delegate respondsToSelector:@selector(socketDidBackData:)]){
        [_delegate socketDidBackData:data];
    }
}

-(void)socketConnection:(SocketConnection *)socket host:(NSString *)host port:(NSInteger)port{
    
}

- (NSString*)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;
    
    SBJSON *jsonSDK = [[SBJSON alloc] init];
    NSString *jsonStr = [jsonSDK stringWithObject:dic error:&parseError];
    return jsonStr;
}


+ (NSString*)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;

    SBJSON *jsonSDK = [[SBJSON alloc] init];
    NSString *jsonStr = [jsonSDK stringWithObject:dic error:&parseError];
    return jsonStr;
}


@end


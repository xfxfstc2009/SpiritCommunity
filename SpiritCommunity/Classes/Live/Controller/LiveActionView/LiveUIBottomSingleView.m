//
//  LiveUIBottomSingleView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/5.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveUIBottomSingleView.h"

static char liveUiBottomactionBlcokKey;
@interface LiveUIBottomSingleView()
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)LiveUIBottomViewSingleModel *singleModel;
@property (nonatomic,strong)UILabel *moneyLabel;
@property (nonatomic,strong)PDImageView *backgroundView;

@end

@implementation LiveUIBottomSingleView

-(instancetype)initWithFrame:(CGRect)frame model:(LiveUIBottomViewSingleModel *)model actionBlcok:(void(^)(LiveUIBottomViewSingleModel *model))block{
    self = [super initWithFrame:frame];
    if (self){
        _singleModel = model;
        objc_setAssociatedObject(self, &liveUiBottomactionBlcokKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    if (!self.imgView){
        self.imgView = [[PDImageView alloc]init];
        self.imgView.backgroundColor = [UIColor clearColor];
        self.imgView.layer.cornerRadius = LCFloat(3);
        self.imgView.clipsToBounds = YES;
        [self addSubview:self.imgView];
    }
    
    // title
    self.titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    // moneyLabel
    self.moneyLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    self.moneyLabel.font = [UIFont systemFontOfCustomeSize:10.];
    self.moneyLabel.textAlignment = NSTextAlignmentCenter;
    self.moneyLabel.adjustsFontSizeToFitWidth = YES;
    self.moneyLabel.font = [self.moneyLabel.font boldFont];
    [self addSubview:self.moneyLabel];

    // backgroundView
    self.backgroundView = [[PDImageView alloc]init];
    self.backgroundView.image = [Tool stretchImageWithName:@""];


    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.frame = self.bounds;
    self.actionButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(LiveUIBottomViewSingleModel *model) = objc_getAssociatedObject(strongSelf, &liveUiBottomactionBlcokKey);
        if (block){
            block(strongSelf.singleModel);
        }
    }];
    [self addSubview:self.actionButton];
    
    
    // action
    CGFloat margin_width = LCFloat(11);
    CGFloat width = self.size_width - 2 * margin_width;


    if (_singleModel.giftSingleModel){
        width = self.size_width - 3 * margin_width;
        CGFloat margin_height = (self.size_height - width - [NSString contentofHeightWithFont:self.titleLabel.font] - [NSString contentofHeightWithFont:self.moneyLabel.font]) / 4.;

        self.imgView.frame = CGRectMake((self.size_width - width) / 2., margin_height, width, width);
        [self.imgView uploadImageWithURL:_singleModel.giftSingleModel.img placeholder:nil callback:NULL];

        self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + margin_height, self.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.moneyLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + margin_height, self.titleLabel.size_width, [NSString contentofHeightWithFont:self.moneyLabel.font]);
        self.moneyLabel.hidden = NO;
        self.moneyLabel.text = [NSString stringWithFormat:@"%li金币",_singleModel.giftSingleModel.price];
        self.titleLabel.text = [NSString stringWithFormat:@"%@",_singleModel.giftSingleModel.name];
    } else {
        CGFloat margin_height = (self.size_height - width - [NSString contentofHeightWithFont:self.titleLabel.font]) / 3.;

        self.imgView.frame = CGRectMake(margin_width, margin_height, width, width);
        self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.imgView.frame) + margin_height, self.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);

        self.imgView.image = [UIImage imageNamed:self.singleModel.buttonImg];
        self.titleLabel.text = self.singleModel.title;

        self.moneyLabel.hidden = YES;
    }
}

@end

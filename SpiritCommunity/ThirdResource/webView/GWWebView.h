//
//  GWWebView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@class GWWebView;
@protocol GWWebViewDelegate <NSObject>
@optional
- (void)webView:(GWWebView *)webview didFinishLoadingURL:(NSURL *)URL;
- (void)webView:(GWWebView *)webview didFailToLoadURL:(NSURL *)URL error:(NSError *)error;
- (void)webView:(GWWebView *)webview shouldStartLoadWithURL:(NSURL *)URL;
- (void)webViewDidStartLoad:(GWWebView *)webview;
@end

@interface GWWebView : UIView<WKNavigationDelegate, WKUIDelegate, UIWebViewDelegate>

#pragma mark - Public Properties

//zlcdelegate
@property (nonatomic, weak) id <GWWebViewDelegate> delegate;

// The main and only UIProgressView
@property (nonatomic, strong) UIProgressView *progressView;
// The web views
// Depending on the version of iOS, one of these will be set
@property (nonatomic, strong) WKWebView *wkWebView;
@property (nonatomic, strong) UIWebView *uiWebView;



#pragma mark - Initializers view
- (instancetype)initWithFrame:(CGRect)frame;


#pragma mark - Static Initializers
@property (nonatomic, strong) UIBarButtonItem *actionButton;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, strong) UIColor *barTintColor;
@property (nonatomic, assign) BOOL actionButtonHidden;
@property (nonatomic, assign) BOOL showsURLInNavigationBar;
@property (nonatomic, assign) BOOL showsPageTitleInNavigationBar;

@property (nonatomic, strong) NSArray *customActivityItems;

#pragma mark - Public Interface

- (void)loadRequest:(NSURLRequest *)request;

- (void)loadURL:(NSURL *)URL;

- (void)loadURLString:(NSString *)URLString;

- (void)loadHTMLString:(NSString *)HTMLString;

@end

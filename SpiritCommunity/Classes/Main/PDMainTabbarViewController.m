//
//  PDMainTabbarViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMainTabbarViewController.h"
#import "PDGradientNavBar.h"
#import "PDAppleShenheModel.h"
#import "ShopRootViewController.h"
#import "CartRootViewController.h"
#import "ShopRootViewController.h"
#import "FindShopViewController.h"

@interface PDMainTabbarViewController ()

@end

@implementation PDMainTabbarViewController

+(instancetype)sharedController{
    static PDMainTabbarViewController *_sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray *iconArr = @[@"icon_tabbar_fuli_nor",@"icon_tabbar_pubg_yule_nor",@"icon_tabbar_shejiao_nor",@"icon_tabbar_center_nor"];
        _sharedController = [[PDMainTabbarViewController alloc] initWithTabIconNames:iconArr];
        _sharedController.selectedColor = [UIColor colorWithCustomerName:@"黑"];
        _sharedController.buttonsBackgroundColor = BACKGROUND_VIEW_COLOR;
        _sharedController.separatorLineVisible = YES;
        _sharedController.separatorLineColor = [UIColor lightGrayColor];
        _sharedController.selectionIndicatorHeight = 1;

        // 1. 首页
        HomeRootViewController *homeRootViewController = [HomeRootViewController sharedAccountModel];
        PDNavigationController *homeRootNav = [[PDNavigationController alloc] initWithRootViewController:homeRootViewController];;
        homeRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        homeRootNav.navigationBar.layer.shadowOpacity = .2f;
        homeRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:homeRootNav atIndex:0];

        // 2. 发现
        FindShopViewController *findRootViewController = [[FindShopViewController alloc]init];
        PDNavigationController *findRootNav = [[PDNavigationController alloc] initWithRootViewController:findRootViewController];;
        findRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        findRootNav.navigationBar.layer.shadowOpacity = .2f;
        findRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:findRootNav atIndex:1];

        // 消息
        MessageRootViewController *messageRootmainViewController = [[MessageRootViewController alloc]init];
        PDNavigationController *messageRootNav = [[PDNavigationController alloc] initWithRootViewController:messageRootmainViewController];;
        messageRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        messageRootNav.navigationBar.layer.shadowOpacity = .2f;
        messageRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:messageRootNav atIndex:2];

        // 4. vc
        MainCenterMenuViewController *centerViewController = [[MainCenterMenuViewController alloc]init];
        PDNavigationController *centerRootNav = [[PDNavigationController alloc] initWithRootViewController:centerViewController];;
        centerRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        centerRootNav.navigationBar.layer.shadowOpacity = .2f;
        centerRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:centerRootNav atIndex:3];
        _sharedController.centerViewController = centerViewController;
        
    });
    return _sharedController;
}

-(CGFloat)tabBarHeight{
    if ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO){
        return  49 + 34;
    } else {
        return  49 ;
    }
}

#pragma mark - 审核标记
-(void)sendRequestToGetShenheWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"version":[Tool appVersion]};
    [[NetworkAdapter sharedAdapter] fetchWithPath:appShenhe requestParams:params responseObjectClass:[PDAppleShenheModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!
            weakSelf){
            return ;
        }
        if (isSucceeded){
            PDAppleShenheModel *shenheModel = (PDAppleShenheModel *)responseObject;
            [AccountModel sharedAccountModel].lifeIsShenhe = shenheModel.enable;
        #ifdef DEBUG            // 测试
            [AccountModel sharedAccountModel].lifeIsShenhe = YES;
        #else               // 线上
        #endif
        }
        if (block){
            block();
        }
    }];
}

-(void)shenheManager{
    ShopRootViewController *shopViewController = [[ShopRootViewController alloc]init];
    PDNavigationController *shopNav = [[PDNavigationController alloc] initWithNavigationBarClass:nil toolbarClass:nil];
    [shopNav setViewControllers:@[shopViewController]];
    [self setViewController:shopNav atIndex:0];
    shopNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    shopNav.navigationBar.layer.shadowOpacity = .2f;
    shopNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);

    CartRootViewController *cartViewController = [[CartRootViewController alloc]init];
    PDNavigationController *cartNav = [[PDNavigationController alloc] initWithNavigationBarClass:nil toolbarClass:nil];
    [cartNav setViewControllers:@[cartViewController]];
    [self setViewController:cartNav atIndex:1];
    shopNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    shopNav.navigationBar.layer.shadowOpacity = .2f;
    shopNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
}



@end

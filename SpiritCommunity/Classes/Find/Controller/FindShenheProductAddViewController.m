//
//  FindShenheProductAddViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/31.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FindShenheProductAddViewController.h"
#import "ZSImageSelecetdTableViewCell.h"
#import "PDInputViewCell.h"

static char actionClickBlockProductKey;
@interface FindShenheProductAddViewController ()<UITableViewDataSource,UITableViewDelegate,ZSImageSelectedDelegate>
{
    UIImage *avatar;
    UITextField *textFieldOne;
    UITextField *textFieldTwo;
    UITextField *textFieldThr;

}
@property (nonatomic,strong)UITableView *inputTableView;
@property (nonatomic,strong)NSArray *inputArr;
@property (nonatomic,strong)NSMutableArray *selectedMutableArr;
@property (nonatomic,strong)PDInputViewCell *cellWithRowFour;;
@end

@implementation FindShenheProductAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加商品";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.inputArr = @[@[@"商品名称",@"商品价格",@"新旧程度",@"商品详情"],@[@"添加图片"],@[@"提交"]];
    avatar = [UIImage imageNamed:@"icon_addImg"];
    self.selectedMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.inputTableView){
        self.inputTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.inputTableView.dataSource = self;
        self.inputTableView.delegate = self;
        [self.view addSubview:self.inputTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.inputArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.inputArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        if (indexPath.row == 3){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            PDInputViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if(!cellWithRowFour){
                cellWithRowFour = [[PDInputViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFour.transferCellHeight = cellHeight;

            cellWithRowFour.transferCellHeight = cellHeight;
            cellWithRowFour.transferPlaceHolder = @"请输入商品详情";
            cellWithRowFour.limitCount = 120;
            __weak typeof(self)weakSelf = self;
            [cellWithRowFour inputTextViewDidEndEditingBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.inputTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }];
            self.cellWithRowFour = cellWithRowFour;
            return cellWithRowFour;
        } else {
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferTitle = [[self.inputArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            if (indexPath.row == 0){
                cellWithRowOne.transferPlaceholeder = @"请输入商品名称";
                textFieldOne = cellWithRowOne.inputTextField;
            } else if (indexPath.row == 1){
                cellWithRowOne.transferPlaceholeder = @"请输入商品价格";
                textFieldTwo = cellWithRowOne.inputTextField;
            } else if (indexPath.row == 2){
                cellWithRowOne.transferPlaceholeder = @"请输入商品新旧程度";
                textFieldThr = cellWithRowOne.inputTextField;
            }
            return cellWithRowOne;
        }
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        ZSImageSelecetdTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[ZSImageSelecetdTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.isSelectedImageArr = self.selectedMutableArr;
        cellWithRowTwo.delegate = self;

        return cellWithRowTwo;
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"提交商品";
        [cellWithRowThr setButtonStatus:YES];
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __weak typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToInfo];
        }];
        return cellWithRowThr;

    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if (indexPath.row == 3){
            return LCFloat(200);
        } else {
            return [GWInputTextFieldTableViewCell calculationCellHeight];
        }
    } else if (indexPath.section == 1){
        return [ZSImageSelecetdTableViewCell calculationCellHeightWithImgArr:self.selectedMutableArr];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1){
        GWAssetsLibraryViewController *assetViewController = [[GWAssetsLibraryViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [assetViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->avatar = [selectedImgArr lastObject];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
            [strongSelf.inputTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
        [self.navigationController pushViewController:assetViewController animated:YES];
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.inputTableView) {
        if (indexPath.section < 1){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.inputArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
            }
            if ([[self.inputArr objectAtIndex:indexPath.section] count] == 1) {
                separatorType  = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

-(void)resManager{
    if ([textFieldOne isFirstResponder]){
        [textFieldOne resignFirstResponder];
    } else if ([textFieldThr isFirstResponder]){
        [textFieldThr resignFirstResponder];
    } else if ([textFieldTwo isFirstResponder]){
        [textFieldTwo resignFirstResponder];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self resManager];
}

-(void)imageSelectedButtonClick{
    GWAssetsLibraryViewController *assetViewController = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetViewController selectImageArrayFromImagePickerWithMaxSelected:9 - self.selectedMutableArr.count andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.selectedMutableArr addObjectsFromArray:selectedImgArr];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        [strongSelf.inputTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
    [self.navigationController pushViewController:assetViewController animated:YES];
}



#pragma mark 手势通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 键盘通知
- (void)keyboardWillShow:(NSNotification *)notification {

    NSDictionary *userInfo = [notification userInfo];

    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];

    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];

    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;

    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];

    _inputTableView.frame = newTextViewFrame;
    [UIView commitAnimations];

}

#pragma mark 键盘通知
- (void)keyboardWillHide:(NSNotification *)notification {

    NSDictionary* userInfo = [notification userInfo];

    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];

    _inputTableView.frame = self.view.bounds;

    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark - sendRequestToUpload

-(void)sendRequestToUploadWithBlock:(void(^)(NSArray *imgArr))block{
    __weak typeof(self)weakSelf = self;
    [[AliOSSManager sharedOssManager] uploadFileWithImgArray:self.selectedMutableArr successBlock:^(BOOL isSuccessed, NSArray *imgArr) {
        if (!weakSelf){
            return ;
        }
        if(isSuccessed){
            if (block){
                block(imgArr);
            }
        }
    }];
}

-(void)sendRequestToInfo{
    NSString *error = @"";
    if (!textFieldOne.text.length){
        error = @"请输入商品详情";
    } else if (!textFieldTwo.text.length){
        error = @"请输入商品价格";
    } else if (!textFieldThr.text.length){
        error = @"请输入商品新旧程度";
    } else if (!self.cellWithRowFour.inputView.text.length){
        error = @"请输入商品介绍";
    } else if (!self.selectedMutableArr.count){
        error = @"请上传商品图片";
    }
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }

    __weak typeof(self)weakSelf = self;
    [self sendRequestToUploadWithBlock:^(NSArray *imgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToUploadInfo:imgArr];
    }];
}

#pragma mark - sendRequestInfo
-(void)sendRequestToUploadInfo:(NSArray *)imgs{
    __weak typeof(self)weakSelf = self;

    NSString *imgsStr = @"";
    for (int i = 0 ; i < imgs.count;i++){
        NSString *singleImg = [imgs objectAtIndex:i];
        if (i == imgs.count - 1){
            imgsStr = [imgsStr stringByAppendingString:[NSString stringWithFormat:@"%@",singleImg]];
        } else {
            imgsStr = [imgsStr stringByAppendingString:[NSString stringWithFormat:@"%@,",singleImg]];
        }
    }


    NSDictionary *params = @{@"name":textFieldOne.text,@"price":textFieldTwo.text,@"oldnew":textFieldThr.text,@"des":self.cellWithRowFour.inputView.text,@"imgs":imgsStr};
    [[NetworkAdapter sharedAdapter] fetchWithPath:product_add requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"提交成功"];
            [[UIAlertView alertViewWithTitle:@"提交成功" message:@"商品已经发布成功" buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickBlockProductKey);
                if (block){
                    block();
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }]show];
        } else {
            [StatusBarManager statusBarHidenWithText:@"提交失败"];
        }
    }];
}

-(void)actionClickBlockProduct:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickBlockProductKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end

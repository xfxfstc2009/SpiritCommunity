//
//  PushManager.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/27.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CloudPushSDK/CloudPushSDK.h>
@interface PushManager : NSObject

+ (instancetype)shareInstance;                                  /**< 单利*/

-(void)registWithPUSHWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions callbackBlock:(void(^)(NSDictionary *params))block;
+ (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken ;           // 注册deviceToken
#pragma mark - 3.APNs注册失败回调
+ (void)applicationDidFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
#pragma mark 5.App处于启动状态时，通知打开回调
+ (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo ;

@end

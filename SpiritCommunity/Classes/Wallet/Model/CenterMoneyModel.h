//
//  CenterMoneyModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/5.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface CenterMoneyModel : FetchModel

@property (nonatomic,copy)NSString *s_id;           /**< 船家id*/
@property (nonatomic,assign)CGFloat total_sum;      /**< 可提现金额*/
@property (nonatomic,copy)NSString *alipay_no;      /**< 支付宝账号*/
@property (nonatomic,copy)NSString *alipay_real_name;   /**< 支付宝姓名*/
@property (nonatomic,assign)CGFloat lj_sum;         /**< 累计收入*/


@end

//
//  UserInfoViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/4.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "UserInfoViewController.h"
#import "KMScrollingHeaderView.h"
#import "UserinfoImgsViewController.h"
#import "UserInfoLocationViewController.h"
#import "ToLinkSingleModel.h"
#import "AliChatManager.h"
#import "CenterRoundView.h"
#import "UserMoneyTransferViewController.h"

static char actionClickLinkStatusBlockKey;
@interface UserInfoViewController ()<UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate>
@property (nonatomic,strong) KMScrollingHeaderView* scrollingHeaderView;            /**< 头部*/
@property (nonatomic, strong) UITableView *myShopTableView;                 /**< tableView*/
@property (nonatomic,strong)PDImageView *headerbgView;
@property (nonatomic,strong)NSArray *fixedArr;
@property (nonatomic,strong)PDImageView *avatar;                            /**< 头像*/
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)CenterRoundView *centetFlag;                    /**< 标签*/
@property (nonatomic,strong)UIButton *rightButton;                          /**< 右侧的按钮*/
@property (nonatomic,strong)UIButton *imButton;
@property (nonatomic,strong)UIButton *transferButton;                       /**< 转账*/


@end

@implementation UserInfoViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)setupNavbarButtons{
    self.scrollingHeaderView.navbarView = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self pageSetting];
    [self setupDetailsPageView];        // 1. 创建主题view
    [self setupNavbarButtons];          // 2. 创建返回按钮
    [self createIMButton];

}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.fixedArr = @[@[@"我在哪",@"用户相册"]];
}

- (void)setupDetailsPageView {
    self.scrollingHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds];
    self.scrollingHeaderView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.tableView.dataSource = self;
    self.scrollingHeaderView.tableView.delegate = self;
    self.scrollingHeaderView.delegate = self;
    
    self.scrollingHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.scrollingHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.scrollingHeaderView.tableView.showsVerticalScrollIndicator = YES;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    
    self.scrollingHeaderView.tableView.separatorColor = [UIColor whiteColor];
    self.scrollingHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:self.scrollingHeaderView];
    
    self.scrollingHeaderView.navbarViewFadingOffset = 200;
    
    [self.scrollingHeaderView reloadScrollingHeader];
}

#pragma mark - PageSetting
-(void)pageSetting{
    self.barMainTitle = @"个人中心";
    
    __weak typeof(self)weakSelf = self;
    self.rightButton = [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"home_ranking_guanzhu2_nor"] barHltImage:[UIImage imageNamed:@"home_ranking_guanzhu2_nor"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf autoLoginToLink];
    }];

    if(self.transferSearchSingleModel.link){
        [self.rightButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_hlt"] forState:UIControlStateNormal];
    } else {
        [self.rightButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_nor"] forState:UIControlStateNormal];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.fixedArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.fixedArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
    GWNormalTableViewCell *cellWithZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
    if (!cellWithZero){
        cellWithZero = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        cellWithZero.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithZero.backgroundColor = [UIColor clearColor];
    }
    cellWithZero.transferCellHeight = cellHeight;
    cellWithZero.transferTitle = [[self.fixedArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cellWithZero.transferHasArrow = YES;
    return cellWithZero;
  
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0 ){                   // 我在哪
        UserInfoLocationViewController *userLocationViewController = [[UserInfoLocationViewController alloc]init];
        userLocationViewController.transferSearchSingleModel = self.transferSearchSingleModel;
        [self.navigationController pushViewController:userLocationViewController animated:YES];
    } else if (indexPath.row == 1){             // 用户相册
        UserinfoImgsViewController *userinfoImgView = [[UserinfoImgsViewController alloc]init];
        userinfoImgView.transferSearchSingleModel = self.transferSearchSingleModel;
        [self.navigationController pushViewController:userinfoImgView animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return LCFloat(50);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section != 0){
        return LCFloat(15);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.scrollingHeaderView.tableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.fixedArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.fixedArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - KMScrollingHeaderViewDelegate

- (void)detailsPage:(KMScrollingHeaderView *)detailsPageView headerImageView:(PDImageView *)imageView{
    [self createHeaderView:imageView];
}


#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.fixedArr.count ; i++){
        NSArray *dataTempArr = [self.fixedArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.fixedArr.count ; i++){
        NSArray *dataTempArr = [self.fixedArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

#pragma mark - 创建头部信息
-(void)createHeaderView:(UIView *)bgView{
    self.avatar = [[PDImageView alloc]init];
    self.avatar.frame = CGRectMake((kScreenBounds.size.width - LCFloat(50)) / 2., 100 - 64, LCFloat(50), LCFloat(50));
    self.avatar.backgroundColor = [UIColor clearColor];
    self.avatar.clipsToBounds = YES;
    self.avatar.layer.cornerRadius = self.avatar.size_width / 2.;
    if (self.transferSearchSingleModel.avatar.length){
        [self.avatar uploadImageWithURL:self.transferSearchSingleModel.avatar placeholder:nil callback:NULL];
    } else {
        self.avatar.image = [UIImage imageNamed:@"center_header_nor_icon"];
    }
    
    [bgView addSubview:self.avatar];
    
    // 2. 创建名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatar.frame) + LCFloat(8), kScreenBounds.size.width, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]]);
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.text = self.transferSearchSingleModel.nick.length?self.transferSearchSingleModel.nick:[NSString stringWithFormat:@"精灵%@",self.transferSearchSingleModel.phone.length?self.transferSearchSingleModel.phone:self.transferSearchSingleModel.ID];
    [bgView addSubview:self.nameLabel];

    // 3. 创建标签
    self.centetFlag = [[CenterRoundView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(10), kScreenBounds.size.width, 30) withTitle:self.transferSearchSingleModel.union_number];
    self.centetFlag.frame = CGRectMake((kScreenBounds.size.width - self.centetFlag.size_width) / 2., CGRectGetMaxY(self.nameLabel.frame) + LCFloat(15), self.centetFlag.size_width, self.centetFlag.size_height);
    [self.centetFlag changeGender:self.transferSearchSingleModel.sex];
    [bgView addSubview:self.centetFlag];
}


-(void)autoLoginToLink{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            [strongSelf sendReqeustToLink];
        } else {
            [StatusBarManager statusBarHidenWithText:@"请先登录"];
        }
    }];
}

-(void)sendReqeustToLink{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"toUser":self.transferSearchSingleModel.ID,@"action":@"1"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_link requestParams:params responseObjectClass:[ToLinkSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            ToLinkSingleModel *singleModel = (ToLinkSingleModel *)responseObject;
            if (singleModel.haslink){           // 关注
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"已关注,%@",self.transferSearchSingleModel.nick]];
                [[PDMainTabbarViewController sharedController].centerViewController guanzhuManagerStatus:YES];
            } else {
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"已取消关注,%@",self.transferSearchSingleModel.nick]];
                [[PDMainTabbarViewController sharedController].centerViewController guanzhuManagerStatus:NO];
            }
            [Tool clickZanWithView:strongSelf.rightButton block:^{
                if (singleModel.haslink){           // 关注
                    [strongSelf.rightButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_hlt"] forState:UIControlStateNormal];
                } else {
                    [strongSelf.rightButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_nor"] forState:UIControlStateNormal];
                }
            }];

            strongSelf.transferSearchSingleModel.link = singleModel.haslink;
            void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickLinkStatusBlockKey);
            if (block){
                block();
            }
        }
    }];
}

-(void)createIMButton{
    UIView *bottomView = [[UIView alloc]init];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    if (IS_iPhoneX){
        bottomView.frame = CGRectMake(0, kScreenBounds.size.height - 84 - LCFloat(50) - LCFloat(32), kScreenBounds.size.width, LCFloat(50) + LCFloat(32));
    } else {
        bottomView.frame = CGRectMake(0, kScreenBounds.size.height - 64 - LCFloat(50) - LCFloat(32), kScreenBounds.size.width, LCFloat(50) + LCFloat(32));
    }
    
    bottomView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    bottomView.layer.shadowOpacity = .2f;
    bottomView.layer.shadowOffset = CGSizeMake(.2f, .2f);


    __weak typeof(self)weakSelf = self;
    self.imButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.imButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self.imButton setTitle:@"聊一聊" forState:UIControlStateNormal];
    [self.imButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.imButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf chatManager];
    }];
    self.imButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [bottomView addSubview:self.imButton];

    // 转账
    self.transferButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.transferButton.backgroundColor = [UIColor colorWithCustomerName:@"白"];
    [self.transferButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    [self.transferButton setTitle:@"金币转账" forState:UIControlStateNormal];
    [self.transferButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        UserMoneyTransferViewController *transferViewController = [[UserMoneyTransferViewController alloc]init];
        transferViewController.transferSearchSingleModel = self.transferSearchSingleModel;
        [strongSelf.navigationController pushViewController:transferViewController animated:YES];
    }];
    self.transferButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [bottomView addSubview:self.transferButton];

    if (IS_iPhoneX){
        self.imButton.frame = CGRectMake(0,0, kScreenBounds.size.width / 2., bottomView.size_height);
        self.transferButton.frame = CGRectMake(kScreenBounds.size.width / 2.,0, kScreenBounds.size.width / 2.,bottomView.size_height);
    } else {
        self.imButton.frame = CGRectMake(0,0, kScreenBounds.size.width / 2., bottomView.size_height);
        self.transferButton.frame = CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., bottomView.size_height);
    }
}


-(void)chatManager{
    YWPerson *person = [[YWPerson alloc]initWithPersonId:self.transferSearchSingleModel.im_id];
    __weak typeof(self)weakSelf = self;
    [[AliChatManager sharedInstance] openConversationWithPerson:person callBack:^(GWCurrentIMViewController *conversationController) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController pushViewController:conversationController animated:YES];
    }];
}

-(void)actionClickLinkStatusBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickLinkStatusBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end

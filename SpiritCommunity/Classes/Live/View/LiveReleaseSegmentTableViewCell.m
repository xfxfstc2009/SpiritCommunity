//
//  LiveReleaseSegmentTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/8.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseSegmentTableViewCell.h"


static char segmentActionClickKey;
@interface LiveReleaseSegmentTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;

@end

@implementation LiveReleaseSegmentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    self.titleLabel.text = @"直播类型";
    self.titleLabel.font = [self.titleLabel.font boldFont];
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(LCFloat(11), 0, titleSize.width, 44);

    [self addSubview:self.titleLabel];

    __weak typeof(self)weakSelf = self;
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"直播",@"一对一"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSInteger index) = objc_getAssociatedObject(strongSelf, &segmentActionClickKey);
        if (block){
            block(index);
        }
    }];
    self.segmentList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleButtonBorder;
    [self.segmentList setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.segmentList setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    self.segmentList.selectionIndicatorColor = [UIColor whiteColor];
    self.segmentList.selectionIndicatorHeight = 1;
    [self addSubview:self.segmentList];
}



-(void)setTransferCellHeight:(CGFloat)transferCellHeight{

    self.titleLabel.frame = CGRectMake(3 * LCFloat(11), 0, self.titleLabel.size_width, transferCellHeight);
    self.segmentList.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) * 3 - LCFloat(150), (transferCellHeight - 30) / 2., LCFloat(150), 30);
}


-(void)segmentActionClick:(void(^)(NSInteger status))block{
    objc_setAssociatedObject(self, &segmentActionClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}

@end

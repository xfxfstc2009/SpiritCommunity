//
//  BannerListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/18.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "BannerSingleModel.h"
@interface BannerListModel : FetchModel

@property (nonatomic,strong)NSArray<BannerSingleModel> *list;

@end

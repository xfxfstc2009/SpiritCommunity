//
//  HomeRootBannerTempCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "HomeRootBannerTempCell.h"
#import "CenterOrderRootSingleView.h"

static char actionClickWithTypeBlockKey;
@interface HomeRootBannerTempCell()
@property (nonatomic,strong)CenterOrderRootSingleView *view1;
@property (nonatomic,strong)CenterOrderRootSingleView *view2;
@property (nonatomic,strong)CenterOrderRootSingleView *view3;
@property (nonatomic,strong)CenterOrderRootSingleView *view4;

@end


@implementation HomeRootBannerTempCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    CGFloat width = kScreenBounds.size.width / 4.;
    // 1. 积分商城
    self.view1 = [[CenterOrderRootSingleView alloc]initWithFrame:CGRectMake(0, 0, width, LCFloat(70))];
    self.view1.image = [UIImage imageNamed:@"dl_ic_jfsc"];
    self.view1.tranferType = CenterOrderRootSingleViewTypeHome;
    self.view1.title = @"积分商城";
    [self addSubview:self.view1];
    __weak typeof(self)weakSelf = self;
    [self.view1 actionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BannerTempType orderType) = objc_getAssociatedObject(strongSelf, &actionClickWithTypeBlockKey);
        if (block){
            block(BannerTempTypeJifen);
        }
    }];
    
    // 1. 积分夺宝
    self.view2 = [[CenterOrderRootSingleView alloc]initWithFrame:CGRectMake(width, 0, width, LCFloat(70))];
    self.view2.image = [UIImage imageNamed:@"dl_ic_jfdb"];
    self.view2.tranferType = CenterOrderRootSingleViewTypeHome;
    self.view2.title = @"积分夺宝";
    [self addSubview:self.view2];
    [self.view2 actionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BannerTempType orderType) = objc_getAssociatedObject(strongSelf, &actionClickWithTypeBlockKey);
        if (block){
            block(BannerTempTypeDuobao);
        }
    }];
    // 1. 分类
    self.view3 = [[CenterOrderRootSingleView alloc]initWithFrame:CGRectMake(width * 2, 0, width, LCFloat(70))];
    self.view3.image = [UIImage imageNamed:@"dl_ic_fl"];
    self.view3.tranferType = CenterOrderRootSingleViewTypeHome;
    self.view3.title = @"分类";
    [self addSubview:self.view3];
    [self.view3 actionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BannerTempType orderType) = objc_getAssociatedObject(strongSelf, &actionClickWithTypeBlockKey);
        if (block){
            block(BannerTempTypeFenlei);
        }
    }];
    // 1. 签到
    self.view4 = [[CenterOrderRootSingleView alloc]initWithFrame:CGRectMake(width * 3, 0, width, LCFloat(70))];
    self.view4.image = [UIImage imageNamed:@"dl_ic_qd"];
    self.view4.tranferType = CenterOrderRootSingleViewTypeHome;
    self.view4.title = @"签到";
    [self addSubview:self.view4];
    [self.view4 actionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BannerTempType orderType) = objc_getAssociatedObject(strongSelf, &actionClickWithTypeBlockKey);
        if (block){
            block(BannerTempTypeQiandao);
        }
    }];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(70);
}




-(void)actionClickWithTypeBlock:(void(^)(BannerTempType Type))Block{
    objc_setAssociatedObject(self, &actionClickWithTypeBlockKey, Block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end

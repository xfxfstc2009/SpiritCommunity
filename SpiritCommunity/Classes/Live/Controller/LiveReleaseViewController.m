//
//  LiveReleaseViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "LiveReleaseInputTableViewCell.h"
#import "LiveReleaseSegmentTableViewCell.h"

static char liveReleaseBtnClickManagerKey;
@interface LiveReleaseViewController ()<UITableViewDataSource,UITableViewDelegate>{
    dispatch_queue_t videoSessionQueue;             //  线程
}
@property (nonatomic,strong)UITableView *liveReleaseTableView;
@property (nonatomic,strong)NSArray *liveReleaseArr;
@property (nonatomic,strong)AVCaptureSession *captureSession;                                    // 自定义摄像头
@property (nonatomic,strong)AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;                // 摄像取景Layer
@property (nonatomic,strong)PDImageView *alphaView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UISwitch *showLocationSwtch;                        /**< 显示地址的switch*/
@property (nonatomic,copy)NSString *liveTitle;;
@property (nonatomic,copy)NSString *liveImgUrl;
@property (nonatomic,strong)UIImage *liveImg;
@property (nonatomic,assign)LiveObOorLiveType liveType;
@end

@implementation LiveReleaseViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createBgView];
    [self pageSetting];
    [self createTableView];
}

-(void)pageSetting{
    self.barMainTitle = @"发布直播";
    
    self.alphaView = [[PDImageView alloc]init];
    self.alphaView.backgroundColor = [UIColor clearColor];
    self.alphaView.image = [Tool stretchImageWithName:@"live_release_bg"];
    self.alphaView.frame = CGRectMake(0, - 50, kScreenBounds.size.width, kScreenBounds.size.height+ 50);
    [self.view addSubview:self.alphaView];
    
    UIButton *leftActionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftActionButton setImage:[UIImage imageNamed:@"live_release_cancel_icon"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [leftActionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }];
    leftActionButton.frame = CGRectMake(LCFloat(11), 20 + (44 - LCFloat(30)) / 2., LCFloat(30), LCFloat(30));
    [self.view addSubview:leftActionButton];
    
    // 2. 创建标题
    self.titleLabel = [GWViewTool createLabelFont:@"标题" textColor:@"白"];
    self.titleLabel.frame = CGRectMake(0, 20, kScreenBounds.size.width, 44);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.text = @"发布直播";
    [self.view addSubview:self.titleLabel];
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self tapManager];
}

-(void)tapManager{
    LiveReleaseInputTableViewCell *cell = (LiveReleaseInputTableViewCell *)[self.liveReleaseTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"标题" sourceArr:self.liveReleaseArr] inSection:[self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.liveReleaseArr]]];
    if ([cell.inputTextField isFirstResponder]){
        [cell.inputTextField resignFirstResponder];
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.liveReleaseArr = @[@[@"输入标题头",@"标题",@"图片",@"显示地址",@"直播状态",@"清晰度"],@[@"发布"]];
    
    self.liveImgUrl = [AccountModel sharedAccountModel].loginModel.live_img;
    self.liveTitle = [AccountModel sharedAccountModel].loginModel.live_title;
    self.liveImg = [UIImage imageNamed:@"icon_live_create_add"];;
    self.liveType = LiveObOorLiveTypeLive;
}

#pragma mark - 创建view
-(void)createBgView{
    if (!self.captureSession){
        NSArray *camerasArr = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        AVCaptureDevice *backCaptureDevice;
        for (AVCaptureDevice *device in camerasArr){
            if (device.position == AVCaptureDevicePositionBack){
                backCaptureDevice = device;
                break;
            }
        }
        if (backCaptureDevice){
            NSError *error = nil;
            AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:backCaptureDevice error:&error];
            if (!videoInput || error){
                return;
            }
            
            self.captureSession = [[AVCaptureSession alloc]init];
            self.captureSession.sessionPreset = AVCaptureSessionPresetMedium;
            [self.captureSession addInput:videoInput];
            
            AVCaptureVideoDataOutput *avCaptureVideoDataOutput = [[AVCaptureVideoDataOutput alloc]init];
            NSDictionary *settings = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA],kCVPixelBufferPixelFormatTypeKey, nil];
            avCaptureVideoDataOutput.videoSettings = settings;
            self.captureVideoPreviewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
            self.captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            self.captureVideoPreviewLayer.frame = self.view.bounds;
            [self.view.layer addSublayer:self.captureVideoPreviewLayer];
        }
    }
    [self actionRunningCapture];

}

-(void)actionRunningCapture{
    if (!videoSessionQueue) {
        videoSessionQueue = dispatch_queue_create("smartmin", NULL);
    }
    __weak typeof(self) weakSelf = self;
    dispatch_async(videoSessionQueue, ^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.captureSession startRunning];
    });
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.liveReleaseTableView){
        self.liveReleaseTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.liveReleaseTableView.orgin_y = 64;
        self.liveReleaseTableView.size_height -= 64;
        self.liveReleaseTableView.dataSource = self;
        self.liveReleaseTableView.delegate = self;
        self.liveReleaseTableView.userInteractionEnabled = YES;
        [self.view addSubview:self.liveReleaseTableView];
    }
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.liveReleaseArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.liveReleaseArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入标题头" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"输入标题头" sourceArr:self.liveReleaseArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];

        }
        return cellWithRowOne;
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"图片" sourceArr:self.liveReleaseArr]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        GWAvatarTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[GWAvatarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowZero.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowZero.backgroundColor = [UIColor clearColor];
        }
        cellWithRowZero.transferCellHeight = cellHeight;
        cellWithRowZero.fixedLabel.text = @"直播图片";
        if (self.liveImgUrl.length){
            cellWithRowZero.transferAvatarUrl = self.liveImgUrl;
        } else {
            cellWithRowZero.transferAvatar = self.liveImg;
        }

        cellWithRowZero.fixedLabel.orgin_x = LCFloat(11) * 3;
        cellWithRowZero.fixedLabel.textColor = [UIColor whiteColor];
        cellWithRowZero.fixedLabel.size_width = 150;
        cellWithRowZero.fixedLabel.font = [cellWithRowZero.fixedLabel.font boldFont];
        cellWithRowZero.avatar.orgin_x = kScreenBounds.size.width - 3 * LCFloat(11) - cellWithRowZero.avatar.size_width;

        return cellWithRowZero;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.liveReleaseArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        LiveReleaseInputTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[LiveReleaseInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = [UIColor clearColor];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionInputWithText:^(NSString *str) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.liveTitle = str;
        }];
        if ([AccountModel sharedAccountModel].loginModel.live_title.length){
            cellWithRowTwo.transferInfo = [AccountModel sharedAccountModel].loginModel.live_title;
        }
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"显示地址" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"显示地址" sourceArr:self.liveReleaseArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWSwitchTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWSwitchTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowThr.backgroundColor = [UIColor clearColor];
            cellWithRowThr.isOn = YES;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferInfo = @"显示地址";
        self.showLocationSwtch = cellWithRowThr.normalSwitch;
        cellWithRowThr.fixedLabel.orgin_x = LCFloat(11) * 3;
        cellWithRowThr.fixedLabel.textColor = [UIColor whiteColor];
        cellWithRowThr.fixedLabel.size_width = 150;
        cellWithRowThr.fixedLabel.font = [cellWithRowThr.fixedLabel.font boldFont];
        cellWithRowThr.normalSwitch.orgin_x = kScreenBounds.size.width - 3 * LCFloat(11) - cellWithRowThr.normalSwitch.size_width;
        
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"清晰度" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"清晰度" sourceArr:self.liveReleaseArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        UITableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowFour.backgroundColor = [UIColor clearColor];
        }
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"发布" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"发布" sourceArr:self.liveReleaseArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWButtonTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferTitle = @"发布";
        [cellWithRowFour setButtonStatus:YES];
        __weak typeof(self)weakSelf = self;
        [cellWithRowFour buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf usingGameWithBlock:^(BOOL successed) {
                if (successed){
                    [strongSelf sendRequestToOpenHouse];;
                }
            }];
        }];
        
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"直播状态" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"直播状态" sourceArr:self.liveReleaseArr]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        LiveReleaseSegmentTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[LiveReleaseSegmentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowFiv.backgroundColor = [UIColor clearColor];
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        [cellWithRowFiv segmentActionClick:^(NSInteger status) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (status == 0){
                strongSelf.liveType = LiveObOorLiveTypeLive;
            } else if (status == 1){
                strongSelf.liveType = LiveObOorLiveTypeOnebyOne;
            }
        }];
        return cellWithRowFiv;
    }
    
    return nil;
}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入标题头" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"输入标题头" sourceArr:self.liveReleaseArr]){
        return LCFloat(70);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"图片" sourceArr:self.liveReleaseArr]){
        return [GWAvatarTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标题" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"标题" sourceArr:self.liveReleaseArr]){
        return [LiveReleaseInputTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"显示地址" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"显示地址" sourceArr:self.liveReleaseArr]){
        return [GWSwitchTableViewCell calculationCellHeight] ;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"直播状态" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"直播状态" sourceArr:self.liveReleaseArr]){
        return [LiveReleaseSegmentTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"清晰度" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"清晰度" sourceArr:self.liveReleaseArr]){
        return kScreenBounds.size.height - 64 - LCFloat(70) - [GWAvatarTableViewCell calculationCellHeight] -  [LiveReleaseInputTableViewCell calculationCellHeight] - [GWSwitchTableViewCell calculationCellHeight] - [LiveReleaseSegmentTableViewCell calculationCellHeight] - [GWButtonTableViewCell calculationCellHeight] - LCFloat(50);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"发布" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"发布" sourceArr:self.liveReleaseArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    }
    return LCFloat(44);
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.liveReleaseArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"图片" sourceArr:self.liveReleaseArr]){
        GWAssetsLibraryViewController *assetLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [assetLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf uploadImgManager:[selectedImgArr lastObject]];
        }];
        [self.navigationController pushViewController:assetLibraryViewController animated:YES];
    }
}


-(void)liveReleaseBtnClickManager:(void(^)(HomeLiveSingleModel *liveModel))block{
    objc_setAssociatedObject(self, &liveReleaseBtnClickManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 选择权限
-(void)usingGameWithBlock:(void(^)(BOOL successed))block{
    BOOL successed = YES;
    AVAuthorizationStatus videoStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(videoStatus == AVAuthorizationStatusRestricted || videoStatus == AVAuthorizationStatusDenied) {
        [UIAlertView alertViewWithTitle:@"相机权限" message:@"您没有相机使用权限,请到 设置->隐私->相机 中开启权限" buttonTitles:@[@"确定"] callBlock:NULL];
        successed = NO;
        return;
    }
    AVAuthorizationStatus audioStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio];
    if(audioStatus == AVAuthorizationStatusRestricted || audioStatus == AVAuthorizationStatusDenied) {
        [UIAlertView alertViewWithTitle:@"相机权限" message:@"您没有麦克风使用权限,请到 设置->隐私->麦克风 中开启权限" buttonTitles:@[@"确定"] callBlock:NULL];
        successed = NO;
        return;
    }
    if (block){
        block(successed);
    }
}


#pragma mark - 开通房间
-(void)sendRequestToOpenHouse{
    if(!self.liveTitle.length){
        [PDHUD showText:@"请输入标题。"];
        return;
    } else if (!self.liveImgUrl.length){
        [PDHUD showText:@"请选择直播图片。"];
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@(self.showLocationSwtch.on) forKey:@"show_location"];           // 是否显示地址
    [params setValue:self.liveTitle forKey:@"title"];
    [params setValue:@(self.liveType) forKey:@"show_type"];             // 1 直播 2一对一
    [params setValue:self.liveImgUrl forKey:@"img"];
    
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_create requestParams:params responseObjectClass:[HomeLiveSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSucceeded){
            HomeLiveSingleModel *liveSingleModel = (HomeLiveSingleModel *)responseObject;
            void(^block)(HomeLiveSingleModel *liveModel) = objc_getAssociatedObject(strongSelf, &liveReleaseBtnClickManagerKey);
            if (block){
                block(liveSingleModel);
            }
            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            [strongSelf sendRequestToUpdateInfo];
        }
    }];
}


#pragma mark - 上传图片
-(void)uploadImgManager:(UIImage *)img{
    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusBarHidenWithText:@"正在上传直播图片"];
    NSInteger section = [self cellIndexPathSectionWithcellData:@"图片" sourceArr:self.liveReleaseArr];
    NSInteger row = [self cellIndexPathRowWithcellData:@"图片" sourceArr:self.liveReleaseArr];

    [[AliOSSManager sharedOssManager] uploadFileWithObjctype:ImageUsingTypeLive name:nil objImg:img block:^(NSString *fileUrl) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.liveImgUrl = fileUrl;
        [strongSelf.liveReleaseTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationFade];
        [StatusBarManager statusBarHidenWithText:@"直播图片上传成功"];
    } progress:^(CGFloat progress) {

    }];
}

#pragma mark - UploadUserInfo
-(void)sendRequestToUpdateInfo{
    __weak typeof(self)weakSelf = self;
    NSString *err = @"";
    if (!self.liveImgUrl.length){
        err = @"请上传直播图片";
    } else if(!self.liveTitle.length){
        err = @"请输入直播标题";
    }
    if (err.length){
        [PDHUD showText:err];
        return;
    }

    NSDictionary *params = @{@"liveTitle":self.liveTitle,@"liveImg":self.liveImgUrl};
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_uploadLive requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [AccountModel sharedAccountModel].loginModel.live_title = strongSelf.liveTitle;
            [AccountModel sharedAccountModel].loginModel.live_img = strongSelf.liveImgUrl;
            [StatusBarManager statusBarHidenWithText:@"直播信息更新成功"];
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

@end

//
//  RankingSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "OtherSearchSingleModel.h"

@protocol RankingSingleModel <NSObject>

@end


@interface RankingSingleModel : FetchModel

@property (nonatomic,strong)OtherSearchSingleModel *userModel;
@property (nonatomic,assign)NSInteger count;

@property (nonatomic,assign)BOOL hasAnimation;      /**< 是否动画*/
@end

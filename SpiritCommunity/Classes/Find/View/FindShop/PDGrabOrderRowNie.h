//
//  PDGrabOrderRowNie.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface PDGrabOrderRowNie : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferBeizhuInfo;

+(CGFloat)calculationCellHeightWithInfo:(NSString *)info;

@end

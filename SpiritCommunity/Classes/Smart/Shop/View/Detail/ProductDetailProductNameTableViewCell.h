//
//  ProductDetailProductNameTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailRootModel.h"

@interface ProductDetailProductNameTableViewCell : UITableViewCell

@property (nonatomic,strong)ProductDetailRootModel *transferProductDetailModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight:(ProductDetailRootModel *)transferProductDetailModel;

@end

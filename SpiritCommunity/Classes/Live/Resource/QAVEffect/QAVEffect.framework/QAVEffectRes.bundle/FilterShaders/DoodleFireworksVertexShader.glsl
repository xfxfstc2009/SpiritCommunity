precision highp float;
attribute vec4 position;
attribute float inputBlendAlpha;
varying float blendAlpha;

void main(){
    vec4 framePos = position;
    gl_Position = framePos;
    blendAlpha = inputBlendAlpha;
}

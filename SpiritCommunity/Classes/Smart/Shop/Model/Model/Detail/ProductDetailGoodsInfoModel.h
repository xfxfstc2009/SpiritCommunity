//
//  ProductDetailGoodsInfoModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface ProductDetailGoodsInfoModel : FetchModel

@property (nonatomic,assign)NSInteger clickCount;
@property (nonatomic,assign)NSInteger giveFrequency;
@property (nonatomic,copy)NSString * goodDetailImg;
@property (nonatomic,copy)NSString * goodsImg;
@property (nonatomic,copy)NSString * goodsName;
@property (nonatomic,assign)NSInteger  goodsNumber;

@property (nonatomic,copy)NSString * goodsSummary;
@property (nonatomic,copy)NSString * goodsUnit;
@property (nonatomic,copy)NSString * ID;
@property (nonatomic,assign)CGFloat marketPrice;
@property (nonatomic,copy)NSString * maxGiveRatio;
@property (nonatomic,copy)NSString * saleNumber;
@property (nonatomic,copy)NSString * salePriceJF;
@property (nonatomic,assign)CGFloat salePriceRMB;
@property (nonatomic,copy)NSString * salePriceUB;
@property (nonatomic,copy)NSString * shippingFee;
@property (nonatomic,copy)NSString * shippingWay;
@property (nonatomic,copy)NSString * suppliersId;
@property (nonatomic,copy)NSString * supplyName;

@end

//
//  GWUserCurrentInfoGetManager.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/9.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWUserCurrentInfoGetManager.h"
#import "PDSliderRootViewController.h"

@implementation GWUserCurrentInfoGetManager

+ (GWUserCurrentInfoGetManager *)sharedHealthManager {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}


// 1. 获取当前地理信息
-(void)getCurrentLocationManager{
    __weak typeof(self)weakSelf = self;

    [[PDCurrentLocationManager sharedLocation]getCurrentLocationManager:^(CGFloat lat, CGFloat lng, AMapAddressComponent *addressComponent) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.locationLat = lat;
        strongSelf.locationLng = lng;
        [PDCurrentLocationManager sharedLocation].addressComponent = addressComponent;
        strongSelf.addressComponent = addressComponent;
        
        [AccountModel sharedAccountModel].userLocation = addressComponent;
        [AccountModel sharedAccountModel].lat = lat;
        [AccountModel sharedAccountModel].lng = lng;
        
        // 2. 获取当前地理信息
        [strongSelf getCurrentWeatherManagerWithInterface];
    }];
}

// 【2. 获取当前天气信息】
-(void)getCurrentWeatherManagerWithInterface{
    NSString *proviceParams;
    NSString *cityParams;
    if ([[PDCurrentLocationManager sharedLocation].addressComponent.province hasSuffix:@"省"]){  // 省结尾
        proviceParams = [self jiequManagerWithString:[PDCurrentLocationManager sharedLocation].addressComponent.province flag:@"省"];
    } else {
        proviceParams = [PDCurrentLocationManager sharedLocation].addressComponent.province;
    }
    if ([[PDCurrentLocationManager sharedLocation].addressComponent.city hasSuffix:@"市"]){
        cityParams = [self jiequManagerWithString:[PDCurrentLocationManager sharedLocation].addressComponent.city flag:@"市"];
    } else {
        cityParams = [PDCurrentLocationManager sharedLocation].addressComponent.city ;
    }
    // 请求数据
    GWWeatherSingleModel *weatherFetchModel = [[GWWeatherSingleModel alloc]init];
    __weak typeof(self)weakSelf = self;
    weatherFetchModel.requestParams = @{@"city":cityParams,@"province":proviceParams};
    [weatherFetchModel fetchWithGetShareAPIPath:weather_get completionHandler:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWWeatherCurrentModel *weatherCurrentModel =(GWWeatherCurrentModel *)[weatherFetchModel.data lastObject];
            [strongSelf saveWeatherDataWithWeatherModel:weatherCurrentModel];
            // 1. 刷新侧边栏
//            PDSliderRootViewController *sliderViewController = (PDSliderRootViewController *)[RESideMenu shareInstance].leftMenuViewController;
//            [sliderViewController reloadWeatherManager];
        }
 
    }];
}

-(void)saveWeatherDataWithWeatherModel:(GWWeatherCurrentModel *)weatherCurrentModel{
    self.weatherCurrentModel = weatherCurrentModel;
}

-(NSString *)jiequManagerWithString:(NSString *)string flag:(NSString *)flag{
    NSRange range = [string rangeOfString:flag];
    if(range.location==NSNotFound){
        return @"";
    }else{
        return [string substringToIndex:range.location];
    }
}



#pragma mark - 获取当前健康信息
-(void)getHealthManager{
//    GWHealthManager *healthManager = [[GWHealthManager alloc]init];
//    [healthManager gethealthMainManagerWithBlock:^(GWHealthModel *healthModel) {
//        self.currentHealthModel = healthModel;
//    }];
}

-(void)getUserCurrentInfoManager{
    [self getCurrentLocationManager];
//    [self getHealthManager];
}


@end

//
//  CenterUserImgsCollectionViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/18.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "CenterUserImgsCollectionViewCell.h"

static char actionTapGestureBlockKey;
static char actionLongGestureBlockKey;
@interface CenterUserImgsCollectionViewCell()

@end

@implementation CenterUserImgsCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.frame = self.bounds;
    [self addSubview:self.imgView];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self addGestureRecognizer:tapGesture];

    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longManager:)];
    [self addGestureRecognizer:longGesture];
}

-(void)tapManager{
    void(^block)() = objc_getAssociatedObject(self, &actionTapGestureBlockKey);
    if (block){
        block();
    }
}

- (void)longManager:(UIGestureRecognizer *)gestrue
{
    //直接return掉，不在开始的状态里面添加任何操作，则长按手势就会被少调用一次了
    if (gestrue.state != UIGestureRecognizerStateBegan){
        return;
    }
    void(^block)() = objc_getAssociatedObject(self, &actionLongGestureBlockKey);
    if (block){
        block();
    }
}

-(void)setTransferCellImg:(NSString *)transferCellImg{
    _transferCellImg = transferCellImg;
    [self.imgView uploadImageWithURL:transferCellImg placeholder:nil callback:NULL];
}

-(void)actionTapGestureBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionTapGestureBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
-(void)actionLongGestureBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionLongGestureBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end

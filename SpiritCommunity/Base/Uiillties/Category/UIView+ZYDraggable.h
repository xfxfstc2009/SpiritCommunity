//
//  UIView+ZYDraggable.h
//  DraggableView
//
//  Created by 张志延 on 16/8/25. (https://github.com/zzyspace)
//  Copyright © 2016年 tongbu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Draggable)

- (void)makeDraggable;
- (void)makeDraggableInView:(UIView *)view damping:(CGFloat)damping;

- (void)removeDraggable;

- (void)updateSnapPoint;

- (void)panBlock:(void(^)(UIPanGestureRecognizer *pan))block;

@end

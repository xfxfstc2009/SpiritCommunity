//
//  ProductDetailBottomView.m
//  17Live
//
//  Created by 裴烨烽 on 2018/2/1.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "ProductDetailBottomView.h"

static char addCartKey;
static char buyKey;
static char cartKey;
static char collectionkey;
static char customerKey;

@interface ProductDetailBottomView()
@property (nonatomic,strong)UIButton *addCartButton;            /**< 加入购物车*/
@property (nonatomic,strong)UIButton *buyButton;                /**< 立即购买*/
@property (nonatomic,strong)UIButton *cartButton;               /**< 跳转到购物车*/
@property (nonatomic,strong)UIButton *collectionButton;         /**< 收藏*/
@property (nonatomic,strong)UIButton *customerButton;           /**< 客服按钮*/

@end

@implementation ProductDetailBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - CreateView
-(void)createView{
    // 1.创建加入购物车
    self.addCartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.addCartButton.backgroundColor = [UIColor colorWithCustomerName:@"橙"];
    [self.addCartButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addCartButton setTitle:@"加入购物车" forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.addCartButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &addCartKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.addCartButton];
    self.addCartButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(120), 0, LCFloat(120), self.size_height);

    // 2. 创建立即购买
    self.buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.buyButton.backgroundColor = [UIColor colorWithCustomerName:@"绿"];
    [self.buyButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    [self.buyButton setTitle:@"立即购买" forState:UIControlStateNormal];
    [self.buyButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &buyKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.buyButton];
    self.buyButton.frame = CGRectMake(self.addCartButton.orgin_x - LCFloat(120), 0, LCFloat(120), self.size_height);

    CGFloat width = (kScreenBounds.size.width - 2 * LCFloat(120)) / 3.;
    // 3. 客服
    self.customerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.customerButton.backgroundColor = [UIColor clearColor];
    [self.customerButton setImage:[UIImage imageNamed:@"jjr_ic_lxkh"] forState:UIControlStateNormal];
    [self.customerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &customerKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.customerButton];
    self.customerButton.frame = CGRectMake(0, 0, width, self.size_height);

    // 4.收藏
    self.collectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.collectionButton.backgroundColor = [UIColor clearColor];
    [self.collectionButton setImage:[UIImage imageNamed:@"mmhtab_icon_heart_nor"] forState:UIControlStateNormal];
    [self.collectionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &collectionkey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.collectionButton];
    self.collectionButton.frame = CGRectMake(CGRectGetMaxX(self.customerButton.frame), 0, width, self.size_height);

    // 4.购物车
    self.cartButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cartButton.backgroundColor = [UIColor clearColor];
    [self.cartButton setImage:[UIImage imageNamed:@"icon_gouwuche"] forState:UIControlStateNormal];
    [self.cartButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &cartKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.cartButton];
    self.cartButton.frame = CGRectMake(CGRectGetMaxX(self.collectionButton.frame), 0, width, self.size_height);

}


-(void)actionClickWithAddCard:(void(^)())block buyNowBlock:(void(^)())block1 cart:(void(^)())block2 collection:(void(^)())block3 customer:(void(^)())block4{
    objc_setAssociatedObject(self, &addCartKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &buyKey, block1, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &cartKey, block2, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &collectionkey, block3, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &customerKey, block4, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setProductDetailRootModel:(ProductDetailRootModel *)productDetailRootModel{
    CGFloat width = (kScreenBounds.size.width - 2 * LCFloat(120)) / 3.;

        self.addCartButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(120), 0, LCFloat(120), self.size_height);
        self.buyButton.frame = CGRectMake(self.addCartButton.orgin_x - LCFloat(120), 0, LCFloat(120), self.size_height);
        self.collectionButton.frame = CGRectMake(0, 0, width, self.size_height);
        self.cartButton.frame = CGRectMake(CGRectGetMaxX(self.collectionButton.frame), 0, width, self.size_height);

        self.addCartButton.hidden = NO;
        self.buyButton.hidden = NO;
        self.collectionButton.hidden = NO;
        self.cartButton.hidden = NO;
        self.buyButton.backgroundColor = [UIColor whiteColor];

}

-(void)collection{
    __weak typeof(self)weakSelf = self;
    [Tool clickZanWithView:self.collectionButton block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.collectionButton setImage:[UIImage imageNamed:@"mmhtab_icon_heart_hlt"] forState:UIControlStateNormal];
    }];
}

-(void)unCollection{
    __weak typeof(self)weakSelf = self;
    [Tool clickZanWithView:self.collectionButton block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.collectionButton setImage:[UIImage imageNamed:@"mmhtab_icon_heart_nor"] forState:UIControlStateNormal];
    }];
}


@end

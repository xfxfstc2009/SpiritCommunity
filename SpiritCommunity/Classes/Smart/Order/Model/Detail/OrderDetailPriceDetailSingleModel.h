//
//  OrderDetailPriceDetailSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol OrderDetailPriceDetailSingleModel <NSObject>


@end

@interface OrderDetailPriceDetailSingleModel : FetchModel

@property (nonatomic,copy)NSString *desc;
@property (nonatomic,assign)CGFloat discount_amount;
@property (nonatomic,assign)BOOL ischeck;
@property (nonatomic,copy)NSString *price;
@property (nonatomic,copy)NSString *code;

// temp
@property (nonatomic,assign)BOOL checked;
@property (nonatomic,assign)BOOL isShow;

@end

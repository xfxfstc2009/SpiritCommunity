//
//  HomeLiveSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "OtherSearchSingleModel.h"

@protocol HomeLiveSingleModel <NSObject>

@end

@interface HomeLiveSingleModel : FetchModel

@property (nonatomic,copy,nonnull)NSString *ID;                     /**< 编号*/
@property (nonatomic,assign)NSInteger count;                /**< 人数*/
@property (nonatomic,copy,nonnull)NSString *house_id;               /**< 房间编号*/
@property (nonatomic,copy,nonnull)NSString *group_id;               /**< IM编号*/
@property (nonatomic,copy,nonnull)NSString *img;                    /**< 图片编号*/
@property (nonatomic,copy,nonnull)NSString *in_house;               /**< 主播是否属于播放状态*/
@property (nonatomic,assign)BOOL show_location;             /**< 是否显示地址*/
@property (nonatomic,assign)NSTimeInterval time;
@property (nonatomic,copy,nonnull)NSString *title;
@property (nonatomic,assign)LiveObOorLiveType show_status;  /**< 展示类型*/
@property (nonnull,copy)NSString *role;

@property (nonatomic,strong,nonnull)OtherSearchSingleModel *userInfo;

@end

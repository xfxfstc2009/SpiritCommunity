//
//  HomeHouseNormalSingleCell.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/12.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeNormalSingleGoodsModel.h"

@interface HomeHouseNormalSingleCell : UITableViewCell

@property (nonatomic,strong)HomeNormalSingleGoodsModel *transferModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

@end


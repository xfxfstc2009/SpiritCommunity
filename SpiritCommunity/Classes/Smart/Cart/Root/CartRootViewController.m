//
//  CartRootViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CartRootViewController.h"
#import "CartBottomView.h"
#import "CartSingleTableViewCell.h"
#import "CartListModel.h"
#import "ShopDetailViewController.h"
#import "OrderPayViewController.h"

@interface CartRootViewController ()<UITableViewDataSource,UITableViewDelegate>{
    BOOL isSelectAll;
}
@property (nonatomic,strong)UITableView *cartTableView;
@property (nonatomic,strong)NSMutableArray *cartMutableArr;
@property (nonatomic,strong)CartBottomView *bottomView;
@property (nonatomic,strong)NSMutableArray *selectedArr;

@end

@implementation CartRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createBottomView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self interfaceManager];
}

-(void)interfaceManager{
    __weak typeof(self)weakSlef = self;
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        [weakSlef sendRequestToGetCartList];
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"购物车";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.cartMutableArr = [NSMutableArray array];
    // 1. 是否全选
    isSelectAll = YES;
    self.selectedArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if(!self.cartTableView){
        self.cartTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.cartTableView.size_height = self.view.size_height - self.bottomView.size_height - 64;
        self.cartTableView.delegate = self;
        self.cartTableView.dataSource = self;
        [self.view addSubview:self.cartTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.cartMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString*cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CartSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if(!cellWithRowOne){
        cellWithRowOne = [[CartSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    CartSingleModel *cartSingleModel = [self.cartMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferCartSingleModel = cartSingleModel;
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne actionClickWithDelete:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToDeleteWithId:cartSingleModel];
    }];
    
    [cellWithRowOne actionClickWithCheck:^(BOOL checked) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (![strongSelf.selectedArr containsObject:cartSingleModel]){
            [strongSelf.selectedArr addObject:cartSingleModel];
            [cellWithRowOne selectedButtonStatus:YES];
        } else {
            [strongSelf.selectedArr removeObject:cartSingleModel];
            [cellWithRowOne selectedButtonStatus:NO];
        }
        [strongSelf calculationWithPrice];
    }];
    
    [cellWithRowOne changeNumberOfCount:^(NSInteger count) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf calculationWithPrice];
    }];
    cellWithRowOne.transferType = CartSingleTableViewCellTypeCart;
    cellWithRowOne.transferOrderPageType = productTypeNormal;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CartSingleModel *cartSingleModel = [self.cartMutableArr objectAtIndex:indexPath.row];
    ShopDetailViewController *productDetailViewController = [[ShopDetailViewController alloc]init];
    productDetailViewController.transferProductId = cartSingleModel.goodsId;
    [self.navigationController pushViewController:productDetailViewController animated:YES];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CartSingleTableViewCell calculationCellHeight];
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.cartTableView) {
        NSArray *sectionOfArr = self.cartMutableArr;
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == sectionOfArr.count - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if (sectionOfArr.count == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - createBottomView
-(void)createBottomView{
    self.bottomView = [[CartBottomView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height - 64 - LCFloat(50) - LCFloat(50), kScreenBounds.size.width, LCFloat(50))];
    if (IS_iPhoneX){
        self.bottomView.orgin_y = kScreenBounds.size.height - LCFloat(50) - 88 - self.bottomView.size_height - LCFloat(34);
    }

    __weak typeof(self)weakSelf = self;
    [self.bottomView actionClickWithChooseAll:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool clickZanWithView:strongSelf.bottomView.allButton block:^{
            if (strongSelf->isSelectAll){
                [strongSelf.bottomView.allButton setImage:[UIImage imageNamed:@"dizhi_weixuanzhong"] forState:UIControlStateNormal];
                for (CartSingleTableViewCell *cell in strongSelf.cartTableView.visibleCells){
                    [cell selectedButtonStatus:NO];
                }
                [strongSelf.selectedArr removeAllObjects];
            } else {
                [strongSelf.bottomView.allButton setImage:[UIImage imageNamed:@"dizhi_xuanzhong"] forState:UIControlStateNormal];
                for (CartSingleTableViewCell *cell in strongSelf.cartTableView.visibleCells){
                    [cell selectedButtonStatus:YES];
                }
                [strongSelf.selectedArr addObjectsFromArray:strongSelf.cartMutableArr];
            }
            strongSelf->isSelectAll = !strongSelf->isSelectAll;
            [strongSelf calculationWithPrice];
        }];
    }];
    [self.bottomView actionClickWithBuyNow:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        OrderPayViewController *orderConfirmViewController = [[OrderPayViewController alloc]init];
        orderConfirmViewController.transferCartArr = strongSelf.selectedArr;
        [strongSelf.navigationController pushViewController:orderConfirmViewController animated:YES];
    }];
    [self.view addSubview:self.bottomView];
}

-(void)selectAllManager{
    
}


#pragma mark - 获取购物车列表
-(void)sendRequestToGetCartList{
    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusBarHidenWithText:@"正在获取购物车信息"];
    [[NetworkAdapter sharedAdapter] fetchWithPath:_cartList requestParams:nil responseObjectClass:[CartListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"获取购物车信息成功"];
            CartListModel *cartListModel = (CartListModel *)responseObject;

            if (strongSelf.cartMutableArr.count){
                [strongSelf.cartMutableArr removeAllObjects];
            }
            if (strongSelf.selectedArr.count){
                [strongSelf.selectedArr removeAllObjects];
            }
            CartListSingleModel *cartListSingleModel = [cartListModel.infoList lastObject];

            [strongSelf.cartMutableArr addObjectsFromArray:cartListSingleModel.cartItems];
            [strongSelf.selectedArr addObjectsFromArray:cartListSingleModel.cartItems];
            [strongSelf.cartTableView reloadData];
            // 计算价格
            [strongSelf calculationWithPrice];
            
            if (strongSelf.cartMutableArr.count){
                [strongSelf.cartTableView dismissPrompt];
            } else {
                [strongSelf.cartTableView showPrompt:@"当前购物车没有商品" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
        }
    }];
}

#pragma mark - 删除
-(void)sendRequestToDeleteWithId:(CartSingleModel*)cartSingleModel{
    __weak typeof(self)weakSelf = self;
    NSDictionary*params = @{@"goodId":cartSingleModel.goodsId,@"goodSkuId":@"0"};

    [StatusBarManager statusBarHidenWithText:@"正在删除商品"];
    [[NetworkAdapter sharedAdapter]fetchWithPath:_cartDelete requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"商品删除成功"];
            NSInteger index = [strongSelf.cartMutableArr indexOfObject:cartSingleModel];
            [strongSelf.cartMutableArr removeObject:cartSingleModel];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [strongSelf.cartTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        }
    }];
}

#pragma mark - 计算价格
-(CGFloat)calculationWithPrice{
    CGFloat price = 0;
    for (int i = 0 ; i < self.selectedArr.count;i++){
        CartSingleModel *cartSingleModel = [self.selectedArr objectAtIndex:i];
        price += cartSingleModel.goodsMoney * cartSingleModel.goodsNum;
    }
    [self.bottomView setPrice:price];
    return price;
}

@end

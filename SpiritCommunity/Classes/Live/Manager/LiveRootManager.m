//
//  LiveRootManager.m
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/21.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "LiveRootManager.h"
#import "LiveReleaseViewController.h"                   // 发布直播
#import "LiveReleaseShowViewController.h"               // 直播页面





@implementation LiveRootManager

+(void)registerLiveSDK{
    [[ILiveSDK getInstance]initSdk:[QQLiveID intValue] accountType:[QQAccountType intValue]];
}

+(void)registerLiveIMSDK{
    TIMManager *manager = [[ILiveSDK getInstance] getTIMManager];
    
    NSNumber *evn = [[NSUserDefaults standardUserDefaults] objectForKey:kEnvParam];
    [manager setEnv:[evn intValue]];
    
    NSNumber *logLevel = [[NSUserDefaults standardUserDefaults] objectForKey:kLogLevel];
    if (!logLevel)//默认debug等级
    {
        [[NSUserDefaults standardUserDefaults] setObject:@(TIM_LOG_DEBUG) forKey:kLogLevel];
        logLevel = @(TIM_LOG_DEBUG);
    }
//        [self disableLogPrint];//禁用日志控制台打印
    [manager setLogLevel:(TIMLogLevel)[logLevel integerValue]];
}



+(void)liveLoginManager:(NSString *)userId pwd:(NSString *)sig{                // 直播登录
    __weak typeof(self)weakSelf = self;
    [[ILiveLoginManager getInstance] iLiveLogin:userId sig:sig succ:^{
        if (!weakSelf){
            return ;
        }
        [StatusBarManager statusBarHidenWithText:@"腾讯云 登录成功"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"腾讯云 登录失败%@",errMsg]];
    }];
}


+(void)liveToReleaseManager:(UIViewController *)controller{
    LiveReleaseViewController *liveReleaseViewController = [[LiveReleaseViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [liveReleaseViewController liveReleaseBtnClickManager:^(HomeLiveSingleModel *liveModel) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        LiveReleaseShowViewController *liveReleaseShowViewController = [[LiveReleaseShowViewController alloc]init];
        liveReleaseShowViewController.transferLiveShowType = LiveShowTypeAnchor;
        liveReleaseShowViewController.transferLiveSingleModel = liveModel;

        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:liveReleaseShowViewController];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [controller.navigationController presentViewController:nav animated:YES completion:NULL];
        });
    }];
    UINavigationController *liveNav = [[UINavigationController alloc]initWithRootViewController:liveReleaseViewController];
    [controller.navigationController presentViewController:liveNav animated:YES completion:NULL];
}

+(void)liveHomeDirectToModel:(HomeLiveSingleModel *)singleModel controller:(UIViewController *)controller{
    LiveReleaseShowViewController *liveRootViewController = [[LiveReleaseShowViewController alloc]init];
    liveRootViewController.transferLiveShowType = LiveShowTypeAudience;

    liveRootViewController.transferLiveSingleModel = singleModel;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:liveRootViewController];
    [controller.navigationController presentViewController:nav animated:YES completion:NULL];
}

+(void)disableLogPrint{
//    TIMManager *manager = [[ILiveSDK getInstance] getTIMManager];
//    [manager initLogSettings:NO logPath:[manager getLogPath]];
//    [[TIMManager sharedInstance] getLogPath];
//    [[ILiveSDK getInstance] setConsoleLogPrint:NO];
//    [QAVAppChannelMgr setExternalLogger:self];
}

+(void)liveLoginManager{
    
}

@end

//
//  HomeRootHotCollectionViewHeader.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BannerListModel.h"

@interface HomeRootHotCollectionViewHeader : UICollectionReusableView

@property (nonatomic,assign)CGSize transferSize;

@property (nonatomic,strong)NSArray *transferArr;


-(void)remove;
+(CGFloat) calculationCellHeight;

-(void)homeBannerActionClickBlock:(void(^)(GWScrollViewSingleModel *singleModel))block;

@end

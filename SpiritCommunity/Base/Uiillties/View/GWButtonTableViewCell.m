//
//  GWButtonTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWButtonTableViewCell.h"


static char buttonKey;
@interface GWButtonTableViewCell()

@end

@implementation GWButtonTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &buttonKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.button];
    [self setButtonStatus:NO];
    
}

-(void)setTransferTitle:(NSString *)transferTitle {
    _transferTitle = transferTitle;
    self.button.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2* LCFloat(11), self.transferCellHeight);
    [self.button setTitle:transferTitle forState:UIControlStateNormal];
}

-(void)setButtonStatus:(BOOL)enable{
    if(enable){         // 可以进行点击
        self.button.userInteractionEnabled = YES;
        [self.button setBackgroundImage:[Tool stretchImageWithName:@"icon_grab_btn_blue"] forState:UIControlStateNormal];
    } else {
        self.button.userInteractionEnabled = NO;
        [self.button setBackgroundImage:[Tool stretchImageWithName:@"icon_grab_btn_gray"] forState:UIControlStateNormal];
    }
}

-(void)buttonClickManager:(void(^)())block{
    objc_setAssociatedObject(self, &buttonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeight{
    return LCFloat(80);
}

@end

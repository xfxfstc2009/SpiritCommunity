//
//  OrderPayViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2018/2/11.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "OrderPayViewController.h"
#import "OrderRootRowOneTableViewCell.h"
#import "ChongzhiRootViewController.h"
#import "MoneyChongzhiSingleModel.h"
#import "PDWXPayHandle.h"

@interface OrderPayViewController ()<UITableViewDataSource,UITableViewDelegate>{
    ChongZhiSingleModelSimple *chongZhiSingleModel;
}
@property (nonatomic,strong)UITableView *orderPayTableView;
@property (nonatomic,strong)NSArray *orderPayArr;
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UIButton *chongzhiButton;

@end

@implementation OrderPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTabeView];
    [self createBottomView];
    [self sendRequestToGetOrderInfo];
}

-(void)pageSetting{
    self.barMainTitle = @"支付订单";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    if (self.transferCartArr.count){
        self.orderPayArr = @[@[@"订单号",@"订单状态"],self.transferCartArr,@[@"微信支付"],@[@"支付"]];
    } else {
        self.orderPayArr = @[@[@"订单号",@"订单状态"],@[@"订单信息"],@[@"微信支付"],@[@"支付"]];
    }
}

-(void)createTabeView{
    if (!self.orderPayTableView){
        self.orderPayTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.orderPayTableView.dataSource = self;
        self.orderPayTableView.delegate = self;
        [self.view addSubview:self.orderPayTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.orderPayArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.orderPayArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if(indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        if (indexPath.row == 0){        // 订单号
            cellWithRowOne.transferTitle = [NSString stringWithFormat:@"订单号:%@",self.transferSingleModel.orderSn.length?self.transferSingleModel.orderSn:@"2018MMSKORMS234"];
        } else if (indexPath.row == 1){     // 订单状态
            cellWithRowOne.transferTitle = [NSString stringWithFormat:@"订单状态:%@",@"待支付"];
        }
        return cellWithRowOne;
    } else if(indexPath.section == 1){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        OrderRootRowOneTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[OrderRootRowOneTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        cellWithRowFour.transferCellHeight = cellHeight;
        if (self.transferSingleModel){
            cellWithRowFour.transferSingleModel = self.transferSingleModel;
        } else if (self.transferProductDetailModel){
            cellWithRowFour.transferProductDetailModel = self.transferProductDetailModel;
        } else {
            cellWithRowFour.transferCartModel = [self.transferCartArr objectAtIndex:indexPath.row];
        }

        return cellWithRowFour;
    } else if (indexPath.section == 3){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle =  @"立即支付";
        [cellWithRowTwo setButtonStatus:YES];
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf payManager];
        }];
        return cellWithRowTwo;
    } else if (indexPath.section == 2){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWSelectedTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWSelectedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"微信支付";
        cellWithRowThr.iconImg = [UIImage imageNamed:@"icon_wechat_Pay"];
        [cellWithRowThr setChecked:YES];
        return cellWithRowThr;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return LCFloat(44);
    }else if (indexPath.section == 1){
        return [OrderRootRowOneTableViewCell calculationCellHeight];
    }else if (indexPath.section == 3){
        return [GWButtonTableViewCell calculationCellHeight];
    } else {
        return LCFloat(50);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}


#pragma mark - 创建底部的View
-(void)createBottomView{
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    if (IS_iPhoneX){
        self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(70) - 88, kScreenBounds.size.width, LCFloat(70));
    } else {
        self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(50) - 64, kScreenBounds.size.width, LCFloat(50));
    }
    [self.view addSubview:self.bottomView];
    self.bottomView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.bottomView.layer.shadowOpacity = .2f;
    self.bottomView.layer.shadowOffset = CGSizeMake(.2f, .2f);

    // 2. 创建左侧label
    self.fixedLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.fixedLabel.text = @"总价：";
    self.fixedLabel.font = [self.fixedLabel.font boldFont];
    [self.bottomView addSubview:self.fixedLabel];
    CGSize fixeSize = [Tool makeSizeWithLabel:self.fixedLabel];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, fixeSize.width, fixeSize.height);
    self.fixedLabel.center_y = self.bottomView.size_height / 2.;

    // 3. 创建价格
    self.dymicLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"红"];
    self.dymicLabel.text = @"￥0";
    self.dymicLabel.font = [self.dymicLabel.font boldFont];
    [self.bottomView addSubview:self.dymicLabel];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(3), 0, LCFloat(150), self.fixedLabel.size_height);
    self.dymicLabel.center_y = self.fixedLabel.center_y;

    // 4.充值按钮
    self.chongzhiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.chongzhiButton setTitle:@"去充值" forState:UIControlStateNormal];
    self.chongzhiButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.bottomView addSubview:self.chongzhiButton];
    CGSize contentOfSize = [@"去充值" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.fixedLabel.size_height)];
    CGFloat width = contentOfSize.width + 2 * LCFloat(11);
    self.chongzhiButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - width, 0, width, self.fixedLabel.size_height + 2 * LCFloat(5));
    self.chongzhiButton.center_y = self.dymicLabel.center_y;
    self.chongzhiButton.backgroundColor = [UIColor colorWithCustomerName:@"蓝"];
    __weak typeof(self)weakSelf = self;
    [self.chongzhiButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ChongzhiRootViewController *chongzhiViewController = [[ChongzhiRootViewController alloc]init];
        [strongSelf.navigationController pushViewController:chongzhiViewController animated:YES];
    }];
    self.chongzhiButton.layer.cornerRadius = LCFloat(3);
    self.chongzhiButton.clipsToBounds = YES;



    if (self.transferSingleModel){
        self.dymicLabel.text = [NSString stringWithFormat:@"￥%.2f",self.transferSingleModel.payMoney];
    } else if (self.transferProductDetailModel){
        self.dymicLabel.text = [NSString stringWithFormat:@"￥%.2f",self.transferProductDetailModel.goodBaseInfo.salePriceRMB];
    } else {
        CGFloat price = 0;
        for (int i = 0 ; i < self.transferCartArr.count;i++){
            CartSingleModel *cartSingleModel = [self.transferCartArr objectAtIndex:i];
            price += cartSingleModel.goodsMoney * cartSingleModel.goodsNum;
        }
        self.dymicLabel.text = [NSString stringWithFormat:@"￥%.2f",price];
    }
}

#pragma mark 请求服务器申请订单
-(void)sendRequestToGetOrderInfo{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"productId":@"8",@"productName":@"伊穆购自营商品",@"productPrice":self.dymicLabel.text};
    [[NetworkAdapter sharedAdapter] fetchWithPath:infoOrder requestParams:params responseObjectClass:[ChongZhiSingleModelSimple class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        [StatusBarManager statusBarHidenWithText:@"正在向服务器申请订单"];
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->chongZhiSingleModel = (ChongZhiSingleModelSimple *)responseObject;
            strongSelf.transferSingleModel.orderSn = strongSelf->chongZhiSingleModel.out_trade_no;
            [strongSelf.orderPayTableView reloadData];
        }
    }];
}

-(void)payManager{
    PDWXPayHandle *handle = [[PDWXPayHandle alloc]init];
    [PDWXPayHandle sharedManager].orderNumber = chongZhiSingleModel.out_trade_no;
    [PDWXPayHandle sharedManager].coin = 30;
    [handle sendPayRequestWithOrder:chongZhiSingleModel];

}

@end

//
//  CenterMoneyDetailCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CenterMoneyDetailCell.h"

@interface CenterMoneyDetailCell()
@property (nonatomic,strong)UILabel *titleLabel ;
@property (nonatomic,strong)UILabel *moneyLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *statusLabel;
@property (nonatomic,strong)UIButton *statusButton;

@end

@implementation CenterMoneyDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - pageSetting
-(void)createView{
    // 1. 名字
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"小正文"]boldFont];
    [self addSubview:self.titleLabel];
    
    // 2. 创建流水label
    self.moneyLabel = [[UILabel alloc]init];
    self.moneyLabel.backgroundColor = [UIColor clearColor];
    self.moneyLabel.font = [[UIFont fontWithCustomerSizeName:@"小正文"] boldFont];
    self.moneyLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.moneyLabel];
    
    // 3. 创建时间
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.timeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.timeLabel];
}

-(void)setTransferSingleModel:(MoneyFlowSIngleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;

    // 1. 价格
    if (transferSingleModel.coin > 0){
        self.moneyLabel.text = [NSString stringWithFormat:@"+%li",(long)transferSingleModel.coin];
        self.moneyLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    } else {
        self.moneyLabel.text = [NSString stringWithFormat:@"%li",(long)transferSingleModel.coin];
        self.moneyLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
    }

    CGSize moneySize = [Tool makeSizeWithLabel:self.moneyLabel];
    self.moneyLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - moneySize.width, 0, moneySize.width, self.transferCellHeight);

    // 2. 创建内容
    CGFloat width = kScreenBounds.size.width - LCFloat(11) - LCFloat(11) - moneySize.width - 2 * LCFloat(11) - LCFloat(70);
    self.titleLabel.text = transferSingleModel.title;
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), width, [NSString contentofHeightWithFont:self.titleLabel.font]);

    // 2. 创建time
    self.timeLabel.text = [NSDate getTimeWithDuobaoString:transferSingleModel.time];
    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), width, [NSString contentofHeightWithFont:self.timeLabel.font]);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end

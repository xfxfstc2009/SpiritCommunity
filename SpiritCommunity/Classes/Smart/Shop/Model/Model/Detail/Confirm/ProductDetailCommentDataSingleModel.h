//
//  ProductDetailCommentDataSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol ProductDetailCommentDataSingleModel <NSObject>


@end

@interface ProductDetailCommentDataSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSInteger star;
@property (nonatomic,copy)NSString *good_id;
@property (nonatomic,copy)NSString *username;

@end

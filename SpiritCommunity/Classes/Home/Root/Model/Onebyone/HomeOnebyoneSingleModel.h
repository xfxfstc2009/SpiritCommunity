//
//  HomeOnebyoneSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol HomeOnebyoneSingleModel <NSObject>


@end

@interface HomeOnebyoneSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *area;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *introduce;
@property (nonatomic,copy)NSString *lat;
@property (nonatomic,copy)NSString *lng;
@property (nonatomic,copy)NSString *name;

@end

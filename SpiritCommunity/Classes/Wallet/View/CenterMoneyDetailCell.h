//
//  CenterMoneyDetailCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoneyFlowSIngleModel.h"
@interface CenterMoneyDetailCell : PDBaseTableViewCell

@property (nonatomic,strong)MoneyFlowSIngleModel *transferSingleModel;

+(CGFloat)calculationCellHeight;

@end

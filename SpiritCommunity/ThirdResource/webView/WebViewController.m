//
//  WebViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/6/27.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "WebViewController.h"
#import "GWWebView.h"
#import "ShareRootViewController.h"

@interface WebViewController()<GWWebViewDelegate>
@property (nonatomic,strong)GWWebView *mainWebView;
@property (nonatomic,copy)NSString *mainUrl;
@property (nonatomic,strong)UIButton *rightButton;

@end

@implementation WebViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createWebView];
    [self pageSetting];
}

#pragma mark pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
   self.rightButton = [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"center_share_icon"] barHltImage:[UIImage imageNamed:@"center_share_icon"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
        shareViewController.isHasGesture = YES;
        [shareViewController actionClickWithShareBlock:^(NSString *type) {
            if (!weakSelf){
                return ;
            }
            SSDKPlatformType mainType = SSDKPlatformSubTypeQZone;
            if ([type isEqualToString:@"QQ"]){
                mainType = SSDKPlatformTypeQQ;
            } else if ([type isEqualToString:@"wechat"]){
                mainType = SSDKPlatformTypeWechat;
            } else if ([type isEqualToString:@"friendsCircle"]){
                mainType = SSDKPlatformSubTypeWechatTimeline;
            }

            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf.transferFindRootSingleModel){
                NSString *img = [NSString stringWithFormat:@"%@%@",aliyunOSS_BaseURL,strongSelf.transferFindRootSingleModel.image];
                [[ShareSDKManager sharedShareSDKManager] shareManagerWithType:mainType title:strongSelf.transferFindRootSingleModel.title desc:strongSelf.transferFindRootSingleModel.desc img:img url:strongSelf.mainUrl callBack:^(BOOL success) {
                    [[UIAlertView alertViewWithTitle:@"分享成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
                }];
            }
        }];
        [shareViewController showInView:strongSelf.parentViewController];
    }];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark createWebView
-(void)createWebView{
    self.mainWebView = [[GWWebView alloc]initWithFrame:self.view.bounds];
    self.mainWebView.delegate = self;
    self.mainWebView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.mainWebView];
}

// 跳转
-(void)webViewControllerWithAddress:(NSString *)url{
    if (![url hasPrefix:@"http://"]){
        if (![url hasPrefix:@"https://"]){
            url = [NSString stringWithFormat:@"http://%@",url];
        }
    }

    self.mainUrl = url;
    if (!self.mainWebView){
        [self createWebView];
    }
    [self.mainWebView loadURLString:url];

    if (self.transferFindRootSingleModel){
        self.rightButton.hidden = NO;
    } else {
        self.rightButton.hidden = YES;
    }
}


- (void)webViewDidStartLoad:(GWWebView *)webview {
    NSLog(@"页面开始加载");
    [StatusBarManager statusBarHidenWithText:@"页面开始加载"];
}

- (void)webView:(GWWebView *)webview shouldStartLoadWithURL:(NSURL *)URL {
    NSLog(@"截取到URL：%@",URL);
}

- (void)webView:(GWWebView *)webview didFinishLoadingURL:(NSURL *)URL {
    NSString *title = @"";
    if (webview.uiWebView){
        title = [webview.uiWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
    } else {
        title = webview.wkWebView.title;
    }
    self.barMainTitle = title;
}

- (void)webView:(GWWebView *)webview didFailToLoadURL:(NSURL *)URL error:(NSError *)error {
    NSLog(@"加载出现错误");
    [StatusBarManager statusBarHidenWithText:@"页面出现错误"];
}


@end

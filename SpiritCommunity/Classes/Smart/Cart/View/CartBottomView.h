//
//  CartBottomView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartBottomView : UIView

@property (nonatomic,strong)UIButton *allButton;

-(void)actionClickWithChooseAll:(void(^)())block;
-(void)actionClickWithBuyNow:(void (^)())block;

-(void)setPrice:(CGFloat)price;

@end

//
//  HomeNormalSingleGoodsModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol HomeNormalSingleGoodsModel <NSObject>

@end

@interface HomeNormalSingleGoodsModel : FetchModel

@property (nonatomic,copy)NSString *address;
@property (nonatomic,copy)NSString *contactName;
@property (nonatomic,copy)NSString *latitude;
@property (nonatomic,copy)NSString *detailInfo;
@property (nonatomic,copy)NSString *phoneNum;
@property (nonatomic,copy)NSString *merchantService;
@property (nonatomic,copy)NSString *saleNumber;
@property (nonatomic,copy)NSString *maxGiveRatio;
@property (nonatomic,copy)NSString *merchantName;
@property (nonatomic,copy)NSString *avgScore;
@property (nonatomic,copy)NSString *shopImg;
@property (nonatomic,copy)NSString *shortDesc;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *merchantType;
@property (nonatomic,copy)NSString *mapAddress;
@property (nonatomic,copy)NSString *longitude;

@property (nonatomic,copy)NSString *goodsImg;
@property (nonatomic,assign)CGFloat salePriceRmb;
@property (nonatomic,assign)CGFloat marketPrice;

@property (nonatomic,assign)CGFloat goodsSn;
@property (nonatomic,assign)CGFloat salePriceJf;
@property (nonatomic,assign)CGFloat weight;
@property (nonatomic,copy)NSString *goodsSummary;
@property (nonatomic,copy)NSString *sharePrice;
@property (nonatomic,copy)NSString *giveFrequency;
@property (nonatomic,copy)NSString *goodsNumber;
@property (nonatomic,copy)NSString *salePriceUb;
@property (nonatomic,copy)NSString *goodsUnit;
@property (nonatomic,copy)NSString *goodsName;
@property (nonatomic,copy)NSString *suggestPrice;
@property (nonatomic,copy)NSString *goodsDesc;

@property (nonatomic,copy)NSString *imgUrl;
@property (nonatomic,copy)NSString *linkUrl;



@end

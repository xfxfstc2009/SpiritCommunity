//
//  CenterMoneyRootHeaderCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/5.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterMoneyModel.h"

@interface CenterMoneyRootHeaderCell : PDBaseTableViewCell

@property (nonatomic,strong)CenterMoneyModel *transferMoneyModel;

+(CGFloat)calculationCellHeight;


@end

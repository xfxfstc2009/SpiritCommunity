//
//  SocketPacket.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/23.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SocketPacket <NSObject>

@property (nonatomic,assign)NSInteger tag;
@property (nonatomic,strong)NSData *data;

-(instancetype)initWithData:(NSData *)data;

@optional
-(void)setTag:(NSInteger)tag;
-(void)setData:(NSData *)data;

@end

@interface SocketPacket : NSObject

@end

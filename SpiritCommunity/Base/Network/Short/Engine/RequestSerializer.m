//
//  RequestSerializer.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//
// 设置请求头
#import "RequestSerializer.h"
#import "GTMBase64.h"
#import "PDRSAManager.h"
@implementation RequestSerializer

-(NSMutableURLRequest *)requestWithMethod:(NSString *)method URLString:(NSString *)URLString parameters:(id)parameters error:(NSError *__autoreleasing *)error{
    NSMutableURLRequest *request = [super requestWithMethod:method URLString:URLString parameters:parameters error:error];
    if ([AccountModel sharedAccountModel].authToken.length){
            [request setValue:[AccountModel sharedAccountModel].authToken forHTTPHeaderField:@"token"];
    }
    
    return request;
}


@end

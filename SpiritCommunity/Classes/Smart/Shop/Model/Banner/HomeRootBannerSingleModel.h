//
//  HomeRootBannerSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol HomeRootBannerSingleModel <NSObject>

@end

@interface HomeRootBannerSingleModel : FetchModel
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *pic_id;
@property (nonatomic,copy)NSString *img_url;
@property (nonatomic,copy)NSString *target_url;
@property (nonatomic,copy)NSString *pic_name;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *url;

@end

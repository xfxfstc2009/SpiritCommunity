//
//  ShopDetailSub2DetailViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2018/2/9.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "ShopDetailSub2DetailViewController.h"
#import "ProductDetailSub2TableViewCell.h"

@interface ShopDetailSub2DetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)ProductDetailSub2TableViewCell *tempCellWithRowSev;
@property (nonatomic,strong)UITableView *productDetail2TableView;

@end

@implementation ShopDetailSub2DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createTableView];
}

#pragma mark - UITableView
-(void)createTableView{
    if(!self.productDetail2TableView){
        self.productDetail2TableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.productDetail2TableView.dataSource = self;
        self.productDetail2TableView.delegate = self;
        [self.view addSubview:self.productDetail2TableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
    ProductDetailSub2TableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
    if (!cellWithRowSev){
        cellWithRowSev = [[ProductDetailSub2TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
        cellWithRowSev.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    self.tempCellWithRowSev = cellWithRowSev;
    cellWithRowSev.transferImg = self.transferImgs;
    __weak typeof(self)weakSelf = self;
    [cellWithRowSev actionBlockHeight:^(CGFloat imgHeihgt) {
        if (!weakSelf){
            return ;
        }
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
    return cellWithRowSev;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height = self.tempCellWithRowSev.cellHeight;
    return height;
}


-(void)pageLoad{
    PDImageView *imgView = [[PDImageView alloc]init];
    [self.productDetail2TableView showPrompt:@"商品详情正在加载中" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
    __weak typeof(self)weakSelf = self;
    [imgView uploadImageWithURL:self.transferImgs placeholder:nil callback:^(UIImage *image) {

        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.productDetail2TableView reloadData];
        [strongSelf.productDetail2TableView dismissPrompt];
    }];
}

@end

//
//  RankingTwoTableViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RankingSingleModel.h"
@interface RankingTwoTableViewCell : PDBaseTableViewCell

@property (nonatomic,assign)CGFloat transcerCellHeight;
@property (nonatomic,assign)NSInteger transferIndex;
@property (nonatomic,strong)RankingSingleModel *transferSingleModel;

+(CGFloat)calculationCellHeight;


-(void)loadContentWithIndex:(NSInteger)index;
- (void)startAnimationWithDelay:(CGFloat)delayTime ;            // 动画

-(void)guanzhuButtonClick:(void(^)(RankingSingleModel *transferSingleModel))block;

-(void)guanzhuBtnClickManagerStatus:(BOOL)status;
@end

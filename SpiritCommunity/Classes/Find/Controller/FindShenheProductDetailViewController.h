//
//  FindShenheProductDetailViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/31.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "GWShopShopSingleModel.h"

@interface FindShenheProductDetailViewController : AbstractViewController

@property (nonatomic,strong)GWShopShopSingleModel *transferSingleModel;

@end

//
//  OrderRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2018/2/8.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "OrderRootViewController.h"
#import "OrderSegmentSingleModel.h"
#import "OrderDetailRootViewController.h"
#import "OrderListModel.h"
#import "OrderRootRowOneTableViewCell.h"
#import "OrderRootRowTwoTableViewCell.h"
#import "OrderPayViewController.h"


@interface OrderRootViewController ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;         /**< segment*/
@property (nonatomic,strong)UIScrollView *tableViewMainScrollView;          /**< scrollView*/
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;              /**< segment数组*/
@property (nonatomic,strong)NSMutableDictionary *tableViewMutableDic;       /**< 列表的字典*/
@property (nonatomic,strong)NSMutableDictionary *dataSourceMutableDic;      /**< 数据源字典*/
@property (nonatomic,strong)NSMutableArray *currentMutableArr;              /**< 当前的数组*/

@end

@implementation OrderRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createSegment];
    [self arrayWithInit];
    [self createBaseScrollView];
    [self hasTableAndManagerWithIndex:self.segmentList.selectedButtonIndex];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的订单";

}

#pragma mark - ArrayWithInit
-(void)arrayWithInit{
    self.segmentMutableArr = [NSMutableArray array];                        // 1. segment

    self.tableViewMutableDic = [NSMutableDictionary dictionary];            // 2. tableView
    self.dataSourceMutableDic = [NSMutableDictionary dictionary];           // 3. 数据源
    self.currentMutableArr = [NSMutableArray array];                        // 5. 当前数据源

    // 1. 添加segment
    for (int i = 0 ; i < 5;i++){
        OrderSegmentSingleModel *orderSegmentSingleModel = [[OrderSegmentSingleModel alloc]init];
        if (i == 0){
            orderSegmentSingleModel.name = @"全部";
            orderSegmentSingleModel.itemId = [NSString stringWithFormat:@"%li",(long)orderTypeALL];
        } else if (i == 1){
            orderSegmentSingleModel.name = @"待付款";
            orderSegmentSingleModel.itemId = [NSString stringWithFormat:@"%li",(long)orderTypeDaifukuan];
        } else if (i == 2){
            orderSegmentSingleModel.name = @"待发货";
            orderSegmentSingleModel.itemId = [NSString stringWithFormat:@"%li",(long)orderTypeDaifahuo];
        } else if (i == 3){
            orderSegmentSingleModel.name = @"待收货";
            orderSegmentSingleModel.itemId =  [NSString stringWithFormat:@"%li",(long)orderTypeDaiShouhuo];
        } else if (i == 4){
            orderSegmentSingleModel.name = @"待评价";
            orderSegmentSingleModel.itemId =  [NSString stringWithFormat:@"%li",(long)orderTypeDaipingjia];
        }
        [self.segmentMutableArr addObject:orderSegmentSingleModel];
    }
}

#pragma mark - Segment
-(void)createSegment{
    __weak typeof(self)weakSelf = self;
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"全部",@"待付款",@"待发货",@"待收货",@"待评价"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.tableViewMainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
        [strongSelf hasTableAndManagerWithIndex:index];
    }];
    [self.view addSubview:self.segmentList];
}

#pragma mark - createScrollView
-(void)createBaseScrollView{
    if (!self.tableViewMainScrollView){
        self.tableViewMainScrollView = [[UIScrollView alloc]init];
        self.tableViewMainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.segmentList.frame));
        self.tableViewMainScrollView.delegate = self;
        self.tableViewMainScrollView.backgroundColor = [UIColor clearColor];
        self.tableViewMainScrollView.pagingEnabled = YES;
        self.tableViewMainScrollView.alwaysBounceVertical = NO;
        self.tableViewMainScrollView.showsVerticalScrollIndicator = NO;
        self.tableViewMainScrollView.showsHorizontalScrollIndicator = NO;
        self.tableViewMainScrollView.scrollEnabled = YES;
        [self.view addSubview:self.tableViewMainScrollView];
        self.tableViewMainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.segmentMutableArr.count, self.tableViewMainScrollView.size_height);
    }
}

#pragma makr - 临时创建一个tableView
-(void)hasTableAndManagerWithIndex:(NSInteger)index{
    // 2. 判断当前是否存在tableView
    if (![self.tableViewMutableDic.allKeys containsObject:[NSString stringWithFormat:@"%li",(long)index]]){            // 如果不存在
        if (self.currentMutableArr.count){
            [self.currentMutableArr removeAllObjects];
        }
        UITableView *tableView = [self createTableView];                            // 创建tableView
        tableView.orgin_x = index *kScreenBounds.size.width;                        // 修改frame

        [self.tableViewMutableDic setObject:tableView forKey:[NSString stringWithFormat:@"%li",(long)index]];

        // 2. 走接口
        __weak typeof(self)weakSelf = self;
        OrderSegmentSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
        studyItemSingleModel.tableIndex = [NSString stringWithFormat:@"%li",(long)index];
        [weakSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:NO];
    } else {            // 已经存在
        // 2. 走接口
        __weak typeof(self)weakSelf = self;
        OrderSegmentSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
        [weakSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:YES];
    }
}

-(UITableView *)createTableView{
    UITableView *tableView= [[UITableView alloc] initWithFrame:self.tableViewMainScrollView.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.size_height = self.tableViewMainScrollView.size_height - 64;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.showsVerticalScrollIndicator = YES;
    tableView.backgroundColor = [UIColor clearColor];
    [self.tableViewMainScrollView addSubview:tableView];

    __weak typeof(self)weakSelf = self;
    OrderSegmentSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:self.segmentList.selectedButtonIndex];
    // 下拉刷新
    [tableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:NO];
    }];

    [tableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:NO];
    }];

    return tableView;
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.currentMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;

        OrderSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
        UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
        if (tableView == currentTableView){         // 获取当前的model
            // 1.获取当前tableView的key
            NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.itemId];
            OrderListSingleModel *orderSingleModel = [currentArr objectAtIndex:indexPath.section];
            cellWithRowOne.transferTitle = orderSingleModel.orderSn;
        }

        return cellWithRowOne;
    } else if(indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        OrderRootRowOneTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[OrderRootRowOneTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        OrderSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
        UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
        if (tableView == currentTableView){         // 获取当前的model
            // 1.获取当前tableView的key
            NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.itemId];
            OrderListSingleModel *orderSingleModel = [currentArr objectAtIndex:indexPath.section];
            cellWithRowTwo.transferSingleModel = orderSingleModel;
        }

        return cellWithRowTwo;
    } else if (indexPath.row == 2){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        OrderRootRowTwoTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[OrderRootRowTwoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        OrderSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr actionClickWithBlock:^(OrderListSingleModel *transferSingleModel, NSString *title) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if ([title isEqualToString:@"立即评价"]){
//                EvaluateInputViewController *evaluateInputViewController = [[EvaluateInputViewController alloc]init];
//                evaluateInputViewController.transferOrderId = transferSingleModel.ID;
//                [evaluateInputViewController actionEvaluateBlock:^{
//                    [strongSelf sendRequestToPingjia:transferSingleModel];
//                }];

//                [strongSelf.navigationController pushViewController:evaluateInputViewController animated:YES];
            } else if ([title isEqualToString:@"确认收货"]){
                [[UIAlertView alertViewWithTitle:nil message:@"是否确认收货" buttonTitles:@[@"确认收货",@"否"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                    if (buttonIndex == 0){
//                        [strongSelf sendRequestToConfirmOrderWith:transferSingleModel];
//                    }
                }]show];
            } else if([title isEqualToString:@"确认发货"]){
//                [strongSelf sendRequestToFahuo:transferSingleModel];
            } else if ([title isEqualToString:@"立即支付"]){
                OrderPayViewController *payViewController = [[OrderPayViewController alloc]init];
                payViewController.transferSingleModel = transferSingleModel;
                [strongSelf.navigationController pushViewController:payViewController animated:YES];
            }
        }];
        UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
        if (tableView == currentTableView){         // 获取当前的model
            // 1.获取当前tableView的key
            NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.itemId];
            OrderListSingleModel *orderSingleModel = [currentArr objectAtIndex:indexPath.section];
            cellWithRowThr.transferSingleModel = orderSingleModel;
        }

        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return LCFloat(44);
    } else if (indexPath.row == 1){
        return  [OrderRootRowOneTableViewCell calculationCellHeight];
    } else {
        return LCFloat(44);
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    OrderSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    if (tableView == currentTableView){         // 获取当前的model
//        // 1.获取当前tableView的key
//        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.itemId];
//        OrderListSingleModel *orderSingleModel = [currentArr objectAtIndex:indexPath.section];
//        OrderDetailViewController *orderDetailViewController = [[OrderDetailViewController alloc]init];
//        [orderDetailViewController hidesTabBarWhenPushed];
//        orderDetailViewController.transferOrderId = orderSingleModel.ID;
//        [self.navigationController pushViewController:orderDetailViewController animated:YES];
//
//        // 1. 支付成功
//        __weak typeof(self)weakSelf = self;
//        [orderDetailViewController orderPaySuccessManagerWithBlock:^(NSString *transferOrderId) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            NSInteger index = [self.currentMutableArr indexOfObject:orderSingleModel];
//            [strongSelf.currentMutableArr removeObject:orderSingleModel];
//            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
//            [tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
//        }];

    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType  = SeparatorTypeHead;
    } else if ([indexPath row] == 2) {
        separatorType  = SeparatorTypeBottom;
    } else {
        separatorType  = SeparatorTypeMiddle;
    }

    [cell addSeparatorLineWithType:separatorType];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    }
    return LCFloat(15);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


#pragma mark - 接口
-(void)sendRequestToGetInfoWithItemId:(OrderSegmentSingleModel *)itemSingleModel isHorizontal:(BOOL)isHorizontal{
    if (isHorizontal){
        return;
    }
    __weak typeof(self)weakSelf = self;
    //1. 找到当前的tableView
    UITableView *currentTableView = (UITableView *)[self.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
    NSDictionary *params = [NSDictionary dictionary];
    if ([itemSingleModel.itemId isEqualToString: [NSString stringWithFormat:@"%li",(long)orderTypeALL]]){
        params = @{@"orderType":@"0",@"page":@(currentTableView.currentPage) ,@"row":@"20"};
    } else {
        params = @{@"orderType":@"0",@"page":@(currentTableView.currentPage),@"queryType":itemSingleModel.itemId,@"row":@"20"};
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:_orderList requestParams:params responseObjectClass:[OrderListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            OrderListModel *customerListModel = (OrderListModel *)responseObject;
            // 2. 判断是否下啦
            if (currentTableView.isXiaLa){
               [strongSelf.dataSourceMutableDic removeObjectForKey:itemSingleModel.itemId];
            }

            // 3. 判断当前是否存在这样的数据源
            NSMutableArray *appendingMutableArr = [NSMutableArray array];
            if (![strongSelf.dataSourceMutableDic.allKeys containsObject:itemSingleModel.itemId]){          // 不存在
                [strongSelf.dataSourceMutableDic setObject:customerListModel.infoList forKey:itemSingleModel.itemId];
            } else {                                                                        // 存在
                // 1. 获取上一个数据源
                NSArray *lastArr = [strongSelf.dataSourceMutableDic objectForKey:itemSingleModel.itemId];
                // 2. 加入上一个数据源
                [appendingMutableArr addObjectsFromArray:lastArr];
                // 3. 加入新的数据源
                [appendingMutableArr addObjectsFromArray:customerListModel.infoList];
                // 4. 加入信息
                [strongSelf.dataSourceMutableDic setObject:appendingMutableArr forKey:itemSingleModel.itemId];
            }

            // 4. 获取当前的数据源
            NSArray *currentDataSourceArr = [strongSelf.dataSourceMutableDic objectForKey:itemSingleModel.itemId];

            if (strongSelf.currentMutableArr.count){
                [strongSelf.currentMutableArr removeAllObjects];
            }
            [strongSelf.currentMutableArr addObjectsFromArray:currentDataSourceArr];

            // dismiss
            [currentTableView stopPullToRefresh];
            [currentTableView stopFinishScrollingRefresh];

            // placeholder

            [currentTableView reloadData];

            if (!strongSelf.currentMutableArr.count){
                [currentTableView showPrompt:@"当前没有该订单信息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            } else {
                [currentTableView dismissPrompt];
            }
        } else {
            if (!strongSelf.currentMutableArr.count){
                [currentTableView showPrompt:@"当前没有该订单信息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            } else {
                [currentTableView dismissPrompt];
            }
            [currentTableView stopPullToRefresh];
            [currentTableView stopFinishScrollingRefresh];
        }
    }];
}
@end

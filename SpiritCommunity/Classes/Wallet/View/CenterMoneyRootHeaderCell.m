//
//  CenterMoneyRootHeaderCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/5.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "CenterMoneyRootHeaderCell.h"

@interface CenterMoneyRootHeaderCell()
@property (nonatomic,strong)UILabel *ketixianLabel;
@property (nonatomic,strong)UILabel *ketixianDymicLabel;
@property (nonatomic,strong)UILabel *leijiLabel;
@property (nonatomic,strong)UILabel *leijiDymicLabel;

@end

@implementation CenterMoneyRootHeaderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier   {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.ketixianLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.ketixianLabel.text = @"可提现(元)";
    self.ketixianLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.ketixianLabel];
    
    // 2. 创建可提现
    self.ketixianDymicLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.ketixianDymicLabel.textAlignment = NSTextAlignmentCenter;
    self.ketixianDymicLabel.font = [UIFont systemFontOfCustomeSize:40.];
    [self addSubview:self.ketixianDymicLabel];
    
    // 3. 创建累计收入
    self.leijiLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.leijiLabel.textAlignment = NSTextAlignmentCenter;
    self.leijiLabel.text = @"累计收入(元)";
    [self addSubview:self.leijiLabel];
    
    // 4. 创建累计
    self.leijiDymicLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.leijiDymicLabel.font = [UIFont systemFontOfCustomeSize:24.];
    self.leijiDymicLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.leijiDymicLabel];
}

-(void)setTransferMoneyModel:(CenterMoneyModel *)transferMoneyModel{
    _transferMoneyModel = transferMoneyModel;
    
    self.ketixianLabel.frame = CGRectMake(0, LCFloat(25), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.ketixianLabel.font]);
    
    self.ketixianDymicLabel.text = [NSString stringWithFormat:@"%.2f",transferMoneyModel.total_sum];
    self.ketixianDymicLabel.frame=CGRectMake(0, CGRectGetMaxY(self.ketixianLabel.frame)+ LCFloat(25), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.ketixianDymicLabel.font]);
    
    self.leijiDymicLabel.text = [NSString stringWithFormat:@"%.2f",transferMoneyModel.lj_sum];
    self.leijiDymicLabel.frame = CGRectMake(0, self.transferCellHeight - LCFloat(40) - [NSString contentofHeightWithFont:self.leijiDymicLabel.font], kScreenBounds.size.width, [NSString contentofHeightWithFont:self.leijiDymicLabel.font]);
    
    self.leijiLabel.frame = CGRectMake(0, self.leijiDymicLabel.orgin_y - LCFloat(11) - [NSString contentofHeightWithFont:self.leijiLabel.font], kScreenBounds.size.width, [NSString contentofHeightWithFont:self.leijiLabel.font]);
    
}

+(CGFloat)calculationCellHeight{
    return LCFloat(280);
}
@end

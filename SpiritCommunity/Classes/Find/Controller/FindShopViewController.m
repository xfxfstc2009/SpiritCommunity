//
//  FindShopViewController.m
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/28.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import "FindShopViewController.h"
#import "FindRootTableViewCell.h"
#import "FindShenheProductAddViewController.h"
#import "GWShopShopSingleCell.h"
#import "GWShopShopSingleModel.h"
#import "FindShenheProductDetailViewController.h"


@interface FindShopViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *findTableView;
@property (nonatomic,strong)NSMutableArray *findArr;
@property (nonatomic,strong)UIButton *rightButton;;
@end

@implementation FindShopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetProductInfo];
}


#pragma mark - 发现
-(void)pageSetting{
    self.barMainTitle = @"精灵购物街";
    __weak typeof(self)weakSelf = self;
    self.rightButton = [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_nav_add"] barHltImage:[UIImage imageNamed:@"icon_nav_add"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
            FindShenheProductAddViewController *findViewController = [[FindShenheProductAddViewController alloc]init];
            [findViewController actionClickBlockProduct:^{
                strongSelf.findTableView.currentPage = 1;
                [strongSelf sendRequestToGetProductInfo];
            }];
            [strongSelf.navigationController pushViewController:findViewController animated:YES];
        }];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.findArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.findTableView){
        self.findTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.findTableView.dataSource = self;
        self.findTableView.delegate = self;
        [self.view addSubview:self.findTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.findTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetProductInfo];
    }];
    
    [self.findTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetProductInfo];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.findArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWShopShopSingleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWShopShopSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    cellWithRowOne.transferShopSingleModel = [self.findArr objectAtIndex:indexPath.section];;
    return cellWithRowOne;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    static CGFloat initialDelay = 0.09f;
    static CGFloat stutter = 0.1f;
    
    GWShopShopSingleCell *cardCell = (GWShopShopSingleCell *)cell;
    GWShopShopSingleModel *findRootSingleModel = [self.findArr objectAtIndex:indexPath.section];
    if (findRootSingleModel.hasAnimation == NO){
        [cardCell startAnimationWithDelay:initialDelay + ((indexPath.section) * stutter)];
        findRootSingleModel.hasAnimation = YES;
    }
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FindShenheProductDetailViewController *productDetailViewController = [[FindShenheProductDetailViewController alloc]init];
    GWShopShopSingleModel *findRootSingleModel = [self.findArr objectAtIndex:indexPath.section];
    
    productDetailViewController.transferSingleModel = findRootSingleModel;
    [self.navigationController pushViewController:productDetailViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    GWShopShopSingleModel *findRootSingleModel = [self.findArr objectAtIndex:indexPath.section];
    return [GWShopShopSingleCell calculationCellHeightWithModel:findRootSingleModel];
}

#pragma mark - 获取当前的商品列表
-(void)sendRequestToGetProductInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page":@(self.findTableView.currentPage),@"size":@"20"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:product_list requestParams:params responseObjectClass:[GWShopShopListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            GWShopShopListModel *listModel = (GWShopShopListModel *)responseObject;
            if (strongSelf.findTableView.isXiaLa){
                [strongSelf.findArr removeAllObjects];
            }
            
            [strongSelf.findArr addObjectsFromArray:listModel.list];
            [strongSelf.findTableView reloadData];
            if (!strongSelf.findArr.count){
                [strongSelf.findTableView showPrompt:@"没有数据哦~点击重新获取" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
                    [strongSelf sendRequestToGetProductInfo];
                }];
            } else {
                [strongSelf.findTableView dismissPrompt];
            }
            if (!listModel.list.count){
                if (!strongSelf.findArr.count){
                    [strongSelf.findTableView hiddenBottomTitle];
                } else {
                    [strongSelf.findTableView showBottomTitle];
                }
            } else {
                [strongSelf.findTableView hiddenBottomTitle];
            }
            
            [strongSelf.findTableView stopPullToRefresh];
            [strongSelf.findTableView stopFinishScrollingRefresh];
        }
    }];
}

@end

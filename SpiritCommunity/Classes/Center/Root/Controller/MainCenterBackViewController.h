//
//  MainCenterBackViewController.h
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2018/1/15.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "GWBaseMapViewController.h"
#import "MainCenterMenuViewController.h"

@interface MainCenterBackViewController :GWBaseMapViewController

@property (strong, nonatomic) MainCenterMenuViewController * transferMenuController;

-(void)actionWithToMapReloadManager;                        // 地图刷新

@end

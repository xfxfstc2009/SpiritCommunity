//
//  HomeRootHotCollectionViewHeader.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "HomeRootHotCollectionViewHeader.h"
#import "PDScrollView.h"

@interface HomeRootHotCollectionViewHeader()
@property (nonatomic,strong)PDScrollView *bannerView;

@end

static char homeBannerActionClickBlockKey;
@implementation HomeRootHotCollectionViewHeader

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self pageSetting];
        self.backgroundColor = [UIColor yellowColor];
    }
    return self;
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (!self.bannerView){
        self.bannerView = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(200))];
        self.bannerView.backgroundColor = BACKGROUND_VIEW_COLOR;
        __weak typeof(self)weakSelf = self;
        [self.bannerView bannerImgTapManagerWithInfoblock:^(GWScrollViewSingleModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(GWScrollViewSingleModel *mainModel) = objc_getAssociatedObject(strongSelf, &homeBannerActionClickBlockKey);
            if (block){
                block(singleModel);
            }
        }];
        [self addSubview:self.bannerView];
    }
}

-(void)setTransferSize:(CGSize)transferSize{
    _transferSize = transferSize;
}

-(void)setTransferArr:(NSArray *)transferArr{
    _transferArr = transferArr;
    if (!self.bannerView.transferImArr.count){
        NSMutableArray *tempScrollMutableArr = [NSMutableArray array];
        for (int i = 0 ; i < self.transferArr.count;i++){
            BannerSingleModel *singleModel = [self.transferArr objectAtIndex:i];
            
            GWScrollViewSingleModel *scrollViewSingleModel = [[GWScrollViewSingleModel alloc]init];
            scrollViewSingleModel.img = singleModel.banner;
            scrollViewSingleModel.content = singleModel.title;
            
            scrollViewSingleModel.ID = singleModel.ID;
            scrollViewSingleModel.type = singleModel.type;
            scrollViewSingleModel.info = singleModel.info;
            [tempScrollMutableArr addObject:scrollViewSingleModel];
        }
        self.bannerView.transferImArr = tempScrollMutableArr;
    }
    
}

-(void)remove{
    [self.bannerView removeFromSuperview];
    self.bannerView = nil;
}


+(CGFloat) calculationCellHeight{
    return LCFloat(200);
}

-(void)homeBannerActionClickBlock:(void(^)(GWScrollViewSingleModel *singleModel))block{
    objc_setAssociatedObject(self, &homeBannerActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end

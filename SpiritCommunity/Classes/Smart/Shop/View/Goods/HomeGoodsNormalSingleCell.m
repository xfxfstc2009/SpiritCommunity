//
//  HomeGoodsNormalSingleCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/15.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "HomeGoodsNormalSingleCell.h"
#import "HomeGoodsNormalSingleView.h"

static char actionClickCellBlockKey;
@interface HomeGoodsNormalSingleCell()
@property (nonatomic,strong)HomeGoodsNormalSingleView *leftView;
@property (nonatomic,strong)HomeGoodsNormalSingleView *rightView;


@end

@implementation HomeGoodsNormalSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = BACKGROUND_VIEW_COLOR;
        [self createView];
    }
    return self;
}

-(void)createView{
    self.leftView = [[HomeGoodsNormalSingleView alloc]initWithFrame:CGRectMake(LCFloat(6), LCFloat(6), [HomeGoodsNormalSingleView calculationSize].width, [HomeGoodsNormalSingleView calculationSize].height)];
    __weak typeof(self)weakSelf = self;
    [self.leftView actionClickBlock:^(HomeNormalSingleGoodsModel *transferHomeNormalGoodsModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(HomeNormalSingleGoodsModel *transferHomeNormalGoodsModel) = objc_getAssociatedObject(strongSelf, &actionClickCellBlockKey);
        if(block){
            block(transferHomeNormalGoodsModel);
        }
    }];
    [self addSubview:self.leftView];

    self.rightView = [[HomeGoodsNormalSingleView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftView.frame) + LCFloat(6), LCFloat(6), [HomeGoodsNormalSingleView calculationSize].width, [HomeGoodsNormalSingleView calculationSize].height)];
    [self.rightView actionClickBlock:^(HomeNormalSingleGoodsModel *transferHomeNormalGoodsModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(HomeNormalSingleGoodsModel *transferHomeNormalGoodsModel) = objc_getAssociatedObject(strongSelf, &actionClickCellBlockKey);
        if(block){
            block(transferHomeNormalGoodsModel);
        }
    }];
    [self addSubview:self.rightView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferLeftGoodsModel:(HomeNormalSingleGoodsModel *)transferLeftGoodsModel{
    _transferLeftGoodsModel = transferLeftGoodsModel;
    self.leftView.transferHomeNormalGoodsModel = transferLeftGoodsModel;
}

-(void)setTransferRightGoodsModel:(HomeNormalSingleGoodsModel *)transferRightGoodsModel{
    _transferRightGoodsModel = transferRightGoodsModel;
    self.rightView.transferHomeNormalGoodsModel = transferRightGoodsModel;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellheight = 0;
    cellheight += LCFloat(6);
    cellheight += [HomeGoodsNormalSingleView calculationSize].height;
    return cellheight;
}

-(void)actionClickCellBlock:(void(^)(HomeNormalSingleGoodsModel *transferGoodsModel))block{
    objc_setAssociatedObject(self, &actionClickCellBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end

//
//  MoneyFlowSIngleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/8.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol MoneyFlowSIngleModel <NSObject>

@end

@interface MoneyFlowSIngleModel : FetchModel

@property (nonatomic,assign)NSTimeInterval time;
@property (nonatomic,assign)NSInteger coin;
@property (nonatomic,copy)NSString *title;

@end

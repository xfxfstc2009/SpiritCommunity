//
//  HomeOneByOneTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "HomeOneByOneTableViewCell.h"

@interface HomeOneByOneTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *areaLabel;
@property (nonatomic,strong)UILabel *countlabel;
@property (nonatomic,strong)PDImageView *onebyoneImgView;
@property (nonatomic,strong)PDImageView *alphaImgView;
@property (nonatomic,strong)PDImageView *guanzhuImgView;


@end

@implementation HomeOneByOneTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建图片
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    // 5.透明
    self.alphaImgView = [[PDImageView alloc]init];
    self.alphaImgView.backgroundColor = [UIColor blackColor];
    self.alphaImgView.alpha = .7f;
    [self addSubview:self.alphaImgView];
    
    // 4. 创建一对一标志
    self.onebyoneImgView = [[PDImageView alloc]init];
    self.onebyoneImgView.backgroundColor = [UIColor clearColor];
    self.onebyoneImgView.image = [UIImage imageNamed:@"home_onebyone_icon"];
    [self.alphaImgView addSubview:self.onebyoneImgView];
    
    // 2. 创建label
    self.titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    [self.alphaImgView addSubview:self.titleLabel];
    
    // 6.地区
    self.areaLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    [self addSubview:self.areaLabel];
    
    // 7 .nickName
    self.nickNameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    [self addSubview:self.nickNameLabel];
    
    // 8.关注
    self.guanzhuImgView = [[PDImageView alloc]init];
    self.guanzhuImgView.image = [UIImage imageNamed:@"home_onebyone_guanzhu_icon"];
    [self addSubview:self.guanzhuImgView];
    
    // 3. 创建数量
    self.countlabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    [self addSubview:self.countlabel];
}

-(void)setTransferLiveModel:(HomeLiveSingleModel *)transferLiveModel{
    _transferLiveModel = transferLiveModel;
    // 1. img
    self.avatarImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
    [self.avatarImgView uploadImageWithURL:transferLiveModel.img placeholder:nil callback:NULL];
    
    // 2.title
    self.titleLabel.text = transferLiveModel.title;
    CGSize titleSize = [NSString makeSizeWithLabel:self.titleLabel];
    
    // 3. 透明的view
    CGFloat alphaWidth = LCFloat(7) + LCFloat(7) + LCFloat(10) + titleSize.width + LCFloat(7);
    CGFloat alphaHeight = titleSize.height + 2 * LCFloat(5);
    
    self.alphaImgView.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(11) - alphaWidth, LCFloat(11), alphaWidth, alphaHeight);
    self.onebyoneImgView.frame =CGRectMake(LCFloat(7), LCFloat(7), LCFloat(10), LCFloat(10));
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.onebyoneImgView.frame) + LCFloat(7), 0, titleSize.width, self.alphaImgView.size_height);
    self.alphaImgView.layer.cornerRadius = LCFloat(3);
    
    // 4. 地区
    self.areaLabel.text = transferLiveModel.userInfo.city.length?transferLiveModel.userInfo.city:@"大约在火星";
    CGSize areaSize = [NSString makeSizeWithLabel:self.areaLabel];
    self.areaLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(11) - areaSize.width, self.avatarImgView.size_height - LCFloat(11) - areaSize.height, areaSize.width, areaSize.height);
    
    // 5. 昵称
    self.nickNameLabel.text = transferLiveModel.userInfo.nick.length?transferLiveModel.userInfo.nick:[NSString stringWithFormat:@"精灵%@",transferLiveModel.ID];


    CGSize nickSize = [NSString makeSizeWithLabel:self.nickNameLabel];
    self.nickNameLabel.frame = CGRectMake(self.avatarImgView.orgin_x + LCFloat(11), 0, self.avatarImgView.size_width * .7f, nickSize.height);
    self.nickNameLabel.center_y = self.areaLabel.center_y;
    
    // 6. 关注
    self.guanzhuImgView.frame = CGRectMake(self.avatarImgView.orgin_x + LCFloat(11) , LCFloat(11) , LCFloat(10), LCFloat(10));
    
    self.countlabel.text = [NSString stringWithFormat:@"%li人观看",(long)transferLiveModel.count];
    CGSize countSize = [NSString makeSizeWithLabel:self.countlabel];
    self.countlabel.frame = CGRectMake(CGRectGetMaxX(self.guanzhuImgView.frame) + LCFloat(5), 0, countSize.width, countSize.height);
    self.countlabel.center_y = self.guanzhuImgView.center_y;

}


+(CGFloat)calculationCellHeight{
    return kScreenBounds.size.width;
}

@end

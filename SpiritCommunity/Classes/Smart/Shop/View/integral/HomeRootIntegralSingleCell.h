//
//  HomeRootIntegralSingleCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeIntegralSingleModel.h"

@interface HomeRootIntegralSingleCell : UITableViewCell

@property (nonatomic,strong)HomeIntegralSingleModel *transferSingleModel;
@property (nonatomic,assign)CGFloat transferCellheight;

+(CGFloat)calculationCellHeight;

@end

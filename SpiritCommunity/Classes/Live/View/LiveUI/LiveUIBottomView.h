//
//  LiveUIBottomView.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/29.
//  Copyright © 2017年 PPWhale. All rights reserved.
//
// 【最底层的view】
#import <UIKit/UIKit.h>
#import "LiveUIBottomViewSingleModel.h"

@interface LiveUIBottomView : UIView

@property (nonatomic,assign)LiveShowType transferShowType;      /**< 上个页面传递的直播类型*/


@property (nonatomic,strong)UIButton *cameraButton;            /**< 切换相机的按钮*/
@property (nonatomic,strong)UIButton *messageButton;           /**< 发送消息的按钮*/
@property (nonatomic,strong)UIButton *clickZanButton;          /**< 点赞的按钮*/
@property (nonatomic,strong)UIButton *moreButton;              /**< 更多按钮*/
@property (nonatomic,strong)UIButton *micButton;               /**< 麦克风按钮*/

-(void)liveBottomUIViewClickBlock:(void(^)(LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel))block;

@end

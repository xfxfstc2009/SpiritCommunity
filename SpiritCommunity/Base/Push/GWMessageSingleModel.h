//
//  GWMessageSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"

@interface GWMessageSingleModel : FetchModel
@property (nonatomic,copy)NSString *identify;                   /**< 编号*/
@property (nonatomic,assign)NSInteger messageType;              /**< 消息类别*/
@property (nonatomic,copy)NSString *title;                      /**< 标题*/
@property (nonatomic,copy)NSString *content;                    /**< 内容*/
@property (nonatomic,copy)NSString *direct;                     /**< 跳转*/
@property (nonatomic,assign)NSTimeInterval time;                /**< 时间*/
@property (nonatomic,assign)BOOL selected;                      /**< 是否查看*/
@property (nonatomic,copy)NSString *msgImg;                     /**< 消息图片*/

@end

//
//  HomeHouseNormalSingleCell.m
//  17Live
//
//  Created by 裴烨烽 on 2018/2/12.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "HomeHouseNormalSingleCell.h"

@interface HomeHouseNormalSingleCell()
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *houseImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *locationLabel;
@property (nonatomic,strong)UILabel *distanceLabel;
@end

@implementation HomeHouseNormalSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.layer.borderColor =[UIColor colorWithCustomerName:@"浅灰"].CGColor;
    self.convertView.layer.borderWidth = .5f;
    self.convertView.clipsToBounds = YES;
    [self addSubview:self.convertView];

    // 2.创建图片
    self.houseImgView = [[PDImageView alloc]init];
    self.houseImgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.houseImgView];

    // 3. 创建商品名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.nameLabel];

    // 4. 创建商品价格
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.locationLabel.adjustsFontSizeToFitWidth = YES;
//    self.locationLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.locationLabel];

    // 5. 创建数量
    self.distanceLabel = [[UILabel alloc]init];
    self.distanceLabel.backgroundColor = [UIColor clearColor];
    self.distanceLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
//    self.distanceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.distanceLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferModel:(HomeNormalSingleGoodsModel *)transferModel{
    _transferModel = transferModel;

    self.convertView.frame = CGRectMake(LCFloat(11), LCFloat(11), ([HomeHouseNormalSingleCell calculationCellHeight] - 2 * LCFloat(11)), ([HomeHouseNormalSingleCell calculationCellHeight] - 2 * LCFloat(11)));

    __weak typeof(self)weakSelf = self;
    [self.houseImgView uploadImageWithURL:transferModel.shopImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool setTransferSingleAsset:image imgView:strongSelf.houseImgView convertView:strongSelf.convertView];
    }];

    // 名字
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(11) - CGRectGetMaxX(self.houseImgView.frame);
    self.nameLabel.text = transferModel.merchantName;
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, width, [NSString contentofHeightWithFont:self.nameLabel.font]);

    // margin
    CGFloat margin = (self.transferCellHeight - 3 * [NSString contentofHeightWithFont:self.nameLabel.font]) / 4.;
    self.nameLabel.orgin_y = margin;

    self.locationLabel.text = transferModel.address;
    self.locationLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + margin, width, [NSString contentofHeightWithFont:self.locationLabel.font]);

    self.distanceLabel.text = [NSString stringWithFormat:@"门店电话:%@",transferModel.phoneNum];
    self.distanceLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.locationLabel.frame) + margin, width, [NSString contentofHeightWithFont:self.distanceLabel.font]);
    self.distanceLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];

}

+(CGFloat)calculationCellHeight{
    return 100;
}

@end

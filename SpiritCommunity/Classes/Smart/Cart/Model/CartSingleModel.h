//
//  CartSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/22.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol CartSingleModel <NSObject>

@end

@interface CartSingleModel : FetchModel

@property (nonatomic,copy)NSString *goodDetailImg;
@property (nonatomic,assign)NSInteger goodSkuId;
@property (nonatomic,assign)NSInteger goodType;
@property (nonatomic,copy)NSString *goodsId;
@property (nonatomic,copy)NSString *goodsImg;
@property (nonatomic,copy)NSString *goodsJf;
@property (nonatomic,assign)CGFloat goodsMoney;
@property (nonatomic,copy)NSString *goodsName;
@property (nonatomic,assign)NSInteger goodsNum;
@property (nonatomic,copy)NSString *goodsUb;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *invNum;
@property (nonatomic,copy)NSString *shippingWay;
@property (nonatomic,copy)NSString *suppliersId;
@property (nonatomic,copy)NSString *supplyName;
@property (nonatomic,copy)NSString *userId;

@end

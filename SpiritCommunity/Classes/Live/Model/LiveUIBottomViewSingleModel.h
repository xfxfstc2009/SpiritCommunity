//
//  LiveUIBottomViewSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "LiveGiftSingleModel.h"

typedef NS_ENUM(NSInteger,LiveUIBottomViewType) {
    LiveUIBottomViewTypeActionNone = -1,                                        /**< 什么都不做*/
    LiveUIBottomViewTypeActionRoot = 0,                                         /**< 首页点击进入*/
    // 【萌颜特效】
    LiveUIBottomViewTypeActionMengyantexiao = 1,                                /**< 萌颜特效*/
    LiveUIBottomViewTypeActionMengyantexiaoNone = 101,                          /**< 取消激萌特效*/
    LiveUIBottomViewTypeActionMengyantexiaoTuzi = 102,                          /**< 激萌特效兔子*/
    LiveUIBottomViewTypeActionMengyantexiaoBaixuegongzhu = 103,                 /**< 激萌特效白雪公主*/
    // 【美颜】
    LiveUIBottomViewTypeActionBeauty = 2,                                       /**< 美颜*/
    LiveUIBottomViewTypeActionBeautyColor = 201,                                /**< 设置美颜*/
    LiveUIBottomViewTypeActionBeautyWhite = 202,                                /**< 设置美白*/
    // 【滤镜】
    LiveUIBottomViewTypeActionFilter = 3,                                       /**< 滤镜*/
    LiveUIBottomViewTypeActionFilterNone = 301,                                 /**< 清空滤镜*/
    LiveUIBottomViewTypeActionFilter1 = 302,                                    /**< 清新*/
    LiveUIBottomViewTypeActionFilter2 = 303,                                    /**< 唯美*/
    LiveUIBottomViewTypeActionFilter3 = 304,                                    /**< 粉嫩*/
    LiveUIBottomViewTypeActionFilter4 = 305,                                    /**< 美颜美白*/
    LiveUIBottomViewTypeActionFilter5 = 306,                                    /**< 怀旧*/
    LiveUIBottomViewTypeActionFilter6 = 307,                                    /**< 浪漫*/
    LiveUIBottomViewTypeActionFilter7 = 308,                                    /**< 清凉*/
    LiveUIBottomViewTypeActionFilter8 = 309,                                    /**< 日系*/
    LiveUIBottomViewTypeActionFilter9 = 310,                                    /**< 蓝调*/

    // 【分辨率】
    LiveActionTypeResolving = 4,                                                /**< 分辨率*/
    LiveActionTypeResolving1 = 401,                                             /**< 分辨率高清*/
    LiveActionTypeResolving2 = 402,                                             /**< 分辨率标清*/
    LiveActionTypeResolving3 = 403,                                             /**< 分辨率流畅*/
    // 【静音】
    LiveUIBottomViewTypeActionSounds = 501,                                     /**< 开关静音*/
    LiveUIBottomViewTypeActionLighting = 601,                                   /**< 灯光*/
    LiveUIBottomViewTypeActionLensFlip = 701,                                   /**< 镜头翻转*/
    
    // 【声音】
    LiveUIBottomViewTypeActionSpecialSounds = 8,                                /**< 变声*/
    LiveUIBottomViewTypeActionSpecialSoundsNone = 801,                          /**< 原声*/
    LiveUIBottomViewTypeActionSpecialSoundsLolita = 802,                        /**< 萝莉*/
    LiveUIBottomViewTypeActionSpecialSoundsUncle = 803,                         /**< 大叔*/
    LiveUIBottomViewTypeActionSpecialSoundsIntangible = 804,                    /**< 空灵*/
    LiveUIBottomViewTypeActionSpecialSoundsKinder = 805,                        /**< 幼稚园*/
    LiveUIBottomViewTypeActionSpecialSoundsHeavy = 806,                         /**< 重机器*/
    LiveUIBottomViewTypeActionSpecialSoundsPrime= 807,                          /**< 擎天柱*/
    LiveUIBottomViewTypeActionSpecialSoundsAnimal = 808,                        /**< 困兽*/
    LiveUIBottomViewTypeActionSpecialSoundsDialect = 809,                       /**< 方言*/
    LiveUIBottomViewTypeActionSpecialSoundsRobot = 810,                         /**< 机器人*/
    LiveUIBottomViewTypeActionSpecialSoundsFatboy = 811,                        /**< 肥仔*/
    
    // 【底部】
    LiveUIBottomViewTypeActionChat = 24,                                        /**< 聊天*/
    LiveUIBottomViewTypeActionShare = 25,                                       /**< 分享*/
    LiveUIBottomViewTypeActionMore = 26,                                        /**< 更多*/
    LiveUIBottomViewTypeActionGift = 27,                                        /**< 礼物*/
    LiveUIBottomViewTypeActionGiftOther = 2701,                                 /**< 礼物*/
    LiveUIBottomViewTypeActionClickZan = 28,                                    /**< 点赞*/
    LiveUIBottomViewTypeActionEnlarge = 29,                                     /**< 放大与缩小*/
    
    LiveUIBottomViewTypeActionDismiss = 30,                                     /**< 小时*/
};

@interface LiveUIBottomViewSingleModel : FetchModel

@property (nonatomic,copy)NSString *buttonImg;
@property (nonatomic,assign)LiveUIBottomViewType transferUIBottomType;
@property (nonatomic,copy)NSString *title;

// gift
@property (nonatomic,strong)LiveGiftSingleModel *giftSingleModel;       /**< 礼物Model*/




@end

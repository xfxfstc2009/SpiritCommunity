//
//  UserInfoLocationViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/4.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "UserInfoLocationViewController.h"
#import "GWHouseAnnotationView.h"
#import "PlusingAnnotationView.h"
#import "MMHLocationNavigationManager.h"

#define kCalloutViewMargin          -8


@interface UserInfoLocationViewController ()
@property (nonatomic,strong)GWHouseAnnotationView *annotaionView;
@property (nonatomic,strong)MAPointAnnotation *annotation;

@end

@implementation UserInfoLocationViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    
}



#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"当前地址";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"显示地址" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.mapView.selectedAnnotations = @[strongSelf.annotation];
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self mapBaseSetup];
    [self addlocation];
}

-(void)mapBaseSetup{
    [self changeMapCompassShow:NO];                                         // 1. 不显示罗盘
    [self changeMapScaleShow:NO];                                           // 1. 不显示比例尺
    [self changeMapTrafficType:NO];                                         // 1. 显示当前交通情况
    [self changeMapTypeWithType:MAMapTypeStandard];                         // 2. 显示当前普通地图
    [self showLocationWithStatus:YES];                                      // 3. 显示当前定位地址
    [self showLocationWithType:MAUserTrackingModeFollow];                   // 3.1 当前定位模式
//    self.mapView.showsBuildings = YES;                                      // 4. 显示楼块
    self.mapView.zoomLevel = 20;
}


#pragma mark - 显示罗盘
-(void)changeMapCompassShow:(BOOL)isShow{
    self.mapView.showsCompass = isShow;
}

#pragma mark - 显示比例尺
-(void)changeMapScaleShow:(BOOL)isShow{
    self.mapView.showsScale = isShow;
}

#pragma mark - 修改地图类型【标准，卫星，黑夜】
-(void)changeMapTypeWithType:(MAMapType)mapType{
    self.mapView.mapType = mapType;
}

#pragma mark - 修改是否交通
-(void)changeMapTrafficType:(BOOL)show{
    self.mapView.showTraffic = show;
}

#pragma mark - 显示当前定位地址
#pragma mark 定位状态【打开\关闭】
-(void)showLocationWithStatus:(BOOL)isOpen{
    self.mapView.showsUserLocation = isOpen;
}

#pragma mark 定位模式
-(void)showLocationWithType:(MAUserTrackingMode)type{
    [self.mapView setUserTrackingMode:type animated:YES];
//    self.mapView.customizeUserLocationAccuracyCircleRepresentation = YES;           // 去除周围的光圈
}

#pragma mark - 获取当前的大头指针
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation {
    if ([annotation isKindOfClass:[MAUserLocation class]]){         // 【当前的定位地址】
        static NSString *annotationIdentifyWithOne = @"annotationIdentifyWithOne";
        PlusingAnnotationView *annotaionView = (PlusingAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifyWithOne];
        if (!annotaionView){
            annotaionView = [[PlusingAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifyWithOne];
        }
        return annotaionView;
    }
    //    else if ([annotation isKindOfClass:[POIAnnotation class]]){   // 【POI】
    //        static NSString *annotaionIdentifyWithTwo = @"annotaionIdentifyWithTwo";
    //        MAPinAnnotationView *annotationView = (MAPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:annotaionIdentifyWithTwo];
    //        if (!annotationView){
    //            annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotaionIdentifyWithTwo];
    //        }
    //        annotationView.canShowCallout = YES;
    //        annotationView.animatesDrop = YES;
    //        annotationView.draggable = YES;
    //        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    //        return annotationView;
    //    }
    else if ([annotation isKindOfClass:[MAPointAnnotation class]]){
        static NSString *annotationIdentifyWithThr = @"annotationIdentifyWithThr";
        GWHouseAnnotationView *annotaionView = (GWHouseAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifyWithThr];
        if (!annotaionView){
            annotaionView = [[GWHouseAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifyWithThr];
            
        }
        annotaionView.backgroundColor = [UIColor clearColor];
        
        annotaionView.canShowCallout   = NO;
        annotaionView.draggable        = YES;
        if (annotaionView.annotation.coordinate.latitude == 23.107576){
            OtherSearchSingleModel *singleModel = [[OtherSearchSingleModel alloc]init];
            singleModel.nick = @"荷尔蒙";
            singleModel.city = @"";
            singleModel.lat = 23.107576;
            singleModel.lng = 113.338907;
            annotaionView.transferSearchSingleModel = singleModel;
        } else {
            annotaionView.transferSearchSingleModel = self.transferSearchSingleModel;
        }

        annotaionView.calloutOffset    = CGPointMake(annotaionView.center_x, annotaionView.center_y - LCFloat(15));
        
        //        annotaionView.portrait = [UIImage imageNamed:@"center_icon_location"];
        annotaionView.image = [UIImage imageNamed:@"home_userinfo_location"];

        __weak typeof(self)weakSelf = self;
        [annotaionView locationShow:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            MMHLocationNavigationManager *locationNav = [[MMHLocationNavigationManager alloc]init];
            [locationNav showActionSheetWithView:strongSelf.view shopName:annotaionView.transferSearchSingleModel.nick shopLocationLat:annotaionView.transferSearchSingleModel.lat shopLocationLon:annotaionView.transferSearchSingleModel.lng];
        }];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            CGRect frame = [annotaionView convertRect:annotaionView.calloutView.frame toView:self.mapView];
            annotaionView.frame = CGRectMake(frame.origin.x, frame.origin.y, LCFloat(30), LCFloat(30));
            [annotaionView setSelected:YES animated:YES];
        });
        return annotaionView;
    }
    return nil;
}

#pragma mark - 点击方法
- (void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view {
    if ([view isKindOfClass:[GWHouseAnnotationView class]]){
        GWHouseAnnotationView *cusView = (GWHouseAnnotationView *)view;
        CGRect frame = [cusView convertRect:cusView.calloutView.frame toView:self.mapView];
        
        frame = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin));
        
        if (!CGRectContainsRect(self.mapView.frame, frame))
        {
            /* Calculate the offset to make the callout view show up. */
//            CGSize offset = [self offsetToContainRect:frame inRect:self.mapView.frame];

//            CGPoint screenAnchor = [self.mapView getMapStatus].screenAnchor;
//            CGPoint theCenter = CGPointMake(self.mapView.bounds.size.width * screenAnchor.x, self.mapView.bounds.size.height * screenAnchor.y);
//            theCenter = CGPointMake(theCenter.x - offset.width, theCenter.y - offset.height);

//            CLLocationCoordinate2D coordinate = [self.mapView convertPoint:theCenter toCoordinateFromView:self.mapView];

//            [self.mapView setCenterCoordinate:coordinate animated:YES];
        }
    }
    
}


- (CGSize)offsetToContainRect:(CGRect)innerRect inRect:(CGRect)outerRect
{
    CGFloat nudgeRight = fmaxf(0, CGRectGetMinX(outerRect) - (CGRectGetMinX(innerRect)));
    CGFloat nudgeLeft = fminf(0, CGRectGetMaxX(outerRect) - (CGRectGetMaxX(innerRect)));
    CGFloat nudgeTop = fmaxf(0, CGRectGetMinY(outerRect) - (CGRectGetMinY(innerRect)));
    CGFloat nudgeBottom = fminf(0, CGRectGetMaxY(outerRect) - (CGRectGetMaxY(innerRect)));
    return CGSizeMake(nudgeLeft ?: nudgeRight, nudgeTop ?: nudgeBottom);
}







#pragma mark - 添加坐标
-(void)addlocation{
    self.annotation = [[MAPointAnnotation alloc] init];
    CLLocationCoordinate2D location;
    location.latitude = self.transferSearchSingleModel.lat;
    location.longitude = self.transferSearchSingleModel.lng;
    self.annotation.coordinate = location;
    self.annotation.title    = self.transferSearchSingleModel.nick;
    self.annotation.subtitle = self.transferSearchSingleModel.city;
    


    CLLocationCoordinate2D location2;
    location2.latitude = 23.107576;
    location2.longitude = 113.338907;

    MAPointAnnotation *annotation2 = [[MAPointAnnotation alloc] init];
    annotation2.coordinate = location2;

    [self.mapView addAnnotations:@[self.annotation]];
    [self.mapView showAnnotations:@[self.annotation] animated:YES];
//

}



@end

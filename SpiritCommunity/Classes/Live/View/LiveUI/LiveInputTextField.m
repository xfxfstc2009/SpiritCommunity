//
//  LiveInputTextField.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/1.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveInputTextField.h"
#import <TILLiveSDK/TILLiveSDK.h>
#import <ImSDK/ImSDK.h>

static char inputTextFieldChangeKey;
static char btnActionClickKey;
@interface LiveInputTextField()
@property (nonatomic,strong)UIButton *actionButton;                 /**< 活动按钮*/

@end

@implementation LiveInputTextField

-(instancetype)initWithFrame:(CGRect)frame actionChangeBlock:(void(^)(UITextField *inputTextField))block btnClick:(void(^)(UITextField *inputTextField))block1{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        objc_setAssociatedObject(self, &btnActionClickKey, block1, OBJC_ASSOCIATION_COPY);
        objc_setAssociatedObject(self, &inputTextFieldChangeKey, block, OBJC_ASSOCIATION_COPY);
        [self createInputView];
    }
    return self;
}

#pragma mark - createView
-(void)createInputView{
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
    lineView.frame = CGRectMake(0, 0, kScreenBounds.size.width, .5f);
    [self addSubview:lineView];
    
    // 1. 创建view
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.inputTextField addTarget:self action:@selector(textFieldDidChanged) forControlEvents:UIControlEventEditingChanged];
    self.inputTextField.frame = CGRectMake(LCFloat(11), LCFloat(3), kScreenBounds.size.width - 3 * LCFloat(11) - LCFloat(70), self.size_height - 2 * LCFloat(3));
    self.inputTextField.borderStyle = UITextBorderStyleRoundedRect;
    [self addSubview:self.inputTextField];
    
    // 2. 按钮
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self.actionButton setTitle:@"发送" forState:UIControlStateNormal];
    self.actionButton.frame = CGRectMake(CGRectGetMaxX(self.inputTextField.frame) + LCFloat(11), self.inputTextField.orgin_y, LCFloat(70), self.inputTextField.size_height);
    self.actionButton.layer.cornerRadius = LCFloat(4);
    self.actionButton.clipsToBounds = YES;
    __weak typeof(self)weakSelf = self;
    self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf actionClickBlockSendMsg:strongSelf.inputTextField.text];

        void(^block)(UITextField *inputTextField) = objc_getAssociatedObject(strongSelf, &btnActionClickKey);
        if (block){
            block(strongSelf.inputTextField);
        }
    }];
    [self addSubview:self.actionButton];
}

-(void)textFieldDidChanged{
    void (^block)(UITextField *inputTextField) = objc_getAssociatedObject(self, &inputTextFieldChangeKey);
    if (block){
        block(self.inputTextField);
    }
}

-(void)actionClickBlockSendMsg:(NSString *)info{
    ILVLiveTextMessage *msg = [[ILVLiveTextMessage alloc]init];
    msg.type = ILVLIVE_IMTYPE_GROUP;
    msg.text = info;
    TIMUserProfile *personInfo = [[TIMUserProfile alloc]init];
    personInfo.nickname = @"自己";
    msg.senderProfile = personInfo;
    __weak typeof(self)weakSelf = self;
    [[TILLiveManager getInstance] sendTextMessage:msg succ:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.transferActionView.messageView viewSendMsg:msg];
        [StatusBarManager statusBarHidenWithText:@"发送成功"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        NSLog(@"createRoom fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg);
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"发送失败%@",errMsg]];
    }];
}


-(void)cleanInfo{
    self.inputTextField.text = @"";
}
@end

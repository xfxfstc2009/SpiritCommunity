//
//  LiveReleaseShowViewController+IMListener.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController+IMListener.h"
#import "UserViewManager.h"

@implementation LiveReleaseShowViewController (IMListener)

#pragma mark - 收到消息的回调
-(void)onTextMessage:(ILVLiveTextMessage *)msg{
    [self sendMsg:msg];
}

#pragma mark 自定义消息的回调
- (void)onCustomMessage:(ILVLiveCustomMessage *)msg{
    if (msg.type == ILVLIVE_IMTYPE_GROUP) { //非当前直播间的群消息，不处理
        if (![self.transferLiveSingleModel.house_id isEqualToString:msg.recvId]) {
            return;
        }
    }
    int cmd = msg.cmd;
    if (msg.type == ILVLIVE_IMTYPE_C2C) {
        if (cmd == AVIMCMD_Multi_Host_Invite){
            NSString *title = [NSString stringWithFormat:@"收到%@视频邀请",msg.sendId];
            
            NSArray *btnArr = @[kSxbRole_InteractHDTitle,kSxbRole_InteractSDTitle,kSxbRole_InteractLDTitle];
            __weak typeof(self)weakSelf = self;
            [[UIActionSheet actionSheetWithTitle:title buttonTitles:btnArr callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (buttonIndex == 0){          // 取消
                    [strongSelf rejectToVideo:nil];
                } else if (buttonIndex == 1){
                    [self upToVideo:nil roleName:kSxbRole_InteractHD];
                } else if(buttonIndex == 2){
                    [self upToVideo:nil roleName:kSxbRole_InteractSD];
                } else if (buttonIndex == 3){
                    [self upToVideo:nil roleName:kSxbRole_InteractLD];
                }
            }]showInView:self.view];;
        } else if (cmd == AVIMCMD_Multi_Interact_Refuse){           // 对方拒绝
            [[UIAlertView alertViewWithTitle:@"拒绝视频邀请" message:[NSString stringWithFormat:@"%@拒绝了你的邀请",msg.sendId] buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [[UserViewManager shareInstance] removePlaceholderView:msg.sendId];
            }]show];;
        } else if (cmd == AVIMCMD_Multi_Host_CancelInvite){         // 取消互动
            [[UIAlertView alertViewWithTitle:@"取消视频互动" message:msg.sendId buttonTitles:@[@"确定"] callBlock:NULL]show];
        } else if (cmd == ILVLIVE_IMCMD_LINKROOM_REQ){      // 跨房连麦克风
            [self recvLinkRoomReq:msg.sendId];
        } else if (cmd == ILVLIVE_IMCMD_LINKROOM_ACCEPT){   // 对方同意连麦
             [self recvLinkRoomAccept:msg];
        }  else if (cmd == ILVLIVE_IMCMD_LINKROOM_REFUSE){
            [self recvLinkRoomRefuse:msg.sendId];
        }  else if (cmd == ILVLIVE_IMCMD_LINKROOM_LIMIT){
            [self recvLinkRoomLimit:msg.sendId];
        } else {
        
        }
    } else if (msg.type == ILVLIVE_IMTYPE_GROUP){
        switch (cmd) {
            case AVIMCMD_Praise:                // 点赞
                [[NSNotificationCenter defaultCenter] postNotificationName:kUserParise_Notification object:nil];
                break;
            case AVIMCMD_Multi_CancelInteract:
                if ([self isSendToSelf:msg])
                {
                    [self downToVideo:nil];
                }
                break;
            case AVIMCMD_EnterLive:                 // 用户加入直播
                [self sendMsg:msg];
                break;
            case AVIMCMD_ExitLive:                  // 用户退出直播
                [[NSNotificationCenter defaultCenter] postNotificationName:kGroupDelete_Notification object:nil];
                break;
            default:
                break;
        }
    }
}


#pragma mark - Action
-(void)recvLinkRoomReq:(NSString *)fromId{
    //界面上已经有3个画面了 或 自己不是主播，则回复拒绝
    NSString *loginUser = [[ILiveLoginManager getInstance] getLoginId];
    if ([UserViewManager shareInstance].total >= kMaxUserViewCount) {
        [[TILLiveManager getInstance] refuseLinkRoom:fromId succ:^{
            [StatusBarManager statusBarHidenWithText:@"已经拒绝连麦，原因：可能连麦数量上限或拒绝"];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"拒绝失败，失败原因%@",errMsg]];
        }];
        return;
    }
    NSString *title = [NSString stringWithFormat:@"收到来自%@的跨房连麦邀请",fromId];
    __weak typeof(self)weakSelf = self;
    [[UIAlertView alertViewWithTitle:@"连麦邀请" message:title buttonTitles:@[@"同意",@"拒绝"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){          // 已经同意
            [strongSelf acceptionLinkRoom:fromId];
        } else {                        // 已经拒绝
            [strongSelf refuseLinkRoom:fromId];
        }
    }]show];
}
// 同意连麦
-(void)acceptionLinkRoom:(NSString *)fromId{
//    __weak typeof(self)weakSelf = self;
    [[TILLiveManager getInstance] acceptLinkRoom:fromId succ:^{
        [StatusBarManager statusBarHidenWithText:@"已同意"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"连麦错误,%@",errMsg]];
    }];
}
// 拒绝连麦
-(void)refuseLinkRoom:(NSString *)fromId{
    [[TILLiveManager getInstance] refuseLinkRoom:fromId succ:^{
        [StatusBarManager statusBarHidenWithText:@"已拒绝"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"连麦错误,%@",errMsg]];
    }];
}

#pragma mark - 收到链接消息
-(void)recvLinkRoomAccept:(ILVLiveCustomMessage *)msg{
    if (!msg.data){     // 无房间号。不在房间中
        NSString *title = [NSString stringWithFormat:@"%@不在房间中",msg.sendId];
        [[UIAlertView alertViewWithTitle:title message:nil buttonTitles:@[@"好吧"] callBlock:NULL]show];
    }
    //界面上已经有3个画面了，则回复拒绝
    if ([UserViewManager shareInstance].total >= kMaxUserViewCount) {
        NSString *msgInfo = [NSString stringWithFormat:@"%@同意了你的跨房连麦请求，但是你本身的界面视图已经达到视图显示个数的上限了",msg.sendId];
        [UIAlertView alertViewWithTitle:@"超出视图个数" message:msgInfo buttonTitles:@[@"好的"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            ILVLiveCustomMessage *overLimitMsg = [[ILVLiveCustomMessage alloc] init];
            overLimitMsg.type = ILVLIVE_IMTYPE_C2C;
            overLimitMsg.cmd = ILVLIVE_IMCMD_LINKROOM_LIMIT;
            overLimitMsg.recvId = msg.sendId;
            [[TILLiveManager getInstance] sendOnlineCustomMessage:overLimitMsg succ:nil failed:nil];
        }];
        return;
    }
    [self actionToLinkManager:msg];
}

// 进行跨房连麦
-(void)actionToLinkManager:(ILVLiveCustomMessage *)msg{
    NSString *title = [NSString stringWithFormat:@"%@同意跨房连麦",msg.sendId];
    __weak typeof(self)weakSelf = self;
    [[UIAlertView alertViewWithTitle:title message:@"是否发起连麦" buttonTitles:@[@"确定",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){
            NSString *roomId = [[NSString alloc] initWithData:msg.data encoding:NSUTF8StringEncoding];
            [[TILLiveManager getInstance] linkRoom:[roomId intValue] identifier:msg.sendId authBuff:@"D8A4FA924C000F6E65C4B7863AD475160BFFF6BDD932E7D782B65236C653837470A1156EDCBC056EFE85624A09790325F3223FB60D53702F" succ:^{
                [StatusBarManager statusBarHidenWithText:@"链接成功"];
            } failed:^(NSString *module, int errId, NSString *errMsg) {
                NSString *msgInfo = [NSString stringWithFormat:@"Module=%@,code=%d,Msg=%@",module,errId,errMsg];
                [StatusBarManager statusBarHidenWithText:msgInfo];
            }];
        }
    }]show];
}

// 拒绝跨房连麦
- (void)recvLinkRoomRefuse:(NSString *)fromId {
    NSString *title = [NSString stringWithFormat:@"%@拒绝跨房连麦",fromId];
    [[UIAlertView alertViewWithTitle:title message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
}

// 达到上限
- (void)recvLinkRoomLimit:(NSString *)fromId {
    NSString *msg = [NSString stringWithFormat:@"%@的房间跨房连麦成员已达上限,无法建立连麦",fromId];
    [[UIAlertView alertViewWithTitle:msg message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
}

- (BOOL)isSendToSelf:(ILVLiveCustomMessage *)msg {
    NSString *recvId = [[NSString alloc] initWithData:msg.data encoding:NSUTF8StringEncoding];
    NSString *selfId = [[ILiveLoginManager getInstance] getLoginId];
    
    return [recvId isEqualToString:selfId];
}

#pragma mark 上麦
- (void)upToVideo:(id)sender roleName:(NSString *)role{
    ILVLiveCustomMessage *msg = [[ILVLiveCustomMessage alloc] init];
    msg.type = ILVLIVE_IMTYPE_C2C;
    msg.cmd = (ILVLiveIMCmd)AVIMCMD_Multi_Interact_Join;
    msg.recvId = self.transferLiveSingleModel.userInfo.ID;
    
    [[TILLiveManager getInstance] sendCustomMessage:msg succ:^{                 // 1. 发送自定义消息
        [StatusBarManager statusBarHidenWithText:@"发送消息成功"];
        ILiveRoomManager *roomManager = [ILiveRoomManager getInstance];
        [roomManager changeRole:role succ:^{                                    // 2. 发布分辨率
            [StatusBarManager statusBarHidenWithText:@"修改分辨率"];
            [roomManager enableCamera:CameraPosFront enable:YES succ:^{
                [StatusBarManager statusBarHidenWithText:@"打开当前的摄像头"];
                [roomManager enableMic:YES succ:^{
                    [StatusBarManager statusBarHidenWithText:@"打开当前麦克风"];
                    [TXLiveManager sharedAccountModel].role = role;
                    [[NSNotificationCenter defaultCenter] postNotificationName:kUserUpVideo_Notification object:role];
                    
                } failed:^(NSString *module, int errId, NSString *errMsg) {
                    [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"麦克风打开失败，失败原因%@",errMsg]];
                }];
            } failed:^(NSString *module, int errId, NSString *errMsg) {
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"摄像头打开失败，失败原因%@",errMsg]];
            }];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"分辨率修改失败，失败原因%@",errMsg]];
        }];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"发送消息失败，失败原因%@",errMsg]];
    }];
}


//下麦
- (void)downToVideo:(id)sender {
    ILVLiveCustomMessage *msg = [[ILVLiveCustomMessage alloc] init];
    msg.type = ILVLIVE_IMTYPE_GROUP;
    msg.cmd = (ILVLiveIMCmd)AVIMCMD_Multi_CancelInteract;
    msg.recvId = [[ILiveRoomManager getInstance] getIMGroupId];
    ILiveRoomManager *manager = [ILiveRoomManager getInstance];
    [[TILLiveManager getInstance] sendCustomMessage:msg succ:^{
        [StatusBarManager statusBarHidenWithText:@"下麦成功"];
        [manager changeRole:kSxbRole_GuestHD succ:^{
            [StatusBarManager statusBarHidenWithText:@"下麦切换分辨率成功"];
            cameraPos pos = [[ILiveRoomManager getInstance] getCurCameraPos];
            [manager enableCamera:pos enable:NO succ:^{
                [StatusBarManager statusBarHidenWithText:@"关闭摄像头成功"];
                [manager enableMic:NO succ:^{
                    [StatusBarManager statusBarHidenWithText:@"关闭麦克风成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kUserDownVideo_Notification object:nil];
                } failed:^(NSString *module, int errId, NSString *errMsg) {
                   [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"麦克风关闭失败，失败原因%@",errMsg]];
                }];
            } failed:^(NSString *module, int errId, NSString *errMsg) {
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"摄像头关闭失败，失败原因%@",errMsg]];
            }];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"分辨率切换失败，失败原因%@",errMsg]];
        }];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"下麦失败，失败原因%@",errMsg]];
    }];
}

//拒绝上麦
- (void)rejectToVideo:(id)sender {
    ILVLiveCustomMessage *msg = [[ILVLiveCustomMessage alloc] init];
    msg.cmd = (ILVLiveIMCmd)AVIMCMD_Multi_Interact_Refuse;
    msg.recvId = self.transferLiveSingleModel.userInfo.ID;
    msg.type = ILVLIVE_IMTYPE_C2C;
    [[TILLiveManager getInstance] sendCustomMessage:msg succ:^{
        [StatusBarManager statusBarHidenWithText:@"自定义消息发送成功"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"自定义消息发送失败,%@",errMsg]];
    }];
}



#pragma mark 其他信息回调
- (void)onOtherMessage:(TIMMessage *)msg{
    
}





@end

//
//  QiandaoViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/17.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "QiandaoViewController.h"
#import "QiandaoSingleModel.h"
#import "HYCalendarView.h"

@interface QiandaoViewController()
@property (nonatomic,strong)UIButton *qiandaoButton;
@property (nonatomic,strong)UILabel *qiandaolabel1;
@property (nonatomic,strong)UILabel *qiandaoLabel2;
@property (nonatomic,strong)PDImageView *lineImgView;
@property (nonatomic,strong)HYCalendarView *qiandaoView;
@end

@implementation QiandaoViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
    [self sendRequestTogetInfo];
}

#pragma mark -pageSetting
-(void)pageSetting{
    self.barMainTitle = @"签到";
    self.view.backgroundColor = [UIColor colorWithCustomerName:@"金"];
//    [self rightBarButtonWithTitle:@"攻略" barNorImage:nil barHltImage:nil action:^{
//        [self.qiandaoView setStyle_Today_Signed:self.qiandaoView.qiandaoSingleView];
//    }];
}


#pragma mark -
-(void)createView{
    self.qiandaoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.qiandaoButton.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(150));
    [self.qiandaoButton setImage:[UIImage imageNamed:@"bg_qiandao"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.qiandaoButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToSign];
    }];
    [self.view addSubview:self.qiandaoButton];
    
    self.qiandaolabel1 = [GWViewTool createLabelFont:@"标题" textColor:@"黑"];
    self.qiandaolabel1.frame = CGRectMake(0, CGRectGetMaxY(self.qiandaoButton.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.qiandaolabel1.font]);
    self.qiandaolabel1.textAlignment = NSTextAlignmentCenter;
    self.qiandaolabel1.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.qiandaolabel1];
    
    self.qiandaoLabel2 = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.qiandaoLabel2.frame = CGRectMake(0, CGRectGetMaxY(self.qiandaolabel1.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.qiandaoLabel2.font]);
    self.qiandaoLabel2.textAlignment = NSTextAlignmentCenter;
    self.qiandaoLabel2.text = @"连续签到达到指定天数可获得以下额外奖励哦";
    [self.view addSubview:self.qiandaoLabel2];
    
//    self.lineImgView = [[PDImageView alloc]init];
//    self.lineImgView.frame = CGRectMake(0, CGRectGetMaxY(self.qiandaoLabel2.frame) + LCFloat(11), LCFloat(351), LCFloat(50));
//    self.lineImgView.center_x = kScreenBounds.size.width / 2.;
//    self.lineImgView.image = [UIImage imageNamed:@"bg_ewjl"];
//    [self.view addSubview:self.lineImgView];

    self.qiandaoView = [[HYCalendarView alloc]init];
    self.qiandaoView.frame = CGRectMake(0, kScreenBounds.size.height - 300 - 64, kScreenBounds.size.width, 300);
    [self.view addSubview:self.qiandaoView];
    
    

//    NSDateComponents *comp = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
//    //日期点击事件
//    __weak typeof(HYCalendarView) *weakDemo = self.qiandaoView;
//    self.qiandaoView.calendarBlock =  ^(NSInteger day, NSInteger month, NSInteger year){
//        if ([comp day]==day) {

//            [weakDemo setStyle_Today_Signed:weakDemo.dayButton];
//        }
//    };
}


-(void)sendRequestTogetInfo{
//    __weak typeof(self)weakSelf = self;
//    QiandaoSingleModel *qiandaoSingleModel = (QiandaoSingleModel *)responseObject;

    // 1. 标题
    NSString *info = [NSString stringWithFormat:@"今日签到可领取%@",@"10金币"];
    self.qiandaolabel1.attributedText = [Tool rangeLabelWithContent:info hltContentArr:@[[NSString stringWithFormat:@"%@",@"10金币"]] hltColor:[UIColor colorWithCustomerName:@"红"] normolColor:[UIColor colorWithCustomerName:@"黑"]];
    // 2. 是否已签到

    if ([Tool userDefaultGetWithKey:@"qiandao"].length){
        [self.qiandaoButton setImage:[UIImage imageNamed:@"bg_yiqiandao"] forState:UIControlStateNormal];
        self.qiandaoButton.userInteractionEnabled = NO;
    } else {
        [self.qiandaoButton setImage:[UIImage imageNamed:@"bg_qiandao"] forState:UIControlStateNormal];
        self.qiandaoButton.userInteractionEnabled = YES;
    }

    //设置已经签到的天数日期
    NSMutableArray* _signArray = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < 28;i++){
        if (i == 2 || i == 7){
            [_signArray addObject:[NSNumber numberWithInteger:i]];
        }
    }
    //
    self.qiandaoView.signArray = _signArray;
    self.qiandaoView.date = [NSDate date];
}

-(void)sendRequestToSign{
//    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusBarHidenWithText:@"正在签到中"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [Tool userDefaulteWithKey:@"qiandao" Obj:@"qiandao"];
        [StatusBarManager statusBarHidenWithText:@"签到成功"];
        [self.qiandaoView setStyle_Today_Signed:self.qiandaoView.qiandaoSingleView];
        [self.qiandaoButton setImage:[UIImage imageNamed:@"bg_yiqiandao"] forState:UIControlStateNormal];
    });
}


@end

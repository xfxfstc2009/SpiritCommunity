//
//  UITextField+CustomShowBar.m
//  RegenerationPlan
//
//  Created by SmartMin on 15/11/12.
//  Copyright © 2015年 baimifan. All rights reserved.
//

#import "UITextField+CustomShowBar.h"
#import <objc/runtime.h>
static char cancelCallbackBolckKey;
static char checkCallbackBlockKey;
static char leftButtonKey;
static char setInputTextFieldKey;
typedef NS_ENUM(NSInteger,barType) {
    barTypeNormal,                  /**< 默认*/
    barTypeNext,                    /**< 下一步内容*/
};

@implementation UITextField (CustomShowBar)

-(void)showBarCallback:(void(^)(UITextField *textField,NSInteger buttonIndex,UIButton *button))callBack{
    UIView *toolbarView = [self createInputAccessoryViewWithType:barTypeNormal];
    
    if (callBack){
        objc_setAssociatedObject(self, &cancelCallbackBolckKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
        objc_setAssociatedObject(self, &checkCallbackBlockKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    [self setInputAccessoryView:toolbarView];
}


#pragma mark 创建inputAccessoryView
-(UIView *)createInputAccessoryViewWithType:(barType)barType{
    UIView *toolbarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 40)];
    toolbarView.backgroundColor = [UIColor hexChangeFloat:@"F0F1F2"];
    toolbarView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0, 0, kScreenBounds.size.width , .5f);
    topBorder.backgroundColor = [UIColor colorWithWhite:0.678 alpha:1].CGColor;
    [toolbarView.layer addSublayer:topBorder];
    
    UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    checkButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    checkButton.layer.cornerRadius = LCFloat(4);
    checkButton.clipsToBounds = YES;
    [checkButton setTitle:@"确定" forState:UIControlStateNormal];
    [checkButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    [checkButton addTarget:self action:@selector(checkClick) forControlEvents:UIControlEventTouchUpInside];
    checkButton.frame = CGRectMake(kScreenBounds.size.width - 80, 0, 80, 40);
    [toolbarView addSubview:checkButton];
    
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.frame = CGRectMake(LCFloat(11), 5, kScreenBounds.size.width - LCFloat(11) - 80 - 2 * LCFloat(11), 40);
    self.inputTextField.delegate = self;
    self.inputTextField.placeholder = @"请输入您的信息";
    [toolbarView addSubview:self.inputTextField];
    
    return toolbarView;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

-(void)cancelClick{
    void (^callBack)(UITextField *textField,NSInteger buttonIndex,UIButton *button) = objc_getAssociatedObject(self, &cancelCallbackBolckKey);
    UIButton *button = objc_getAssociatedObject(self, &leftButtonKey);
    if (callBack){
        callBack(self,0,button);
    }
}

-(void)checkClick{
    void (^callBack)(UITextField *textField,NSInteger buttonIndex,UIButton *button) = objc_getAssociatedObject(self, &checkCallbackBlockKey);
    if (callBack){
        callBack(self,1,nil);
    }
}

#pragma mark - GetSet
-(void)setInputTextField:(UITextField *)inputTextField{
    objc_setAssociatedObject(self, &setInputTextFieldKey, inputTextField, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(UITextField *)inputTextField{
    UITextField *inputtextField = objc_getAssociatedObject(self, &setInputTextFieldKey);
    return inputtextField;

}


@end

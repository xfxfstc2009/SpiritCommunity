//
//  LiveShowUserSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/27.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface LiveShowUserSingleModel : FetchModel

@property (nonatomic,copy)NSString *area;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *city;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *location;
@property (nonatomic,copy)NSString *nick;
@property (nonatomic,copy)NSString *province;

@end

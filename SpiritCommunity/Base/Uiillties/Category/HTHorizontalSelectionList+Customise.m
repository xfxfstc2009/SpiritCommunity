//
//  HTHorizontalSelectionList+Customise.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/3.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "HTHorizontalSelectionList+Customise.h"

static char dataSourceArrKey;
static char didSelectKey;

static char bridageNumberKey;
static char bridageIndexKey;

@implementation HTHorizontalSelectionList (Customise)

+(HTHorizontalSelectionList *)createSegmentWithDataSource:(NSArray *)dataSource actionClickBlock:(void(^)(HTHorizontalSelectionList *segment, NSInteger index))block{

    PDSelectionListTitleView *segment = [[PDSelectionListTitleView alloc]initWithFrame: CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(40))];
    segment.dataSourceArr = dataSource;

    segment.delegate = segment;
    segment.dataSource = segment;
    [segment setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    segment.selectionIndicatorColor = [UIColor colorWithCustomerName:@"黑"];
    segment.bottomTrimColor = [UIColor clearColor];
    segment.isNotScroll = YES;
    [segment setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    segment.backgroundColor = [UIColor clearColor];
    
    if (block){
         objc_setAssociatedObject(segment, &didSelectKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    return segment;
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.dataSourceArr.count;
}

-(NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index{
    NSString *showString = [self.dataSourceArr objectAtIndex:index];
    if ([AccountModel sharedAccountModel].isShenhe){
        if ([showString isEqualToString:@"一对一"]){
            return @"视频";
        } else {
            return showString;
        }
    }
    return showString;
}

-(void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index{
    void(^block)(HTHorizontalSelectionList *segment,NSInteger index) = objc_getAssociatedObject(selectionList, &didSelectKey);
    if(block){
        block(selectionList,index);
    }
}

- (nullable NSString *)selectionList:(HTHorizontalSelectionList *)selectionList badgeValueForItemWithIndex:(NSInteger)index{
    if (index == self.bridageIndex){
        if (self.bridageIndex == -1){
            return nil;
        } else {
            return self.bridageNumber;
        }
    } else{
        return nil;
    }
}

-(void)cleanSegmentBridge{
    self.bridageIndex = -1;
    self.bridageNumber = nil;
    [self reloadData];
}


#pragma mark - GetSet
-(void)setDataSourceArr:(NSArray *)dataSourceArr{
    objc_setAssociatedObject(self, &dataSourceArrKey, dataSourceArr, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSArray *)dataSourceArr{
    NSArray *dataSourceArr = objc_getAssociatedObject(self, &dataSourceArrKey);
    return dataSourceArr;
}

-(void)setBridageNumber:(NSString *)bridageNumber{
    objc_setAssociatedObject(self, &bridageNumberKey, bridageNumber, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSString *)bridageNumber{
    NSString *bridageNumber = objc_getAssociatedObject(self, &bridageNumberKey);
    return bridageNumber;
}

-(void)setBridageIndex:(NSInteger)bridageIndex{
    objc_setAssociatedObject(self, &bridageIndexKey, @(bridageIndex), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSInteger)bridageIndex{
    NSInteger bridageIndex = [objc_getAssociatedObject(self, &bridageIndexKey) integerValue];
    return bridageIndex;
}



@end

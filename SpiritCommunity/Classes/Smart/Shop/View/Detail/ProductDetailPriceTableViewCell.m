//
//  ProductDetailPriceTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "ProductDetailPriceTableViewCell.h"

@interface ProductDetailPriceTableViewCell()
@property (nonatomic,strong)UILabel *flagLabel;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *oldPriceLabel;
@property (nonatomic,strong)UILabel *jifenLabel;

@end

@implementation ProductDetailPriceTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.flagLabel = [[UILabel alloc]init];
    self.flagLabel.backgroundColor = [UIColor clearColor];
    self.flagLabel.font = [[UIFont systemFontOfCustomeSize:15]boldFont];
    self.flagLabel.text = @"￥";
    CGSize flagSize = [self.flagLabel.text sizeWithCalcFont:self.flagLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.flagLabel.font])];
    self.flagLabel.frame = CGRectMake(LCFloat(11), LCFloat(20), flagSize.width, [NSString contentofHeightWithFont:self.flagLabel.font]);
    self.flagLabel.textColor = [UIColor colorWithCustomerName:@"棕黑"];
    [self addSubview:self.flagLabel];
    
    // 2. 创建价格
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [[UIFont systemFontOfCustomeSize:40]boldFont];
    self.priceLabel.textColor = [UIColor colorWithCustomerName:@"棕黑"];
    [self addSubview:self.priceLabel];
    
    // 3. 创建
    self.oldPriceLabel = [[UILabel alloc]init];
    self.oldPriceLabel.backgroundColor = [UIColor clearColor];
    self.oldPriceLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.oldPriceLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self addSubview:self.oldPriceLabel];
    
    // 4. 积分label
    self.jifenLabel = [[UILabel alloc]init];
    self.jifenLabel.backgroundColor = [UIColor clearColor];
    self.jifenLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.jifenLabel.font = self.oldPriceLabel.font;
    [self addSubview:self.jifenLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferProductDetailModel:(ProductDetailRootModel *)transferProductDetailModel{
    _transferProductDetailModel = transferProductDetailModel;
    
    // 1. 商品价格
    if (transferProductDetailModel){
        self.priceLabel.text = [NSString stringWithFormat:@"%.2f",transferProductDetailModel.goodBaseInfo.salePriceRMB];
        CGSize priceSize= [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
        self.priceLabel.frame = CGRectMake(CGRectGetMaxX(self.flagLabel.frame), LCFloat(11), priceSize.width, [NSString contentofHeightWithFont:self.priceLabel.font]);
        
        // 2. 创建老价格
        self.oldPriceLabel.attributedText = [Tool addLineWithStr:[NSString stringWithFormat:@"%.2f",transferProductDetailModel.goodBaseInfo.marketPrice]];
        CGSize oldPriceSize = [[NSString stringWithFormat:@"%.2f",transferProductDetailModel.goodBaseInfo.marketPrice] sizeWithCalcFont:self.oldPriceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.oldPriceLabel.font])];
        self.oldPriceLabel.frame = CGRectMake(CGRectGetMaxX(self.priceLabel.frame) + LCFloat(11), CGRectGetMaxY(self.priceLabel.frame) - [NSString contentofHeightWithFont:self.oldPriceLabel.font], oldPriceSize.width, [NSString contentofHeightWithFont:self.oldPriceLabel.font]);
        
        
        self.flagLabel.orgin_y = CGRectGetMaxY(self.priceLabel.frame) - [NSString contentofHeightWithFont:self.flagLabel.font] - LCFloat(10);
        
        // 3. 创建normalLabel
        NSString *info = [NSString stringWithFormat:@"购买可获得%.2f积分",transferProductDetailModel.goodBaseInfo.salePriceRMB];
        self.jifenLabel.attributedText = [Tool rangeLabelWithContent:info hltContentArr:@[[NSString stringWithFormat:@"%.2f",transferProductDetailModel.goodBaseInfo.salePriceRMB]] hltColor:[UIColor colorWithCustomerName:@"棕黑"] normolColor:[UIColor colorWithCustomerName:@"浅灰"]];
        CGSize jifenSize = [info sizeWithCalcFont:self.jifenLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.jifenLabel.font])];
        self.jifenLabel.frame = CGRectMake(CGRectGetMaxX(self.oldPriceLabel.frame) + LCFloat(7), self.oldPriceLabel.orgin_y, jifenSize.width, [NSString contentofHeightWithFont:self.jifenLabel.font]);
        
    }
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont systemFontOfCustomeSize:40.] boldFont]];
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end

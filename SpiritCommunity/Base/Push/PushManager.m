//
//  PushManager.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/27.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "PushManager.h"
#import <AlicloudUtils/AlicloudUtils.h>
#import "SBJSON.h"
#

#ifdef IS_IOS10_LATER
//#import <UserNotifications/UserNotifications.h>
#else

#endif
#import "GWMessageSingleModel.h"
#import "GWStupSQLiteManager.h"

@interface PushManager()
@end

static char alipushNotificationKey;
@implementation PushManager

+ (instancetype)shareInstance{
    static PushManager *pushManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pushManager = [[PushManager alloc] init];
    });
    
    return pushManager;
}

-(void)registWithPUSHWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions callbackBlock:(void(^)(NSDictionary *params))block{
    [self registerAPNSWithApplication:application launchOptions:launchOptions];                     // 1. apns注册
    [self initCloudPush];                                       // 2. 初始化SDK
    [self listenerOnChannelOpened];                             // 3. 监听推送通道打开动作
    [self registerMessageReceive];                              // 4. 监听推送消息到达
    [CloudPushSDK sendNotificationAck:launchOptions];           // 5.
    
    objc_setAssociatedObject(self, &alipushNotificationKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 1.APNS 注册
- (void)registerAPNSWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions{
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [application registerForRemoteNotifications];
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound |
        UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    } else{
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

#pragma mark - 2.APNs注册成功回调，将返回的deviceToken上传到CloudPush服务器
+ (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [CloudPushSDK registerDevice:deviceToken withCallback:^(CloudPushCallbackResult *res) {
        if (res.success) {
            NSLog(@"===>APNS注册成功 %@", [CloudPushSDK getApnsDeviceToken]);
        } else {
            NSLog(@"===> APNS 注册失败, error: %@", res.error);
        }
    }];
}

#pragma mark - 3.APNs注册失败回调
+ (void)applicationDidFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error);
}

/*
 *  App处于启动状态时，通知打开回调
 */
-(void)registerWithReceiveRemoteNotification:(NSDictionary*)userInfo {
    [CloudPushSDK sendNotificationAck:userInfo];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        return;
    }
    
}

#pragma mark - 4.SDK 初始化
- (void)initCloudPush {                 // 正式上线建议关闭
    [CloudPushSDK turnOnDebug];
    [CloudPushSDK asyncInit:AliPushKey appSecret:AliPushAppSecret callback:^(CloudPushCallbackResult *res) {
        if (res.success) {
            NSLog(@"Push SDK init success, deviceId: %@.", [CloudPushSDK getDeviceId]);
        } else {
            NSLog(@"Push SDK init failed, error: %@", res.error);
        }
    }];
}

#pragma mark 5.App处于启动状态时，通知打开回调
+ (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    // 取得APNS通知内容
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    // 内容
    NSString *content = [aps valueForKey:@"alert"];
    // badge数量
    NSInteger badge = [[aps valueForKey:@"badge"] integerValue];
    // 播放声音
    NSString *sound = [aps valueForKey:@"sound"];
    // 取得通知自定义字段内容，例：获取key为"Extras"的内容
    NSString *Extras = [userInfo valueForKey:@"Extras"]; //服务端中Extras字段，key是自己定义的
    NSLog(@"content = [%@], badge = [%ld], sound = [%@], Extras = [%@]", content, (long)badge, sound, Extras);
    // iOS badge 清0
    application.applicationIconBadgeNumber = 0;
    // 通知打开回执上报
    // [CloudPushSDK handleReceiveRemoteNotification:userInfo];(Deprecated from v1.8.1)
    [CloudPushSDK sendNotificationAck:userInfo];
}

#pragma mark 6.1 注册推送通道打开监听
- (void)listenerOnChannelOpened {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onChannelOpened:) name:@"CCPDidChannelConnectedSuccess" object:nil];
}

#pragma mark 6.2 推送通道打开回调
- (void)onChannelOpened:(NSNotification *)notification {
//    [StatusBarManager statusBarHidenWithText:@"消息通道建立成功"];
}

- (void)registerMessageReceive {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageReceived:) name:@"CCPDidReceiveMessageNotification" object:nil];
}

#pragma mark 6.3 处理到来推送消息
- (void)onMessageReceived:(NSNotification *)notification {
    
    CCPSysMessage *message = [notification object];
    NSString *title = [[NSString alloc] initWithData:message.title encoding:NSUTF8StringEncoding];
    NSString *body = [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];
    NSLog(@"Receive message title: %@, content: %@.", title, body);
    
    if((![title isEqualToString:@"3"]) || (![title isEqualToString:@"1"])) {
        [StatusBarManager statusBarHidenWithText:@"你有一条新消息哦~"];
    }
    
    void(^block)(NSDictionary *params) = objc_getAssociatedObject(self, &alipushNotificationKey);
    if (block){
        block(notification.userInfo);
    }
}


#pragma mark - 10.Other version 10. 以上

@end

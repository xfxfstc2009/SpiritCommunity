//
//  GWSettingSliderListModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/22.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "GWSettingSliderSingleModel.h"

@interface GWSettingSliderListModel : FetchModel

@property (nonatomic,strong)NSArray<GWSettingSliderSingleModel> *sliderList;

@end

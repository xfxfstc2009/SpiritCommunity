//
//  LiveRootManager.h
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/21.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomeLiveSingleModel.h"
#import "TXLiveManager.h"

#import <ImSDK/ImSDK.h>
#import <QAVSDK/QAVSDK.h>
#import <QAVSDK/QAVVideoEffectCtrl.h>
#import <QALSDK/QalSDKProxy.h>
#import <TLSSDK/TLSHelper.h>
#import <IMSDKBugly/IMSDKBugly.h>

//#import "ILiveCoreHeader.h"
//#import "ILiveQualityData.h"
//#import "ILiveSpeedTestManager.h"
//#import "TILLiveSDK.h"

#import <ILiveSDK/ILiveCoreHeader.h>
#import <ILiveSDK/ILiveQualityData.h>
#import <ILiveSDK/ILiveSpeedTestManager.h>
#import <TILLiveSDK/TILLiveSDK.h>

//美颜滤镜sdk
#import <TILFilterSDK/TILFilter.h>

/******************** local param **********************/
#define kLoginParam         @"kLoginParam"
#define kLoginIdentifier    @"kLoginIdentifier"
#define kLoginPassward      @"kLoginPassward"
#define kEnvParam           @"kEnvParam"
#define kLogLevel           @"kLogLevel"
#define kBeautyScheme       @"kBeautyScheme"
#define kILiveBeauty        @"插件美颜"
#define kQAVSDKBeauty       @"内置美颜"

//观众
#define kSxbRole_GuestHD    @"Guest"
#define kSxbRole_GuestLD    @"Guest2"

/******************** custom msg cmd **********************/
typedef NS_ENUM(NSInteger, ShowCustomCmd)
{
    AVIMCMD_Text = -1,          // 普通的聊天消息
    
    AVIMCMD_None,               // 无事件：0
    
    // 以下事件为TCAdapter内部处理的通用事件
    AVIMCMD_EnterLive,          // 用户加入直播, Group消息 ： 1
    AVIMCMD_ExitLive,           // 用户退出直播, Group消息 ： 2
    AVIMCMD_Praise,             // 点赞消息, Demo中使用Group消息 ： 3
    AVIMCMD_Host_Leave,         // 主播或互动观众离开, Group消息 ： 4
    AVIMCMD_Host_Back,          // 主播或互动观众回来, Group消息 ： 5
    
    //    ShowCustomCmd_Begin = ILVLIVE_IMCMD_CUSTOM_LOW_LIMIT,
    //    ShowCustomCmd_Praise,
    //    ShowCustomCmd_JoinRoom,
    //    ShowCustomCmd_DownVideo,//主播发送下麦通知
    
    AVIMCMD_Multi = ILVLIVE_IMCMD_CUSTOM_LOW_LIMIT,              // 多人互动消息类型 ： 2048
    
    AVIMCMD_Multi_Host_Invite,          // 多人主播发送邀请消息, C2C消息 ： 2049
    AVIMCMD_Multi_CancelInteract,       // 已进入互动时，断开互动，Group消息，带断开者的imUsreid参数 ： 2050
    AVIMCMD_Multi_Interact_Join,        // 多人互动方收到AVIMCMD_Multi_Host_Invite多人邀请后，同意，C2C消息 ： 2051
    AVIMCMD_Multi_Interact_Refuse,      // 多人互动方收到AVIMCMD_Multi_Invite多人邀请后，拒绝，C2C消息 ： 2052
    
    // =======================
    // 暂未处理以下
    AVIMCMD_Multi_Host_EnableInteractMic,  // 主播打开互动者Mic，C2C消息 ： 2053
    AVIMCMD_Multi_Host_DisableInteractMic, // 主播关闭互动者Mic，C2C消息 ：2054
    AVIMCMD_Multi_Host_EnableInteractCamera, // 主播打开互动者Camera，C2C消息 ：2055
    AVIMCMD_Multi_Host_DisableInteractCamera, // 主播关闭互动者Camera，C2C消息 ： 2056
    // ==========================
    
    
    AVIMCMD_Multi_Host_CancelInvite,            // 取消互动, 主播向发送AVIMCMD_Multi_Host_Invite的人，再发送取消邀请， 已发送邀请消息, C2C消息 ： 2057
    AVIMCMD_Multi_Host_ControlCamera,           // 主动控制互动观众摄像头, 主播向互动观众发送,互动观众接收时, 根据本地摄像头状态，来控制摄像头开关（即控制对方视频是否上行视频）， C2C消息 ： 2058
    AVIMCMD_Multi_Host_ControlMic,              // 主动控制互动观众Mic, 主播向互动观众发送,互动观众接收时, 根据本地MIC状态,来控制摄像头开关（即控制对方视频是否上行音频），C2C消息 ： 2059
};

#define kSxbRole_InteractHDTitle @"1、高清(1280x720,25fps)"
#define kSxbRole_InteractSDTitle @"2、标清(960x540,20fps)"
#define kSxbRole_InteractLDTitle @"3、流畅(640x480,15fps)"
#define kSxbRole_HostHD     @"HD"
#define kSxbRole_HostSD     @"SD"
#define kSxbRole_HostLD     @"LD"
#define kSxbRole_InteractHD @"HDGuest"
#define kSxbRole_InteractSD @"SDGuest"
#define kSxbRole_InteractLD @"LDGuest"

//最大小画面数量（不包含大画面）
#define kMaxUserViewCount 1

#define kUserParise_Notification        @"kUserParise_Notification"
//#define kUserJoinRoom_Notification      @"kUserJoinRoom_Notification"
//#define kUserExitRoom_Notification      @"kUserExitRoom_Notification"
#define kUserMemChange_Notification      @"kUserMemChange_Notification"
#define kUserUpVideo_Notification       @"kUserUpVideo_Notification"
#define kUserDownVideo_Notification     @"kUserDownVideo_Notification"
#define kUserSwitchRoom_Notification    @"kUserSwitchRoom_Notification"
#define kGroupDelete_Notification       @"kGroupDelete_Notification"
#define kPureDelete_Notification        @"kPureDelete_Notification"
#define kNoPureDelete_Notification      @"kNoPureDelete_Notification"
#define kClickConnect_Notification      @"kClickConnect_Notification"
#define kCancelConnect_Notification     @"kCancelConnect_Notification"
#define kClickDownVideo_Notification      @"kClickDownVideo_Notification"
#define kEnterBackGround_Notification   @"kEnterBackGround_Notification"
#define kLinkRoomBtn_Notification       @"kLinkRoomBtn_Notification"

#define share_live_baseUrl  @"http://live.cloud.tencent.com/live/play.html?url="
///////Mine Gift
#define kLiveGift_Notification       @"kLiveGift_Notification"


@interface LiveRootManager : NSObject

+(void)registerLiveSDK;


+(void)registerLiveIMSDK;

+(void)disableLogPrint;                     // 日志

+(void)liveLoginManager:(NSString *)sig pwd:(NSString *)pwd;

+(void)liveToReleaseManager:(UIViewController *)controller;

+(void)liveHomeDirectToModel:(HomeLiveSingleModel *)singleModel controller:(UIViewController *)controller;
@end

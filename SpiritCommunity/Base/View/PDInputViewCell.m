//
//  PDInputViewCell.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/25.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDInputViewCell.h"

static char inputTextViewDidEndEditingBlockKey;
static char inputTextViewDidChangeBlockKey;
@interface PDInputViewCell()<UITextViewDelegate>
@property (nonatomic,strong)PDImageView *inputBgView;
@property (nonatomic,strong)UILabel *placeHolder;
@property (nonatomic,strong)UILabel *countLabel;

@end

@implementation PDInputViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.inputBgView = [[PDImageView alloc]init];
    self.inputBgView.frame = CGRectMake(LCFloat(20), LCFloat(14), kScreenBounds.size.width - 2 * LCFloat(20), LCFloat(150));
    self.inputBgView.image = [Tool stretchImageWithName:@"icon_orderDetail_bg"];
    self.inputBgView.userInteractionEnabled = YES;
    [self addSubview:self.inputBgView];
    
    if(!self.inputView){
        // 输入框
        self.inputView = [[UITextView alloc]init];
        self.inputView.delegate = self;
        self.inputView.textColor = [UIColor colorWithCustomerName:@"黑"];
        self.inputView.returnKeyType = UIReturnKeyDefault;
        self.inputView.backgroundColor = [UIColor clearColor];
        self.inputView.scrollEnabled = YES;
        self.inputView.userInteractionEnabled = YES;
        self.inputView.frame = self.inputBgView.bounds;
        [self.inputBgView addSubview:self.inputView];
    }
    if (!self.placeHolder){
        self.placeHolder = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
        self.placeHolder.textColor = [UIColor lightGrayColor];
        self.placeHolder.frame = CGRectMake(LCFloat(7), LCFloat(8), self.inputBgView.size_width - 2 * LCFloat(5), [NSString contentofHeightWithFont:self.placeHolder.font]);
        [self.inputBgView addSubview:self.placeHolder];
    }
    // 3. 创建总数字
    self.countLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
    [self.inputBgView addSubview:self.countLabel];
    self.countLabel.textAlignment = NSTextAlignmentRight;
    self.countLabel.frame = CGRectMake(0, self.inputBgView.size_height - [NSString contentofHeightWithFont:self.countLabel.font], self.inputBgView.size_width - LCFloat(7), [NSString contentofHeightWithFont:self.countLabel.font]);

    //
    self.limitCount = 0;
    self.transferPlaceHolder = @"请输入信息";
    self.transferTitle = @"";
}

-(void)setTransferPlaceHolder:(NSString *)transferPlaceHolder{
    _transferPlaceHolder = transferPlaceHolder;
    self.placeHolder.text = transferPlaceHolder;
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
}

-(void)setLimitCount:(NSInteger)limitCount{
    _limitCount = limitCount;
    if (limitCount){
        self.countLabel.text = [NSString stringWithFormat:@"最多输入%li字",(long)limitCount];
    }
}

#pragma mark - UItextViewDelegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        return YES;
    }
    NSString *toBeString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if(self.inputView == textView) {
        if([toBeString length]> (self.limitCount == 0 ? 120 : self.limitCount)) {
            textView.text = [toBeString substringToIndex:(self.limitCount == 0 ? 120 : self.limitCount)];
            return NO;
        }
    }
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView {
    void(^block)() = objc_getAssociatedObject(self, &inputTextViewDidEndEditingBlockKey);
    if(block){
        block();
    }
}

-(void)inputTextViewDidEndEditingBlock:(void(^)())block{
    objc_setAssociatedObject(self, &inputTextViewDidEndEditingBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)textViewDidChange:(UITextView *)textView {
    if (self.inputView.text.length == 0) {
        self.placeHolder.text = self.transferPlaceHolder;
        self.countLabel.text = [NSString stringWithFormat:@"最多输入%li字",(long)self.limitCount];
    }else{
        self.placeHolder.text = @"";
        NSString *limit = [NSString stringWithFormat:@"%li",(long)(self.limitCount == 0 ? 120 : self.limitCount)];
        [self.countLabel setText:[NSString stringWithFormat:@"%lu/%@",(unsigned long)[Tool calculateCharacterLengthForAres:self.inputView.text],limit]];
    }
    void(^block)(NSString *) = objc_getAssociatedObject(self, &inputTextViewDidChangeBlockKey);
    if (block){
        block(self.inputView.text);
    }
}

-(void)setTransferType:(PDInputViewCellType)transferType{
    _transferType = transferType;
    if (transferType == PDInputViewCellTypePingjia){
        self.inputBgView.frame = CGRectMake(LCFloat(20), LCFloat(11), LCFloat(270) - 2 * LCFloat(20), self.transferCellHeight - 2 * LCFloat(11));
        self.inputView.frame = self.inputBgView.bounds;
    }
}

-(void)inputTextViewDidChangeBlock:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &inputTextViewDidChangeBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(150);
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end

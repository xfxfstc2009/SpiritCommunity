//
//  TXLiveManager.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/27.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "LiveUIBottomViewSingleModel.h"
#import <TILFilterSDK/TILFilter.h>
#import <ImSDK/ImSDK.h>

@interface TXLiveManager : FetchModel

+(instancetype)sharedAccountModel;

// 【美颜】
@property (nonatomic,assign)CGFloat transferBeautyValue;                /**< 美颜度*/
@property (nonatomic,assign)CGFloat transferWhiteValue;                 /**< 美白度*/
@property (nonatomic,assign)BOOL transferMicStatus;                     /**< 麦克风状态*/
// 【当前的亮度】
@property (nonatomic,assign)BOOL lightStatus;                           /**< YES 表示正面，NO反面*/
@property (nonatomic,copy)NSString *role;
// 是否上麦
@property (nonatomic,assign)BOOL isUpVideo;                             /**< 判断是否上麦*/
// 判断是否是主播
@property (nonatomic,assign)BOOL isHost;


// 1. 推流
+(void)sendRequestToPushFlow;
// 2. 设置当前我的信息
+(void)getInfoWithMySelfWithBlcok:(void(^)(BOOL successed, TIMUserProfile *profile))block;
// 3. 添加滤镜
+(void)addFilterWithType:(TILFilter *)tilFilter type:(LiveUIBottomViewType)type;
// 4.添加挂件
+(void)addSpecilWithType:(LiveUIBottomViewType)type;
// 5. 切换相机的方向
+(void)switchCamera;
// 6. 开启闪光
+(void)openLinght:(BOOL)status;
// 7. 美颜
+ (void)openBeauty:(TILFilter *)tilFilter;
// 8.美白
+ (void)openWhite:(TILFilter *)tilFilter;
// 9. 点赞
+(void)messageWithClickZan:(UIButton *)button block:(void(^)(BOOL success))block;

+(void)closeAndOpenMic;
@end

//
//  MapBuilderAnnotationView.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/24.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "OtherSearchSingleModel.h"
#import "MapBuildSingleModel.h"

#define kCalloutViewMargin          -8

@interface MapBuilderAnnotationView : MAAnnotationView

@property (nonatomic, strong) UIView *calloutView;
@property (nonatomic,strong)MapBuildSingleModel *transferLiverModel;


-(void)locationShow:(void(^)(MapBuildSingleModel *transferLiverModel))block;
-(void)actionSelectedBlock:(void(^)(BOOL selected))block;                   //   点击方法
@end

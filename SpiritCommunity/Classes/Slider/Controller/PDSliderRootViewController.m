//
//  PDSliderRootViewController.m
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/23.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import "PDSliderRootViewController.h"
#import "GWWeatherView.h"
#import "GWWeatherMainViewController.h"
#import "GWSettingSliderSingleModel.h"
#import "PDGradientNavBar.h"
#import "MainCenterMenuViewController.h"                    // 首页
#import "SettingRootViewController.h"                       // 设置
#import "FindRootViewController.h"                          // 发现
#import "MessageRootViewController.h"                       // 消息
#import "CenterMoneyViewController.h"                       // 我的钱包
#import "CenterFansViewController.h"                        // fans
#import "OrderRootViewController.h"                         // 我的订单
#import "CartRootViewController.h"                          // 购物车
#import "HomeRankingViewController.h"                       // 排行榜
#import "CenterEditingViewController.h"                     // 个人信息

@interface PDSliderRootViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *sliderArr;
@property (nonatomic,strong)GWWeatherView *weatherView;     /**< 天气*/
@property (nonatomic,strong)UIView *bottomView;             /**< 创建底部的view*/

@end

@implementation PDSliderRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createBottomView];
    [self arrayWithInit];
    [self createTableView];
//    [self autoLogin];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark -
-(void)pageSetting{
    
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.sliderArr = [NSMutableArray array];
//    NSArray *sliderArr = @[@[@"登录"],@[@"首页",@"发现",@"排行榜"],@[@"我的钱包",@"我的订单",@"我的关注",@"购物车"],@[@"设置"]];
    NSArray *sliderArr = @[@[@"登录"],@[@"首页",@"发现",@"排行榜"],@[@"我的钱包"],@[@"设置"]];
    [self.sliderArr addObjectsFromArray:sliderArr];
}

-(void)createTableView{
    if (!self.sliderTableView){
        self.sliderTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.sliderTableView.dataSource = self;
        self.sliderTableView.delegate = self;
        [self.view addSubview:self.sliderTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sliderArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.sliderArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"登录"]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        GWSliderLeftTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[GWSliderLeftTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowZero.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowZero.transferCellHeiht = cellHeight;
        cellWithRowZero.transferLoginModel = [AccountModel sharedAccountModel];
        
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDSliderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDSliderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferTitle = [[self.sliderArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowOne.transferCellHeight = cellHeight;
        return cellWithRowOne;
    }
}

#pragma mark - UITableDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"登录"]){
        return [GWSliderLeftTableViewCell calculationCellHeight];
    } else {
        return [PDSliderTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 64;
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PDNavigationController *navigationController = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [[navigationController navigationBar] setTranslucent:NO];
    NSArray *navBarArr;
    UIColor *color1 = [UIColor whiteColor];
    UIColor *color6 = [UIColor whiteColor];
    navBarArr = [NSArray arrayWithObjects:(id)color1.CGColor,(id)color6.CGColor,nil];
    
    navigationController.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    navigationController.navigationBar.layer.shadowOpacity = .2f;
    navigationController.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
    
    __weak typeof(self)weakSelf = self;
    if (indexPath.section == [self cellIndexPathRowWithcellData:@"登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"登录"]){
        if (![[AccountModel sharedAccountModel] hasLoggedIn]){
            HomeRootViewController *homeMainViewController = [[HomeRootViewController alloc]init];
            [navigationController setViewControllers:@[homeMainViewController]];
            [self authorizeWithCompletionHandler:^(BOOL successed) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf.headerImageView = [[PDImageView alloc]init];
                strongSelf.headerImageView.frame = CGRectMake(0, 0, LCFloat(150), LCFloat(150));
                [strongSelf loginSuccessManager];
            }];
        } else {
            CenterEditingViewController *centerEditingViewController = [[CenterEditingViewController alloc]init];
            [navigationController setViewControllers:@[centerEditingViewController]];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"首页"] && indexPath.row == [self cellIndexPathRowWithcellData:@"首页"]){           // 首页
        [self.sideMenuViewController setContentViewController:[PDMainTabbarViewController sharedController] animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        return;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"设置"] && indexPath.row == [self cellIndexPathRowWithcellData:@"设置"]){
        // 设置
        SettingRootViewController *settingRootViewController = [[SettingRootViewController alloc]init];
        [navigationController setViewControllers:@[settingRootViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"发现"] && indexPath.row == [self cellIndexPathRowWithcellData:@"发现"]){
        FindRootViewController *findViewController = [[FindRootViewController alloc]init];
        [navigationController setViewControllers:@[findViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"消息"] && indexPath.row == [self cellIndexPathRowWithcellData:@"消息"]){
        MessageRootViewController *messageViewController = [[MessageRootViewController alloc]init];
        [navigationController setViewControllers:@[messageViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的钱包"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的钱包"]){
        CenterMoneyViewController *aboutViewController = [[CenterMoneyViewController alloc]init];
        [navigationController setViewControllers:@[aboutViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的关注"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的关注"]){            // 关于我
        CenterFansViewController *aboutViewController = [[CenterFansViewController alloc]init];
        [navigationController setViewControllers:@[aboutViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的订单"] && indexPath.row == [self cellIndexPathRowWithcellData:@"我的订单"]){             // 【健康】
        OrderRootViewController *aboutViewController = [[OrderRootViewController alloc]init];
        [navigationController setViewControllers:@[aboutViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"购物车"] && indexPath.row == [self cellIndexPathRowWithcellData:@"购物车"]){             // 【健康】
        CartRootViewController *aboutViewController = [[CartRootViewController alloc]init];
        [navigationController setViewControllers:@[aboutViewController]];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"排行榜"] && indexPath.row == [self cellIndexPathRowWithcellData:@"排行榜"]){             // 【健康】
        HomeRankingViewController *rankingViewController = [[HomeRankingViewController alloc]init];
        [navigationController setViewControllers:@[rankingViewController]];
    }
    [[PDGradientNavBar appearance] setBarTintGradientColors:navBarArr];
    [self.sideMenuViewController setContentViewController:navigationController animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.sliderArr.count ; i++){
        NSArray *dataTempArr = [self.sliderArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.sliderArr.count ; i++){
        NSArray *dataTempArr = [self.sliderArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}


-(void)loginSuccess{
    [self.sideMenuViewController setContentViewController:[PDMainTabbarViewController sharedController] animated:YES];
    [self.sideMenuViewController hideMenuViewController];
    
    [[AccountModel sharedAccountModel] reloadAllAccountInfo];
}

-(void)logoutSuccess{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"登录"] inSection: [self cellIndexPathSectionWithcellData:@"登录"]];
    GWSliderLeftTableViewCell *cell = (GWSliderLeftTableViewCell *)[self.sliderTableView cellForRowAtIndexPath:indexPath];
    [cell loginManager];
}


-(void)updateList:(NSArray *)sliderList{
    if (self.sliderArr.count){
        [self.sliderArr removeAllObjects];
    }
    NSMutableArray *sliderTempArr = [NSMutableArray array];
    for (int i = 0 ; i < sliderList.count;i++){
        GWSettingSliderSingleModel *singleModel = [sliderList objectAtIndex:i];
        [sliderTempArr addObject:singleModel.sign];
    }
    [self.sliderArr addObject:sliderTempArr];
    
    [self.sliderTableView reloadData];
}

#pragma mark - createBottomView
-(void)createBottomView{
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor clearColor];
    if (IS_iPhoneX){
        self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(70) - LCFloat(34), kScreenBounds.size.width, LCFloat(70));
    } else {
        self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(70), kScreenBounds.size.width, LCFloat(70));
    }

    [self.view addSubview:self.bottomView];
    
    CGFloat main_width = kScreenBounds.size.width / 2. + kScreenBounds.size.width / 15.;
    
    // 2. 创建天气
    CGRect weatherFrame = CGRectMake(0, 0, main_width / 3 * 2, self.bottomView.size_height);
    __weak typeof(self)weakSelf = self;
    self.weatherView = [[GWWeatherView alloc]initWithFrame:weatherFrame withBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        GWWeatherMainViewController *weatherViewController = [[GWWeatherMainViewController alloc]init];
        [strongSelf.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:weatherViewController] animated:YES];
        [strongSelf.sideMenuViewController hideMenuViewController];
    }];
    self.weatherView.backgroundColor = [UIColor clearColor];
    [self.bottomView addSubview:self.weatherView];
}



#pragma mark - 更新天气
-(void)reloadWeatherManager{
    NSString *location = [GWUserCurrentInfoGetManager sharedHealthManager].addressComponent.district.length?[GWUserCurrentInfoGetManager sharedHealthManager].addressComponent.district:[GWUserCurrentInfoGetManager sharedHealthManager].addressComponent.city;
    [self.weatherView setDataWithTemp:[GWUserCurrentInfoGetManager sharedHealthManager].weatherCurrentModel.temperature city:location];
}

#pragma mark - 创建动画
-(void)createLeftAnimationMovie{
    
}

@end

//
//  OrderRootRowOneTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "OrderRootRowOneTableViewCell.h"

@interface OrderRootRowOneTableViewCell()
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *productImgView;
@property (nonatomic,strong)UILabel *productNameLabel;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *skuLabel;
@property (nonatomic,strong)UILabel *countLabel;

@end

@implementation OrderRootRowOneTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.layer.borderColor =[UIColor colorWithCustomerName:@"浅灰"].CGColor;
    self.convertView.layer.borderWidth = .5f;
    self.convertView.clipsToBounds = YES;
    [self addSubview:self.convertView];
    
    // 2.创建图片
    self.productImgView = [[PDImageView alloc]init];
    self.productImgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.productImgView];
    
    // 3. 创建商品名字
    self.productNameLabel = [[UILabel alloc]init];
    self.productNameLabel.backgroundColor = [UIColor clearColor];
    self.productNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.productNameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.productNameLabel];
    
    // 4. 创建商品价格
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.priceLabel.adjustsFontSizeToFitWidth = YES;
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.priceLabel];
    
    // 5. 创建数量
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.countLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.countLabel];
    
    // 6. 创建商品sku
    self.skuLabel = [[UILabel alloc]init];
    self.skuLabel.backgroundColor = [UIColor clearColor];
    self.skuLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.skuLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.skuLabel];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferSingleModel:(OrderListSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    OrderListGoodsSingleModel *orderGoodsSingleModel = [transferSingleModel.goodSet firstObject];
    // 1. convertView
    self.convertView.frame = CGRectMake(LCFloat(11), LCFloat(11), (self.transferCellHeight - 2 * LCFloat(11)), (self.transferCellHeight - 2 * LCFloat(11)));
    
    // 1.1 img
    __weak typeof(self)weakSelf = self;
    [self.productImgView uploadImageWithURL:orderGoodsSingleModel.goodsImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool setTransferSingleAsset:image imgView:strongSelf.productImgView convertView:strongSelf.convertView];
    }];
    
    
    // 2. 创建名字
    self.productNameLabel.text = orderGoodsSingleModel.goodName;
    
    // 3. 创建商品价格
    NSString*tempString = [NSString stringWithFormat:@"%@%.2f",@"￥",1000.];
    CGSize mainWidth = [tempString sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",orderGoodsSingleModel.goodMoney];
    self.priceLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - mainWidth.width, LCFloat(11), mainWidth.width, [NSString contentofHeightWithFont:self.priceLabel.font]);
    
    // 4. 创建商品数量
    self.countLabel.text = [NSString stringWithFormat:@"X%li",(long)orderGoodsSingleModel.buyNum];
    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(kScreenBounds.size.width -LCFloat(11) - countSize.width, CGRectGetMaxY(self.priceLabel.frame) + LCFloat(11), countSize.width, [NSString contentofHeightWithFont:self.countLabel.font]);
    
    // 5. 创建width
    CGFloat nameWidth = kScreenBounds.size.width -LCFloat(11) - mainWidth.width - LCFloat(11) - CGRectGetMaxX(self.convertView.frame) - LCFloat(11);
    self.productNameLabel.text = orderGoodsSingleModel.goodName;
    CGSize productSize = [self.productNameLabel.text sizeWithCalcFont:self.productNameLabel.font constrainedToSize:CGSizeMake(nameWidth, CGFLOAT_MAX)];
    if (productSize.height >= [NSString contentofHeightWithFont:self.productNameLabel.font] * 2){
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, nameWidth, 2 * [NSString contentofHeightWithFont:self.productNameLabel.font]);
        self.productNameLabel.numberOfLines = 2;
    } else {
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, nameWidth,  [NSString contentofHeightWithFont:self.productNameLabel.font]);
        self.productNameLabel.numberOfLines = 1;
    }
    
    // 6. 创建sku
    self.skuLabel.text = orderGoodsSingleModel.goodsSummary;
    CGSize skuSize = [self.skuLabel.text sizeWithCalcFont:self.skuLabel.font constrainedToSize:CGSizeMake(nameWidth, CGFLOAT_MAX)];
    if (skuSize.height >= 2 * [NSString contentofHeightWithFont:self.skuLabel.font]){
        self.skuLabel.numberOfLines = 2;
        self.skuLabel.frame = CGRectMake(self.productNameLabel.orgin_x, LCFloat(11) + 2 * [NSString contentofHeightWithFont:self.productNameLabel.font] + LCFloat(11), nameWidth, 2 * [NSString contentofHeightWithFont:self.skuLabel.font]);
    } else {
        self.skuLabel.numberOfLines = 1;
        self.skuLabel.frame = CGRectMake(self.productNameLabel.orgin_x, LCFloat(11) + 2 * [NSString contentofHeightWithFont:self.productNameLabel.font] + LCFloat(11), nameWidth,  [NSString contentofHeightWithFont:self.skuLabel.font]);
    }
    // 7.
    self.countLabel.center_y = self.skuLabel.center_y;
}

-(void)setTransferCartModel:(CartSingleModel *)transferCartModel{
    _transferCartModel = transferCartModel;

    // 1. convertView
    self.convertView.frame = CGRectMake(LCFloat(11), LCFloat(11), (self.transferCellHeight - 2 * LCFloat(11)), (self.transferCellHeight - 2 * LCFloat(11)));

    // 1.1 img
    __weak typeof(self)weakSelf = self;
    [self.productImgView uploadImageWithURL:transferCartModel.goodsImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool setTransferSingleAsset:image imgView:strongSelf.productImgView convertView:strongSelf.convertView];
    }];


    // 2. 创建名字
    self.productNameLabel.text = transferCartModel.goodsName;

    // 3. 创建商品价格
    NSString*tempString = [NSString stringWithFormat:@"%@%.2f",@"￥",1000.];
    CGSize mainWidth = [tempString sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",transferCartModel.goodsMoney];
    self.priceLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - mainWidth.width, LCFloat(11), mainWidth.width, [NSString contentofHeightWithFont:self.priceLabel.font]);

    // 4. 创建商品数量
    self.countLabel.text = [NSString stringWithFormat:@"X%li",(long)transferCartModel.goodsNum];
    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(kScreenBounds.size.width -LCFloat(11) - countSize.width, CGRectGetMaxY(self.priceLabel.frame) + LCFloat(11), countSize.width, [NSString contentofHeightWithFont:self.countLabel.font]);

    // 5. 创建width
    CGFloat nameWidth = kScreenBounds.size.width -LCFloat(11) - mainWidth.width - LCFloat(11) - CGRectGetMaxX(self.convertView.frame) - LCFloat(11);
    self.productNameLabel.text = transferCartModel.goodsName;
    CGSize productSize = [self.productNameLabel.text sizeWithCalcFont:self.productNameLabel.font constrainedToSize:CGSizeMake(nameWidth, CGFLOAT_MAX)];
    if (productSize.height >= [NSString contentofHeightWithFont:self.productNameLabel.font] * 2){
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, nameWidth, 2 * [NSString contentofHeightWithFont:self.productNameLabel.font]);
        self.productNameLabel.numberOfLines = 2;
    } else {
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, nameWidth,  [NSString contentofHeightWithFont:self.productNameLabel.font]);
        self.productNameLabel.numberOfLines = 1;
    }

    // 6. 创建sku
    self.skuLabel.text = transferCartModel.supplyName;
    CGSize skuSize = [self.skuLabel.text sizeWithCalcFont:self.skuLabel.font constrainedToSize:CGSizeMake(nameWidth, CGFLOAT_MAX)];
    if (skuSize.height >= 2 * [NSString contentofHeightWithFont:self.skuLabel.font]){
        self.skuLabel.numberOfLines = 2;
        self.skuLabel.frame = CGRectMake(self.productNameLabel.orgin_x, LCFloat(11) + 2 * [NSString contentofHeightWithFont:self.productNameLabel.font] + LCFloat(11), nameWidth, 2 * [NSString contentofHeightWithFont:self.skuLabel.font]);
    } else {
        self.skuLabel.numberOfLines = 1;
        self.skuLabel.frame = CGRectMake(self.productNameLabel.orgin_x, LCFloat(11) + 2 * [NSString contentofHeightWithFont:self.productNameLabel.font] + LCFloat(11), nameWidth,  [NSString contentofHeightWithFont:self.skuLabel.font]);
    }
    // 7.
    self.countLabel.center_y = self.skuLabel.center_y;
}

-(void)setTransferProductDetailModel:(ProductDetailRootModel *)transferProductDetailModel{
    _transferProductDetailModel = transferProductDetailModel;

    self.convertView.frame = CGRectMake(LCFloat(11), LCFloat(11), (self.transferCellHeight - 2 * LCFloat(11)), (self.transferCellHeight - 2 * LCFloat(11)));

    // 1.1 img
    __weak typeof(self)weakSelf = self;
    [self.productImgView uploadImageWithURL:transferProductDetailModel.goodBaseInfo.goodsImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool setTransferSingleAsset:image imgView:strongSelf.productImgView convertView:strongSelf.convertView];
    }];


    // 2. 创建名字
    self.productNameLabel.text = transferProductDetailModel.goodBaseInfo.goodsName;

    // 3. 创建商品价格
    NSString*tempString = [NSString stringWithFormat:@"%@%.2f",@"￥",1000.];
    CGSize mainWidth = [tempString sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",transferProductDetailModel.goodBaseInfo.salePriceRMB];
    self.priceLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - mainWidth.width, LCFloat(11), mainWidth.width, [NSString contentofHeightWithFont:self.priceLabel.font]);

    // 4. 创建商品数量
    self.countLabel.text = [NSString stringWithFormat:@"X%li",(long)1];
    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(kScreenBounds.size.width -LCFloat(11) - countSize.width, CGRectGetMaxY(self.priceLabel.frame) + LCFloat(11), countSize.width, [NSString contentofHeightWithFont:self.countLabel.font]);

    // 5. 创建width
    CGFloat nameWidth = kScreenBounds.size.width -LCFloat(11) - mainWidth.width - LCFloat(11) - CGRectGetMaxX(self.convertView.frame) - LCFloat(11);
    self.productNameLabel.text = transferProductDetailModel.goodBaseInfo.goodsName;
    CGSize productSize = [self.productNameLabel.text sizeWithCalcFont:self.productNameLabel.font constrainedToSize:CGSizeMake(nameWidth, CGFLOAT_MAX)];
    if (productSize.height >= [NSString contentofHeightWithFont:self.productNameLabel.font] * 2){
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, nameWidth, 2 * [NSString contentofHeightWithFont:self.productNameLabel.font]);
        self.productNameLabel.numberOfLines = 2;
    } else {
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, nameWidth,  [NSString contentofHeightWithFont:self.productNameLabel.font]);
        self.productNameLabel.numberOfLines = 1;
    }

    // 6. 创建sku
    self.skuLabel.text = transferProductDetailModel.goodBaseInfo.supplyName;
    CGSize skuSize = [self.skuLabel.text sizeWithCalcFont:self.skuLabel.font constrainedToSize:CGSizeMake(nameWidth, CGFLOAT_MAX)];
    if (skuSize.height >= 2 * [NSString contentofHeightWithFont:self.skuLabel.font]){
        self.skuLabel.numberOfLines = 2;
        self.skuLabel.frame = CGRectMake(self.productNameLabel.orgin_x, LCFloat(11) + 2 * [NSString contentofHeightWithFont:self.productNameLabel.font] + LCFloat(11), nameWidth, 2 * [NSString contentofHeightWithFont:self.skuLabel.font]);
    } else {
        self.skuLabel.numberOfLines = 1;
        self.skuLabel.frame = CGRectMake(self.productNameLabel.orgin_x, LCFloat(11) + 2 * [NSString contentofHeightWithFont:self.productNameLabel.font] + LCFloat(11), nameWidth,  [NSString contentofHeightWithFont:self.skuLabel.font]);
    }
    // 7.
    self.countLabel.center_y = self.skuLabel.center_y;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(110);
}
@end

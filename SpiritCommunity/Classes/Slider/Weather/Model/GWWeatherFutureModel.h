//
//  GWWeatherFutureModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchFileModel.h"

@protocol GWWeatherFutureModel <NSObject>

@end

@interface GWWeatherFutureModel : FetchModel

@property (nonatomic,copy)NSString *date;               /**< 时间*/
@property (nonatomic,copy)NSString *dayTime;            /**< 白天天气*/
@property (nonatomic,copy)NSString *night;              /**< 晚上天气*/
@property (nonatomic,copy)NSString *temperature;        /**< 温度*/
@property (nonatomic,copy)NSString *week;               /**< 星期*/
@property (nonatomic,copy)NSString *wind;               /**< 风向*/
@end

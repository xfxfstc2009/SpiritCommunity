//
//  MainCenterTopViewController.h
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2018/1/15.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "MainCenterMenuViewController.h"
#import "CenterRootTopHeaderView.h"


@class MainCenterMenuViewController;
@interface MainCenterTopViewController : AbstractViewController

@property (strong, nonatomic) MainCenterMenuViewController * transferMenuController;

@property (nonatomic,assign)BOOL hasEnable;

-(void)loginManager;

-(void)loginReloadManager;
-(void)loginReloadManagerWithUserInfo;              // 登录之后修改用户其他信息
-(void)updateUserAccountInfoManager;
@end

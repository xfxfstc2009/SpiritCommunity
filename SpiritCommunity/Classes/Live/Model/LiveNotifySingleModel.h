//
//  LiveNotifySingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/7.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "OtherSearchSingleModel.h"
#import "LiveGiftSingleModel.h"

@interface LiveNotifySingleModel : FetchModel

@property (strong,nonatomic)LiveGiftSingleModel *giftModel;
@property (nonatomic,strong)OtherSearchSingleModel *userModel;

@end

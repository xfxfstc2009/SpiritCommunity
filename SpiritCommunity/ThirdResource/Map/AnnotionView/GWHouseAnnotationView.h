//
//  GWHouseAnnotationView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "OtherSearchSingleModel.h"
@interface GWHouseAnnotationView : MAAnnotationView

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) UIImage *portrait;

@property (nonatomic, strong) UIView *calloutView;
@property (nonatomic,strong)OtherSearchSingleModel *transferSearchSingleModel;
-(void)locationShow:(void(^)())block;

@end

//
//  CenterCustomerSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/9.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface CenterCustomerSingleModel : FetchModel
@property (nonatomic,copy)NSString *fixedStr;               /**< 固定的文案*/
@property (nonatomic,copy)NSString *dymicStr;               /**< 动态的文案*/
@end

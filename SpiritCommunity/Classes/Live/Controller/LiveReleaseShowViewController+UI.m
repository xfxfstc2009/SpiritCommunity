//
//  LiveReleaseShowViewController+UI.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/5.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController+UI.h"
#import <ShareSDK/ShareSDK.h>
@implementation LiveReleaseShowViewController (UI)


#pragma mark - 点赞
- (void)showLikeHeartStartRect:(NSNotification *)noti
{
    NSDictionary *pointDic = (NSDictionary *)noti.object;
    CGFloat aniX, aniY;
    if (pointDic)
    {
        CGFloat pariseX = [[pointDic objectForKey:@"parise_x"] floatValue];
        CGFloat pariseY = [[pointDic objectForKey:@"parise_y"] floatValue];
        
        CGRect bottomFrame =  self.actionView.bottomUIView.frame;
        
        aniX = pariseX + bottomFrame.origin.x;
        aniY = pariseY + bottomFrame.origin.y;
    }
    else
    {
        aniX = self.view.bounds.size.width - 50;
        aniY = self.view.bounds.size.height - 70;
    }
    if (aniX+30 > self.view.bounds.size.width)
    {
        aniX = self.view.bounds.size.width - 40;
    }
    CGRect frame = CGRectMake(self.view.bounds.size.width - 70, self.view.bounds.size.height - 100, 30, 30);
    UIImageView *imageView = [[UIImageView alloc ] initWithFrame:frame];
    imageView.image = [[UIImage imageNamed:@"img_like"] imageWithTintColor:[UIColor randomFlatDarkColor]];
    [self.view addSubview:imageView];
    imageView.alpha = 0;
    
    [imageView.layer addAnimation:[self hearAnimationFrom:frame] forKey:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [imageView removeFromSuperview];
    });
    
}
- (CAAnimation *)hearAnimationFrom:(CGRect)frame
{
    //位置
    CAKeyframeAnimation *animation=[CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.beginTime = 0.5;
    animation.duration = 2.5;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    animation.repeatCount= 0;
    animation.calculationMode = kCAAnimationCubicPaced;
    
    CGMutablePathRef curvedPath = CGPathCreateMutable();
    CGPoint point0 = CGPointMake(frame.origin.x + frame.size.width / 2, frame.origin.y + frame.size.height / 2);
    
    CGPathMoveToPoint(curvedPath, NULL, point0.x, point0.y);
    
    float x11 = point0.x - arc4random() % 30 + 30;
    float y11 = frame.origin.y - arc4random() % 60 ;
    float x1 = point0.x - arc4random() % 15 + 15;
    float y1 = frame.origin.y - arc4random() % 60 - 30;
    CGPoint point1 = CGPointMake(x1, y1);
    CGPathAddQuadCurveToPoint(curvedPath, NULL, x11, y11, point1.x, point1.y);
    
    int conffset2 = self.view.bounds.size.width * 0.2;
    int conffset21 = self.view.bounds.size.width * 0.1;
    float x2 = point0.x - arc4random() % conffset2 + conffset2;
    float y2 = arc4random() % 30 + 240;
    float x21 = point0.x - arc4random() % conffset21  + conffset21;
    float y21 = (y2 + y1) / 2 + arc4random() % 30 - 30;
    CGPoint point2 = CGPointMake(x2, y2);
    CGPathAddQuadCurveToPoint(curvedPath, NULL, x21, y21, point2.x, point2.y);
    
    animation.path = curvedPath;
    
    CGPathRelease(curvedPath);
    
    //透明度变化
    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnim.fromValue = [NSNumber numberWithFloat:1.0];
    opacityAnim.toValue = [NSNumber numberWithFloat:0];
    opacityAnim.removedOnCompletion = NO;
    opacityAnim.beginTime = 0;
    opacityAnim.duration = 3;
    
    //比例
    CABasicAnimation *scaleAnim = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    //        int scale = arc4random() % 5 + 5;
    scaleAnim.fromValue = [NSNumber numberWithFloat:.0];//[NSNumber numberWithFloat:((float)scale / 10)];
    scaleAnim.toValue = [NSNumber numberWithFloat:1];
    scaleAnim.removedOnCompletion = NO;
    scaleAnim.fillMode = kCAFillModeForwards;
    scaleAnim.duration = .5;
    
    CAAnimationGroup *animGroup = [CAAnimationGroup animation];
    animGroup.animations = [NSArray arrayWithObjects: scaleAnim,opacityAnim,animation, nil];
    animGroup.duration = 3;
    
    return animGroup;
}




#pragma mark - shareActionManager
-(void)shareActionManagerWithUrl:(NSString *)url img:(NSString *)imgUrl{

    __weak typeof(self)weakSelf = self;
    ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
    shareViewController.isHasGesture = YES;
    [shareViewController actionClickWithShareBlock:^(NSString *type) {
        if (!weakSelf){
            return ;
        }
        
        SSDKPlatformType mainType = SSDKPlatformSubTypeQZone;
        if ([type isEqualToString:@"QQ"]){
            mainType = SSDKPlatformTypeQQ;
        } else if ([type isEqualToString:@"wechat"]){
            mainType = SSDKPlatformTypeWechat;
        } else if ([type isEqualToString:@"friendsCircle"]){
            mainType = SSDKPlatformSubTypeWechatTimeline;
        }
        [[ShareSDKManager sharedShareSDKManager] shareManagerWithType:mainType title:@"我正在精灵直播哦，快来看看" desc:@"我正在精灵直播哦，快来看看" img:imgUrl url:url callBack:^(BOOL success) {
            if (success){
                [[UIAlertView alertViewWithTitle:@"分享成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
            }
        }];
    }];
    [shareViewController showInView:self.parentViewController];
}


@end

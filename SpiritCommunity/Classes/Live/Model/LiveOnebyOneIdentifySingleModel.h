//
//  LiveOnebyOneIdentifySingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/9.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface LiveOnebyOneIdentifySingleModel : FetchModel

@property (strong,nonatomic)NSString *identify;

@end

//
//  HomeRootBannerListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "HomeRootBannerSingleModel.h"
@interface HomeRootBannerListModel : FetchModel

@property (nonatomic,strong)NSArray<HomeRootBannerSingleModel> *infoList;

@end

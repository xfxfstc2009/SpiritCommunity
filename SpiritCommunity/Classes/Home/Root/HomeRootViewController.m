//
//  HomeRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "HomeRootViewController.h"
#import "HomeRankingViewController.h"                       // 排行榜
#import "SearchRootViewController.h"                        // 搜索
#import "HomeRootHotCollectionViewCell.h"                   // Hot cell
#import "HomeRootHotCollectionViewHeader.h"                 // Hot Header
#import "HomeOneByOneTableViewCell.h"
#import "HomeOnebyoneSingleModel.h"
#import "HomeLiveSingleModel.h"
#import "HomeOnebyoneListModel.h"
#import "HomeLiveListModel.h"
#import "ShenheLiveListModel.h"
#import "ShenheLiveTableViewCell.h"
#import "ZFPlayerView.h"
#import "ShenheObOCollectionViewCell.h"
#import "GWPopImgView.h"
#import <pop/POP.h>
#import "LiveErrorStatusSingleModel.h"
#import "HomeHasInSingleModel.h"
#import "ShopRootViewController.h"
#import "RESideMenu.h"


static NSString *hotCollectionCellIdentify = @"hotCollectionCellIdentify";
static NSString *hotCollectionHeaderIdentify = @"hotCollectionHeaderIdentify";
static NSString *shenhehotCollectionHeaderIdentify = @"shenhehotCollectionHeaderIdentify";
typedef struct {
    CGFloat progress;
    CGFloat toValue;
    CGFloat currentValue;
} AnimationInfo;


@interface HomeRootViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource>{
    HomeRootHotCollectionViewHeader *collectionViewHeaderView;
    BOOL hasLoad;
}
@property (nonatomic,strong)NSMutableArray *onebyoneMutableArr;
@property (nonatomic,strong)UICollectionView *onebyoneCollectionView;

@property (nonatomic,strong)UITableView *liveTableView;
@property (nonatomic,strong)NSMutableArray *liveMutableArr;

@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;

@property (nonatomic,strong) ZFPlayerView *playerView;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)GWPopImgView *topTVImgView;

@property (nonatomic,strong)UIView *barTitleViews;
@end

@implementation HomeRootViewController

+(instancetype)sharedAccountModel{
    static HomeRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[HomeRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)autoLogin{
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        PDSliderRootViewController *sliderRootViewController = (PDSliderRootViewController *)[RESideMenu shareInstance].leftMenuViewController;
        [sliderRootViewController logoutSuccess];
    } else {
        if ([Tool userDefaultGetWithKey:LoginType].length && [Tool userDefaultGetWithKey:LoginKey].length){
            
            [[AccountModel sharedAccountModel] autoLoginWithBlock:^(BOOL successed) {
                if (successed){
                    PDSliderRootViewController *sliderRootViewController = (PDSliderRootViewController *)[RESideMenu shareInstance].leftMenuViewController;
                    [sliderRootViewController logoutSuccess];
                    
                }
            }];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createMainScrollView];
    [self autoLogin];
    [self shenheInterfaceManager];
    [[GWUserCurrentInfoGetManager sharedHealthManager] getUserCurrentInfoManager];           // 5.获取当前信息
}

#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
   self.rightButton = [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"home_find_icon"] barHltImage:[UIImage imageNamed:@"home_find_icon"] action:^{
        if  (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        SearchRootViewController *searchRootViewController = [[SearchRootViewController alloc]init];
        [strongSelf.navigationController pushViewController:searchRootViewController animated:YES];
    }];
    

    // segment
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"视频",@"直播"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mainScrollView setContentOffset:CGPointMake(self.mainScrollView.size_width * index, 0) animated:YES];
            if (index == 0){
                strongSelf.onebyoneCollectionView.isXiaLa = YES;
                [strongSelf sendRequestToGetOnebyone];
            } else {            // 刷新直播
                strongSelf.liveTableView.isXiaLa = YES;
                [strongSelf sendRequestToGetLive];
            }
            [strongSelf topViewWithShow:NO];
        }];
    self.navigationItem.titleView = self.segmentList;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    hasLoad = NO;
    self.onebyoneMutableArr = [NSMutableArray array];
    self.liveMutableArr = [NSMutableArray array];
}

#pragma mark - createMainScrollView
-(void)createMainScrollView{
    __weak typeof(self)weakSelf = self;
    if (!self.mainScrollView){
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
            [strongSelf.segmentList setSelectedButtonIndex:page animated:YES];
            if (page == 0){     // 一对一
                strongSelf.onebyoneCollectionView.isXiaLa = YES;
                [strongSelf sendRequestToGetOnebyone];
            } else {            // 直播
                if (strongSelf->hasLoad == NO){
                    [strongSelf sendRequestToGetLive];
                    strongSelf->hasLoad = YES;
                }
            }
        }];
        self.mainScrollView.frame =  CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64 - 44);
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, self.mainScrollView.size_height);
        [self.view addSubview:self.mainScrollView];
    }

    // 2. 创建一对一
    [self createCollectionView];
    // 1. 创建直播
    [self createTableView];

}



#pragma mark - UICollectionView
// 创建直播
-(void)createCollectionView{
    if (!self.onebyoneCollectionView){
        self.onebyoneCollectionView = [GWViewTool gwCreateCollectionViewRect:self.mainScrollView.bounds];
        self.onebyoneCollectionView.dataSource = self;
        self.onebyoneCollectionView.delegate = self;
        self.onebyoneCollectionView.backgroundColor = [UIColor clearColor];
        [self.mainScrollView addSubview:self.onebyoneCollectionView];
    }
    
    // 1. 创建直播HotCell
    [self.onebyoneCollectionView registerClass:[HomeRootHotCollectionViewCell class] forCellWithReuseIdentifier:hotCollectionCellIdentify];
    [self.onebyoneCollectionView registerClass:[ShenheObOCollectionViewCell class] forCellWithReuseIdentifier:shenhehotCollectionHeaderIdentify];
    // header
    [self.onebyoneCollectionView registerClass:[HomeRootHotCollectionViewHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:hotCollectionHeaderIdentify];


    
    __weak typeof(self)weakSelf = self;
    [self.onebyoneCollectionView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;

        if ([AccountModel sharedAccountModel].isShenhe){
            [strongSelf sendRequestToGetShenheOneByOne];
        } else {
            [strongSelf sendRequestToGetOnebyone];
        }
    }];

    [self.onebyoneCollectionView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([AccountModel sharedAccountModel].isShenhe){
            [strongSelf sendRequestToGetShenheOneByOne];
        } else {
            [strongSelf sendRequestToGetOnebyone];
        }
    }];

}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSInteger index = self.onebyoneMutableArr.count;
    return index;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    HomeRootHotCollectionViewCell *cell = (HomeRootHotCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:hotCollectionCellIdentify forIndexPath:indexPath];
    HomeLiveSingleModel *singleModel = [self.onebyoneMutableArr objectAtIndex:indexPath.row];
    cell.transferOnebyoneSingleModel = singleModel;
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    collectionViewHeaderView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:hotCollectionHeaderIdentify forIndexPath:indexPath];

    collectionViewHeaderView.transferSize = CGSizeMake(kScreenBounds.size.width, [HomeRootHotCollectionViewHeader calculationCellHeight]);
    
    __weak typeof(self)weakSelf = self;
    [collectionViewHeaderView homeBannerActionClickBlock:^(GWScrollViewSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (singleModel.type == ActionClickTypeWeb){            // 跳转到网页
            WebViewController *webViewController = [[WebViewController alloc]init];
            [webViewController webViewControllerWithAddress:singleModel.info];
            [strongSelf.navigationController pushViewController:webViewController animated:YES];
        }
    }];
    
    return collectionViewHeaderView;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,[HomeRootHotCollectionViewHeader calculationCellHeight]};
    return size;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize itemSize = CGSizeMake((kScreenBounds.size.width - 25) / 2, (kScreenBounds.size.width - 25) / 2);
    return itemSize;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(2, 7, 2, 7);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    id tempModel = [self.onebyoneMutableArr objectAtIndex:indexPath.section];
    if ([tempModel isKindOfClass:[ShenheLiveSIngleModel class]]){
        ShenheLiveSIngleModel *singleModel = [self.onebyoneMutableArr objectAtIndex:indexPath.row];
        self.playerView = [ZFPlayerView sharedPlayerView];
        NSURL *videoURL = [NSURL URLWithString:singleModel.url];

        self.playerView.playerLayerGravity = ZFPlayerLayerGravityResizeAspectFill;
        [self.playerView setVideoURL:videoURL];
        [self.playerView addPlayerToCellImageView:self.topTVImgView.imageView];
        [self topViewWithShow:YES];
    } else if ([tempModel isKindOfClass:[HomeLiveSingleModel class]]){
        __weak typeof(self)weakSelf = self;
        [self authorizeWithCompletionHandler:^(BOOL successed) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            HomeLiveSingleModel *singleModel = [strongSelf.onebyoneMutableArr objectAtIndex:indexPath.row];
            [strongSelf actionToLive:singleModel];
        }];
        
    }
}

-(void)actionToLive:(HomeLiveSingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusBarHidenWithText:@"正在判断房间是否可以进入"];
    [self sendRequestToHasIn:singleModel.house_id type:2 block:^(BOOL hasIn) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [LiveRootManager liveHomeDirectToModel:singleModel controller:strongSelf];
    }];
}



#pragma mark - 创建直播
-(void)createTableView{
    if (!self.liveTableView){
        self.liveTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.liveTableView.dataSource = self;
        self.liveTableView.delegate = self;
        self.liveTableView.orgin_x = self.mainScrollView.size_width;
        [self.mainScrollView addSubview:self.liveTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.liveTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([AccountModel sharedAccountModel].isShenhe){
            [strongSelf sendRequestToGetShenheLive];
        } else {
            [strongSelf sendRequestToGetLive];
        }
    }];
    [self.liveTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([AccountModel sharedAccountModel].isShenhe){
            [strongSelf sendRequestToGetShenheLive];
        } else {
            [strongSelf sendRequestToGetLive];
        }
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.liveMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    id tempModel = [self.liveMutableArr objectAtIndex:indexPath.section];
    if ([tempModel isKindOfClass:[HomeLiveSingleModel class]]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        HomeOneByOneTableViewCell *cellWithRowOne =[tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[HomeOneByOneTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        HomeLiveSingleModel *singleModel = [self.liveMutableArr objectAtIndex:indexPath.section];
        cellWithRowOne.transferLiveModel = singleModel;
        return cellWithRowOne;
    } else if ([tempModel isKindOfClass:[ShenheLiveSIngleModel class]]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ShenheLiveTableViewCell *cellWithRowOne =[tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ShenheLiveTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferIndexPath = indexPath;
        ShenheLiveSIngleModel *singleModel = [self.liveMutableArr objectAtIndex:indexPath.section];
        cellWithRowOne.transferLiveSingleModel = singleModel;

        __block NSIndexPath *weakIndexPath = indexPath;
        __block ShenheLiveTableViewCell *weakCell = cellWithRowOne;
        __weak typeof(self) weakSelf = self;
        [cellWithRowOne playerViewClickManagerWithBlock:^(ShenheLiveSIngleModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __weak typeof(weakSelf)strongSelf = weakSelf;
            weakSelf.playerView = [ZFPlayerView sharedPlayerView];
            NSURL *videoURL = [NSURL URLWithString:singleModel.url];
            [weakSelf.playerView setVideoURL:videoURL withTableView:strongSelf.liveTableView AtIndexPath:weakIndexPath withImageViewTag:102];
            [weakSelf.playerView addPlayerToCellImageView:weakCell.playerImageView];
            weakSelf.playerView.playerLayerGravity = ZFPlayerLayerGravityResizeAspectFill;
        }];

        return cellWithRowOne;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    id tempModel = [self.liveMutableArr objectAtIndex:indexPath.row];
    if ([tempModel isKindOfClass:[HomeLiveSingleModel class]]){
        return [HomeOneByOneTableViewCell calculationCellHeight];
   } else if ([tempModel isKindOfClass:[ShenheLiveSIngleModel class]]){
        return [ShenheLiveTableViewCell calculationCellHeightWithPlayerSingleModel:tempModel];
   } else {
       return 44;
   }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    id tempModel = [self.liveMutableArr objectAtIndex:indexPath.row];
    if ([tempModel isKindOfClass:[HomeLiveSingleModel class]]){
        __weak typeof(self)weakSelf = self;
        [self authorizeWithCompletionHandler:^(BOOL successed) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            HomeLiveSingleModel *singleModel = [strongSelf.liveMutableArr objectAtIndex:indexPath.row];
            [strongSelf actionToOneByOne:singleModel];
        }];
    } else if ([tempModel isKindOfClass:[ShenheLiveSIngleModel class]]){

    } else {

    }
}

-(void)actionToOneByOne:(HomeLiveSingleModel *)singleModel{
    [StatusBarManager statusBarHidenWithText:@"正在判断房间是否可以进入"];
    __weak typeof(self)weakSelf = self;
    [self sendRequestToHasIn:singleModel.house_id type:1 block:^(BOOL hasIn) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [LiveRootManager liveHomeDirectToModel:singleModel controller:strongSelf];
    }];
}

#pragma mark - 获取banner数据
-(void)sendRequestToGetBannerInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_banner requestParams:nil responseObjectClass:[BannerListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            BannerListModel *bannerListModel = (BannerListModel *)responseObject;
            strongSelf->collectionViewHeaderView.transferArr = bannerListModel.list;
        }
    }];
}

#pragma mark - 获取一对一数据
-(void)sendRequestToGetOnebyone{
    __weak typeof(self)weakSelf = self;
    NSInteger page = self.onebyoneCollectionView.currentPage;
    NSInteger size = 4;
    NSDictionary *params = @{@"page":@(page),@"size":@(size)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_onebyone requestParams:params responseObjectClass:[HomeOnebyoneListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            HomeOnebyoneListModel *listModel = (HomeOnebyoneListModel *)responseObject;
            if (page == 1 || strongSelf.onebyoneCollectionView.isXiaLa){
                [strongSelf.onebyoneMutableArr removeAllObjects];
            }

            [strongSelf.onebyoneMutableArr addObjectsFromArray:listModel.list];
            [strongSelf.onebyoneCollectionView reloadData];
            
//            if(!strongSelf.onebyoneMutableArr.count){
//                [strongSelf.onebyoneCollectionView showPrompt:@"当前没有一对一视频哦" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
//            } else {
//                [strongSelf.onebyoneCollectionView dismissPrompt];
//            }
            
            [strongSelf.onebyoneCollectionView stopFinishScrollingRefresh];
            [strongSelf.onebyoneCollectionView stopPullToRefresh];
        }
    }];
}


#pragma mark - 获取直播
-(void)sendRequestToGetLive{
    __weak typeof(self)weakSelf = self;
    NSInteger page = self.liveTableView.currentPage;
    NSInteger size = 4;
    NSDictionary *params = @{@"page":@(page),@"size":@(size)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_live requestParams:params responseObjectClass:[HomeLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            HomeLiveListModel *listModel = (HomeLiveListModel *)responseObject;
            if (strongSelf.liveTableView.isXiaLa){
                [strongSelf.liveMutableArr removeAllObjects];
            }
            
            [strongSelf.liveMutableArr addObjectsFromArray:listModel.list];
            [strongSelf.liveTableView reloadData];
            
            [strongSelf.liveTableView stopFinishScrollingRefresh];
            [strongSelf.liveTableView stopPullToRefresh];
            if (strongSelf.liveMutableArr.count){
                [strongSelf.liveTableView dismissPrompt];
            } else {
                [strongSelf.liveTableView showPrompt:@"当前没有直播TV哦,点击刷新" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
                    [strongSelf sendRequestToGetLive];
                }];
            }
        }
    }];
}

#pragma mark - 获取审核信息
-(void)sendRequestToGetShenheLive{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"type":@"2",@"page":@(self.liveTableView.currentPage),@"size":@(4)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:shenhe_live requestParams:params responseObjectClass:[ShenheLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            ShenheLiveListModel *singleModel = (ShenheLiveListModel *)responseObject;
            if (strongSelf.liveTableView.isXiaLa){
                [strongSelf.liveMutableArr removeAllObjects];
            }
            if (singleModel.list.count){
                [strongSelf.liveMutableArr addObjectsFromArray:singleModel.list];
                [strongSelf.liveTableView reloadData];
            }
            [strongSelf.liveTableView stopFinishScrollingRefresh];
            [strongSelf.liveTableView stopPullToRefresh];

            if (singleModel.list.count){
                [strongSelf.liveTableView hiddenBottomTitle];
            } else {
                [strongSelf.liveTableView showBottomTitle];
            }
            if (strongSelf.liveMutableArr.count){
                [strongSelf.liveTableView dismissPrompt];
            } else {
                [strongSelf.liveTableView showPrompt:@"当前没有直播TV哦,点击刷新" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
                    [strongSelf sendRequestToGetLive];
                }];
            }

        }
    }];
}

#pragma mark - 获取审核信息
-(void)sendRequestToGetShenheOneByOne{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"type":@"1",@"page":@(self.onebyoneCollectionView.currentPage),@"size":@(6)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:shenhe_live requestParams:params responseObjectClass:[ShenheLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            ShenheLiveListModel *singleModel = (ShenheLiveListModel *)responseObject;
            if (strongSelf.liveTableView.isXiaLa){
                [strongSelf.onebyoneMutableArr removeAllObjects];
            }
            if (singleModel.list.count){
                [strongSelf.onebyoneMutableArr addObjectsFromArray:singleModel.list];
                [strongSelf.onebyoneCollectionView reloadData];
                [strongSelf.onebyoneCollectionView hiddenBottomTitle];
            } else {
                [strongSelf.onebyoneCollectionView showBottomTitle];
            }

            [strongSelf.onebyoneCollectionView stopFinishScrollingRefresh];
            [strongSelf.onebyoneCollectionView stopPullToRefresh];

            if (strongSelf.onebyoneMutableArr.count){
                [strongSelf.onebyoneCollectionView dismissPrompt];
            } else {
                [strongSelf.onebyoneCollectionView showPrompt:@"当前没有缓存视频哦,点击刷新" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
                    [strongSelf sendRequestToGetShenheOneByOne];
                }];
            }

        }
    }];
}

-(void)shenheInterfaceManager{
    [self sendRequestToGetBannerInfo];              // 获取banner

    if ([AccountModel sharedAccountModel].isShenhe){            // 正在审核中
        [self sendRequestToGetShenheLive];
        [self sendRequestToGetShenheOneByOne];
//        self.rightButton.hidden = YES;
    } else {                                                    // 不在审核中
        [self sendRequestToGetLive];
        [self sendRequestToGetOnebyone];
//        self.rightButton.hidden = NO;
    }
}



-(void)topViewWithShow:(BOOL)show{
    if (show){
        [UIView animateWithDuration:.4f animations:^{
            self.topTVImgView.orgin_y = 0;
        } completion:^(BOOL finished) {
            self.topTVImgView.orgin_y = 0;;
        }];
    } else {
        [[ZFPlayerView sharedPlayerView] pause];
        [UIView animateWithDuration:.4f animations:^{
            self.topTVImgView.orgin_y = -self.topTVImgView.size_height;
        } completion:^(BOOL finished) {
            self.topTVImgView.orgin_y = -self.topTVImgView.size_height;
        }];
    }
}



-(void)createTopTVImgView{
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    if (!self.topTVImgView){
        self.topTVImgView = [[GWPopImgView alloc] initWithFrame:CGRectMake(0, -LCFloat(200), kScreenBounds.size.width, LCFloat(200))];
        self.topTVImgView.backgroundColor = [UIColor blackColor];

        [self.view addSubview:self.topTVImgView];
//        [self.topTVImgView addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
        [self.topTVImgView addTarget:self action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [self.topTVImgView addGestureRecognizer:recognizer];
        [self.view addSubview:self.topTVImgView];
        [self scaleDownView:self.topTVImgView];
    }
}

- (void)touchDown:(UIControl *)sender {
    [self pauseAllAnimations:YES forLayer:sender.layer];
}

- (void)touchUpInside:(UIControl *)sender {

    [self.playerView actionClickCustomerAres];
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    [self scaleDownView:recognizer.view];
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];

    if(recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint velocity = [recognizer velocityInView:self.view];

        POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
        positionAnimation.velocity = [NSValue valueWithCGPoint:velocity];
        positionAnimation.dynamicsTension = 10.f;
        positionAnimation.dynamicsFriction = 1.0f;
        positionAnimation.springBounciness = 12.0f;
        [recognizer.view.layer pop_addAnimation:positionAnimation forKey:@"layerPositionAnimation"];
    }
}

-(void)scaleUpView:(UIView *)view
{
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
    positionAnimation.toValue = [NSValue valueWithCGPoint:self.view.center];
    [view.layer pop_addAnimation:positionAnimation forKey:@"layerPositionAnimation"];

    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1, 1)];
    scaleAnimation.springBounciness = 10.f;
    [view.layer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)scaleDownView:(UIView *)view
{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1., 1.)];
    scaleAnimation.springBounciness = 10.f;
    [view.layer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)pauseAllAnimations:(BOOL)pause forLayer:(CALayer *)layer
{
    for (NSString *key in layer.pop_animationKeys) {
        POPAnimation *animation = [layer pop_animationForKey:key];
        [animation setPaused:pause];
    }
}

- (AnimationInfo)animationInfoForLayer:(CALayer *)layer
{
    POPSpringAnimation *animation = [layer pop_animationForKey:@"scaleAnimation"];
    CGPoint toValue = [animation.toValue CGPointValue];
    CGPoint currentValue = [[animation valueForKey:@"currentValue"] CGPointValue];

    CGFloat min = MIN(toValue.x, currentValue.x);
    CGFloat max = MAX(toValue.x, currentValue.x);

    AnimationInfo info;
    info.toValue = toValue.x;
    info.currentValue = currentValue.x;
    info.progress = min / max;
    return info;
}

#pragma mark - 获取当前错误退出信息
-(void)sendRequestToErrStatusManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_err_status requestParams:nil responseObjectClass:[LiveErrorStatusSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            LiveErrorStatusSingleModel *singleModel = (LiveErrorStatusSingleModel *)responseObject;
            if (singleModel){

                NSString *info = [NSString stringWithFormat:@"您在上一次观看直播%@意外退出,开始时间%@，结束时间%@。",singleModel.houseId,[NSDate getTimeWithLotteryString:singleModel.start_time],[NSDate getTimeWithLotteryString:singleModel.end_time]];
                [[UIAlertView alertViewWithTitle:@"提示" message:info buttonTitles:@[@"确定"] callBlock:NULL]show];
            }
        }
    }];
}


#pragma mark - 判断是否可以进入
-(void)sendRequestToHasIn:(NSString *)houseId type:(NSInteger)type block:(void(^)(BOOL hasIn))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"houseId":houseId,@"show_type":@(type)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_has_in requestParams:params responseObjectClass:[HomeHasInSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            HomeHasInSingleModel *singleModel = (HomeHasInSingleModel *)responseObject;
            if(block){
                block(singleModel.hasIn);
            }
        }
    }];
}


@end

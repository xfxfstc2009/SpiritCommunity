//
//  OrderRootRowTwoTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListSingleModel.h"

@interface OrderRootRowTwoTableViewCell : UITableViewCell

@property (nonatomic,strong)OrderListSingleModel *transferSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

-(void)actionClickWithBlock:(void(^)(OrderListSingleModel *transferSingleModel,NSString *title))block;

@end

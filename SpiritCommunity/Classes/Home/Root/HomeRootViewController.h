//
//  HomeRootViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"

@interface HomeRootViewController : AbstractViewController

+(instancetype)sharedAccountModel;

-(void)shenheInterfaceManager;

@end

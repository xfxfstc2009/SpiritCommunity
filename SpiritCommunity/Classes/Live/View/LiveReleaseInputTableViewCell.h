//
//  LiveReleaseInputTableViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/18.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveReleaseInputTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)UITextField *inputTextField;

@property (nonatomic,copy)NSString *transferInfo;

-(void)actionInputWithText:(void(^)(NSString *str))block;

+(CGFloat)calculationCellHeight;
@end

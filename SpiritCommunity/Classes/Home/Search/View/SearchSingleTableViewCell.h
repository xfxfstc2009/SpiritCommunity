//
//  SearchSingleTableViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OtherSearchSingleModel.h"

typedef NS_ENUM(NSInteger,SearchControllerType) {
    SearchControllerTypeSearch,
    SearchControllerTypeFans,
};

@interface SearchSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)OtherSearchSingleModel *transferSingleModel;
@property (nonatomic,assign)SearchControllerType transferType;


+(CGFloat)calculationCellHeight;

-(void)actionClickWithLinkWithBlock:(void(^)(BOOL status))block;

-(void)actionClickZan:(BOOL)status;

@end

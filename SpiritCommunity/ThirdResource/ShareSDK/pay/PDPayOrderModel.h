//
//  PDPayOrderModel.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/16.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDPayOrderModel : NSObject

@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *orderNum;
@property (nonatomic,copy)NSString *amount;
@property (nonatomic,copy)NSString *orderName;



@end

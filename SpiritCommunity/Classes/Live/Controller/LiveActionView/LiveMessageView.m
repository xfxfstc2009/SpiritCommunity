//
//  LiveMessageView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveMessageView.h"
#import "LiveMessageTableViewCell.h"

@interface LiveMessageView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *messageTableView;

@end

@implementation LiveMessageView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    if (!self.messageMutableArr){
        self.messageMutableArr = [NSMutableArray array];
    }
    
    if (!self.messageTableView){
        self.messageTableView = [GWViewTool gwCreateTableViewRect:self.bounds];
        self.messageTableView.backgroundColor = [UIColor clearColor];
        self.messageTableView.dataSource = self;
        self.messageTableView.delegate = self;
        [self addSubview:self.messageTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.messageMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    LiveMessageTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[LiveMessageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    id msg = [self.messageMutableArr objectAtIndex:indexPath.row];
    if ([msg isKindOfClass:[ILVLiveTextMessage class]]){
        ILVLiveTextMessage *textMsg = (ILVLiveTextMessage *)msg;
        [cellWithRowOne configMsg:textMsg.senderProfile.nickname.length ? textMsg.senderProfile.nickname : [[ILiveLoginManager getInstance] getLoginId] msg:textMsg.text];
    } else if([msg isKindOfClass:[ILVLiveCustomMessage class]]){
        ILVLiveCustomMessage *customMsg = (ILVLiveCustomMessage *)msg;
        [cellWithRowOne configTips:customMsg.senderProfile.nickname];
    }
    
    return cellWithRowOne;
}


#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.messageTableView){
        LiveMessageTableViewCell *cell = (LiveMessageTableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        NSLog(@"index = %ld, height = %f", (long)indexPath.row, cell.frame.size.height);
        return cell.height;
    } else {
        return 44;
    }
}

-(void)viewSendMsg:(ILVLiveMessage *)msg{
    [self.messageMutableArr addObject:msg];
    if (self.messageMutableArr.count >= 500) {
        
        NSRange range = NSMakeRange(self.messageMutableArr.count - 100, 100);//只保留最新的100条消息
        NSArray *temp = [self.messageMutableArr subarrayWithRange:range];
        [self.messageMutableArr removeAllObjects];
        [self.messageMutableArr addObjectsFromArray:temp];
        [self.messageTableView reloadData];
    } else {
        [self.messageTableView beginUpdates];
        NSIndexPath *index = [NSIndexPath indexPathForRow:self.messageMutableArr.count - 1 inSection:0];
        [self.messageTableView insertRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationBottom];
        [self.messageTableView endUpdates];
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.messageMutableArr.count-1  inSection:0];
    if (indexPath.row < [self.messageTableView numberOfRowsInSection:0]) {
        [self.messageTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }

}
@end

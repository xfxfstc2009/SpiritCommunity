//
//  GWWeatherCurrentHeaderTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GWWeatherCurrentModel.h"
@interface GWWeatherCurrentHeaderTableViewCell : UITableViewCell

@property (nonatomic,strong)GWWeatherCurrentModel *transferCurrentModel;


+(CGFloat)calculationCellHeight;
@end

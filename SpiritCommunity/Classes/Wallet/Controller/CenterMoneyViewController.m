//
//  CenterMoneyViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CenterMoneyViewController.h"
#import "CenterMoneyRootHeaderCell.h"
#import "CenterMoneyModel.h"
#import "CenterMoneyDetailViewController.h"
#import "ChongzhiRootViewController.h"
#import "CenterZhiFuViewController.h"               // 支付宝


@interface CenterMoneyViewController()<UITableViewDataSource,UITableViewDelegate>{
    CenterMoneyModel *centerMoneyModel;
}
@property (nonatomic,strong)UITableView *moneyTableView;
@property (nonatomic,strong)NSArray *moneyArr;

@end

@implementation CenterMoneyViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestTogetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的钱包";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"center_money_chongzhi"] barHltImage:[UIImage imageNamed:@"center_money_chongzhi"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ChongzhiRootViewController *chongzhiViewController = [[ChongzhiRootViewController alloc]init];
        [strongSelf.navigationController pushViewController:chongzhiViewController animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.moneyArr = @[@[@"头部",@"收支明细",@"收款支付宝"],@[@"提现"]];
    
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.moneyTableView){
        self.moneyTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.moneyTableView.dataSource = self;
        self.moneyTableView.delegate = self;
        [self.view addSubview:self.moneyTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.moneyArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.moneyArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if(indexPath.section == 0){
        if(indexPath.row == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            CenterMoneyRootHeaderCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[CenterMoneyRootHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferMoneyModel = centerMoneyModel;
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferTitle = [[self.moneyArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            cellWithRowTwo.transferHasArrow = YES;
            if([cellWithRowTwo.transferTitle isEqualToString:@"收款支付宝"]){
                if ([AccountModel sharedAccountModel].loginModel.zhifupay.length){
                   cellWithRowTwo.transferDesc = [AccountModel sharedAccountModel].loginModel.zhifupay;
                    cellWithRowTwo.transferHasArrow = NO;
                } else {
                    cellWithRowTwo.transferDesc = @"去设置";
                }
            } else {
                cellWithRowTwo.transferDesc = @"";
            }
            return cellWithRowTwo;
        }
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell*cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"提现";
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if(!weakSelf){
                return ;
            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
            [[UIAlertView alertViewWithTitle:@"提现" message:@"提现" buttonTitles:@[@"确定"] callBlock:NULL]show];
        }];
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"收支明细" sourceArr:self.moneyArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"收支明细" sourceArr:self.moneyArr]){
        CenterMoneyDetailViewController *centerMoneyDetailViewController = [[CenterMoneyDetailViewController alloc]init];
        [self.navigationController pushViewController:centerMoneyDetailViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"收款支付宝" sourceArr:self.moneyArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"收款支付宝" sourceArr:self.moneyArr]){
        if ([AccountModel sharedAccountModel].loginModel.zhifupay.length){
            [StatusBarManager statusBarHidenWithText:@"已设置收款信息"];
            return;
        }
        CenterZhiFuViewController *zhifuPayViewController = [[CenterZhiFuViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [zhifuPayViewController actionBidingSuccessedManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.moneyTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        }];
        [self.navigationController pushViewController:zhifuPayViewController animated:YES];
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == 0){
        if (indexPath.row == 0){
            return [CenterMoneyRootHeaderCell calculationCellHeight];
        } else {
            return LCFloat(44);
        }
    } else {
        return LCFloat(44);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 1){
        return LCFloat(40);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.moneyTableView) {
        if (indexPath.section != 1){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.moneyArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
            }
            if ([[self.moneyArr objectAtIndex:indexPath.section] count] == 1) {
                separatorType  = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}


#pragma mark - 获取接口
-(void)sendRequestTogetInfo{
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter] fetchWithPath:SpUserwalletInfo requestParams:nil responseObjectClass:[CenterMoneyModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSucceeded){
//            strongSelf->centerMoneyModel = (CenterMoneyModel *)responseObject;
//            
//        }
//    }];
}


@end

//
//  LiveUIBottomSingleView.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/5.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveUIBottomViewSingleModel.h"

@interface LiveUIBottomSingleView : UIView

-(instancetype)initWithFrame:(CGRect)frame model:(LiveUIBottomViewSingleModel *)model actionBlcok:(void(^)(LiveUIBottomViewSingleModel *model))block;

@end

//
//  EnumConstance.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#ifndef EnumConstance_h
#define EnumConstance_h

#define AUTO_Login_Token @"AUTO_Login_Token"

#pragma mark - 三方登录
typedef NS_ENUM(NSInteger,PDThirdLoginType) {
    PDThirdLoginTypeNor = 0,                /**< 手机*/
    PDThirdLoginTypeWechat = 1,             /**< 微信*/
    PDThirdLoginTypeQQ = 2,                 /**< QQ*/
};

#pragma mark - 登录页面action
typedef NS_ENUM(NSInteger,PDLoginViewControllerActionType) {
    PDLoginViewControllerActionTypeWeChatLogin,                     /**< 微信登录*/
};

typedef NS_ENUM(NSInteger,ActionClickType) {
    ActionClickTypeWeb = 1,                 /**< 跳转到网页*/
};

typedef NS_ENUM(NSInteger,LiveUserType) {
    LiveUserTypeNormal = 0,                 /**< 普通用户*/
    LiveUserTypeLive = 1,                   /**< 开通直播的*/
    LiveUserTypeObO = 2,                    /**< 开通一对一的*/
    LiveUserTypeALL = 3,                    /**< 全部开通的*/
};

typedef NS_ENUM(NSInteger,LiveShowType) {
    LiveShowTypeAudience = 1,               /**< 观众*/
    LiveShowTypeAnchor = 2,                 /**< 主播*/
};

typedef NS_ENUM(NSInteger,LiveObOorLiveType) {
    LiveObOorLiveTypeLive = 1,                          /**< 直播*/
    LiveObOorLiveTypeOnebyOne = 2,                      /**< 一对一*/
};


typedef NS_ENUM(NSInteger,ImageUsingType) {
    ImageUsingTypeBanner = 1,                   /**< Banner 首页的banner*/
    ImageUsingTypeUser = 2,                     /**< User/Avatar 用户头像*/
    ImageUsingTypeUserShow = 3,                 /**< User/Show 用户相册*/
    ImageUsingTypeObO =4,                       /**< Live/OneByOne 一对一视频*/
    ImageUsingTypeLive = 5,                     /**< Live/Live 直播*/
    ImageUsingTypeGift = 6,                     /**< Live/Gift 礼物*/
    ImageUsingTypeActivity = 7,                 /**< Activity/ 活动页面*/
    ImageUsingTypeShare = 8,                    /**< Share/ 分享页面*/
    ImageUsingTypeLaungch = 9,                  /**< Laungch/ 分享页面*/
};

typedef NS_ENUM(NSInteger,UserEditType) {
    UserEditTypeAvatar,                         /**< 修改用户头像*/
    UserEditTypeNickName,                       /**< 修改用户昵称*/
    UserEditTypeSex,                            /**< 修改用户性别*/
    UserEditTypeAge,                            /**< 修改用户年龄*/
};


// 【app配置】
#define User_Choose_Game @"User_Choose_Game"
#define TestNet_Log @"testNet_Log"

#endif /* EnumConstance_h */

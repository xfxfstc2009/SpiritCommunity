//
//  LiveEndRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/8.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveEndRootViewController.h"

@interface LiveEndRootViewController ()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *numberLabel;

@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeDescLabel;

@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation LiveEndRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createView];
}

#pragma mark - UIView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.avatarImgView];

    // name
    self.nameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.nameLabel];

    // number
    self.numberLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.numberLabel];

    // title
    self.titleLabel = [GWViewTool createLabelFont:@"标题" textColor:@"白"];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.titleLabel];

    // time
    self.timeDescLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.timeDescLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.timeDescLabel];

    // btn
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;

    }];
    [self.view addSubview:self.actionButton];
}

@end

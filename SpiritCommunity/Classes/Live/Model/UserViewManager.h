//
//  UserViewManager.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/1.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LiveCallView.h"

//随心播限制界面上最多有4路画面（包括1路主播和3路连麦用户的画面），本类对这些画面的位置进行管理
@interface UserViewManager : NSObject

+ (instancetype)shareInstance;

@property (nonatomic, assign) NSInteger total;                                          //小画面的总数（不算主画面）
@property (nonatomic, strong) NSMutableDictionary *placeholderViews;
@property (nonatomic, strong) NSMutableDictionary *renderViews;
@property (nonatomic, strong) ILiveRenderView *mainRenderView;                          /**< 主渲染图 用来做渲染的view*/
@property (nonatomic, strong) NSString *mainCodeUserId;                                 
@property (nonatomic, strong) NSString *mainUserId;

- (ILiveRenderView *)addRenderView:(NSString *)userId srcType:(avVideoSrcType)type;     /**< 添加渲染*/
- (void)removeRenderView:(NSString *)userId srcType:(avVideoSrcType)type;               /**< 移除渲染*/


- (LiveCallView *)addPlaceholderView:(NSString *)userId;
- (CGRect)removePlaceholderView:(NSString *)userId;

- (ILiveRenderView *)renderviewReplacePlaceholderView:(NSString *)userId srcType:(avVideoSrcType)type;

- (BOOL)switchToMainView:(NSString *)codeUserId;

- (void)refreshViews;//刷新View

- (void)releaseManager;

//是否已经存在占位符
- (BOOL)isExistPlaceholder:(NSString *)userId;
//是否已经存在渲染视图
- (BOOL)isExistRenderView:(NSString *)userId;

- (avVideoSrcType)getUserType:(NSString *)userId;

+ (NSDictionary *)decodeUser:(NSString *)identifier;

+ (NSString *)codeUser:(NSString *)identifier type:(avVideoSrcType)type;

@end

//
//  FindRootTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FindRootTableViewCell.h"
#import <pop/POP.h>

@interface FindRootTableViewCell()
@property (nonatomic,strong)PDImageView *findImageView;         /**< 图片*/
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@end

@implementation FindRootTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView{
-(void)createView{
    UIImageView *bgView = [[UIImageView alloc]init];
    self.backgroundView.userInteractionEnabled = YES;
    bgView.image = [Tool stretchImageWithName:@"common_card_background"];
    self.backgroundView = bgView;
    
    // img
    self.findImageView = [[PDImageView alloc]init];
    self.findImageView.userInteractionEnabled = YES;
    self.findImageView.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200));
    self.findImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.findImageView];
    
    // 2. title
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.titleLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.findImageView.frame) + LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    // 3. 创建创建时间
    self.timeLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    self.timeLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(5), [NSString contentofHeightWithFont:self.timeLabel.font]);
    [self addSubview:self.timeLabel];
}

-(void)setTransferIndexPath:(NSInteger)transferIndexPath{
    _transferIndexPath = transferIndexPath;
}

-(void)setTransferSingleModel:(FindRootSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    //1. 设置图片
    [self.findImageView uploadImageWithURL:transferSingleModel.image placeholder:nil callback:NULL];
    
    // 2. 创建label
    self.titleLabel.text = transferSingleModel.title;
    self.titleLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.findImageView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    GWStudyItem *item = [GWStudyItem itemWithName:transferSingleModel.title object:@"123"];
    item.index = self.transferIndexPath + 1;
    [item createAttributedString];
    self.titleLabel.attributedText = item.nameString;
    
    // 3.获取创建时间
    self.timeLabel.text = [NSDate getTimeGap:transferSingleModel.datetime];
    self.timeLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(5), self.titleLabel.size_width, [NSString contentofHeightWithFont:self.timeLabel.font]);
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    if (self.highlighted) {
        [self showBlunesManager1];
    } else {
        [self showBlunesManager];
    }
}

-(void)showBlunesManager{
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.duration           = 0.2f;
    scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
    [self.titleLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.timeLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

-(void)showBlunesManager1{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(6, 6)];
    scaleAnimation.springBounciness    = 1.f;
    [self.titleLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.timeLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)startAnimationWithDelay:(CGFloat)delayTime {
    self.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        //        [self showBlunesManager];
    }];
}

+(CGFloat)calculationCellHeight:(FindRootSingleModel *)transferSingleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(200);
    cellHeight += LCFloat(11);
    CGSize contentOfSize = [transferSingleModel.title sizeWithCalcFont:[UIFont systemFontOfCustomeSize:12.] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeightWithFont:[UIFont AvenirLightWithFontSize:9.f]];
    cellHeight += LCFloat(11);
    return cellHeight;
}
@end

//
//  AppDelegate.h
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/21.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


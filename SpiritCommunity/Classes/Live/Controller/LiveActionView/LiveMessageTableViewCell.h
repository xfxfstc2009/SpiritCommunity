//
//  LiveMessageTableViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/5.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveMessageTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)UILabel *msgLabel;              /**< 消息label*/
@property (nonatomic,strong)UILabel *tipsLabel;             /**< 提示label*/
@property (nonatomic, assign) CGFloat height;

- (void)configMsg:(NSString *)userId msg:(NSString *)text;
- (void)configTips:(NSString *)user;

@end

//
//  ProductDetailSub2TableViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/9.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailSub2TableViewCell : UITableViewCell

@property (nonatomic,copy)NSString *transferImg;
@property (nonatomic,assign)CGFloat cellHeight;
-(void)actionBlockHeight:(void(^)(CGFloat imgHeihgt))block;
@end

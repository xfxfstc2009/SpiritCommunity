//
//  SearchRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "SearchRootViewController.h"
#import "SearchSingleTableViewCell.h"
#import "UserInfoViewController.h"
#import "OtherSearchListModel.h"
#import "ToLinkSingleModel.h"

@interface SearchRootViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate>
@property (nonatomic,strong)UITableView *searchTableView;
@property (nonatomic,strong)NSMutableArray *searchMutableArr;
@property (nonatomic,strong)NSMutableArray *searchControllerMutableArr;
@property (nonatomic, strong) UISearchBar *searchBar;                           /**< 搜索条*/
@property (nonatomic, strong) UISearchDisplayController *displayController;
@end

@implementation SearchRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSearchBar];
    [self createSearchDisplay];
    [self createTableView];
    [self sendRequestGetHot];                   // 获取当前的热门
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"搜索";
}

#pragma mark - searchBar
-(void)createSearchBar{
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 44)];
    self.searchBar.backgroundColor = [UIColor clearColor];
    self.searchBar.barStyle = UIBarStyleDefault;
    self.searchBar.translucent = YES;
    self.searchBar.delegate = self;
    if ([AccountModel sharedAccountModel].isShenhe){
        self.searchBar.placeholder = @"请输入要搜索的小伙伴";
    } else {
        self.searchBar.placeholder = @"请输入要搜索的小伙伴";
    }

    self.searchBar.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:self.searchBar];
}
- (void)createSearchDisplay {
    self.displayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.displayController.delegate = self;
    self.displayController.searchResultsDataSource = self;
    self.displayController.searchResultsDelegate = self;
}

#pragma mark - 创建tableView
-(void)arrayWithInit{
    self.searchMutableArr = [NSMutableArray array];
    self.searchControllerMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.searchTableView){
        self.searchTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.searchTableView.dataSource = self;
        self.searchTableView.delegate = self;
        self.searchTableView.orgin_y = CGRectGetMaxY(self.searchBar.frame);
        self.searchTableView.size_height -= self.searchBar.size_height;
        [self.view addSubview:self.searchTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.searchTableView){
        return self.searchMutableArr.count;
    } else {
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.searchTableView){
        NSArray *sectionOfArr = [self.searchMutableArr objectAtIndex:section];
        return sectionOfArr.count;
    } else {
        return self.searchControllerMutableArr.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.searchTableView){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        SearchSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[SearchSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
        }
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        OtherSearchSingleModel *singleModel = [[self.searchMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowOne.transferSingleModel = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne actionClickWithLinkWithBlock:^(BOOL status) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestTolinkManger:singleModel cell:cellWithRowOne];
        }];
        return cellWithRowOne;
    } else if (tableView == self.displayController.searchResultsTableView){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        SearchSingleTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[SearchSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = [UIColor clearColor];
        }
        cellWithRowTwo.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        OtherSearchSingleModel *singleModel = [self.searchControllerMutableArr  objectAtIndex:indexPath.row];
        cellWithRowTwo.transferSingleModel = singleModel;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionClickWithLinkWithBlock:^(BOOL status) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestTolinkManger:singleModel cell:cellWithRowTwo];
        }];
        return cellWithRowTwo;
    }
    return  nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [SearchSingleTableViewCell calculationCellHeight];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    UILabel *fixedLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    fixedLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(20));
    if(section == 0){
        fixedLabel.text = @"粉丝推荐";
    } else {
        fixedLabel.text = @"历史搜索";
    }
    [headerView addSubview:fixedLabel];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchTableView && self.searchMutableArr.count) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.searchMutableArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.searchMutableArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.searchTableView){
        UserInfoViewController *userInfoViewController = [[UserInfoViewController alloc]init];
        OtherSearchSingleModel *singleModel = [[self.searchMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        userInfoViewController.transferSearchSingleModel = singleModel;
        [self.navigationController pushViewController:userInfoViewController animated:YES];
    } else if (tableView == self.displayController.searchResultsTableView){
        UserInfoViewController *userInfoViewController = [[UserInfoViewController alloc]init];
        OtherSearchSingleModel *singleModel = [self.searchControllerMutableArr  objectAtIndex:indexPath.row];
        userInfoViewController.transferSearchSingleModel = singleModel;
        __weak typeof(self)weakSelf = self;
        [userInfoViewController actionClickLinkStatusBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.displayController.searchResultsTableView reloadData];
        }];
        [self.navigationController pushViewController:userInfoViewController animated:YES];


        if ([self.searchBar isFirstResponder]){
            [self.searchBar resignFirstResponder];
        }
    }
}


#pragma mark - UISearchBarDelegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *key = searchBar.text;
    [self.displayController setActive:NO animated:YES];
    self.searchBar.placeholder = key;
//    [GWStupSQLiteManager insertInfoWithSearchText:key];
//    
//    [self insertManager];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [UIView animateWithDuration:.3f animations:^{
        self.searchBar.orgin_x = 0;
    }];
    return YES;
}

#pragma mark - UISearchDisplayDelegate
- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller{
    [UIView animateWithDuration:.3f animations:^{
//                self.searchBar.orgin_x = CGRectGetMaxX(self.areaView.frame);
//                self.searchBar.size_width = kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame);
    }];
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
    [UIView animateWithDuration:.3f animations:^{
//    self.searchBar.orgin_x = CGRectGetMaxX(self.areaView.frame);
//    self.searchBar.size_width = kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame);
    }];
}

// 这里进行搜索
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    __weak typeof(self)weakSelf = self;

    [weakSelf sendRequestToSearchInfo:searchString];
    
    return YES;
}


#pragma mark - searchInfo
-(void)sendRequestToSearchInfo:(NSString *)info{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"keyword":info};
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_search requestParams:params responseObjectClass:[OtherSearchListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            OtherSearchListModel *searchList = (OtherSearchListModel *)responseObject;
            if (strongSelf.searchControllerMutableArr.count){
                [strongSelf.searchControllerMutableArr removeAllObjects];
            }
            [strongSelf.searchControllerMutableArr addObjectsFromArray:searchList.list];
            [self.displayController.searchResultsTableView reloadData];
        }
    }];
}

-(void)sendRequestGetHot{
    __weak typeof(self)weakSelf = self;

    [[NetworkAdapter sharedAdapter] fetchWithPath:home_search_hot requestParams:nil responseObjectClass:[OtherSearchListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            OtherSearchListModel *searchList = (OtherSearchListModel *)responseObject;
            if (strongSelf.searchMutableArr.count){
                [strongSelf.searchMutableArr removeAllObjects];
            }
            [strongSelf.searchMutableArr addObject:searchList.list];
            [strongSelf.searchTableView reloadData];
        }
    }];
}


#pragma mark - 进行关注
-(void)sendRequestTolinkManger:(OtherSearchSingleModel *)model cell:(SearchSingleTableViewCell *)cell{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            [strongSelf subSendRequestTolinkManger:model cell:cell];
        } else {
            [StatusBarManager statusBarHidenWithText:@"请先登录"];
        }
    }];
}

-(void)subSendRequestTolinkManger:(OtherSearchSingleModel *)model cell:(SearchSingleTableViewCell *)cell{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"toUser":model.ID,@"action":@"1"};
    [StatusBarManager statusBarHidenWithText:@"正在努力关注这个小朋友"];
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_link requestParams:params responseObjectClass:[ToLinkSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        if (isSucceeded){
            ToLinkSingleModel *singleModel = (ToLinkSingleModel *)responseObject;
            if (singleModel.haslink){           // 关注
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"已关注,%@",model.nick]];
                [[MainCenterMenuViewController sharedAccountModel] guanzhuManagerStatus:YES];
            } else {
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"已取消关注,%@",model.nick]];
                [[MainCenterMenuViewController sharedAccountModel] guanzhuManagerStatus:NO];
            }

            model.link = singleModel.haslink;
            [cell actionClickZan:singleModel.haslink];
        }
    }];
}


@end

//
//  ProductEvaluaTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailCommentDataSingleModel.h"

@interface ProductEvaluaTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellheight;
@property (nonatomic,strong)ProductDetailCommentDataSingleModel *transferCommentDataSingleModel;

+(CGFloat)calculationCellHeight:(ProductDetailCommentDataSingleModel *)transferCommentDataSingleModel;

@end

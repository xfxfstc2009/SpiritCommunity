//
//  GWShopShopSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/31.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "OtherSearchSingleModel.h"
@class GWShopShopSingleModel;

@protocol GWShopShopSingleModel <NSObject>

@end


@interface GWShopShopSingleModel : FetchModel

@property (nonatomic,assign)NSTimeInterval createtime;
@property (nonatomic,copy)NSString *desc;
@property (nonatomic,strong)NSArray *imgArr;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *olenew;
@property (nonatomic,copy)NSString *price;
@property (nonatomic,copy)NSString *productId;
@property (nonatomic,strong)OtherSearchSingleModel *userInfo;

@property (nonatomic,assign)BOOL hasAnimation;

@end


@interface GWShopShopListModel :FetchModel

@property (nonatomic,strong)NSArray<GWShopShopSingleModel> *list;

@end

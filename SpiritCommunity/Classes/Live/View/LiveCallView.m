//
//  LiveCallView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/1.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveCallView.h"

#define kCallTimeout 60                 // 超时时间

@interface LiveCallView()

@end

@implementation LiveCallView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.imgView = [[PDImageView alloc]init];
    self.imgView.userInteractionEnabled = YES;
    [self addSubview:self.imgView];
    
    // userLabel
    self.userLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.userLabel.textAlignment = NSTextAlignmentCenter;
    [self.imgView addSubview:self.userLabel];
    
    
    
}

@end

//
//  OrderListGoodsSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol OrderListGoodsSingleModel <NSObject>


@end

@interface OrderListGoodsSingleModel : FetchModel

@property (nonatomic,copy)NSString *attrName1;
@property (nonatomic,copy)NSString *attrName2;
@property (nonatomic,copy)NSString *attrName3;
@property (nonatomic,copy)NSString *attrVal1;
@property (nonatomic,copy)NSString *attrVal2;
@property (nonatomic,copy)NSString *attrVal3;
@property (nonatomic,assign)NSInteger buyNum;
@property (nonatomic,copy)NSString *goodId;
@property (nonatomic,copy)NSString *goodJF;
@property (nonatomic,assign)CGFloat goodMoney;
@property (nonatomic,copy)NSString *goodName;
@property (nonatomic,copy)NSString *goodUB;
@property (nonatomic,copy)NSString *goodsDesc;
@property (nonatomic,copy)NSString *goodsImg;
@property (nonatomic,copy)NSString *goodsSummary;
@property (nonatomic,assign)CGFloat marketPrice;
@property (nonatomic,copy)NSString *supplierName;


@property (nonatomic,assign)NSInteger goodsNum;


@end

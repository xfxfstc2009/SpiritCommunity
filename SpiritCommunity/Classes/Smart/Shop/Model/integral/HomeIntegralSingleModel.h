//
//  HomeIntegralSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol HomeIntegralSingleModel <NSObject>

@end

@interface HomeIntegralSingleModel : FetchModel

@property (nonatomic,copy)NSString *appointment;
@property (nonatomic,copy)NSString *appointment_node;
@property (nonatomic,copy)NSString *area;
@property (nonatomic,assign)NSTimeInterval create_time;
@property (nonatomic,assign)CGFloat discount_price;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *img_path;
@property (nonatomic,assign)NSInteger integral;
@property (nonatomic,assign)CGFloat original_price;
@property (nonatomic,assign)CGFloat postage;
@property (nonatomic,assign)NSTimeInterval production_time;
@property (nonatomic,assign)CGFloat progress;
@property (nonatomic,copy)NSString *purchase_num;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,assign)NSInteger stock;
@property (nonatomic,copy)NSString *sub_title;
@property (nonatomic,copy)NSString *supplier;           /**< 供应商*/
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *type;



@end

//
//  QiandaoSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/19.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@interface QiandaoSingleModel : FetchModel

@property (nonatomic,assign)BOOL today_sign;        /**< 今天是否签到*/
@property (nonatomic,strong)NSArray *signlist;
@property (nonatomic,copy)NSString *prize;

@end

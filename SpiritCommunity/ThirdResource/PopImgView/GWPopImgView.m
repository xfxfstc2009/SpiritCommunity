//
//  GWPopImgView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/9.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWPopImgView.h"

@interface GWPopImgView()


@end

@implementation GWPopImgView

+(instancetype)sharedPop{
    static GWPopImgView *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[GWPopImgView alloc] init];
    });
    return _sharedAccountModel;
}

-(void)popShow{
    
}

-(void)popImgManager{
    self.frame = CGRectMake(100, 100, 100, 100);
    self.layer.cornerRadius = 5.f;
    self.layer.masksToBounds = YES;
    
    self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:self.imageView];

}


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 5.f;
        self.layer.masksToBounds = YES;
        
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:_imageView];
    }
    return self;
}

#pragma mark - Property Setters

- (void)setImage:(UIImage *)image {
    [self.imageView setImage:image];
    _image = image;
}

-(void)popDismiss{
    
}


@end

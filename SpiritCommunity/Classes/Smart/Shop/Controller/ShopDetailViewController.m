//
//  ShopDetailViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2018/2/1.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "ShopDetailViewController.h"
#import "ProductDetailRootModel.h"
#import "ProductDetailBottomView.h"                     // 商品详情底部信息
#import "ShareRootViewController.h"                     // 分享
#import "ShopDetailSubDetailViewController.h"
#import "ShopDetailSub2DetailViewController.h"
#import "OrderPayViewController.h"

@interface ShopDetailViewController ()<UIScrollViewDelegate>{
    ProductDetailRootModel *productDetailRootModel;
}
//@property (nonatomic,strong)HomeProuctViewController *homeProductController;
@property (nonatomic,strong)WebViewController *detailController;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)ProductDetailBottomView *bottomView;
@property (nonatomic,strong)ShopDetailSubDetailViewController *shopDetailViewController;
@property (nonatomic,strong)ShopDetailSub2DetailViewController *shopDetailViewController2;

@end

@implementation ShopDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createBottomView];
    [self createScrollView];
    [self sendReqeustToGetProductDetail];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"商品详情";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"center_share_icon"] barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
        shareViewController.isHasGesture = YES;
        [shareViewController actionClickWithShareBlock:^(NSString *type) {
            if (!weakSelf){
                return ;
            }
            SSDKPlatformType mainType = SSDKPlatformSubTypeQZone;
            if ([type isEqualToString:@"QQ"]){
                mainType = SSDKPlatformTypeQQ;
            } else if ([type isEqualToString:@"wechat"]){
                mainType = SSDKPlatformTypeWechat;
            } else if ([type isEqualToString:@"friendsCircle"]){
                mainType = SSDKPlatformSubTypeWechatTimeline;
            }

            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (productDetailRootModel){
                NSString *img = [NSString stringWithFormat:@"http://img.yimugo.com/%@",productDetailRootModel.goodBaseInfo.goodsImg];
                [[ShareSDKManager sharedShareSDKManager] shareManagerWithType:mainType title:strongSelf->productDetailRootModel.goodBaseInfo.goodsName desc:strongSelf->productDetailRootModel.goodBaseInfo.goodsSummary img:img url:nil callBack:^(BOOL success) {
                    [[UIAlertView alertViewWithTitle:@"分享成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
                }];
            }
        }];
        [shareViewController showInView:strongSelf.parentViewController];
    }];
}

#pragma mark - createScrollView
-(void)createScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        if (IS_iPhoneX){
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.view.size_height - 88 - self.bottomView.size_height);
        } else {
            self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.view.size_height - 64 - self.bottomView.size_height);
        }

        self.mainScrollView.delegate = self;
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, self.mainScrollView.size_height * 2);
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.alwaysBounceVertical = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.showsHorizontalScrollIndicator = YES;
        [self.view addSubview:self.mainScrollView];
    }
    [self createProductViewController];
    [self createProductViewController2];
}

#pragma mark - 加载竞猜
- (void)createProductViewController{
    self.shopDetailViewController = [[ShopDetailSubDetailViewController alloc] init];
    self.shopDetailViewController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.shopDetailViewController.view];
    [self addChildViewController:self.shopDetailViewController];
}

- (void)createProductViewController2{
    self.shopDetailViewController2 = [[ShopDetailSub2DetailViewController alloc] init];
    self.shopDetailViewController2.view.frame = CGRectMake(0, self.mainScrollView.size_height, kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.shopDetailViewController2.view];
    [self addChildViewController:self.shopDetailViewController2];
}





#pragma mark -  创建底部的view
-(void)createBottomView{
    __weak typeof(self)weakSelf = self;
    if(IS_iPhoneX){
        self.bottomView =[[ProductDetailBottomView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height - 88 - LCFloat(50) - LCFloat(34), kScreenBounds.size.width, LCFloat(50) + LCFloat(34))];
    } else {
        self.bottomView =[[ProductDetailBottomView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height - LCFloat(50) - 64, kScreenBounds.size.width, LCFloat(50))];
    }
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.bottomView.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    self.bottomView.layer.shadowOpacity = 2.0;
    self.bottomView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [self.bottomView actionClickWithAddCard:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf authorizeWithCompletionHandler:^(BOOL success) {
            if (success){
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [strongSelf addToCart];
                });
            }
        }];

    } buyNowBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf authorizeWithCompletionHandler:^(BOOL success) {
            if (success){
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [strongSelf buyManager];
                });
            }
        }];
    } cart:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf directToCart];
    } collection:^{
        NSLog(@"收藏");
    } customer:^{
        if (!weakSelf){
            return ;
        }

        [[UIAlertView alertViewWithTitle:@"是否联系客服" message:@"客服电话15669969617" buttonTitles:@[@"确定",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 0){
                [Tool callCustomerServerPhoneNumber:@"15669969617"];
            }
        }]show] ;
    }];

    [self.view addSubview:self.bottomView];
}











-(void)sendReqeustToGetProductDetail{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"goodId":self.transferProductId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:_goodsInfo requestParams:params responseObjectClass:[ProductDetailRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->productDetailRootModel = (ProductDetailRootModel *)responseObject;
        strongSelf.shopDetailViewController.transferDetailModel = strongSelf->productDetailRootModel;
        [strongSelf.shopDetailViewController pageLoad];
        strongSelf.shopDetailViewController2.transferImgs = strongSelf->productDetailRootModel.goodBaseInfo.goodDetailImg;
        [strongSelf.shopDetailViewController2 pageLoad];
//
//        // 1. 增加评价信息
//        [strongSelf sendRequestToGetComment];
//        // 2. 修改底部内容收藏/不收藏
//        strongSelf.bottomView.productDetailRootModel = strongSelf->productDetailRootModel;
//        // 3. 优惠
//        if (strongSelf->productDetailRootModel.goodinfo.isfirst){
//            [strongSelf.homeProductController.productMutableArr addObject:@[@"优惠"]];
//            NSInteger youhuiIndex = [strongSelf cellIndexPathSectionWithcellData:@"优惠" sourceArr:strongSelf.homeProductController.productMutableArr];
//            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:youhuiIndex];
//            [strongSelf.homeProductController.productDetailTableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
//        }
//        // 4. 创建
//        [strongSelf.detailController webViewControllerWithAddress:strongSelf->productDetailRootModel.goodinfo.info];

    }];
}


#pragma mark - 加入购物车
-(void)addToCart{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"goodId":productDetailRootModel.goodBaseInfo.ID,@"goodNum":@"1",@"goodSkuId":@"",@"isExistSku":@"0"};
    [StatusBarManager statusBarHidenWithText:@"正在加入购物车"];
    [[NetworkAdapter sharedAdapter] fetchWithPath:_addCart requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"加入购物车成功"];
        } else {
            [StatusBarManager statusBarHidenWithText:@"加入购物车失败"];
        }
    }];
}

#pragma mark - 跳转到购物车
-(void)directToCart{
    [self.navigationController popViewControllerAnimated:YES];
    [[PDMainTabbarViewController sharedController] setSelectedIndex:1 animated:YES];
}


-(void)buyManager{
    OrderPayViewController *orderPayViewController = [[OrderPayViewController alloc]init];
    orderPayViewController.transferProductDetailModel = productDetailRootModel;
    [self.navigationController pushViewController:orderPayViewController animated:YES];
}
@end

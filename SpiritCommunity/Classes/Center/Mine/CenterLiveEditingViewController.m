//
//  CenterLiveEditingViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/2.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "CenterLiveEditingViewController.h"

@interface CenterLiveEditingViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSString *titleStr;
    NSString *liveImgUrl;
}
@property (nonatomic,strong)UITableView *liveSettingTableView;
@property (nonatomic,strong)NSArray *liveSettingArr;
@end

@implementation CenterLiveEditingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)pageSetting{
    self.barMainTitle = @"直播设置";
}

-(void)arrayWithInit{
    self.liveSettingArr = @[@[@"直播图片"],@[@"直播标题"],@[@"确认修改"]];
    liveImgUrl = [AccountModel sharedAccountModel].loginModel.live_img;
    titleStr = [AccountModel sharedAccountModel].loginModel.live_title;
}

-(void)createTableView{
    if (!self.liveSettingTableView){
        self.liveSettingTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.liveSettingTableView.dataSource = self;
        self.liveSettingTableView.delegate = self;
        [self.view addSubview:self.liveSettingTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.liveSettingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOArr = [self.liveSettingArr objectAtIndex:section];
    return sectionOArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0 ){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWAvatarTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWAvatarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferAvatarUrl = liveImgUrl;
        cellWithRowOne.fixedLabel.text = @"直播封面图图片";
        cellWithRowOne.fixedLabel.size_width = 150;
        return cellWithRowOne;
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"直播标题";
        cellWithRowOne.transferPlaceholeder = @"请输入直播的标题";
        cellWithRowOne.inputTextField.text = titleStr;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->titleStr = info;
        }];
        
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowThr.backgroundColor = [UIColor clearColor];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = [[self.liveSettingArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [cellWithRowThr setButtonStatus:YES];
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToUpdateInfo];
        }];
        return cellWithRowThr;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0){        // 直播图片
        GWAssetsLibraryViewController *assetLibraryViewController = [[GWAssetsLibraryViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [assetLibraryViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf uploadAvatar:[selectedImgArr lastObject]];
        }];
        [self.navigationController pushViewController:assetLibraryViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [GWAvatarTableViewCell calculationCellHeight];
    } else if (indexPath.section == 1){
        return [GWInputTextFieldTableViewCell calculationCellHeight];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


#pragma mark - 上传头像
-(void)uploadAvatar:(UIImage *)image{
    __weak typeof(self)weakSelf = self;
    [[AliOSSManager sharedOssManager] uploadFileWithObjctype:ImageUsingTypeLive name:@"" objImg:image block:^(NSString *fileUrl) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->liveImgUrl = fileUrl;
        [self.liveSettingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    } progress:^(CGFloat progress) {
        if (!weakSelf){
            return ;
        }
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"图片正在上传中%.2f",progress]];
    }];
}



-(void)sendRequestToUpdateInfo{
    __weak typeof(self)weakSelf = self;
    NSString *err = @"";
    if (!liveImgUrl.length){
        err = @"请上传直播图片";
    } else if(!titleStr.length){
        err = @"请输入直播标题";
    }
    if (err.length){
        [PDHUD showText:err];
        return;
    }
    
    NSDictionary *params = @{@"liveTItle":titleStr,@"liveImg":liveImgUrl};
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_uploadLive requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [AccountModel sharedAccountModel].loginModel.live_title = strongSelf->titleStr;
            [AccountModel sharedAccountModel].loginModel.live_img = strongSelf->liveImgUrl;
            [StatusBarManager statusBarHidenWithText:@"直播信息提交成功"];
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}


@end

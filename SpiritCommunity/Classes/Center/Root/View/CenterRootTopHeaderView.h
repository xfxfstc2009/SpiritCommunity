//
//  CenterRootTopHeaderView.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/20.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterCustomerSingleModel.h"

#define kCenterTopHeaderAvatar LCFloat(80)                      // 头像的宽高
#define kCenterTopHeader LCFloat(100)
@interface CenterRootTopHeaderView : UIView

@property (nonatomic,strong)AccountModel *transferAccountModel;             /**< 用户的Model*/
@property (nonatomic,strong)UIView *avatarBgView;                          /**< 头像背景*/

-(void)actionClickWithHeader:(void(^)())block;              // 头像进行点击
-(void)actionClickBlock:(void(^)(CenterCustomerSingleModel *recordSingleModel))block;           // 点击用户信息
-(void)loginAnimationActionManager;             // 登录之后的动画
+(CGFloat)calculationCellHeight;
@end

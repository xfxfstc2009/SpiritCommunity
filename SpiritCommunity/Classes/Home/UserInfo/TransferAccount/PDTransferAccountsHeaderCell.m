//
//  PDTransferAccountsHeaderCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDTransferAccountsHeaderCell.h"


static NSString *const kName = @"animationName";    /**< 动画的key*/
#if defined(__IPHONE_10_0) && (__IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0)
@interface PDTransferAccountsHeaderCell () <CAAnimationDelegate>
#else
@interface PDTransferAccountsHeaderCell ()
#endif


@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *leftAvatar;
@property (nonatomic,strong)PDImageView *rightAvatar;
@property (nonatomic,strong)UILabel *leftLabel;
@property (nonatomic,strong)UILabel *rightLabel;

// 添加动态的动画
@property (nonatomic,strong)UIView *portView;                   /**< portView*/
@property (nonatomic,strong)CALayer *cirlce1;
@property (nonatomic,strong)CALayer *cirlce2;
@property (nonatomic,strong)CALayer *cirlce3;
@property (nonatomic,strong)CALayer *cirlce4;
@property (nonatomic,strong)NSMutableArray *circles;

@end

@implementation PDTransferAccountsHeaderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        _circles = [NSMutableArray array];
        [self createView];
    }
    return self;
}

#pragma mark- createView
-(void)createView{
    [self createLeftView];
}

#pragma mark - createLeftView
-(void)createLeftView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.frame = self.bounds;
    [self addSubview:self.bgImgView];
    
    self.leftAvatar = [[PDImageView alloc]init];
    self.leftAvatar.backgroundColor = [UIColor clearColor];
    [self addSubview:self.leftAvatar];
    
    // left title
    self.leftLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    self.leftLabel.textAlignment = NSTextAlignmentCenter;
    self.leftLabel.font = [self.leftLabel.font boldFont];
    self.leftLabel.textColor = c26;
    [self addSubview:self.leftLabel];
    
    
    // right avatar
    self.rightAvatar = [[PDImageView alloc]init];
    self.rightAvatar.backgroundColor = [UIColor clearColor];
    [self addSubview:self.rightAvatar];
    
    // right title
    self.rightLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    self.rightLabel.textAlignment = NSTextAlignmentCenter;
    self.rightLabel.font = [self.rightLabel.font boldFont];
    [self addSubview:self.rightLabel];
    
    self.portView = [[UIView alloc]init];
    self.portView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.portView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferSearchSingleModel:(OtherSearchSingleModel *)transferSearchSingleModel{
    _transferSearchSingleModel = transferSearchSingleModel;
    
    self.bgImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
    self.bgImgView.image = [UIImage imageNamed:@"bg_transfer_account"];
    
    // 1. 计算间距
    CGFloat height_margin = (self.transferCellHeight - LCFloat(60) - [NSString contentofHeightWithFont:self.leftLabel.font]) / 4.;

    // 2. 左侧头像
    self.leftAvatar.frame = CGRectMake(LCFloat(65), 2 * height_margin, LCFloat(60), LCFloat(60));
    self.leftAvatar.clipsToBounds = YES;
    self.leftAvatar.layer.cornerRadius = MIN(self.leftAvatar.size_width, self.leftAvatar.size_height) / 2.;
    [self.leftAvatar uploadImageWithURL:[AccountModel sharedAccountModel].loginModel.avatar placeholder:nil callback:NULL];
    
    // 3. 左侧名字
    self.leftLabel.text = @"我";
    CGSize leftSize = [self.leftLabel.text sizeWithCalcFont:self.leftLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftLabel.font])];
    self.leftLabel.frame = CGRectMake(0, CGRectGetMaxY(self.leftAvatar.frame) + height_margin, MIN(leftSize.width, kScreenBounds.size.width / 2.), [NSString contentofHeightWithFont:self.leftLabel.font]);
    self.leftLabel.center_x = self.leftAvatar.center_x;
    self.leftLabel.adjustsFontSizeToFitWidth = YES;
    
    // 4. 右侧头像
    self.rightAvatar.frame = CGRectMake(kScreenBounds.size.width - LCFloat(65) - self.leftAvatar.size_width, self.leftAvatar.orgin_y, self.leftAvatar.size_width, self.leftAvatar.size_height);
    self.rightAvatar.clipsToBounds = YES;
    self.rightAvatar.layer.cornerRadius = MIN(self.leftAvatar.size_width, self.leftAvatar.size_height) / 2.;
    [self.rightAvatar uploadImageWithURL:self.transferSearchSingleModel.avatar placeholder:nil callback:NULL];
    
    // 5. 右侧名字
    self.rightLabel.text = self.transferSearchSingleModel.nick;
    CGSize rightSize = [self.rightLabel.text sizeWithCalcFont:self.rightLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rightLabel.font])];
    self.rightLabel.frame = CGRectMake(0, self.leftLabel.orgin_y, MIN(rightSize.width, kScreenBounds.size.width / 2.), self.leftLabel.size_height);
    self.rightLabel.center_x = self.rightAvatar.center_x;
    self.rightLabel.adjustsFontSizeToFitWidth = YES;
    
    // 6. power
    self.portView.frame = CGRectMake(CGRectGetMaxX(self.leftAvatar.frame), self.rightAvatar.orgin_y, (self.rightAvatar.orgin_x - CGRectGetMaxX(self.leftAvatar.frame)), LCFloat(60));
    self.portView.backgroundColor = [UIColor clearColor];
}




#pragma mark 动画1
-(void)addAnimation1{
    CGFloat width = 20.f;
    CGFloat x = (self.portView.size_width - width * 4) / 5.;
    CGFloat y = (self.portView.size_height - width) / 2.;
    
    for (NSInteger index = 0; index < 4; ++index) {
        CGFloat new_width = LCFloat(10) + 2 * index;
        CALayer *cirlce = [CALayer layer];
        [cirlce setFrame:CGRectMake(x + (width+10)*index, y, new_width, new_width)];
        [cirlce setBackgroundColor:[UIColor colorWithRed:1 - 0.2 * index green:1 - 0.2 * index blue:1 - 0.2 * index alpha:1].CGColor];
        [cirlce setCornerRadius:new_width/2];
        cirlce.hidden = YES;
        [self.portView.layer addSublayer:cirlce];
        
        [_circles addObject:cirlce];
    }
    
    [self doanimation:0];
}

-(void)doanimation:(NSInteger)index{
    if (index == 4){
        for (CALayer *circle1 in _circles){
            circle1.hidden = YES;
        }
        [self doanimation:0];
        return;
    }
    
    if (_circles.count){
        CALayer *cirlce = [_circles objectAtIndex:index];
        cirlce.hidden = NO;
        
        CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [anim setFromValue:@0];
        [anim setToValue:@1];
        [anim setDuration:.5f];
        anim.delegate = self;
        [anim setRemovedOnCompletion:YES];
        NSString *key = [NSString stringWithFormat:@"do%li",(long)index];
        [anim setValue:key forKey:kName];
        [anim setRepeatCount:1];
        [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [cirlce addAnimation:anim forKey:nil];
    }
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if ([[anim valueForKey:kName] isEqualToString:@"do0"]){
        [self doanimation:1];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do1"]){
        [self doanimation:2];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do2"]){
        [self doanimation:3];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do3"]){
        [self doanimation:4];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do4"]){
        [self doanimation:5];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do5"]){
        [self doanimation:6];
    }
    
}

#pragma mark 开始动画
- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    if (_circles.count == 0) {
        [self addAnimation1];
    }
}

#pragma mark 停止动画
- (void)removeFromSuperview {
    [super removeFromSuperview];
    for (CALayer *circle in _circles) {
        [circle removeFromSuperlayer];
    }
    [_circles removeAllObjects];
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(160);
    return cellHeight;
}


@end

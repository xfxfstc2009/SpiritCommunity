//
//  PDGrabOrderRowTen.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

// 安全提醒
#import "PDBaseTableViewCell.h"

@interface PDGrabOrderRowTen : PDBaseTableViewCell

+(CGFloat)calculationCellHeight;

@end

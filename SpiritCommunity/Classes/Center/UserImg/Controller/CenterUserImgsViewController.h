//
//  CenterUserImgsViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"

@protocol CenterUserImgsViewControllerDelegate <NSObject>

- (void)scrollViewDelegateManager:(UIScrollView *)scrollView andDrop:(BOOL)drop;

-(void)lotteryscrollViewWillBeginDragging:(UIScrollView *)scrollView;
-(void)lotteryscrollViewDidEndDragging:(UIScrollView *)scrollView;

@end


@interface CenterUserImgsViewController : AbstractViewController
@property (nonatomic,strong)UICollectionView *imgsCollectionView;

@property (nonatomic,weak)id<CenterUserImgsViewControllerDelegate> delegate;

-(void)collectionCellContentOfHeightBlock:(void(^)(CGFloat height))block;

-(void)reloadImgsManagae;

@end


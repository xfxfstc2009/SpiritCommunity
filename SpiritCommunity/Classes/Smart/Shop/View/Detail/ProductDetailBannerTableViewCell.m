//
//  ProductDetailBannerTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "ProductDetailBannerTableViewCell.h"
#import "PDScrollView.h"
@interface ProductDetailBannerTableViewCell()
@property (nonatomic,strong)PDScrollView *banner;

@end

@implementation ProductDetailBannerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 2. 创建北京scrollView
    self.banner = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(375))];
    self.banner.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.banner.isPageControl = NO;
    __weak typeof(self)weakSelf = self;
    [self.banner bannerImgTapManagerWithInfoblock:^(GWScrollViewSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
    }];
    [self addSubview:self.banner];
}

-(void)setTransferProductDetailModel:(ProductDetailRootModel *)transferProductDetailModel{
    _transferProductDetailModel = transferProductDetailModel;
    
    NSMutableArray *bannerMutableArr = [NSMutableArray array];
    if (transferProductDetailModel.goodBannerImgs.count){
        for (int i = 0 ; i < transferProductDetailModel.goodBannerImgs.count;i++){
            GWScrollViewSingleModel *singleModel = [[GWScrollViewSingleModel alloc]init];
            ProductDetailGoodBannerImgsModel *proSingleModel = [transferProductDetailModel.goodBannerImgs objectAtIndex:i];
            singleModel.img = proSingleModel.imgUrl;
            [bannerMutableArr addObject:singleModel];
        }
        self.banner.transferImArr = bannerMutableArr;
        [self.banner startTimerWithTime:3.];
    }
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(375);
}
@end

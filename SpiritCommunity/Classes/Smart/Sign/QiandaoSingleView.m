//
//  QiandaoSingleView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/19.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "QiandaoSingleView.h"

static char actionClickClickBlockKey;
@interface QiandaoSingleView()
@property(nonatomic,strong)UILabel*infoLabel;
@property (nonatomic,strong)PDImageView *checkImgView;
@property (nonatomic,strong)UIButton *button;

@end

@implementation QiandaoSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self createView];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.infoLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    self.infoLabel.font = [UIFont systemFontOfCustomeSize:10.];
    self.infoLabel.frame = CGRectMake(0, 0, LCFloat(15), LCFloat(15));
    [self addSubview:self.infoLabel];
    
    self.checkImgView = [[PDImageView alloc]init];
    self.checkImgView.frame = CGRectMake((self.size_width - LCFloat(15)) / 2., (self.size_height - LCFloat(15)) / 2., LCFloat(15), LCFloat(15));
    self.checkImgView.image = [UIImage imageNamed:@"xx"];
    [self addSubview:self.checkImgView];
    
    // button
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.frame = self.bounds;
    __weak typeof(self)weakSelf = self;
    [self.button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void((^block)()) = objc_getAssociatedObject(strongSelf, &actionClickClickBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.button];
}

-(void)setDate:(NSString *)date{
    self.infoLabel.text = date;
}

-(void)setCheck:(BOOL)checked{
    if (checked){
        self.checkImgView.image = [UIImage imageNamed:@"dui"];
    } else {
        self.checkImgView.image = [UIImage imageNamed:@"xx"];
    }
}

-(void)setClean:(BOOL)clean{
    self.checkImgView.hidden = clean;
}

-(void)actionClickClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}
@end

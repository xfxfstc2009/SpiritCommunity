//
//  PlusingAnnotationView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "PlusingAnnotationView.h"
#import <QuartzCore/QuartzCore.h>
#import "GWPulseLayer.h"

@interface PlusingAnnotationView()
@property (nonatomic, strong) GWPulseLayer *pulseLayer;         /**< player*/


@end

@implementation PlusingAnnotationView

- (id)initWithAnnotation:(id<MAAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    if(self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        self.layer.anchorPoint = CGPointMake(0.5, 0.5);
        self.calloutOffset = CGPointMake(0, 4);
        self.bounds = CGRectMake(0, 0, 23, 23);
        self.pulseAnimationDuration = 1;
        self.delayBetweenPulseCycles = 1;
        self.annotationColor =  [UIColor colorWithRed:0.082 green:0.369 blue:0.918 alpha:1];
        self.image = [UIImage imageNamed:@"icon_user_location"];
    }
    return self;
}

- (void)rebuildLayers {
    [_pulseLayer removeFromSuperlayer];
    _pulseLayer = nil;
    
    [self.layer addSublayer:self.haloLayer];
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    if(newSuperview){
        [self rebuildLayers];
    }
}

#pragma mark - Setters

- (void)setAnnotationColor:(UIColor *)annotationColor {
    if(CGColorGetNumberOfComponents(annotationColor.CGColor) == 2) {
        float white = CGColorGetComponents(annotationColor.CGColor)[0];
        float alpha = CGColorGetComponents(annotationColor.CGColor)[1];
        annotationColor = [UIColor colorWithRed:white green:white blue:white alpha:alpha];
    }
    _annotationColor = annotationColor;
    
    if(self.superview){
        [self rebuildLayers];
    }
}

- (void)setDelayBetweenPulseCycles:(NSTimeInterval)delayBetweenPulseCycles {
    _delayBetweenPulseCycles = delayBetweenPulseCycles;
    
    if(self.superview){
        [self rebuildLayers];
    }
}

- (void)setPulseAnimationDuration:(NSTimeInterval)pulseAnimationDuration {
    _pulseAnimationDuration = pulseAnimationDuration;
    
    if(self.superview)
        [self rebuildLayers];
}

#pragma mark - Getters
- (CALayer *)haloLayer {
    if(!_pulseLayer) {
        _pulseLayer = [GWPulseLayer layer];
        _pulseLayer.bounds = CGRectMake(0, 0, LCFloat(300), LCFloat(300));
        _pulseLayer.position = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
        _pulseLayer.contentsScale = [UIScreen mainScreen].scale;
        _pulseLayer.radius = LCFloat(150);
    }
    return _pulseLayer;
}


@end

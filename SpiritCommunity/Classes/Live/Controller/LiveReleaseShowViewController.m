//
//  LiveReleaseShowViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/19.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController.h"
#import "TXLiveManager.h"                                       // 直播的一些方法
#import "LiveReleaseShowViewController+AVListener.h"
#import "LiveReleaseShowViewController+Audio.h"
#import "LiveReleaseShowViewController+IMListener.h"
#import "LiveReleaseShowViewController+UI.h"
#import "LiveReleaseShowViewController+ObyO.h"      // 表示一对一.

// 创建底部view
#import "LiveActionViewController.h"
#import "LiveInputTextField.h"
#import "ShareRootViewController.h"
#import "UserViewManager.h"
#import "PushManager.h"
#import "SBJSON.h"
#import "LiveNotifyRootModel.h"
#import "AnimOperationManager.h"                    // 动画
#import "ChongzhiRootViewController.h"              // 充值方法
#import "UserInfoViewController.h"


@interface LiveReleaseShowViewController ()<QAVLocalVideoDelegate, QAVRemoteVideoDelegate,ILiveRoomDisconnectListener,UITextFieldDelegate>
@property (nonatomic,strong)NSMutableArray *messageMutableArr;              /**< 消息列表的回调*/
@property (nonatomic,strong)UIButton *closeButton;

@property (nonatomic,strong)UIScrollView *topScrollView;                    /**< 创建顶部的scrollview*/
@property (nonatomic,strong)LiveInputTextField *inputTextField;

@property (nonatomic,strong)LiveActionViewController *tempActionview;
@property (nonatomic,strong)OtherSearchSingleModel *tempUserSingleModel;

@property (nonatomic,assign)NSInteger zanCount;
@property (nonatomic,strong)NSTimer *timer;
@property (nonatomic,assign)NSInteger timeInteger;

@end

@implementation LiveReleaseShowViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self timerWithDealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];                   // 0. 初始化数据
    [self addSubViews];                     // 1. 创建视图view
    [self createTilFilter];                 // 2. 创建滤镜
    [self initLive];                        // 3. 初始化直播
    [self enterRoom];                       // 4. 创建房间
    [self addNotification];                 // 6. 添加通知
    [self updateMyNickInfo];                // 7. 修改我的昵称信息
    [self sendRequestToGetUserInCurrentHouse];  // 8. 获取当前房间信息
}

-(void)updateMyNickInfo{
    NSString * nick = [AccountModel sharedAccountModel].loginModel.nick;

    [[TIMFriendshipManager sharedInstance] SetNickname:nick succ:^() {
        NSLog(@"SetNickname Succ");
    } fail:^(int code, NSString * err) {
        NSLog(@"SetNickname fail: code=%d err=%@", code, err);
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.msgMutableArr = [NSMutableArray array];
    [TXLiveManager sharedAccountModel].transferWhiteValue = .5f;
    [TXLiveManager sharedAccountModel].transferBeautyValue = .5f;
    [TXLiveManager sharedAccountModel].transferMicStatus = YES;
}

#pragma mark - 1. 创建滤镜
-(void)createTilFilter{
    self.tilFilter = [[TILFilter alloc] init];
    [[ILiveRoomManager getInstance] setRemoteVideoDelegate:self];
}

#pragma mark - 2. 创建Live初始化
- (void)initLive {
    [[TILLiveManager getInstance] setAVListener:self];
    [[TILLiveManager getInstance] setIMListener:self];
    [[TILLiveManager getInstance] setAVRootView:self.view];
    [[ILiveRoomManager getInstance] setLocalVideoDelegate:self];            //如果要使用美颜，必须设置本地视频代理
}

-(void)enterRoom{
    //进入房间清空美颜，如果不清空，则进入时还会保留上次房间中的美颜值
    QAVContext *context = [[ILiveSDK getInstance] getAVContext];
    [context.videoCtrl inputBeautyParam:0];
    [context.videoCtrl inputWhiteningParam:0];

    if (self.transferLiveSingleModel.show_status == LiveObOorLiveTypeOnebyOne){         // 一对一
        if (self.transferLiveShowType == LiveShowTypeAnchor){               // 【主播】
            [self createHouse];                     //  【创建房间】
        } else {                                                            // 【观众】
            [self joinHouseManager];                //  【加入房间】
        }
    } else if (self.transferLiveSingleModel.show_status == LiveObOorLiveTypeLive){      // 直播
        if (self.transferLiveShowType == LiveShowTypeAnchor){               // 是主播就创建房间
            [self createHouse];                     //  【创建房间】
        } else {
            [self joinHouseManager];                //  【加入房间】
        }
    }
}


#pragma mark -2. 创建房间
-(void)createHouse{
    TILLiveRoomOption *option = [TILLiveRoomOption defaultHostLiveOption];
    option.controlRole = self.transferLiveSingleModel.userInfo.nick;
    option.avOption.autoHdAudio = YES;//使用高音质模式，可以传背景音乐
    option.roomDisconnectListener = self;
    option.imOption.imSupport = YES;
    // 进行开启房间
    __weak typeof(self)weakSelf = self;
    int roomId = (int)[self.transferLiveSingleModel.house_id integerValue];
    [StatusBarManager statusBarHidenWithText:@"正在创建房间……"];
    [[TILLiveManager getInstance] createRoom:roomId option:option succ:^{
        [StatusBarManager statusBarHidenWithText:@"创建房间成功"];
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [TXLiveManager sharedAccountModel].transferMicStatus = YES; // 0. 重新设置麦克风的状态
        [strongSelf.actionView liveUIWithReset];                    // 1. 界面一些值进行回复
        [strongSelf getInfoWithMySelf];                             // 2. 获取我当前的信息
        [strongSelf audioWithInit];                                 // 3. 初始化音频
        [strongSelf linkManager];                                   // 4. 进行长连接
        [strongSelf timerWithInit];                                 // 5.进行获取点赞信息
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:@"创建房间失败"];
        NSString *errinfo = [NSString stringWithFormat:@"module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        [[UIAlertView alertViewWithTitle:@"失败" message:errinfo buttonTitles:@[@"确定"] callBlock:NULL]show];
    }];
}


-(void)createHouseWithOnebyOne{
    TILLiveRoomOption *option = [TILLiveRoomOption defaultHostLiveOption];
    option.avOption.autoHdAudio = YES;//使用高音质模式，可以传背景音乐
    option.roomDisconnectListener = self;
    option.imOption.imSupport = YES;
    // 进行开启房间
    __weak typeof(self)weakSelf = self;
    int roomId = (int)[[AccountModel sharedAccountModel].loginModel.ID integerValue];
    [StatusBarManager statusBarHidenWithText:@"正在创建房间……"];
    [[TILLiveManager getInstance] createRoom:roomId option:option succ:^{
        [StatusBarManager statusBarHidenWithText:@"创建房间成功"];
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [TXLiveManager sharedAccountModel].transferMicStatus = YES; // 0. 重新设置麦克风的状态
        [strongSelf.actionView liveUIWithReset];                    // 1. 界面一些值进行回复
        [strongSelf getInfoWithMySelf];                             // 2. 获取我当前的信息
        [strongSelf audioWithInit];                                 // 3. 初始化音频
        [strongSelf toLinkHouseWithZhubo:strongSelf.transferLiveSingleModel];

    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:@"创建房间失败"];
        NSString *errinfo = [NSString stringWithFormat:@"module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        [[UIAlertView alertViewWithTitle:@"失败" message:errinfo buttonTitles:@[@"确定"] callBlock:NULL]show];
    }];
}




#pragma mark 2.1 获取我的信息
-(void)getInfoWithMySelf{
    __weak typeof(self)weakSelf = self;
    [TXLiveManager getInfoWithMySelfWithBlcok:^(BOOL successed, TIMUserProfile *profile) {
        if (!weakSelf){
            return ;
        }
        __weak typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            [strongSelf sendRequestToIdentify:profile.identifier];
            strongSelf.selfProfile = profile;
        } else {
            [StatusBarManager statusBarHidenWithText:@"获取当前信息失败"];
            strongSelf.selfProfile = nil;
        }
    }];
}

#pragma mark - 如果是观众
-(void)joinHouseManager{
    // 2. 获取控件
    TILLiveRoomOption *option = [TILLiveRoomOption defaultGuestLiveOption];
    option.controlRole = kSxbRole_GuestHD;
    option.avOption.autoHdAudio = YES;
    
    __weak typeof(self)weakSelf = self;
    int houseId = (int)[self.transferLiveSingleModel.house_id integerValue];
    [[TILLiveManager getInstance] joinRoom:houseId option:option succ:^{
        [StatusBarManager statusBarHidenWithText:@"加入房间成功"];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendJoinRoomMsg];
        [strongSelf getInfoWithMySelf];
        [strongSelf sendRequestToJoinHouse];                // 后端发送加入房间接口
        [strongSelf sendRequestToGetUserInfo];              // 获取房主信息
        [strongSelf timerWithInit];                         // 进行获取点赞信息
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"加入房间失败%@",errMsg]];
        [[UIAlertView alertViewWithTitle:@"提示" message:[NSString stringWithFormat:@"加入房间失败%@",errMsg] buttonTitles:@[@"确定"] callBlock:NULL]show];
    }];
}

- (void)sendJoinRoomMsg {
    ILVLiveCustomMessage *msg = [[ILVLiveCustomMessage alloc] init];
    msg.type = ILVLIVE_IMTYPE_GROUP;
    msg.cmd = (ILVLiveIMCmd)AVIMCMD_EnterLive;
    msg.recvId = [[ILiveRoomManager getInstance] getIMGroupId];
    
    [[TILLiveManager getInstance] sendCustomMessage:msg succ:^{
        [StatusBarManager statusBarHidenWithText:@"加入房间成功"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"加入房间失败%@",errMsg]];
    }];
}


#pragma mark - ViewAction
-(void)addSubViews{
    __weak typeof(self)weakSelf = self;
    self.noCameraDatatalabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.noCameraDatatalabel.text = @"主播没有打开摄像头";
    self.noCameraDatatalabel.textAlignment = NSTextAlignmentCenter;
    self.noCameraDatatalabel.hidden = YES;
    [self.view addSubview:self.noCameraDatatalabel];
    
    //如果5S都没有来相机事件，那么就显示对方没有开摄像头的字样,且把背景改成蓝色（增强体验的作用）
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (!weakSelf.isCameraEvent) {
            weakSelf.noCameraDatatalabel.hidden = NO;
        }
    });
    
    [self createTopScrollView];
    // 添加关闭按钮
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(30), 20 + LCFloat(11), LCFloat(30), LCFloat(30));
    self.closeButton.backgroundColor = [UIColor clearColor];
    [weakSelf.closeButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf btnClickWithClose];
    }];
    [self.closeButton setImage:[UIImage imageNamed:@"live_detail_close"] forState:UIControlStateNormal];
    [self.view addSubview:self.closeButton];
}

#pragma mark -  退出
-(void)btnClickWithClose{
    __weak typeof(self)weakSelf = self;
    [self sendRequestToOutHouseWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf houseRelease];
    }];
}

-(void)houseRelease{
    __weak typeof(self)weakSelf = self;
    if ([TXLiveManager sharedAccountModel].isHost){     // 主播退群时，发送退群消息
        ILVLiveCustomMessage *customMsg = [[ILVLiveCustomMessage alloc] init];
        customMsg.type = ILVLIVE_IMTYPE_GROUP;
        customMsg.recvId = [[ILiveRoomManager getInstance] getIMGroupId];
        customMsg.cmd = (ILVLiveIMCmd)AVIMCMD_ExitLive;
        [[TILLiveManager getInstance] sendCustomMessage:customMsg succ:^{
            [StatusBarManager statusBarHidenWithText:@"主播退群成功"];
            [weakSelf onClose];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            [StatusBarManager statusBarHidenWithText:@"主播退群失败"];
            [weakSelf onClose];
        }];
    } else {
        [self onClose];
    }
}

-(void)onClose{
    // 1. 停止心跳
    
    // 2. 通知业务服务器
    if ([TXLiveManager sharedAccountModel].isHost){
        // 退房
    } else {
        
    }
    
    TILLiveManager *manager = [TILLiveManager getInstance];
    //退出房间
    __weak typeof(self)weakSelf = self;
    [manager quitRoom:^{
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
        [weakSelf dismissViewControllerAnimated:YES completion:NULL];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"exit room fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg]];
        [weakSelf.navigationController setNavigationBarHidden:NO animated:YES];
        [weakSelf dismissViewControllerAnimated:YES completion:NULL];
    }];

    [[UserViewManager shareInstance] releaseManager];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kClickConnect_Notification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kCancelConnect_Notification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserSwitchRoom_Notification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupDelete_Notification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kUserParise_Notification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kPureDelete_Notification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNoPureDelete_Notification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kEnterBackGround_Notification object:nil];
}



// 创建顶部的scrollView
-(void)createTopScrollView{
    __weak typeof(self)weakSelf = self;
    if (!self.topScrollView){
        self.topScrollView = [GWViewTool gwCreateScrollViewWithRect:self.view.bounds];
        self.topScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, kScreenBounds.size.height);
        [self.topScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0)];
        [self.view addSubview:self.topScrollView];
    }
    // 1. 创建顶部操作的view
    if (!self.actionView){
        self.actionView = [[LiveUIActionView alloc]initWithFrame:self.topScrollView.bounds];
        self.actionView.transferLiveSingleModel = self.transferLiveSingleModel;
        self.actionView.orgin_x = kScreenBounds.size.width;
        self.actionView.transferShowType = self.transferLiveShowType;
        [self.actionView liveUIActionViewActionClickBlock:^(LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf bottomViewActionClickBlockWithModel:liveUIBottomViewSingleModel];
        }];
        [self.actionView liveUIActionViewActionUITopClickBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf.tempUserSingleModel.ID.length){
                UserInfoViewController *userInfoViewController = [[UserInfoViewController alloc]init];
                userInfoViewController.transferSearchSingleModel = strongSelf.tempUserSingleModel;
                [strongSelf.navigationController pushViewController:userInfoViewController animated:YES];
            }
        }];

        [self.topScrollView addSubview:self.actionView];
    }

    // 3. 创建底部的view
    self.inputTextField = [[LiveInputTextField alloc]initWithFrame:CGRectMake(LCFloat(0), kScreenBounds.size.height, kScreenBounds.size.width, LCFloat(44)) actionChangeBlock:^(UITextField *inputTextField) {
        
    } btnClick:^(UITextField *inputTextField) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.inputTextField cleanInfo];
        [strongSelf tapManager];
    }];
    self.inputTextField.transferActionView = self.actionView;
    [self.view addSubview:self.inputTextField];
    
    // 添加键盘通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    // 4. 创建手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self.view addGestureRecognizer:tap];
}

-(void)tapManager{
    [self bottomViewStatus:YES];
    if ([self.inputTextField.inputTextField isFirstResponder]){
        [self.inputTextField.inputTextField resignFirstResponder];
    }
    self.topScrollView.orgin_y = 0;
}

-(void)bottomViewActionClickBlockWithModel:(LiveUIBottomViewSingleModel *)actionSingleModel{
    __weak typeof(self)weakSelf = self;
    if (actionSingleModel.transferUIBottomType > 100){
        return;
    }
    if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionMore){                          //  【点击了更多】
        [self bottomViewStatus:NO];
        LiveActionViewController *view = [[LiveActionViewController alloc]init];
        view.transferActionType = LiveUIBottomViewTypeActionRoot;
        view.transferLiveSingleModel = self.transferLiveSingleModel;
        [view showInView:self.parentViewController withBlock:^(LiveUIBottomViewSingleModel *actionSingleModel1) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (actionSingleModel1.transferUIBottomType == LiveUIBottomViewTypeActionLensFlip){             // 【翻转镜头】
                [TXLiveManager switchCamera];
            } else if (actionSingleModel1.transferUIBottomType == LiveUIBottomViewTypeActionLighting){      // 【闪光灯】
                [TXLiveManager openLinght:![TXLiveManager sharedAccountModel].lightStatus];
            } else if (actionSingleModel1.transferUIBottomType == LiveUIBottomViewTypeActionSounds){         // 【开关声音】
                
                [TXLiveManager closeAndOpenMic];
                
            } else {
                [strongSelf bottomViewActionClickBlockWithModel:actionSingleModel1];
            }
            [self bottomViewStatus:YES];
        }];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionFilter){                 // 【滤镜】
        [self bottomViewStatus:NO];
        LiveActionViewController *view = [[LiveActionViewController alloc]init];
        view.transferActionType = actionSingleModel.transferUIBottomType;
        [view showInView:self.parentViewController withBlock:^(LiveUIBottomViewSingleModel *actionSingleModel1) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if(actionSingleModel1.transferUIBottomType == LiveUIBottomViewTypeActionDismiss){

            } else {
                if (actionSingleModel1.transferUIBottomType > 300){
                    [TXLiveManager addFilterWithType:strongSelf.tilFilter type:actionSingleModel1.transferUIBottomType];
                }
            }
            [strongSelf bottomViewStatus:YES];
        }];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionMengyantexiao){          // 【挂件】
        [self bottomViewStatus:NO];
        LiveActionViewController *view = [[LiveActionViewController alloc]init];
        view.transferActionType = actionSingleModel.transferUIBottomType;
        [view showInView:self.parentViewController withBlock:^(LiveUIBottomViewSingleModel *actionSingleModel1) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionMengyantexiaoTuzi){
                [TXLiveManager addSpecilWithType:actionSingleModel1.transferUIBottomType];
            } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionMengyantexiaoBaixuegongzhu){
                [TXLiveManager addSpecilWithType:actionSingleModel1.transferUIBottomType];
            }
            [strongSelf bottomViewStatus:YES];
        }];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionBeauty){                 // 【美颜】
        [self bottomViewStatus:NO];
        LiveActionViewController *view = [[LiveActionViewController alloc]init];
        view.transferActionType = actionSingleModel.transferUIBottomType;
        [view showInView:self.parentViewController withBlock:^(LiveUIBottomViewSingleModel *actionSingleModel1) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (actionSingleModel1.transferUIBottomType == LiveUIBottomViewTypeActionBeautyWhite){
                [TXLiveManager openWhite:strongSelf.tilFilter];
            } else {
                [TXLiveManager openBeauty:strongSelf.tilFilter];
            }
            if(actionSingleModel1.transferUIBottomType == LiveUIBottomViewTypeActionDismiss){
                [strongSelf bottomViewStatus:YES];
            }
        }];
    } else if (actionSingleModel.transferUIBottomType == LiveActionTypeResolving){                          // 【修改清晰度】
        [self bottomViewStatus:NO];
        LiveActionViewController *view = [[LiveActionViewController alloc]init];
        view.transferActionType = actionSingleModel.transferUIBottomType;
        [view showInView:self.parentViewController withBlock:^(LiveUIBottomViewSingleModel *actionSingleModel1) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf changeRoleWithType:actionSingleModel1.transferUIBottomType];
            [strongSelf bottomViewStatus:YES];
        }];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionSpecialSounds){          // 【声音】
        [self bottomViewStatus:NO];
        LiveActionViewController *view = [[LiveActionViewController alloc]init];
        view.transferActionType = actionSingleModel.transferUIBottomType;
        [view showInView:self.parentViewController withBlock:^(LiveUIBottomViewSingleModel *actionSingleModel1) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf changeVoiceType:actionSingleModel1.transferUIBottomType - 801];
            [strongSelf bottomViewStatus:YES];
        }];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionChat){               // 聊天
        [self bottomViewStatus:NO];
        [self.inputTextField.inputTextField becomeFirstResponder];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionGift){               // 礼物
        [self bottomViewStatus:NO];
        LiveActionViewController *view = [[LiveActionViewController alloc]init];
        view.transferLiveSingleModel = self.transferLiveSingleModel;
        __weak typeof(self)weakSelf = self;
        [view chongzhiBtnClickManagerBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [self bottomViewStatus:YES];
            [view sheetViewDismissWithBlcok:^{
                ChongzhiRootViewController *chongzhiViewController = [[ChongzhiRootViewController alloc]init];
                [strongSelf.navigationController pushViewController:chongzhiViewController animated:YES];
            }];
        }];

        self.tempActionview = view;
        view.transferActionType = actionSingleModel.transferUIBottomType;
        [view showInView:self.parentViewController withBlock:^(LiveUIBottomViewSingleModel *actionSingleModel1) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToSendGift:actionSingleModel1.giftSingleModel];
            [strongSelf bottomViewStatus:YES];
        }];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionShare){          // 分享
        [self bottomViewStatus:YES];
        [self pushStreamEncodeType:ILive_ENCODE_HLS recordType:ILive_RECORD_FILE_TYPE_MP4];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionDismiss){
        [self bottomViewStatus:YES];
    } else if (actionSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionClickZan){       // 【点赞】
        [self bottomViewStatus:YES];
        if (self.transferLiveShowType == LiveShowTypeAnchor){     // 主播
            return;
        }
        
        ILVLiveCustomMessage *msg = [[ILVLiveCustomMessage alloc] init];
        msg.type = ILVLIVE_IMTYPE_GROUP;
        msg.cmd = (ILVLiveIMCmd)AVIMCMD_Praise;
        msg.recvId = [[ILiveRoomManager getInstance] getIMGroupId];
        self.zanCount++;
        [self.actionView.houseInfoView addZan];

        [[TILLiveManager getInstance] sendCustomMessage:msg succ:^{
            NSMutableDictionary *pointDic = [NSMutableDictionary dictionary];
            [pointDic setObject:[NSNumber numberWithFloat:self.actionView.bottomUIView.clickZanButton.center.x] forKey:@"parise_x"];
            [pointDic setObject:[NSNumber numberWithFloat:self.actionView.bottomUIView.clickZanButton.center.y] forKey:@"parise_y"];
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserParise_Notification object:pointDic];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            
        }];
    }
}

-(void)bottomViewStatus:(BOOL)status{
    [UIView animateWithDuration:.3f animations:^{
        if (status){
            self.actionView.bottomUIView.orgin_y = kScreenBounds.size.height - self.actionView.bottomUIView.size_height;
        } else {
            self.actionView.bottomUIView.orgin_y = kScreenBounds.size.height;
        }
    } completion:^(BOOL finished) {
        if (status){
            self.actionView.bottomUIView.orgin_y = kScreenBounds.size.height - self.actionView.bottomUIView.size_height;
        } else {
            self.actionView.bottomUIView.orgin_y = kScreenBounds.size.height;
        }
    }];
}



- (void)pushStreamEncodeType:(ILiveEncodeType)encodeType recordType:(ILiveRecordFileType)recordType {
    ILiveChannelInfo *info = [[ILiveChannelInfo alloc] init];
    info.channelName = [NSString stringWithFormat:@"新随心播推流_%@",[[ILiveLoginManager getInstance] getLoginId]];
    info.channelDesc = [NSString stringWithFormat:@"新随心播推流描述测试文本"];
    
    ILivePushOption *option = [[ILivePushOption alloc] init];
    option.channelInfo = info;
    option.encodeType = encodeType;
    option.recrodFileType = recordType;
    
    ILiveRoomManager *roomManager = [ILiveRoomManager getInstance];
    __weak typeof(self)weakSelf = self;
    [roomManager startPushStream:option succ:^(id selfPtr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [StatusBarManager statusBarHidenWithText:@"正在分配分享URL"];
        AVStreamerResp *resp = (AVStreamerResp *)selfPtr;
        AVLiveUrl *url = nil;
        if (resp && resp.urls && resp.urls.count > 0) {
            url = resp.urls[0];
        }
        NSString *msg = url ? url.playUrl : @"url为nil";
        NSString *mainUrl = [NSString stringWithFormat:@"%@%@",share_live_baseUrl,msg];
        NSString *imgUrl = [PDImageView appendingImgUrl:self.transferLiveSingleModel.img];
        
        [strongSelf shareActionManagerWithUrl:mainUrl img:imgUrl];
        
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        NSString *errinfo = [NSString stringWithFormat:@"push stream fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        NSLog(@"%@",errinfo);
    }];
}

-(void)changeRoleWithType:(LiveUIBottomViewType)transferActionType{
    NSInteger index = 0;
    if (transferActionType == LiveActionTypeResolving1){
        index = 1;
    } else if (transferActionType == LiveActionTypeResolving2){
        index = 2;
    } else if (transferActionType == LiveActionTypeResolving3){
        index = 3;
    }
    NSString *info = @"";
    if(index == 1){// 高清
        info = kSxbRole_HostHD;
    } else if (index == 2){
        info = kSxbRole_HostSD;
    } else if (index == 3){
        info = kSxbRole_HostLD;
    }
    
    [[ILiveRoomManager getInstance] changeRole:info succ:^{
        [StatusBarManager statusBarHidenWithText:@"切换成功"];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        NSString *logInfo = [NSString stringWithFormat:@"切换失败.module=%@,code=%d,msg=%@",module,errId,errMsg];
        [StatusBarManager statusBarHidenWithText:logInfo];
    }];
}

- (void)addNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(upVideoUpdateFuns:) name:kUserUpVideo_Notification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downVideoUpdateFuns) name:kUserDownVideo_Notification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchRoomRefresh) name:kUserSwitchRoom_Notification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLikeHeartStartRect:) name:kUserParise_Notification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageReceived:) name:@"CCPDidReceiveMessageNotification" object:nil];// 礼物
}

#pragma mark 6.3 处理到来推送消息
- (void)onMessageReceived:(NSNotification *)notification {
    CCPSysMessage *message = [notification object];
    NSString *title = [[NSString alloc] initWithData:message.title encoding:NSUTF8StringEncoding];
    NSString *body = [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];
    SBJSON *json = [[SBJSON alloc] init];
    NSDictionary *dicWithRequestJson = [json objectWithString:body error:nil];
    if ([title isEqualToString:@"1"]){      // 表示当前的类型是送礼物
        LiveNotifyRootModel *responseModelObject = (LiveNotifyRootModel *)[[LiveNotifyRootModel alloc] initWithJSONDict:dicWithRequestJson];
        if ([responseModelObject.houseId isEqualToString:self.transferLiveSingleModel.house_id]){
            [self sendGiftManager:responseModelObject];
        }
    } else if ([title isEqualToString:@"2"]){       // 表示类型是主播退出房间
        [UIAlertView alertViewWithTitle:@"主播已经退出房间" message:@"主播已下播,请退出房间" buttonTitles:@[@"确定"] callBlock:NULL];
    } else if ([title isEqualToString:@"3"]){   // 修改赞
        LiveNotifyRootModel *responseModelObject = (LiveNotifyRootModel *)[[LiveNotifyRootModel alloc] initWithJSONDict:dicWithRequestJson];
        if ([responseModelObject.houseId isEqualToString:self.transferLiveSingleModel.house_id]){
            [self.actionView.houseInfoView settingZan:responseModelObject.zanModel.zan];
        }
    }
}

#pragma mark - 送礼物
-(void)sendGiftManager:(LiveNotifyRootModel *)responseModelObject{
    AnimOperationManager *manager = [AnimOperationManager sharedManager];
    __weak LiveReleaseShowViewController *weakVC = self;
    manager.parentView = weakVC.view;
    responseModelObject.giftModel.giftModel.count = 1;
    // 用用户唯一标识 msg.senderChatID 存礼物信息,model 传入礼物模型
    [manager animWithUserID:responseModelObject.giftModel.userModel.ID model:responseModelObject.giftModel finishedBlock:^(BOOL result) {
        
    }];
}



//上麦之后更新界面
- (void)upVideoUpdateFuns:(NSNotification *)notify {
    [TXLiveManager sharedAccountModel].isUpVideo = YES;
    NSString *roleName = (NSString *)notify.object;
    [TXLiveManager sharedAccountModel].role = roleName;
}

//下麦之后更新界面
- (void)downVideoUpdateFuns {
    [TXLiveManager sharedAccountModel].isUpVideo = NO;
}

// 修改房间更新
- (void)switchRoomRefresh {
    [TXLiveManager sharedAccountModel].isUpVideo = NO;
}


















#pragma mark 键盘通知
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 添加手势
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.topScrollView.orgin_y=  - keyboardRect.size.height;
    self.inputTextField.orgin_y = kScreenBounds.size.height - keyboardRect.size.height - self.inputTextField.size_height;
    [UIView commitAnimations];
}

#pragma mark 键盘隐藏
- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.topScrollView.orgin_y-=0;
    self.inputTextField.orgin_y = kScreenBounds.size.height;
    
    [UIView commitAnimations];
}


-(void)sendMsg:(ILVLiveMessage *)msg{
    [self.actionView.messageView viewSendMsg:msg];
}

#pragma mark - 连麦
- (void)connectVideoBegin:(NSNotification *)noti {
    
    //增加连麦小视图
    NSString *userid = (NSString *)noti.object;
    LiveCallView *callView = [[UserViewManager shareInstance] addPlaceholderView:userid];
    [self.view addSubview:callView];
}

// 取消连麦
- (void)connectVideoCancel:(NSNotification *)noti {
    NSString *userId = (NSString *)noti.object;
    [[UserViewManager shareInstance] removePlaceholderView:userId];
    [[UserViewManager shareInstance] refreshViews];
}



- (void)OnLocalVideoPreview:(QAVVideoFrame *)frameData {
}

- (void)OnLocalVideoPreProcess:(QAVVideoFrame *)frameData
{
    TILDataType type = TILDataType_NV12;
    switch (frameData.frameDesc.color_format)
    {
        case AVCOLOR_FORMAT_I420:
            type = TILDataType_I420;
            break;
        case AVCOLOR_FORMAT_NV12:
            type = TILDataType_NV12;
            break;
        default:
            break;
    }
    [_tilFilter processData:frameData.data inType:type outType:type size:frameData.dataSize width:frameData.frameDesc.width height:frameData.frameDesc.height];
}

- (void)OnLocalVideoRawSampleBuf:(CMSampleBufferRef)buf result:(CMSampleBufferRef *)ret
{
}

- (void)OnVideoPreview:(QAVVideoFrame *)frameData {
    
}

- (BOOL)onRoomDisconnect:(int)reason {
    __weak typeof(self) weakSelf = self;
    [UIAlertView alertViewWithTitle:@"房间失去连接" message:nil buttonTitles:@[@"退出"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf onClose];
    }];
    return YES;
}


#pragma mark - 接口
// 获取礼物列表
-(void)goToChongzhiVC{
    [self bottomViewStatus:YES];
    __weak typeof(self)weakSelf = self;
    [self.tempActionview sheetViewDismissWithBlcok:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ChongzhiRootViewController *chongzhiViewController = [[ChongzhiRootViewController alloc]init];
        [strongSelf.navigationController pushViewController:chongzhiViewController animated:YES];
    }];
}

-(void)sendRequestToSendGift:(LiveGiftSingleModel *)giftSingleModel{
    __weak typeof(self)weakSelf = self;
    if (giftSingleModel){
        if ([AccountModel sharedAccountModel].loginModel.money < giftSingleModel.price){
            __weak typeof(self)weakSelf = self;
            [[UIAlertView alertViewWithTitle:@"余额不足" message:@"是否直接进行充值" buttonTitles:@[@"确定",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (buttonIndex == 0){
                    [strongSelf goToChongzhiVC];
                }
            }] show];
            return;
        }

        NSDictionary *params = @{@"toUserId":self.transferLiveSingleModel.userInfo.ID,@"giftId":giftSingleModel.ID,@"count":@(1),@"houseId":self.transferLiveSingleModel.house_id};
        
        [[NetworkAdapter sharedAdapter] fetchWithPath:live_send_gift requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (isSucceeded){
                NSString *info = [NSString stringWithFormat:@"赠送礼物%@成功，消费%liM",giftSingleModel.name,giftSingleModel.price];
                [StatusBarManager statusBarHidenWithText:info];
                [AccountModel sharedAccountModel].loginModel.money -= giftSingleModel.price;
                [strongSelf.tempActionview changeCurrentMoney];
            }
        }];
    }
}

#pragma mark 发送加入房间
-(void)sendRequestToJoinHouse{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"houseId":self.transferLiveSingleModel.house_id,@"show_status":@(self.transferLiveSingleModel.show_status)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_join_house requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"加入房间,%@",self.transferLiveSingleModel.house_id]];
            [strongSelf linkManager];
        }
    }];
}

-(void)linkManager{
    NSString *houseId = [self.transferLiveSingleModel.house_id  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSDictionary *params = @{@"userId":[AccountModel sharedAccountModel].loginModel.ID,@"houseId":houseId};
    NSString *paramsStr = [NetworkAdapter dictionaryToJson:params];
    [[NetworkAdapter sharedAdapter].loginWebSocketConnection webSocketWriteData:paramsStr];
}

#pragma mark 发送退出房间
-(void)sendRequestToOutHouseWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusBarHidenWithText:@"正在通知服务器退出房间，进行结算……"];
    NSDictionary *params = @{@"houseId":self.transferLiveSingleModel.house_id,@"show_status":@(self.transferLiveSingleModel.show_status),@"time":@(1111)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_out_house_root requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"已退出房间"];
            [strongSelf outLinkManager];
            if(block){
                block();
            }
        } else {
            [StatusBarManager statusBarHidenWithText:@"退出房间发生错误"];
        }
    }];
}

-(void)outLinkManager{
    NSDictionary *params = @{@"userId":@"",@"houseId":@""};
    NSString *paramsStr = [NetworkAdapter dictionaryToJson:params];
    [[NetworkAdapter sharedAdapter].loginWebSocketConnection webSocketWriteData:paramsStr];
}

#pragma mark - 主播退出房间
-(void)liveRequestToOutHouseManager{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"houseId":self.transferLiveSingleModel.house_id,@"show_status":@(self.transferLiveSingleModel.show_status)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_root_out_house requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"退出房间成功"];
        }
    }];
}

#pragma mark - 上传identify
-(void)sendRequestToIdentify:(NSString *)identify{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"identify":identify};
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_onebyone_upload requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"房间信息Key上传成功"];
        }
    }];
}


#pragma mark - 获取房主信息
-(void)sendRequestToGetUserInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"target":self.transferLiveSingleModel.userInfo.ID};
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_search_single requestParams:params responseObjectClass:[OtherSearchSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            OtherSearchSingleModel *singleModel = (OtherSearchSingleModel *)responseObject;
            strongSelf.tempUserSingleModel = singleModel;
        }
    }];
}


#pragma mark - 进行赞方法
-(void)timerWithInit{
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
    }
}

-(void)timerWithDealloc{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - 倒计时方法
-(void)daojishiManager{
    self.timeInteger++ ;
    if (self.timeInteger % 15 == 0){
        [self sendRequestToZanManager];
    }
    if (self.timeInteger % 30 == 0){
        [self sendRequestToGetUserInCurrentHouse];
    }
}

#pragma mark 进行提交赞
-(void)sendRequestToZanManager{
    __weak typeof(self)weakSelf = self;
    if (self.zanCount == 0){
        return;
    }
    NSDictionary *params = @{@"house_id":self.transferLiveSingleModel.house_id,@"zan":@(self.zanCount)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_zan requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf.zanCount = 0;
        }
    }];

}


#pragma mark - 获取当前房间里面的用户
-(void)sendRequestToGetUserInCurrentHouse{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params=  @{@"house_id":self.transferLiveSingleModel.house_id};
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_users requestParams:params responseObjectClass:[OtherSearchListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.actionView.userListMutableArr.count){
                [strongSelf.actionView.userListMutableArr removeAllObjects];
            }
            OtherSearchListModel *infoList = (OtherSearchListModel *)responseObject;
            [strongSelf.actionView.userListMutableArr addObjectsFromArray:infoList.list];
            [strongSelf.actionView.userListTableView reloadData];
            
            // 修改用户数量
            [strongSelf.actionView.topUIView changeCustomerCount:infoList.list.count];
        }
    }];
}

@end

//
//  LiveUIActionView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveUIActionView.h"
#import "LiveUITopView.h"
#import "LiveUIBottomView.h"
#import "LiveMessageView.h"                 // 消息的view
#import "LiveUIHouseInfoView.h"
#import "TXLiveManager.h"

static char liveUIActionViewActionClickBlockKey;
static char liveUIActionViewActionUITopClickBlockKey;
@interface LiveUIActionView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)PDImageView *shawBackgroundView;                /**< 底部的阴影条*/
@property (nonatomic,strong)PDImageView *topAlphaView;

@property (nonatomic,assign)CGFloat user_width;
@end

@implementation LiveUIActionView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.userListMutableArr = [NSMutableArray array];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    if (!self.topUIView){
        self.topUIView = [[LiveUITopView alloc]initWithFrame:CGRectMake(LCFloat(11),20 + LCFloat(11), kScreenBounds.size.width / 2./ 1.4, LCFloat(44))];
        self.topUIView.backgroundColor = [UIColor clearColor];
        __weak typeof(self)weakSelf = self;
        [self.topUIView actionClickWithLiveTopBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)() = objc_getAssociatedObject(strongSelf, &liveUIActionViewActionUITopClickBlockKey);
            if(block){
                block();
            }
        }];

        [self addSubview:self.topUIView];
    }
    
    self.shawBackgroundView = [[PDImageView alloc]init];
    self.shawBackgroundView.image = [Tool stretchImageWithName:@"pro_bg_blackshadow"];
    self.shawBackgroundView.frame = CGRectMake(0, kScreenBounds.size.height - 19, kScreenBounds.size.width, 19);
    [self addSubview:self.shawBackgroundView];

    // 黄建顶部的view
//    self.topAlphaView = [[PDImageView alloc]init];
//    self.topAlphaView.image = [Tool stretchImageWithName:@"brannan_blowout"];
//    self.topAlphaView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 50);
//    self.topAlphaView.backgroundColor = [UIColor clearColor];
//    [self addSubview:self.topAlphaView];


    // 创建底部的view
    if (!self.bottomUIView){
        self.bottomUIView = [[LiveUIBottomView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height - LCFloat(60), kScreenBounds.size.width, LCFloat(60))];
        __weak typeof(self)weakSelf = self;
        [self.bottomUIView liveBottomUIViewClickBlock:^(LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(LiveUIBottomViewSingleModel *liveModel) = objc_getAssociatedObject(strongSelf, &liveUIActionViewActionClickBlockKey);
            if (block){
                block(liveUIBottomViewSingleModel);
            }
            if (liveUIBottomViewSingleModel.transferUIBottomType == LiveUIBottomViewTypeActionClickZan){
                [TXLiveManager messageWithClickZan:strongSelf.bottomUIView.clickZanButton block:^(BOOL success) {
                    
                }];
            }
        }];
        [self addSubview:self.bottomUIView];
    }
    
    // 创建消息view
    if (!self.messageView){
        self.messageView = [[LiveMessageView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height - self.bottomUIView.size_height - kScreenBounds.size.width / 2. * 1.4, kScreenBounds.size.width, kScreenBounds.size.width / 2. * 1.3)];

        [self addSubview:self.messageView];
    }
    
    // 2. 创建顶部的view
    if (!self.houseInfoView){
        self.houseInfoView = [[LiveUIHouseInfoView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width / 2., CGRectGetMaxY(self.topUIView.frame) + LCFloat(20), kScreenBounds.size.width / 2., [LiveUIHouseInfoView calculationHeight])];

        [self addSubview:self.houseInfoView];
    }

    // 3. 创建顶部的用户
    self.userListTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.user_width = kScreenBounds.size.width - CGRectGetMaxX(self.topUIView.frame) - LCFloat(11) - LCFloat(60);
    CGFloat center_x = CGRectGetMaxX(self.topUIView.frame) + LCFloat(11) + self.user_width / 2.;
    self.userListTableView.bounds = CGRectMake(0, 0, self.topUIView.size_height,self.user_width);
    self.userListTableView.center = CGPointMake(center_x, self.topUIView.orgin_y + self.topUIView.size_height / 2.);
    self.userListTableView.delegate = self;
    self.userListTableView.backgroundColor = [UIColor clearColor];
    self.userListTableView.dataSource = self;
    self.userListTableView.showsVerticalScrollIndicator = NO;
    self.userListTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    self.userListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:self.userListTableView];

}




-(void)setTransferShowType:(LiveShowType)transferShowType{
    _transferShowType = transferShowType;
    self.bottomUIView.transferShowType = self.transferShowType;
}

-(void)setTransferLiveSingleModel:(HomeLiveSingleModel *)transferLiveSingleModel{
    _transferLiveSingleModel = transferLiveSingleModel;
    self.houseInfoView.transferLiveSingleModel = transferLiveSingleModel;
    self.topUIView.transferLiveSingleModel = transferLiveSingleModel;
}

-(void)liveUIActionViewActionClickBlock:(void(^)(LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel))block{
    objc_setAssociatedObject(self, &liveUIActionViewActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)liveUIActionViewActionUITopClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &liveUIActionViewActionUITopClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}







#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.userListMutableArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CELL"];
        cell.transform = CGAffineTransformMakeRotation(M_PI_2);

        PDImageView *imgView = [[PDImageView alloc]init];
        imgView.backgroundColor = [UIColor clearColor];
        imgView.frame = CGRectMake(0, 0, self.topUIView.size_height, self.topUIView.size_height);
        imgView.stringTag = @"imgView";
        [cell.contentView addSubview:imgView];


        PDImageView *iconImgView = [[PDImageView alloc]init];
        iconImgView.frame = CGRectMake(self.topUIView.size_height / 2., self.topUIView.size_height / 2., self.topUIView.size_height / 2., self.topUIView.size_height / 2.);
        iconImgView.stringTag = @"iconImgView";
        [cell.contentView addSubview:iconImgView];
        cell.backgroundColor = [UIColor clearColor];
    }
    OtherSearchSingleModel *singleModel = [self.userListMutableArr objectAtIndex:indexPath.section];
    PDImageView *imgView = (PDImageView *)[cell viewWithStringTag:@"imgView"];
    [imgView uploadImageWithAvatarURL:singleModel.avatar placeholder:nil callback:NULL];

    PDImageView *iconImgView = (PDImageView *)[cell viewWithStringTag:@"iconImgView"];
    if (indexPath.section == 0){
        iconImgView.image = [UIImage imageNamed:@"ic_guardian_f_1"];
    } else if (indexPath.section == 1){
        iconImgView.image = [UIImage imageNamed:@"ic_guardian_f_2"];
    } else if (indexPath.section == 2){
        iconImgView.image = [UIImage imageNamed:@"ic_guardian_f_3"];
    } else {
        iconImgView.image = nil;
    }
    return cell;
}


#pragma mark - UITableviewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.topUIView.size_height;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    UILabel *label = [cell.contentView viewWithTag:1];
    label.text = [@(indexPath.row + 1) stringValue];
}



















#pragma mark - 视图进行重置
-(void)liveUIWithReset{
    
}












#pragma mark - set


-(void)viewSendMsg:(ILVLiveTextMessage *)msg{
    
}
@end

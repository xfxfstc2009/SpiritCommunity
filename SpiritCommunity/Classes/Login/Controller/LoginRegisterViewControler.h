//
//  LoginRegisterViewControler.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 注册
#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,registerVCType) {
    registerVCTypeRegister,                     /**< 注册*/
    registerVCTypeReset,                        /**< 忘记密码*/
    registerVCTypeChange,                       /**< 修改密码*/
};

@interface LoginRegisterViewControler : AbstractViewController

@property (nonatomic,assign)registerVCType transferType;

@end

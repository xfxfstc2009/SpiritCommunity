//
//  SearchSingleTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "SearchSingleTableViewCell.h"

@interface SearchSingleTableViewCell()
@property (nonatomic,strong)PDImageView *avatar;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)PDImageView *hotIcon;
@property (nonatomic,strong)UIButton *linkButton;

@end

static char actionClickWithLinkWithBlockKey;
@implementation SearchSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatar = [[PDImageView alloc]init];
    self.avatar.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatar];
    
    // 2. 创建label
    self.nameLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    [self addSubview:self.nameLabel];
    
    // 3. 创建hot
    self.hotIcon = [[PDImageView alloc]init];
    self.hotIcon.backgroundColor = [UIColor clearColor];
    self.hotIcon.image = [UIImage imageNamed:@"home_search_hot_icon"];
    [self addSubview:self.hotIcon];
    
    // 4. 创建desc
    self.descLabel = [GWViewTool createLabelFont:@"提示" textColor:@"灰"];
    [self addSubview:self.descLabel];
    
    // 5. 创建按钮
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.linkButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.linkButton];    
}

-(void)setTransferSingleModel:(OtherSearchSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    // avatar
    CGFloat width = LCFloat(40);
    self.avatar.frame = CGRectMake(LCFloat(11), (self.transferCellHeight - width) / 2., width, width);
    [self.avatar uploadImageWithAvatarURL:transferSingleModel.avatar placeholder:nil callback:NULL];
    
    // 2.创建ttle
    self.nameLabel.text = transferSingleModel.nick.length?transferSingleModel.nick: [NSString stringWithFormat:@"精灵%@",transferSingleModel.phone.length?transferSingleModel.phone:transferSingleModel.ID];
    CGSize nameSize = [NSString makeSizeWithLabel:self.nameLabel];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatar.frame) + LCFloat(11), 0, nameSize.width, nameSize.height);
    
    // 3. 创建desc
    self.descLabel.text = transferSingleModel.desc;
    self.descLabel.frame = CGRectMake(CGRectGetMaxX(self.avatar.frame) + LCFloat(11), 0, kScreenBounds.size.width - CGRectGetMaxX(self.avatar.frame) - LCFloat(11) - LCFloat(11) - LCFloat(50) - LCFloat(11), [NSString contentofHeightWithFont:self.descLabel.font]);
    
    // 4. 创建hot
    self.hotIcon.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + LCFloat(11), 0, LCFloat(20), LCFloat(20));
    
    CGFloat margin = (self.transferCellHeight - nameSize.height - [NSString contentofHeightWithFont:self.descLabel.font]) / 3.;
    self.nameLabel.orgin_y = margin;
    self.descLabel.orgin_y = CGRectGetMaxY(self.nameLabel.frame) + margin;
    self.hotIcon.center_y = self.nameLabel.center_y;
    
    // 5. 搜索按钮
    self.linkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(20), (self.transferCellHeight - LCFloat(20)) / 2., LCFloat(20), LCFloat(20));
    __weak typeof(self)weakSelf = self;
    [self.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BOOL status) = objc_getAssociatedObject(strongSelf, &actionClickWithLinkWithBlockKey);
        if (block){
            block(!strongSelf.transferSingleModel.link);
        }
    }];
    
    // link
    if (self.transferSingleModel.link){             // 关注
        [self.linkButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_hlt"] forState:UIControlStateNormal];
    } else {
        [self.linkButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_nor"] forState:UIControlStateNormal];
    }
}

-(void)setTransferType:(SearchControllerType)transferType{
    _transferType = transferType;
    if (transferType == SearchControllerTypeFans){
        self.hotIcon.hidden = YES;
    } else {
        self.hotIcon.hidden = NO;
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(60);
}

-(void)actionClickWithLinkWithBlock:(void(^)(BOOL status))block{
    objc_setAssociatedObject(self, &actionClickWithLinkWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


-(void)actionClickZan:(BOOL)status{
    __weak typeof(self)weakSelf = self;
    [Tool clickZanWithView:self.linkButton block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (status){
            [strongSelf.linkButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_hlt"] forState:UIControlStateNormal];
        } else{
            [strongSelf.linkButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_nor"] forState:UIControlStateNormal];
        }
    }];
}
@end

//
//  HomeHasInSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/17.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface HomeHasInSingleModel : FetchModel

@property (nonatomic,assign)BOOL hasIn;

@end

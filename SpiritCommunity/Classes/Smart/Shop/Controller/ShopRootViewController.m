//
//  ShopRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2018/1/30.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "ShopRootViewController.h"
#import "HomeRootBannerTempCell.h"
#import "HomeRootIntegralSingleCell.h"
#import "HomeGoodsNormalSingleCell.h"
#import "HomeNormalListGoodsModel.h"
#import "ShopDetailViewController.h"
#import "QiandaoViewController.h"               // 签到
#import "HomeHouseNormalSingleCell.h"

@interface ShopRootViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>{
    HomeNormalListGoodsModel *tempHomeSingleModel;
}
@property (nonatomic,strong)UITableView *homeRootTableView;
@property (nonatomic,strong)NSMutableArray *homeMutableArr;
@property (nonatomic,strong)UISearchBar *searchBar;
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)NSMutableArray *normalGoodsMutableArr;          /**< 商品列表*/
@property (nonatomic,strong)NSMutableArray *normalHouseMutableArr;          /**< 商品列表*/

@end

@implementation ShopRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

-(void)pageSetting{
    self.barMainTitle = @"精灵社区";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"签到" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        QiandaoViewController *qiandaoViewController = [[QiandaoViewController alloc]init];
        [strongSelf.navigationController pushViewController:qiandaoViewController animated:YES];
    }];
}

-(void)arrayWithInit{
    self.homeMutableArr = [NSMutableArray array];
    [self.homeMutableArr addObject:@[@"banner"]];
    self.normalGoodsMutableArr = [NSMutableArray array];
    self.normalHouseMutableArr = [NSMutableArray array];
}

-(void)createTableView{
    if(!self.homeRootTableView){
        self.homeRootTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.homeRootTableView.dataSource = self;
        self.homeRootTableView.delegate = self;
        [self.view addSubview:self.homeRootTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.homeMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.homeMutableArr objectAtIndex:section];
    if ([sectionOfArr containsObject:@"精选商品"]){
        return (self.normalGoodsMutableArr.count / 2 + 1);
    }
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if(indexPath.section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.homeMutableArr]){
        if (indexPath.row == 0){        // 【banner】
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            GWBannerTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[GWBannerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferImgModelArr = tempHomeSingleModel.adbanner;
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            HomeRootBannerTempCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[HomeRootBannerTempCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo actionClickWithTypeBlock:^(BannerTempType Type) {
                if(!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if(Type == BannerTempTypeDuobao){
//                    JifenDuobaoController *jifenDuobaoController = [[JifenDuobaoController alloc]init];
//                    [jifenDuobaoController hidesTabBarWhenPushed];
//                    [self.navigationController pushViewController:jifenDuobaoController animated:YES];
                } else if(Type == BannerTempTypeFenlei){
//                    ProductCategoryViewController *productCategoryViewController = [[ProductCategoryViewController alloc]init];
//                    [productCategoryViewController hidesTabBarWhenPushed];
//                    [self.navigationController pushViewController:productCategoryViewController animated:YES];
                } else if (Type == BannerTempTypeJifen){
//                    JifenShopController *jifenShopController = [[JifenShopController alloc]init];
//                    [jifenShopController hidesTabBarWhenPushed];
//                    [strongSelf.navigationController pushViewController:jifenShopController animated:YES];
                } else if (Type == BannerTempTypeQiandao){
                    QiandaoViewController *qiandaoViewController = [[QiandaoViewController alloc]init];
                    [strongSelf.navigationController pushViewController:qiandaoViewController animated:YES];
                }
            }];
            return cellWithRowTwo;
        }
    } else if(indexPath.section == [self cellIndexPathSectionWithcellData:@"积分商城" sourceArr:self.homeMutableArr]){
        if(indexPath.row == 0){
            static NSString *cellIdentifyWithRowThrZero = @"cellIdentifyWithRowThrZero";
            GWNormalTableViewCell*cellWithRowThrZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThrZero];
            if (!cellWithRowThrZero){
                cellWithRowThrZero = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThrZero];
                cellWithRowThrZero.selectionStyle =UITableViewCellSelectionStyleNone;
            }
            cellWithRowThrZero.transferCellHeight = cellHeight;
            cellWithRowThrZero.transferHasArrow = YES;
            cellWithRowThrZero.transferDesc = @"MORE";
            cellWithRowThrZero.transferTitle = @"积分商城";
            return cellWithRowThrZero;
        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            HomeRootIntegralSingleCell*cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[HomeRootIntegralSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle =UITableViewCellSelectionStyleNone;
            }
            cellWithRowThr.transferCellheight = cellHeight;
            cellWithRowThr.transferSingleModel = [[self.homeMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return cellWithRowThr;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"积分夺宝" sourceArr:self.homeMutableArr]){
        if(indexPath.row == 0){
            static NSString *cellIdentifyWithRowFourZero = @"cellIdentifyWithRowFourZero";
            GWNormalTableViewCell*cellWithRowFourZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFourZero];
            if (!cellWithRowFourZero){
                cellWithRowFourZero = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFourZero];
                cellWithRowFourZero.selectionStyle =UITableViewCellSelectionStyleNone;
            }
            cellWithRowFourZero.transferCellHeight = cellHeight;
            cellWithRowFourZero.transferHasArrow = YES;
            cellWithRowFourZero.transferDesc = @"MORE";
            cellWithRowFourZero.transferTitle = @"积分夺宝";
            return cellWithRowFourZero;
        } else{
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            HomeRootIntegralSingleCell*cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[HomeRootIntegralSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];

            }
            cellWithRowFour.transferCellheight = cellHeight;
//            cellWithRowFour.transferSingleModel = [[self.homeMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
//            cellWithRowFour.transferType = IntegralCellTypeProgress;
            return cellWithRowFour;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"精选商品" sourceArr:self.homeMutableArr]){
        if(indexPath.row == 0){
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            GWNormalTableViewCell*cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle =UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferTitle = @"精选商品";
            cellWithRowFiv.textAlignmentCenter = YES;
            return cellWithRowFiv;
        } else {
            static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
            HomeGoodsNormalSingleCell*cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
            if (!cellWithRowOther){
                cellWithRowOther = [[HomeGoodsNormalSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
                cellWithRowOther.selectionStyle =UITableViewCellSelectionStyleNone;
            }
            cellWithRowOther.transferCellHeight = cellHeight;

            NSInteger leftIndex = (indexPath.row - 1 ) * 2 + 1;
            if (self.normalGoodsMutableArr.count >= leftIndex){
                HomeNormalSingleGoodsModel *leftModel = [self.normalGoodsMutableArr objectAtIndex:leftIndex ];
                cellWithRowOther.transferLeftGoodsModel = leftModel;
            }
            NSInteger rightIndex = (indexPath.row - 1) * 2 + 2;
            if (self.normalGoodsMutableArr.count >= rightIndex){
                HomeNormalSingleGoodsModel *rightModel = [self.normalGoodsMutableArr objectAtIndex:rightIndex];
                cellWithRowOther.transferRightGoodsModel = rightModel;

            }
            __weak typeof(self)weakSelf = self;
            [cellWithRowOther actionClickCellBlock:^(HomeNormalSingleGoodsModel *transferGoodsModel) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                ShopDetailViewController *homeProductRootViewController = [[ShopDetailViewController alloc]init];
                homeProductRootViewController.transferProductId = transferGoodsModel.ID;
                homeProductRootViewController.transferProductType = productTypeNormal;
                [strongSelf.navigationController pushViewController:homeProductRootViewController animated:YES];
            }];

            return cellWithRowOther;
        }
    } else if(indexPath.section == [self cellIndexPathSectionWithcellData:@"附近商店" sourceArr:self.homeMutableArr]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
            GWNormalTableViewCell*cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
            if (!cellWithRowSex){
                cellWithRowSex = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
                cellWithRowSex.selectionStyle =UITableViewCellSelectionStyleNone;
            }
            cellWithRowSex.transferCellHeight = cellHeight;
            cellWithRowSex.transferTitle = @"附近商店";
            cellWithRowSex.titleLabel.textAlignment = NSTextAlignmentCenter;
            return cellWithRowSex;
        } else {
            static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
            HomeHouseNormalSingleCell*cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
            if (!cellWithRowSex){
                cellWithRowSex = [[HomeHouseNormalSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
                cellWithRowSex.selectionStyle =UITableViewCellSelectionStyleNone;
            }
            cellWithRowSex.transferCellHeight = cellHeight;
            cellWithRowSex.transferModel = [[self.homeMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

            return cellWithRowSex;
        }
    } else {
        static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
        GWNormalTableViewCell*cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
        if (!cellWithRowSev){
            cellWithRowSev = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
            cellWithRowSev.selectionStyle =UITableViewCellSelectionStyleNone;
        }
        cellWithRowSev.transferCellHeight = cellHeight;
        cellWithRowSev.transferTitle = @"精选商品";
        cellWithRowSev.titleLabel.textAlignment = NSTextAlignmentCenter;
        return cellWithRowSev;

    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.homeMutableArr]){
        if (indexPath.row == 0){        // 【banner】
            return [GWBannerTableViewCell calculationCellHeight];
        } else {
            return [HomeRootBannerTempCell calculationCellHeight];
        }
    } else if(indexPath.section == [self cellIndexPathSectionWithcellData:@"积分商城" sourceArr:self.homeMutableArr]){
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            return [HomeRootIntegralSingleCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"积分夺宝" sourceArr:self.homeMutableArr]){
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            return [HomeRootIntegralSingleCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"精选商品" sourceArr:self.homeMutableArr]){
        if (indexPath.row == 0){
            return LCFloat(44);
        } else {
            return [HomeGoodsNormalSingleCell calculationCellHeight];
        }
    } else if(indexPath.section == [self cellIndexPathSectionWithcellData:@"附近商店" sourceArr:self.homeMutableArr]){
        if (indexPath.row == 0){
            return LCFloat(44);
        } else {
            return [HomeHouseNormalSingleCell calculationCellHeight];
        }
    } else {
        return LCFloat(44);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(10);
}

#pragma mark - 接口
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    if (![AccountModel sharedAccountModel].authToken.length){
        [[AccountModel sharedAccountModel] smartToAuthManagerWithBlock:^(BOOL successed) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (successed){
                [strongSelf sendRequestToGetList];
            }
        }];
    } else {
        [self sendRequestToGetList];
    }
    
}

-(void)sendRequestToGetList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:_goodsList requestParams:nil responseObjectClass:[HomeNormalListGoodsModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            HomeNormalListGoodsModel *singleModel = (HomeNormalListGoodsModel *)responseObject;
            strongSelf->tempHomeSingleModel = singleModel;
            
            // 刷新第一行banner
            NSInteger section = [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.homeMutableArr];
            NSInteger row = 0;
            NSIndexPath *bannerIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
            [strongSelf.homeRootTableView reloadRowsAtIndexPaths:@[bannerIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            if (strongSelf.homeRootTableView.isXiaLa){
                [strongSelf.normalGoodsMutableArr removeAllObjects];
            }
            if (![strongSelf.normalGoodsMutableArr containsObject:@"精选商品"]){
                [strongSelf.normalGoodsMutableArr addObject:@"精选商品"];
            }
            [strongSelf.normalGoodsMutableArr addObjectsFromArray:singleModel.bestGoods];
            if (![strongSelf.homeMutableArr containsObject:strongSelf.normalGoodsMutableArr]){
                [strongSelf.homeMutableArr addObject:strongSelf.normalGoodsMutableArr];
                NSInteger index = [strongSelf.homeMutableArr indexOfObject:strongSelf.normalGoodsMutableArr];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
                [strongSelf.homeRootTableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
            } else {
                NSInteger index = [strongSelf.homeMutableArr indexOfObject:strongSelf.normalGoodsMutableArr];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
                [strongSelf.homeRootTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            }
            
            // 添加附近商店
            [strongSelf merchantListsManager:singleModel.merchantLists];
            
            
            [strongSelf.homeRootTableView stopFinishScrollingRefresh];
            [strongSelf.homeRootTableView stopPullToRefresh];
        }
    }];
}

-(void)merchantListsManager:(NSArray<HomeNormalSingleGoodsModel> *)merchantLists {
    if (![self.normalHouseMutableArr containsObject:@"附近商店"]){
        [self.normalHouseMutableArr addObject:@"附近商店"];
    }
    [self.normalHouseMutableArr addObjectsFromArray:merchantLists];

    if (![self.homeMutableArr containsObject:self.normalHouseMutableArr]){
        [self.homeMutableArr addObject:self.normalHouseMutableArr];
        NSInteger index = [self.homeMutableArr indexOfObject:self.normalHouseMutableArr];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
        [self.homeRootTableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
    } else {
        NSInteger index = [self.homeMutableArr indexOfObject:self.normalHouseMutableArr];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
        [self.homeRootTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }


}
@end

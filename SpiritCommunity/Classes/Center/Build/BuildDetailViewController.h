//
//  BuildDetailViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/30.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "MapBuildSingleModel.h"
@interface BuildDetailViewController : AbstractViewController

@property (nonatomic,strong)MapBuildSingleModel *transferBuildSingleModel;

@end

//
//  ShopDetailSub2DetailViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/9.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"

@interface ShopDetailSub2DetailViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferImgs;

-(void)pageLoad;

@end

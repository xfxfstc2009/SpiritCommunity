//
//  UIButton+ESTabBar.m
//  Pods
//
//  Created by Ezequiel Scaruli on 5/5/15.
//
//

#import "UIButton+ESTabBar.h"


@implementation UIButton (ESTabBar)


#pragma mark - Public methods


- (void)customizeForTabBarWithImage:(UIImage *)image
                      selectedColor:(UIColor *)selectedColor
                        highlighted:(BOOL)highlighted {
    if (highlighted) {
        [self customizeAsHighlightedButtonForTabBarWithImage:image
                                               selectedColor:selectedColor];
    } else {
        [self customizeAsNormalButtonForTabBarWithImage:image
                                          selectedColor:selectedColor];
    }
}


#pragma mark - Private methods


- (void)customizeAsHighlightedButtonForTabBarWithImage:(UIImage *)image
                                         selectedColor:(UIColor *)selectedColor {
    
    // We want the image to be always white in highlighted state.
    self.tintColor = [UIColor whiteColor];
    [self setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
          forState:UIControlStateNormal];
    
    // And its background color should always be the selected color.
    self.backgroundColor = selectedColor;
    
    
    if ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO){
        [self layoutButtonWithEdgeInsetsStyleImageTitleSpace:17];
    } else {
        
    }
    
}


- (void)customizeAsNormalButtonForTabBarWithImage:(UIImage *)image
                                    selectedColor:(UIColor *)selectedColor {
    
    // The tint color is the one used for selected state.
    self.tintColor = selectedColor;
    
    // When the button is not selected, we show the image always with its
    // original color.
    [self setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
          forState:UIControlStateNormal];
    
    // When the button is selected, we apply the tint color using the
    // always template mode.
    [self setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
          forState:UIControlStateSelected];
    
    // We don't want a background color to use the one in the tab bar.
    self.backgroundColor = [UIColor clearColor];
    
    if ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO){
        [self layoutButtonWithEdgeInsetsStyleImageTitleSpace:17];
    } else {
        
    }
}






- (void)layoutButtonWithEdgeInsetsStyleImageTitleSpace:(CGFloat)space
{
    // 1. 得到imageView和titleLabel的宽、高
    CGFloat imageWith = self.imageView.frame.size.width;
    CGFloat imageHeight = self.imageView.frame.size.height;
    
    CGFloat labelWidth = 0.0;
    CGFloat labelHeight = 0.0;
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        // 由于iOS8中titleLabel的size为0，用下面的这种设置
        labelWidth = self.titleLabel.intrinsicContentSize.width;
        labelHeight = self.titleLabel.intrinsicContentSize.height;
    } else {
        labelWidth = self.titleLabel.frame.size.width;
        labelHeight = self.titleLabel.frame.size.height;
    }
    
    // 2. 声明全局的imageEdgeInsets和labelEdgeInsets
    UIEdgeInsets imageEdgeInsets = UIEdgeInsetsZero;
    UIEdgeInsets labelEdgeInsets = UIEdgeInsetsZero;
    
    // 3. 根据style和space得到imageEdgeInsets和labelEdgeInsets的值
    imageEdgeInsets = UIEdgeInsetsMake(-labelHeight - space / 2.0, 0, 0, -labelWidth);
    labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight-space/2.0, 0);
    // 4. 赋值
    self.titleEdgeInsets = labelEdgeInsets;
    self.imageEdgeInsets = imageEdgeInsets;
}





@end


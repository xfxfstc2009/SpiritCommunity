//
//  ProductDetailCategoryTempSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/26.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "ProductDetailCategorySingleModel.h"

@interface ProductDetailCategoryTempSingleModel : FetchModel
@property (nonatomic,copy)NSString *title;
@property (nonatomic,strong)NSArray *categoryArr;

@property (nonatomic,strong)ProductDetailCategorySingleModel *selectedCategorySingleModel;
@end

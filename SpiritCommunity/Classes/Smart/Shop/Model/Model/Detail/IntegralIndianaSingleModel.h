//
//  IntegralIndianaSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/15.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "integralIndianaRecordsSingleModel.h"

@interface IntegralIndianaSingleModel : FetchModel

@property (nonatomic,assign)NSInteger attendTime;       /**< 参与次数*/
@property (nonatomic,assign)NSInteger integral;         /**< 1人次=多少积分*/
@property (nonatomic,assign)BOOL isAttend;              /**< 是否参与*/
@property (nonatomic,assign)NSInteger needPeople;       /**< 还需要多少人次*/
@property (nonatomic,copy)NSString *number;             /**< 第几期*/
@property (nonatomic,assign)NSInteger state;                 /**< 1：进行中，2：已完成，*/
@property (nonatomic,assign)NSInteger totalPeople;      /**< 总需多少人次*/
@property (nonatomic,copy)NSString *winningIcon;        /**< 中奖者头像*/
@property (nonatomic,copy)NSString *winningId;          /**< 中奖者ID*/
@property (nonatomic,copy)NSString *winningName;        /**< 中夹着名字*/
@property (nonatomic,copy)NSString *winningNumber;      /**< 幸运号码*/
@property (nonatomic,copy)NSString *winningTime;        /**< 揭晓时间*/
@property (nonatomic,copy)NSString *isselfwin;          /**< 是否自己获奖*/
@property (nonatomic,strong)NSArray<integralIndianaRecordsSingleModel> *integralIndianaRecords;


@end

//
//  ProductDetailDuobaoInfoCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/15.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailRootModel.h"

@interface ProductDetailDuobaoInfoCell : UITableViewCell


@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)ProductDetailRootModel *transferProductDetailRootModel;

+(CGFloat)calculationCellHeight;
@end

//
//  MessageRootSingleTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "MessageRootSingleTableViewCell.h"

@interface MessageRootSingleTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)PDImageView *newsIcon;

@end

@implementation MessageRootSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.layer.cornerRadius = LCFloat(3);
    self.avatarImgView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.avatarImgView.layer.shadowOpacity = .2f;
    self.avatarImgView.layer.shadowOffset = CGSizeMake(.2f, .2f);
    self.avatarImgView.clipsToBounds = YES;
    [self addSubview:self.avatarImgView];
    
    // 2. 创建标题
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    // 3. 创建详情
    self.descLabel = [GWViewTool createLabelFont:@"提示" textColor:@"浅灰"];
    [self addSubview:self.descLabel];
    
    // 4. 创建icon
    self.newsIcon = [[PDImageView alloc]init];
    self.newsIcon.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20), 0, LCFloat(20), LCFloat(20));
    self.newsIcon.image = [UIImage imageNamed:@"message_new_icon"];
    [self addSubview:self.newsIcon];
    
    // 5. 创建时间
    self.timeLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    [self addSubview:self.timeLabel];
}

-(void)setTransferSingleModel:(MessageSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // 1. avatar
    self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), self.transferCellHeight - 2 * LCFloat(11), self.transferCellHeight - 2 * LCFloat(11));
    [self.avatarImgView uploadImageWithURL:transferSingleModel.img placeholder:nil callback:NULL];
    
    
    // 2. 创建标题
    self.titleLabel.text = transferSingleModel.title;
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), LCFloat(11), kScreenBounds.size.width - CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(11) - LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.numberOfLines = 1;
    
    // 3. 创建图片
    self.newsIcon.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20), 0, LCFloat(20),LCFloat(20) );
    if (transferSingleModel.has_read){
        self.newsIcon.hidden = YES;
    } else {
        self.newsIcon.hidden = NO;
    }
    // 4. 创建desc
    self.descLabel.text = transferSingleModel.desc;
    CGFloat width = kScreenBounds.size.width - CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(11) - LCFloat(11);
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (descSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        self.descLabel.numberOfLines = 2;
        self.descLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), width, 2 * [NSString contentofHeightWithFont:self.descLabel.font]);
    } else {
        self.descLabel.numberOfLines = 1;
        self.descLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), width, 1 * [NSString contentofHeightWithFont:self.descLabel.font]);
    }
    
    // 5. 创建
    self.timeLabel.text = [NSDate getTimeGap:transferSingleModel.time];
    CGSize timeSize = [NSString makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(20) - timeSize.width,0 , timeSize.width, timeSize.height);
    self.timeLabel.center_y = self.titleLabel.center_y;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    cellHeight += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end

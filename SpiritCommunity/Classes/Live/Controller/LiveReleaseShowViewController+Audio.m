//
//  LiveReleaseShowViewController+Audio.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController+Audio.h"

@implementation LiveReleaseShowViewController (Audio)

-(void)audioWithInit{
    [[[ILiveSDK getInstance] getAVContext].audioCtrl registerAudioDataCallback:QAVAudioDataSource_VoiceDispose];
}

- (void)changeVoiceType:(QAVVoiceType)type {
    QAVContext *context = [[ILiveSDK getInstance] getAVContext];
    [context.audioCtrl setVoiceType:type];
}


@end

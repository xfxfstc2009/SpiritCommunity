//
//  MessageRootSingleTableViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageSingleModel.h"

@interface MessageRootSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)MessageSingleModel *transferSingleModel;

+(CGFloat)calculationCellHeight;

@end

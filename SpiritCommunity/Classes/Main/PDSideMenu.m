//
//  PDSideMenu.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSideMenu.h"
#import "PDSliderRootViewController.h"
#import "PDMainTabbarViewController.h"


@interface PDSideMenu()

@end

@implementation PDSideMenu

-(RESideMenu *)setupSlider{
    
    // 1. 创建slider
    PDSliderRootViewController *leftMenuViewController = [[PDSliderRootViewController alloc]init];
    
    PDMainTabbarViewController *tabBarController = [PDMainTabbarViewController sharedController];

    RESideMenu *sideMenuViewController = [[RESideMenu shareInstance] initWithContentViewController: tabBarController leftMenuViewController:leftMenuViewController rightMenuViewController:nil];
    sideMenuViewController.backgroundImage = [UIImage imageWithRenderColor:[UIColor blackColor] renderSize:CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.height)];
    sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
    sideMenuViewController.contentViewShadowColor = [UIColor blackColor];
    sideMenuViewController.contentViewShadowOffset = CGSizeMake(0, 0);
    sideMenuViewController.contentViewShadowOpacity = 8;
    sideMenuViewController.contentViewShadowRadius = 12;
    sideMenuViewController.animationDuration = .3f;
    sideMenuViewController.contentViewScaleValue = 1;
    sideMenuViewController.contentViewInPortraitOffsetCenterX = kScreenBounds.size.width / 4.;
    sideMenuViewController.contentViewShadowEnabled = YES;
    sideMenuViewController.parallaxEnabled = NO;
    sideMenuViewController.menuPrefersStatusBarHidden = YES;
    return sideMenuViewController;
}



@end

//
//  CenterMoneyZhifuPayBindingModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface CenterMoneyZhifuPayBindingModel : FetchModel

//zhifuAccount = 123;
//zhifuStatus = 0;

@property (nonatomic,copy)NSString *zhifuAccount;               /**< 支付宝账号*/
@property (nonatomic,assign)BOOL zhifuStatus;                   /**< 支付宝状态*/
@end

//
//  HomeRootBannerTempCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeRootBannerListModel.h"

typedef NS_ENUM(NSInteger,BannerTempType) {
    BannerTempTypeJifen,                    /**< 积分商城*/
    BannerTempTypeDuobao,                    /**< 积分商城*/
    BannerTempTypeFenlei,                    /**< 积分商城*/
    BannerTempTypeQiandao,                    /**< 积分商城*/
};


@interface HomeRootBannerTempCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

-(void)actionClickWithTypeBlock:(void(^)(BannerTempType Type))Block;


+(CGFloat)calculationCellHeight;
@end

//
//  NickNameViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"


@interface NickNameViewController : AbstractViewController

@property (nonatomic,assign)UserEditType transferEditType;

-(void)actionBlock:(void(^)(UserEditType transferEditType))block;

@end

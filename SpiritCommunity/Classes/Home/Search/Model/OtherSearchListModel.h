//
//  OtherSearchListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/4.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "OtherSearchSingleModel.h"

@interface OtherSearchListModel : FetchModel

@property (nonatomic,strong)NSArray<OtherSearchSingleModel> *list;

@end

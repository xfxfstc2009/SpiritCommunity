//
//  HomeRootGoodsListMainModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/9.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "HomeRootGoodsListMainSingleModel.h"

@protocol HomeRootGoodsListMainModel <NSObject>

@end

@interface HomeRootGoodsListMainModel : FetchModel

@property (nonatomic,strong)NSArray<HomeRootGoodsListMainSingleModel> *goodlist;
@property (nonatomic,copy)NSString *goodstype;
@property (nonatomic,copy)NSString *img_path;
@property (nonatomic,copy)NSString *title;

@end

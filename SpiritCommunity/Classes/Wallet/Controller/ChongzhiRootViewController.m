//
//  ChongzhiRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/7.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "ChongzhiRootViewController.h"
#import "MoneyChongzhiSingleModel.h"
#import "ChongZhiSingleModelSimple.h"
#import "PDWXPayHandle.h"

@interface ChongzhiRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *chongzhiTableView;         /**< tableview*/
@property (nonatomic,strong)NSMutableArray *chongzhiMutableArr;     /**< array*/

@end

@implementation ChongzhiRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToChongZhiList];
}

#pragma mark - pageSetting
-(void)pageSetting{
  self.barMainTitle = @"充值";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    
      self.chongzhiMutableArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.chongzhiTableView){
        self.chongzhiTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.chongzhiTableView.delegate = self;
        self.chongzhiTableView.dataSource = self;
        [self.view addSubview:self.chongzhiTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.chongzhiMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    MoneyChongzhiSingleModel *singleModel = [self.chongzhiMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_coin_chongzhi"];
    cellWithRowOne.transferTitle = [NSString stringWithFormat:@"%li金币",(long)singleModel.cost];
    cellWithRowOne.transferDesc =[NSString stringWithFormat:@"￥%.2f",singleModel.price/ 100.];
    cellWithRowOne.transferHasArrow = YES;
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MoneyChongzhiSingleModel *singleModel = [self.chongzhiMutableArr objectAtIndex:indexPath.row];
    
    [self sendRequestToGetOrderInfo:singleModel];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType  = SeparatorTypeHead;
    } else if ([indexPath row] == [self.chongzhiMutableArr count] - 1) {
        separatorType  = SeparatorTypeBottom;
    } else {
        separatorType  = SeparatorTypeMiddle;
    }
    if ([self.chongzhiMutableArr count] == 1) {
        separatorType  = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithType:separatorType];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(55);
}

#pragma mark - SendRequestToInfo
// 获取列表
-(void)sendRequestToChongZhiList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:pay_list requestParams:nil responseObjectClass:[MoneyChongzhiListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            MoneyChongzhiListModel *listModel = (MoneyChongzhiListModel *)responseObject;
            if (strongSelf.chongzhiMutableArr.count){
                [strongSelf.chongzhiMutableArr removeAllObjects];
            }
            [strongSelf.chongzhiMutableArr addObjectsFromArray:listModel.list];
            [strongSelf.chongzhiTableView reloadData];
        }
    }];
}

#pragma mark - 创建订单

-(void)sendRequestToChongzhi:(MoneyChongzhiSingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"coinId":singleModel.ID};
    [[NetworkAdapter sharedAdapter] fetchWithPath:chongzhi requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        [[UIAlertView alertViewWithTitle:@"假装充值成功" message:@"充值成功" buttonTitles:@[@"充值成功"] callBlock:NULL]show];
        NSString *chongzhiStr = [NSString stringWithFormat:@"充值%li金币币成功",(long)singleModel.cost];
        [StatusBarManager statusBarHidenWithText:chongzhiStr];
        [AccountModel sharedAccountModel].loginModel.money += singleModel.cost;
    }];
}

#pragma mark 请求服务器申请订单
-(void)sendRequestToGetOrderInfo:(MoneyChongzhiSingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"productId":singleModel.ID};
    [[NetworkAdapter sharedAdapter] fetchWithPath:infoOrder requestParams:params responseObjectClass:[ChongZhiSingleModelSimple class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        [StatusBarManager statusBarHidenWithText:@"正在向服务器申请订单"];
        if (isSucceeded){
            ChongZhiSingleModelSimple *chongZhiSingleModel = (ChongZhiSingleModelSimple *)responseObject;
            PDWXPayHandle *handle = [[PDWXPayHandle alloc]init];
            [handle sendPayRequestWithOrder:chongZhiSingleModel];
            [PDWXPayHandle sharedManager].orderNumber = chongZhiSingleModel.out_trade_no;
            [PDWXPayHandle sharedManager].coin = singleModel.cost;
        }
    }];
}


@end

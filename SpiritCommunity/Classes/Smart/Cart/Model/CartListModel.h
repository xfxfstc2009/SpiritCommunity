//
//  CartListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/22.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "CartSingleModel.h"

@protocol CartListSingleModel <NSObject>

@end

@interface CartListSingleModel : FetchModel

@property (nonatomic,strong)NSArray<CartSingleModel> *cartItems;
@property (nonatomic,copy)NSString *suppliersId;
@property (nonatomic,copy)NSString *supplyName;
@end



@interface CartListModel : FetchModel

@property (nonatomic,strong)NSArray<CartListSingleModel> *infoList;

@end



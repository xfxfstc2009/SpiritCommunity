//
//  MessageListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/21.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "MessageSingleModel.h"

@interface MessageListModel : FetchModel

@property (nonatomic,strong)NSArray<MessageSingleModel> *list;

@end

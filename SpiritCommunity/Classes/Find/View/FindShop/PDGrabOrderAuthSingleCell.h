//
//  PDGrabOrderAuthSingleCell.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/21.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "GWShopShopSingleModel.h"

@interface PDGrabOrderAuthSingleCell : PDBaseTableViewCell

@property (nonatomic,strong)GWShopShopSingleModel *transferAuthModel;

+(CGFloat)calculationCellHeight;

@end

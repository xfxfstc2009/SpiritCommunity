//
//  GWCurrentIMViewController.m
//  GiganticWhale
//
//  Created by 巨鲸 on 16/3/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWCurrentIMViewController.h"
#import "AliChatManager.h"
#import "OtherSearchSingleModel.h"
#import "UserInfoViewController.h"

@interface GWCurrentIMViewController()<AliChatManagerDelegate>

@end

@implementation GWCurrentIMViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self leftNavBtn];
    [AliChatManager sharedInstance].messageDelegate = self;

    if (IS_iPhoneX){
        self.messageInputView.orgin_y -= LCFloat(34);
        self.tableView.size_height-=LCFloat(34);
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self != [self.navigationController.viewControllers firstObject]){
        [[PDMainTabbarViewController sharedController] setBarHidden:YES animated:YES];
    } else {
        [[PDMainTabbarViewController sharedController] setBarHidden:NO animated:YES];
    }
}


-(void)leftNavBtn{
    UIButton *button = [self buttonWithTitle:nil  buttonNorImage:[UIImage imageNamed:@"icon_main_back"] buttonHltImage:[UIImage imageNamed:@"icon_main_back"]];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;


}



- (UIButton *)buttonWithTitle:(NSString *)title buttonNorImage:(UIImage *)norImage buttonHltImage:(UIImage *)hltImage {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:norImage forState:UIControlStateNormal];
    [button setImage:hltImage forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithCustomerName:@"黑"]forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateDisabled];
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = BAR_BUTTON_FONT;
    [button sizeToFit];
    __weak typeof(self)weakSelf = self;
    [button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    return button;
}


-(void)openProfileWithPersonId:(NSString *)personId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"target":personId};
    [StatusBarManager statusBarHidenWithText:@"正在查找该用户"];
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_search_single requestParams:params responseObjectClass:[OtherSearchSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [StatusBarManager statusBarHidenWithText:@"正在跳转……"];
            OtherSearchSingleModel *singleModel = (OtherSearchSingleModel *)responseObject;
            if (singleModel.ID.length){
                UserInfoViewController *userInfoViewController = [[UserInfoViewController alloc]init];
                userInfoViewController.transferSearchSingleModel = singleModel;
                [strongSelf.navigationController pushViewController:userInfoViewController animated:YES];
            } else {
                [StatusBarManager statusBarHidenWithText:@"没有找到该用户"];
            }
        }
    }];
}

-(void)listenNewMessageManagerWithMessage:(id<IYWMessage>)message{
    NSLog(@"12");
}

@end

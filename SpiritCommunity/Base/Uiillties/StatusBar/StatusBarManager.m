//
//  StatusBarManager.m
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/30.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "StatusBarManager.h"
#import <WTStatusBar/WTStatusBar.h>
@implementation StatusBarManager

+(void)statusBarWithClear{
    [WTStatusBar setBackgroundColor:UUBlack];
    [WTStatusBar clearStatusAnimated:YES];
}

+(void)statusBarShowWithText:(NSString *)text{
    if (IS_iPhoneX){
        [PDHUD showText:text];
    } else {
        [WTStatusBar setBackgroundColor:UUBlack];
        [WTStatusBar setStatusText:text animated:YES];
    }
}

+(void)statusBarHidenWithText:(NSString *)text{
    if (IS_iPhoneX){
        dispatch_main_async_safe(^{
            [PDHUD showText:text];
        });
    } else {
        [WTStatusBar setTextColor:UUWhite];
        [WTStatusBar setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
        [WTStatusBar setStatusText:text timeout:3 animated:YES];
    }
}

+(void)statusBarWithText:(NSString *)text progress:(CGFloat)progress{
    if (progress < 1.0){
        [WTStatusBar setProgressBarColor:UUBlue];
        [WTStatusBar setStatusText:text animated:YES];
        [WTStatusBar setProgress:progress animated:YES];
    } else {
        [WTStatusBar setProgressBarColor:UUBlue];
    }
}


+(void)testStatusBarWithText:(NSString *)text progress:(CGFloat)progress{
    [WTStatusBar setBackgroundColor:UUBlack];
    [WTStatusBar setProgress:progress animated:YES];
}



@end


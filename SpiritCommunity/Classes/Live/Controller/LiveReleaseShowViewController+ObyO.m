//
//  LiveReleaseShowViewController+ObyO.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/8.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController+ObyO.h"
#import "LiveOnebyOneIdentifySingleModel.h"

@implementation LiveReleaseShowViewController (ObyO)


#pragma mark - 1.跨房连麦，进行通知
-(void)firstToLinkHouseToNotify{

}

#pragma mark - 2.客户要求进行连麦
-(void)toLinkHouseWithZhubo:(HomeLiveSingleModel *)transferLiveSingleModel{
    __weak typeof(self)weakSelf = self;
    [self sendRequestToGetOebyOneIdentify:transferLiveSingleModel.userInfo.ID block:^(LiveOnebyOneIdentifySingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        [[TILLiveManager getInstance] linkRoomRequest:singleModel.identify succ:^{
            [StatusBarManager statusBarHidenWithText:@"发起连麦要求已经发送成功"];
            [[NSNotificationCenter defaultCenter] postNotificationName:kLinkRoomBtn_Notification object:nil];
        } failed:^(NSString *module, int errId, NSString *errMsg) {
            [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"发起连麦要求已经发送失败,%@",errMsg]];
        }];
    }];
}

#pragma mark - 获取当前用户的identify
-(void)sendRequestToGetOebyOneIdentify:(NSString *)user block:(void(^)(LiveOnebyOneIdentifySingleModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"searchUser":user};
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_onebyone_getIdentify requestParams:params responseObjectClass:[LiveOnebyOneIdentifySingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }

        if (isSucceeded){
            LiveOnebyOneIdentifySingleModel *singleModel = (LiveOnebyOneIdentifySingleModel *)responseObject;
            if (block){
                block (singleModel);
            }
        }
    }];
}

@end

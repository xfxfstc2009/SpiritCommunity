//
//  OrderDetailSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "OrderDetailPleaseSingleModel.h"
#import "OrderDetailGoodsSIngleModel.h"

@interface OrderDetailSingleModel : FetchModel

@property (nonatomic,assign)CGFloat amount;
@property (nonatomic,copy)NSString *create_time;
@property (nonatomic,copy)NSString *express_time;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *order_no;
@property (nonatomic,assign)NSInteger status;
@property (nonatomic,copy)NSString *status_name;
@property (nonatomic,copy)NSString *type_name;
@property (nonatomic,assign)NSTimeInterval  update_time;
@property (nonatomic,strong)OrderDetailPleaseSingleModel  *receiptinfo;
@property (nonatomic,strong)NSArray<OrderDetailGoodsSIngleModel> *goodlist;


@end

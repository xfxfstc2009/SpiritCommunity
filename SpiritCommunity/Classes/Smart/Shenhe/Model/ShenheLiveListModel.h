//
//  ShenheLiveListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/10.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "ShenheLiveSIngleModel.h"

@interface ShenheLiveListModel : FetchModel

@property (nonatomic,strong)NSArray<ShenheLiveSIngleModel> *list;

@end

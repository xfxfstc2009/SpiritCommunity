//
//  ShareSDKManager.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/6.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>

@interface ShareSDKManager : NSObject

+(instancetype)sharedShareSDKManager;

-(void)registerShareSDK;

-(void)thirdLoginWithType:(NSInteger) type withBlock:(void(^)(BOOL success))block;

-(void)shareManagerWithType:(NSInteger)type title:(NSString *)title desc:(NSString *)desc img:(id)img url:(NSString *)url callBack:(void(^)(BOOL success))callback;
@end

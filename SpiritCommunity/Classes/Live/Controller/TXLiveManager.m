//
//  TXLiveManager.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/27.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "TXLiveManager.h"

@interface TXLiveManager()

@end

@implementation TXLiveManager

+(instancetype)sharedAccountModel{
    static TXLiveManager *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[TXLiveManager alloc] init];
    });
    return _sharedAccountModel;
}






#pragma mark - 推流
+(void)sendRequestToPushFlow{

}


// 2. 设置当前我的信息
+(void)getInfoWithMySelfWithBlcok:(void(^)(BOOL successed, TIMUserProfile *profile))block{
    __weak typeof(self)weakSelf = self;
    [[TIMFriendshipManager sharedInstance] GetSelfProfile:^(TIMUserProfile *profile) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(YES,profile);
        }
    } fail:^(int code, NSString *msg) {
        [StatusBarManager statusBarHidenWithText:msg];
        if (block){
            block(NO,nil);
        }
    }];
}

// 3. 添加滤镜
+(void)addFilterWithType:(TILFilter *)tilFilter type:(LiveUIBottomViewType)type{
    TILFilterType filterType = type - 300 - 1;
    [tilFilter setFilterType:filterType];
}

// 4. 添加特效
+(void)addSpecilWithType:(LiveUIBottomViewType)type{
    if (type == LiveUIBottomViewTypeActionMengyantexiaoNone){
        [[QAVVideoEffectCtrl shareContext] setPendant:nil];
    } else if (type == LiveUIBottomViewTypeActionMengyantexiaoTuzi){            // 兔子
        NSString *path = [[NSBundle bundleForClass:[self class]].resourcePath stringByAppendingPathComponent:@"DecoRes.bundle"];
        NSString * tmplPath = [path stringByAppendingPathComponent:@"video_rabbit"];
        QAVVideoEffectCtrl *effectCtrl = [QAVVideoEffectCtrl shareContext];
        [effectCtrl setPendant:tmplPath];
    } else if (type == LiveUIBottomViewTypeActionMengyantexiaoBaixuegongzhu){       // 白村公主
        NSString *path = [[NSBundle bundleForClass:[self class]].resourcePath stringByAppendingPathComponent:@"DecoRes.bundle"];
        NSString * tmplPath = [path stringByAppendingPathComponent:@"video_snow_white"];
        QAVVideoEffectCtrl *effectCtrl = [QAVVideoEffectCtrl shareContext];
        [effectCtrl setPendant:tmplPath];
    }
}

// 5. 切换相机的方向
+(void)switchCamera{
    [[ILiveRoomManager getInstance] switchCamera:^{
        NSLog(@"switch camera succ");
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        NSString *errInfo = [NSString stringWithFormat:@"switch camera fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        NSLog(@"%@",errInfo);
    }];
}

// 6. 开启闪光
+(void)openLinght:(BOOL)status{
    cameraPos pos = [[ILiveRoomManager getInstance] getCurCameraPos];
    if (pos == CameraPosFront) {
        [[UIAlertView alertViewWithTitle:@"提示" message:@"前置摄像头打开闪光灯会影响直播" buttonTitles:@[@"确定"] callBlock:NULL]show];
        return;
    }
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch]) {                // 判断是否有手电筒
        [device lockForConfiguration:nil];
        if (status == YES){
            [device setTorchMode:AVCaptureTorchModeOn];
            [TXLiveManager sharedAccountModel].lightStatus = YES;
        } else {
            [device setTorchMode:AVCaptureTorchModeOff];
            [TXLiveManager sharedAccountModel].lightStatus = NO;
        }
        [device unlockForConfiguration];
    } else {
        [[UIAlertView alertViewWithTitle:@"提示" message:@"系统检测到您当前没有闪光灯，请重新再试" buttonTitles:@[@"确定"] callBlock:NULL]show];
    }
}

// 7. 美颜
+ (void)openBeauty:(TILFilter *)tilFilter{
    [tilFilter setBeautyLevel:[TXLiveManager sharedAccountModel].transferBeautyValue * 10];
}
// 8.美白
+ (void)openWhite:(TILFilter *)tilFilter{
    [tilFilter setWhitenessLevel:[TXLiveManager sharedAccountModel].transferWhiteValue * 10];
}

// 9. 点赞
+(void)messageWithClickZan:(UIButton *)button block:(void(^)(BOOL success))block{
    ILVLiveCustomMessage *msg = [[ILVLiveCustomMessage alloc] init];
    msg.type = ILVLIVE_IMTYPE_GROUP;
    msg.cmd = (ILVLiveIMCmd)AVIMCMD_Praise;
    msg.recvId = [[ILiveRoomManager getInstance] getIMGroupId];
    
    [[TILLiveManager getInstance] sendCustomMessage:msg succ:^{
        NSMutableDictionary *pointDic = [NSMutableDictionary dictionary];
        [pointDic setObject:[NSNumber numberWithFloat:button.center.x] forKey:@"parise_x"];
        [pointDic setObject:[NSNumber numberWithFloat:button.center.y] forKey:@"parise_y"];
        if (block){
            block(YES);
        }
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        if (block){
            block(NO);
        }
    }];
}

// 10 声音
+(void)closeAndOpenMic{
    BOOL curMic = ![[ILiveRoomManager getInstance] getCurMicState];
    [[ILiveRoomManager getInstance] enableMic:curMic succ:^{
        NSString *info = @"";
        if (curMic == YES){
            info = @"当前状态为打开麦克风";
            [TXLiveManager sharedAccountModel].transferMicStatus = YES;
        } else {
            info = @"当前状态为关闭麦克风";
            [TXLiveManager sharedAccountModel].transferMicStatus = NO;
        }
        
        [StatusBarManager statusBarHidenWithText:info];
    } failed:^(NSString *module, int errId, NSString *errMsg) {
        NSString *errInfo = [NSString stringWithFormat:@"switch camera fail.module=%@,errid=%d,errmsg=%@",module,errId,errMsg];
        [StatusBarManager statusBarHidenWithText:errInfo];
    }];
}




@end

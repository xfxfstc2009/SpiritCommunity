//
//  MessageChatSingleTableViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/5.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatSingleModel.h"

@interface MessageChatSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)ChatSingleModel *transferSingleModel;

+(CGFloat)calculationCellHeight;

@end

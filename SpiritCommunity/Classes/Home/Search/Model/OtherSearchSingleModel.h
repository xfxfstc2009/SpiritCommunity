//
//  OtherSearchSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/2.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol OtherSearchSingleModel <NSObject>

@end

@interface OtherSearchSingleModel : FetchModel

@property (nonatomic,assign)NSInteger age;
@property (nonatomic,copy)NSString *area;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *city;
@property (nonatomic,assign)NSInteger fans_count;           /**< 粉丝数量*/
@property (nonatomic,copy)NSString *ID;                     /**< 用户ID*/
@property (nonatomic,copy)NSString *im_id;                  /**< im的编号*/
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,assign)NSInteger link_count;           /**< 关注人数*/
@property (nonatomic,assign)CGFloat lng;                    /**< 经纬度*/
@property (nonatomic,copy)NSString *location;               /**< 地址*/
@property (nonatomic,copy)NSString *nick;                   /**< 昵称*/
@property (nonatomic,copy)NSString *phone;                  /**< 手机号*/
@property (nonatomic,copy)NSString *province;               /**< 省*/
@property (nonatomic,assign)NSInteger sex;                  /**< 性别*/
@property (nonatomic,copy)NSString *live_img;               /**< 直播img*/
@property (nonatomic,copy)NSString *live_title;               /**< 直播img*/
@property (nonatomic,copy)NSString *desc;                   /**< 详情*/
@property (nonatomic,assign)BOOL link;                      /**< 是否关注*/
@property (nonatomic,copy)NSString *union_number;            /**< 唯一号*/


// 直播用到的
@property (nonatomic,copy)NSString *liveId;         // 播放者的id
@property (nonatomic,copy)NSString *roleName;         // 播放者的id
@end

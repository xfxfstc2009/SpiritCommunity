//
//  HomeRootHotCollectionViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "HomeRootHotCollectionViewCell.h"

@interface HomeRootHotCollectionViewCell()
@property (nonatomic,strong)PDImageView *guanzhuBgView;
@property (nonatomic,strong)PDImageView *alphaImgView;
@property (nonatomic,strong)PDImageView *guanzhuIcon;
@property (nonatomic,strong)UILabel *guanzhuLabel;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *areaLabel;
@property (nonatomic,strong)PDImageView *img;
@end


@implementation HomeRootHotCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. img
    self.img = [[PDImageView alloc]init];
    self.img.backgroundColor = [UIColor clearColor];
    [self addSubview:self.img];
    self.img.image = [UIImage imageNamed:@"cat"];
    self.img.frame = self.bounds;
    self.img.layer.cornerRadius = LCFloat(3);
    self.img.clipsToBounds = YES;

    self.alphaImgView = [[PDImageView alloc]init];
    self.alphaImgView.backgroundColor = [UIColor clearColor];
    self.alphaImgView.frame = self.bounds;
    self.alphaImgView.alpha = .7f;
//    CGRectMake(0, self.size_height / 2., self.size_width, self.size_height / 2.);
    self.alphaImgView.image = [UIImage imageNamed:@"rise_mask2"];
    [self.img addSubview:self.alphaImgView];

    // 2. 创建关注数量
    self.guanzhuBgView = [[PDImageView alloc]init];
    self.guanzhuBgView.backgroundColor = [UIColor clearColor];
    self.guanzhuBgView.image = [Tool stretchImageWithName:@"home_live_guanzhu_icon"];
    [self addSubview:self.guanzhuBgView];
    
    // 3. 关注icon
    self.guanzhuIcon = [[PDImageView alloc] init];
    self.guanzhuIcon.backgroundColor = [UIColor clearColor];
    self.guanzhuIcon.image = [UIImage imageNamed:@"home_onebyone_guanzhu_icon"];
    [self.guanzhuBgView addSubview:self.guanzhuIcon];
    
    self.guanzhuLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"粉"];
    self.guanzhuLabel.font = [self.guanzhuLabel.font boldFont];
    self.guanzhuLabel.adjustsFontSizeToFitWidth = YES;
    [self.guanzhuBgView addSubview:self.guanzhuLabel];

    // 4. name
    self.nickNameLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.nickNameLabel.font = [self.nickNameLabel.font boldFont];
    [self addSubview:self.nickNameLabel];
    
    // 5. 创建地区
    self.areaLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.areaLabel.font = [self.areaLabel.font boldFont];
    [self addSubview:self.areaLabel];
}

-(void)setTransferOnebyoneSingleModel:(HomeLiveSingleModel *)transferOnebyoneSingleModel{
    _transferOnebyoneSingleModel = transferOnebyoneSingleModel;

    // 1. img
    [self.img uploadImageWithURL:transferOnebyoneSingleModel.img placeholder:nil callback:NULL];

    // 2. guanzhu
    self.guanzhuLabel.text = [NSString stringWithFormat:@"%li",transferOnebyoneSingleModel.userInfo.fans_count];
    CGSize guanzhuSize = [NSString makeSizeWithLabel:self.guanzhuLabel];

    CGFloat guanzhuWidth = LCFloat(5) + LCFloat(10) + LCFloat(5) + guanzhuSize.width + LCFloat(5);
    CGFloat guanzhuHeight = 2 * LCFloat(2) + guanzhuSize.height;

    self.guanzhuBgView.frame = CGRectMake(LCFloat(3), LCFloat(3), guanzhuWidth, guanzhuHeight);
    self.guanzhuIcon.frame = CGRectMake(LCFloat(5), 0, LCFloat(10), LCFloat(10));
    self.guanzhuIcon.center_y = self.guanzhuBgView.size_height / 2.;

    self.guanzhuLabel.frame = CGRectMake(CGRectGetMaxX(self.guanzhuIcon.frame) + LCFloat(5), 0, guanzhuSize.width, guanzhuSize.height);
    self.guanzhuLabel.center_y = self.guanzhuIcon.center_y;

    // 2. 昵称
    self.nickNameLabel.text = transferOnebyoneSingleModel.userInfo.nick.length?transferOnebyoneSingleModel.userInfo.nick:[NSString stringWithFormat:@"精灵%@",transferOnebyoneSingleModel.userInfo.ID];
    self.nickNameLabel.frame = CGRectMake(LCFloat(3), CGRectGetMaxY(self.img.frame) - LCFloat(2) - [NSString contentofHeightWithFont:self.nickNameLabel.font], self.size_width * .7f, [NSString contentofHeightWithFont:self.nickNameLabel.font]);

    // 3. 地区
    self.areaLabel.text = transferOnebyoneSingleModel.show_location?transferOnebyoneSingleModel.userInfo.city:@"大约在火星";
    CGSize areaSize = [NSString makeSizeWithLabel:self.areaLabel];
    self.areaLabel.frame = CGRectMake(self.size_width - LCFloat(3) - areaSize.width, 0, areaSize.width, areaSize.height);
    self.areaLabel.center_y = self.nickNameLabel.center_y;
}



@end

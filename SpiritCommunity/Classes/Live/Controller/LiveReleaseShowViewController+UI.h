//
//  LiveReleaseShowViewController+UI.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/5.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseShowViewController.h"

@interface LiveReleaseShowViewController (UI)

#pragma mark - 点赞
- (void)showLikeHeartStartRect:(NSNotification *)noti;

// 分享直播
-(void)shareActionManagerWithUrl:(NSString *)url img:(NSString *)imgUrl;



@end

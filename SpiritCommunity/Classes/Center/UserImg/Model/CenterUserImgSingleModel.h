//
//  CenterUserImgSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/20.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface CenterUserImgSingleModel : FetchModel

@property (nonatomic,strong)NSArray *imgs;


@end

//
//  GWWeatherCurrentModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/11.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "FetchModel.h"
#import "GWWeatherFutureModel.h"

@protocol GWWeatherCurrentModel <NSObject>


@end

@interface GWWeatherCurrentModel : FetchModel

@property (nonatomic,copy)NSString *airCondition;           /**< 空气质量*/
@property (nonatomic,copy)NSString *city;                   /**< 所在城市*/
@property (nonatomic,copy)NSString *date;                   /**< 时间*/
@property (nonatomic,assign)NSTimeInterval dateWithInterval;/**< 时间戳*/
@property (nonatomic,copy)NSString *distrct;                /**< 区县*/

@property (nonatomic,copy)NSString *humidity;               /**< 湿度*/
@property (nonatomic,copy)NSString *pollutionIndex;         /**< 空气质量指数*/
@property (nonatomic,copy)NSString *coldIndex;              /**< 感冒指数*/
@property (nonatomic,copy)NSString *dressingIndex;          /**< 穿衣指数*/
@property (nonatomic,copy)NSString *exerciseIndex;          /**< 运动指数*/
@property (nonatomic,copy)NSString *washIndex;              /**< 洗衣指数*/
@property (nonatomic,copy)NSString *province;               /**< 省份*/
@property (nonatomic,copy)NSString *sunrise;                /**< 日出时间*/
@property (nonatomic,copy)NSString *sunset;                 /**< 日落时间*/
@property (nonatomic,copy)NSString *temperature;            /**< 温度*/
@property (nonatomic,copy)NSString *time;                   /**< 检测时间*/
@property (nonatomic,copy)NSString *updateTime;             /**< 更新时间*/

@property (nonatomic,copy)NSString *weather;                /**< 天气*/
@property (nonatomic,copy)NSString *week;                   /**< 星期*/
@property (nonatomic,copy)NSString *wind;                   /**< 风向*/
@property (nonatomic,strong)NSArray<GWWeatherFutureModel> *future;/**< 未来*/

@end

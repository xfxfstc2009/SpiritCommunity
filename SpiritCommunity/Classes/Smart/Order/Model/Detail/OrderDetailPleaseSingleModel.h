//
//  OrderDetailPleaseSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 Gigantic. All rights reserved.
//
// 收货地址
#import "FetchModel.h"

@interface OrderDetailPleaseSingleModel : FetchModel

@property(nonatomic,copy)NSString*address;
@property(nonatomic,copy)NSString *create_time;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *isdefault;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,copy)NSString *username;

@end

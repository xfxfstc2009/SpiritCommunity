//
//  HomeIntegralSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "HomeIntegralSingleModel.h"

@implementation HomeIntegralSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end

//
//  PDInputViewCell.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/25.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDBaseTableViewCell.h"

typedef NS_ENUM(NSInteger,PDInputViewCellType) {
    PDInputViewCellTypeNone = 0,                    /**< 默认*/
    PDInputViewCellTypePingjia = 1,                 /**< 评价*/
};

@interface PDInputViewCell : PDBaseTableViewCell

@property (nonatomic,strong)UITextView *inputView;
@property (nonatomic,copy)NSString *transferPlaceHolder;
@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,assign)NSInteger limitCount;



@property (nonatomic,assign)PDInputViewCellType transferType;

-(void)inputTextViewDidEndEditingBlock:(void(^)())block;

-(void)inputTextViewDidChangeBlock:(void(^)(NSString *info))block;

@end

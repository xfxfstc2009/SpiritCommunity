//
//  ShareSDKManager.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/6.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "ShareSDKManager.h"

@implementation ShareSDKManager

+(instancetype)sharedShareSDKManager{
    static ShareSDKManager *_sharedShareSDKManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedShareSDKManager = [[ShareSDKManager alloc] init];
    });
    return _sharedShareSDKManager;
}

-(void)registerShareSDK{
    [ShareSDK registerActivePlatforms:@[@(SSDKPlatformTypeWechat),@(SSDKPlatformTypeQQ)] onImport:^(SSDKPlatformType platformType) {
        if (platformType == SSDKPlatformTypeWechat){
            [ShareSDKConnector connectWeChat:[WXApi class] delegate:self];
        } else {
            [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
        }
    } onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
        if (platformType == SSDKPlatformTypeWechat){
            [appInfo SSDKSetupWeChatByAppId:WeChatAppID appSecret:WeChatAppSecret];
        } else {
            [appInfo SSDKSetupQQByAppId:QQAppID appKey:QQAppAppSecret authType:SSDKAuthTypeBoth];
        }
    }];
}

-(void)thirdLoginWithType:(NSInteger) type withBlock:(void(^)(BOOL success))block{
    __weak typeof(self)weakSelf = self;
    [ShareSDK authorize:type settings:nil onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if(state == SSDKResponseStateSuccess){
             PDThirdLoginModel *thirdLoginModel = [[PDThirdLoginModel alloc]init];
            if (type == SSDKPlatformTypeWechat){
                thirdLoginModel.city = [user.rawData objectForKey:@"city"];
                thirdLoginModel.country = [user.rawData objectForKey:@"country"];
                thirdLoginModel.headimgurl = [user.rawData objectForKey:@"headimgurl"];
                thirdLoginModel.language = [user.rawData objectForKey:@"language"];
                thirdLoginModel.nickname = [user.rawData objectForKey:@"nickname"];
                thirdLoginModel.openid = [user.rawData objectForKey:@"openid"];
                thirdLoginModel.province = [user.rawData objectForKey:@"province"];
                thirdLoginModel.sex = [[user.rawData objectForKey:@"sex"] integerValue] == 1?YES:NO;
                thirdLoginModel.unionid = [user.rawData objectForKey:@"unionid"];
                
                [AccountModel sharedAccountModel].thirdLoginType = PDThirdLoginTypeWechat;              // 当前登录的状态
            } else if (type == SSDKPlatformTypeQQ){
                [AccountModel sharedAccountModel].thirdLoginType = PDThirdLoginTypeQQ;              // 当前登录的状态
                thirdLoginModel.headimgurl = user.icon;
                thirdLoginModel.nickname = user.nickname;
                thirdLoginModel.openid = user.uid;
                thirdLoginModel.unionid = user.uid;
            }
            [AccountModel sharedAccountModel].thirdUserModel = thirdLoginModel;                     // 保存三方登录后的信息
            if (block){
                block(YES);
            }
     
        } else if (state == SSDKResponseStateFail){
            if (type == SSDKPlatformTypeQQ){
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"QQ登录失败,失败原因%@",error.localizedDescription]];
            } else if (type == SSDKPlatformTypeWechat){
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"微信登录失败,失败原因%@",error.localizedDescription]];
            }
            if (block){
                block(NO);
            }
        } else if (state == SSDKResponseStateCancel){
            if (type == SSDKPlatformTypeQQ){
                [StatusBarManager statusBarHidenWithText:@"取消QQ登录"];
            } else if (type == SSDKPlatformTypeWechat){
                [StatusBarManager statusBarHidenWithText:@"取消微信登录"];
            }
        }
    }];
}

-(void)shareManagerWithType:(NSInteger)type title:(NSString *)title desc:(NSString *)desc img:(id)img url:(NSString *)url callBack:(void(^)(BOOL success))callback{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSURL *mainUrl = [NSURL URLWithString:url];
    
    [params SSDKSetupShareParamsByText:desc images:img url:mainUrl title:title type:SSDKContentTypeAuto];

    [ShareSDK share:type parameters:params onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        if (state == SSDKResponseStateSuccess){
            [StatusBarManager statusBarHidenWithText:@"分享成功"];
        } else if (state == SSDKResponseStateFail){
            [StatusBarManager statusBarHidenWithText:@"分享失败"];
        }
    }];
}


@end

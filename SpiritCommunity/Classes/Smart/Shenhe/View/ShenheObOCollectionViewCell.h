//
//  ShenheObOCollectionViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/10.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShenheLiveSIngleModel.h"

@interface ShenheObOCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)ShenheLiveSIngleModel *transferSingleModel;

@end

//
//  CenterUserImgsAddViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/20.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "CenterUserImgsAddViewController.h"
#import "ZSImageSelecetdTableViewCell.h"


static char actionClickBlockKey;
@interface CenterUserImgsAddViewController ()<UITableViewDataSource,UITableViewDelegate,ZSImageSelectedDelegate>
@property (nonatomic,strong)UITableView *addTableView;
@property (nonatomic,strong)NSArray *addArr;
@property (nonatomic,strong)NSMutableArray *selectedMutableArr;
@end

@implementation CenterUserImgsAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加图片";
    __weak typeof(self)weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_cancel"] barHltImage:[UIImage imageNamed:@"icon_main_cancel"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.addArr = @[@[@"图片"],@[@"添加按钮"]];
    self.selectedMutableArr = [NSMutableArray array];
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.addTableView){
        self.addTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.addTableView.dataSource = self;
        self.addTableView.delegate = self;
        [self.view addSubview:self.addTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.addArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.addArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        ZSImageSelecetdTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[ZSImageSelecetdTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.isSelectedImageArr = self.selectedMutableArr;
        cellWithRowOne.delegate = self;
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"确认添加";
        [cellWithRowTwo setButtonStatus:YES];
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToAddUserImgs];
        }];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [ZSImageSelecetdTableViewCell calculationCellHeightWithImgArr:self.selectedMutableArr];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
       
    }
}


-(void)imageSelectedButtonClick{
    GWAssetsLibraryViewController *assetViewController = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [assetViewController selectImageArrayFromImagePickerWithMaxSelected:self.selectedMutableArr.count - 9 andBlock:^(NSArray *selectedImgArr) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.selectedMutableArr addObjectsFromArray:selectedImgArr];
        [strongSelf.addTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }];
    [self.navigationController pushViewController:assetViewController animated:YES];
}

-(void)imageSelectedDeleteClick:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(ZSImageSelecetdTableViewCell *)cell{
    [[UIActionSheet actionSheetWithTitle:@"选择图片" buttonTitles:@[@"取消",@"删除图片"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0){  // 查看大图
            [self.selectedMutableArr removeObjectAtIndex:imgIndex];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection: 0];
            [self.addTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else if (buttonIndex == 1){                // 删除图片

        } else {                // 取消
            
        }
    }]showInView:self.view];
}


#pragma mark - 添加图片
-(void)sendRequestToAddUserImgs{
    if (self.selectedMutableArr.count){
        void(^block)(NSArray *imgArr) = objc_getAssociatedObject(self, &actionClickBlockKey);
        if (block){
            block(self.selectedMutableArr);
        }
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];

}

-(void)actionClickBlock:(void(^)(NSArray *imgArr))block{
    objc_setAssociatedObject(self, &actionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end

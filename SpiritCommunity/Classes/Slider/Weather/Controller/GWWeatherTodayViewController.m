//
//  GWWeatherTodayViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/8.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "GWWeatherTodayViewController.h"
#import "GWWeatherFeatureTableViewCell.h"
#import "GWWeatherTodayOneCell.h"

@interface GWWeatherTodayViewController()<UITableViewDataSource,UITableViewDelegate>{

}
@property (nonatomic,strong)NSArray *todayArr;

@end

@implementation GWWeatherTodayViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithinit];
    [self createMainTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"今日天气";
    self.view.backgroundColor = [UIColor redColor];
}

#pragma mark - arrayWithInit
-(void)arrayWithinit{
    self.todayArr = @[@"空格",@"今日天气"];
}

-(void)createMainTableView{
    if (!self.todayWeatherTableView){
        self.todayWeatherTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.todayWeatherTableView.delegate = self;
        self.todayWeatherTableView.dataSource = self;
        self.todayWeatherTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.todayWeatherTableView.showsVerticalScrollIndicator = NO;
        self.todayWeatherTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.todayWeatherTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.todayWeatherTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.todayArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.backgroundColor = [UIColor clearColor];
        return cellWithRowOne;
    } else if (indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWWeatherTodayOneCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWWeatherTodayOneCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.backgroundColor = [UIColor clearColor];
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferCurrentModel = self.weatherCurrentModel;
        return cellWithRowTwo;
    } else {
        static NSString *cellIdentifyWithRowTow = @"cellIdengifyWithRowTow";
        GWWeatherFeatureTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTow];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWWeatherFeatureTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTow];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.cellheight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return kScreenBounds.size.height - (2 * LCFloat(30) + 30) + 20 - [GWWeatherTodayOneCell calculationCellheight:self.weatherCurrentModel];
    } else {
        return [GWWeatherTodayOneCell calculationCellheight:self.weatherCurrentModel];
    }
}




@end

//
//  LiveMessageView.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveMessageView : UIView

@property (nonatomic,strong)NSMutableArray *messageMutableArr;
//
-(void)viewSendMsg:(ILVLiveMessage *)msg;

@end

//
//  MainCenterTopViewController.m
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2018/1/15.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "MainCenterTopViewController.h"
#import "CenterRootTopHeaderView.h"                     // 1. 顶部的视图

#import "CenterUserImgsCollectionViewCell.h"
#import "CenterUserImgAddCollectionViewCell.h"
#import "ImgSelectorViewController.h"
#import "CenterUserImgsAddViewController.h"
#import "CenterUserImgSingleModel.h"
// view
#import "TNSexyImageUploadProgress.h"                       //

#import "CenterMoneyViewController.h"                       // 【我的金币】
#import "CenterEditingViewController.h"                     // 【修改用户信息】
#import "CenterFansViewController.h"                        // 【我的粉丝】


@interface MainCenterTopViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)CenterRootTopHeaderView *headerView;
@property (nonatomic,strong)UIView *headerBottomView;

@property (nonatomic,strong)NSMutableArray *imgsMutableArr;
@property (nonatomic,strong)NSMutableArray *imgsMainArr;
@property (nonatomic,strong)UICollectionView *imgsCollectionView;

@end

@implementation MainCenterTopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];                 // 页面初始化
    [self arrayWithInit];
    [self createSwipeGestore];           // 创建手势
    [self createCollectionView];
    // placeholder
    
}

-(void)viewDidAppear:(BOOL)animated{
    if (self.imgsCollectionView){
        self.imgsCollectionView.frame = CGRectMake(0, CGRectGetMaxY(self.headerView.frame), kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.headerView.frame));
    }
    
    [self loginReloadManager];
}



#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = [UIColor clearColor];
    self.headerView = [[CenterRootTopHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [CenterRootTopHeaderView calculationCellHeight])];
    self.headerView.transferAccountModel = [AccountModel sharedAccountModel];
    self.headerView.backgroundColor = [UIColor clearColor];

    // 头像进行点击
    __weak typeof(self)weakSelf = self;
    [self.headerView actionClickWithHeader:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (![AccountModel sharedAccountModel].loginModel.ID.length){                // 未登录登录
            [strongSelf loginManager];
        } else {
            CenterEditingViewController *editViewController = [[CenterEditingViewController alloc]init];
            [strongSelf.transferMenuController.navigationController pushViewController:editViewController animated:YES];
        }
    }];

    // 2. 点击用户信息
    [self.headerView actionClickBlock:^(CenterCustomerSingleModel *recordSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf authorizeWithCompletionHandler:^(BOOL successed) {
            if ([recordSingleModel.fixedStr isEqualToString:@"粉丝"]|| [recordSingleModel.fixedStr isEqualToString:@"关注/粉丝"]){
                CenterFansViewController *fansViewController = [[CenterFansViewController alloc]init];
                fansViewController.transferType = CenterFansViewControllerTypeFans;
                [strongSelf.transferMenuController.navigationController pushViewController:fansViewController animated:YES];
            } else if ([recordSingleModel.fixedStr isEqualToString:@"关注中"] || [recordSingleModel.fixedStr isEqualToString:@"关注"]){
                CenterFansViewController *linkViewController = [[CenterFansViewController alloc]init];
                linkViewController.transferType = CenterFansViewControllerTypeLink;
                [strongSelf.transferMenuController.navigationController pushViewController:linkViewController animated:YES];
            } else if ([recordSingleModel.fixedStr isEqualToString:@"我的金币"]){
                CenterMoneyViewController *propertyViewController = [[CenterMoneyViewController alloc]init];
                [strongSelf.transferMenuController.navigationController pushViewController:propertyViewController animated:YES];
            }
        }];
    }];

    [self.view addSubview:self.headerView];

    // 2. 创建底部的view
    self.headerBottomView = [[UIView alloc]init];
    self.headerBottomView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.headerBottomView.frame = CGRectMake(0, CGRectGetMaxY(self.headerView.frame), kScreenBounds.size.width, self.view.size_height - self.headerView.size_height);
    [self.view addSubview:self.headerBottomView];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    if (!self.imgsMutableArr){
        self.imgsMutableArr = [NSMutableArray array];
        if ([[AccountModel sharedAccountModel] hasLoggedIn]){
            [self.imgsMutableArr addObject:@"添加图片"];
        }
    } else {
        [self.imgsMutableArr removeAllObjects];
    }
    if (!self.imgsMainArr){
        self.imgsMainArr = [NSMutableArray array];
    } else {
        [self.imgsMainArr removeAllObjects];
    }
}

#pragma mark - 用户图片
-(void)createCollectionView{
    if (!self.imgsCollectionView){
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        self.imgsCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.imgsCollectionView.dataSource = self;
        self.imgsCollectionView.delegate = self;
        self.imgsCollectionView.showsVerticalScrollIndicator = YES;
        self.imgsCollectionView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.imgsCollectionView];
        [self.imgsCollectionView registerClass:[CenterUserImgsCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentify"];
        [self.imgsCollectionView registerClass:[CenterUserImgAddCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentify1"];
    }
    __weak typeof(self)weakSelf = self;
    [self.imgsCollectionView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.transferMenuController openAndCloseMenu];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [strongSelf.imgsCollectionView stopPullToRefresh];
        });
    }];

    [self.imgsCollectionView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfo];
    }];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imgsMutableArr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        CenterUserImgAddCollectionViewCell *cellWithRowOne = (CenterUserImgAddCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentify1" forIndexPath:indexPath];

        return cellWithRowOne;
    } else {
        CenterUserImgsCollectionViewCell *cell = (CenterUserImgsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentify"  forIndexPath:indexPath];
        cell.transferCellImg = [self.imgsMutableArr objectAtIndex:indexPath.row];
        __weak typeof(self)weakSelf = self;
        [cell actionTapGestureBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            ImgSelectorViewController *imgSelected = [[ImgSelectorViewController alloc]init];
            CenterUserImgsCollectionViewCell * cell = (CenterUserImgsCollectionViewCell *)[strongSelf.imgsCollectionView cellForItemAtIndexPath:indexPath];
            [imgSelected showInView:strongSelf.parentViewController imgArr:strongSelf.imgsMutableArr currentIndex:indexPath.row cell:cell];
        }];
        [cell actionLongGestureBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf showAcctionSheet:[strongSelf.imgsMutableArr objectAtIndex:indexPath.row]];
        }];
        return cell;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat assetMargin = LCFloat(10);
    return CGSizeMake((kScreenBounds.size.width - 4 * assetMargin) / 3., (kScreenBounds.size.width - 4 * assetMargin) / 3.);
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,LCFloat(7),0,LCFloat(7));
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,LCFloat(20)};
    return size;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        __weak typeof(self)weakSelf = self;

        [self authorizeWithCompletionHandler:^(BOOL successed) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf addImgManager];
        }];
    }
}

-(void)addImgManager{
    CenterUserImgsAddViewController *addImgViewController = [[CenterUserImgsAddViewController alloc]init];

    __weak typeof(self)weakSelf = self;
    [addImgViewController actionClickBlock:^(NSArray *imgArr1) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[AliOSSManager sharedOssManager] uploadFileWithImgArray:imgArr1 successBlock:^(BOOL isSuccessed, NSArray *imgArr) {
            [strongSelf.imgsMutableArr addObjectsFromArray:imgArr];
            [strongSelf.imgsMainArr addObjectsFromArray:imgArr];
            [strongSelf.imgsCollectionView reloadData];
        }];
    }];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:addImgViewController];
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
}

#pragma mark - 获取图片信息
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page":@(self.imgsCollectionView.currentPage),@"size":@"20"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:assetList requestParams:params responseObjectClass:[CenterUserImgSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            CenterUserImgSingleModel *singleModel = (CenterUserImgSingleModel *)responseObject;
            if (strongSelf.imgsCollectionView.isXiaLa){
                [strongSelf.imgsMutableArr removeAllObjects];
                [strongSelf.imgsMainArr removeAllObjects];
                [strongSelf.imgsMutableArr addObject:@"添加图片"];
            }
            [strongSelf.imgsMainArr addObjectsFromArray:singleModel.imgs];
            [strongSelf.imgsMutableArr addObjectsFromArray:singleModel.imgs];
            [strongSelf.imgsCollectionView reloadData];
            [strongSelf.imgsCollectionView stopFinishScrollingRefresh];
            [strongSelf.imgsCollectionView stopPullToRefresh];
        }
    }];
}


#pragma mark - 添加手势
-(void)createSwipeGestore{
    [self.transferMenuController activateSwipeToOpenMenu:NO];
}

#pragma mark - 登录动画
-(void)loginSuccessManager{
    if (!self.headerImageView){
        self.headerImageView = [[PDImageView alloc]init];
        self.headerImageView.frame = CGRectMake(0, 0, LCFloat(150), LCFloat(150));
    }
    
    [self.headerImageView uploadImageWithAvatarURL:[AccountModel sharedAccountModel].loginModel.avatar placeholder:nil callback:^(UIImage *image) {
        if (!self.imageUploadProgress){
            self.imageUploadProgress = [[TNSexyImageUploadProgress alloc] init];
        }
        self.imageUploadProgress.radius = LCFloat(100);
        self.imageUploadProgress.progressBorderThickness = -10;
        self.imageUploadProgress.trackColor = [UIColor blackColor];
        self.imageUploadProgress.progressColor = [UIColor clearColor];
        self.imageUploadProgress.imageToUpload = image;
        [self.imageUploadProgress show];
        [self performSelector:@selector(showSliderWithAnimation) withObject:nil afterDelay:.8f];
    }];

}

#pragma mark - 显示
-(void)showSliderWithAnimation{
    [self performSelector:@selector(loginSuccessAnimationWithProView:) withObject:self.imageUploadProgress afterDelay:.3f];
}

#pragma mark - Animation Login
-(void)loginSuccessAnimationWithProView:(TNSexyImageUploadProgress *)progressView{
    // 2. 寻找到view 的位置
    CGRect convertFrame = [self.headerView convertRect:self.headerView.avatarBgView.frame toView:self.view.window];
    progressView.imageView.backgroundColor = [UIColor clearColor];

    // 3. 创建一个副本图像
    UIImageView *animationImageView = [[UIImageView alloc]init];
    animationImageView.image = progressView.imageView.image;
    animationImageView.frame = progressView.imageView.frame;
    [self.view.window addSubview:animationImageView];

    [progressView outroAnimation];
    [UIView animateWithDuration:.9  delay:.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        animationImageView.frame = convertFrame;
    } completion:^(BOOL finished) {
        animationImageView.hidden = YES;
        [animationImageView removeFromSuperview];
        [self.headerView loginAnimationActionManager];
        [self loginReloadManager];
    }];
}

#pragma mark - 跳转到直播去直播
-(void)directToLive{
    [LiveRootManager liveToReleaseManager:self];
}

-(void)setHasEnable:(BOOL)hasEnable{
    if (hasEnable){
        self.imgsCollectionView.scrollEnabled = YES;
    } else {
        self.imgsCollectionView.scrollEnabled = NO;
    }
}

-(void)loginManager{
    LoginViewController *loginViewController = [[LoginViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [loginViewController loginSuccess:^(BOOL success) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (success){
            PDSliderRootViewController *sliderRootViewController = (PDSliderRootViewController *)[RESideMenu shareInstance].leftMenuViewController;
            [sliderRootViewController logoutSuccess];
            [strongSelf loginSuccessManager];
        }
    }];
    UINavigationController *loginNav = [[UINavigationController alloc]initWithRootViewController:loginViewController];
    [[PDMainTabbarViewController sharedController].centerViewController presentViewController:loginNav animated:YES completion:NULL];
}

#pragma mark - 登录刷新
-(void)loginReloadManager{
    if(![[AccountModel sharedAccountModel] hasLoggedIn]){        // 表示没登录
        __weak typeof(self)weakSelf = self;
        [self.imgsCollectionView showPrompt:@"请先登录哦~" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf loginManager];
        }];
        [self loginReloadManagerWithUserInfo];
        [self arrayWithInit];
        [self.imgsCollectionView reloadData];
    } else {
        [self loginReloadManagerWithUserInfo];
        self.imgsCollectionView.isXiaLa = YES;
        [self sendRequestToGetInfo];
        [self.imgsCollectionView dismissPrompt];
    }
}

-(void)loginReloadManagerWithUserInfo{
     self.headerView.transferAccountModel = [AccountModel sharedAccountModel];
}

#pragma mark - actionSheet
-(void)showAcctionSheet:(NSString *)imgUrl{
    __weak typeof(self)weakSelf = self;
    [[UIActionSheet actionSheetWithTitle:@"图片操作" buttonTitles:@[@"取消",@"删除"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){
            [strongSelf sendRequestToDelete:imgUrl];
        } else {

        }
    }]showInView:self.view];
}

-(void)sendRequestToDelete:(NSString *)imgUrl{
    NSDictionary *params = @{@"imgUrl":imgUrl};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:center_deleteImg requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.imgsMutableArr removeObject:imgUrl];
            [strongSelf.imgsMainArr removeObject:imgUrl];
            
            [strongSelf.imgsCollectionView reloadData];
        }
    }];
}

-(void)updateUserAccountInfoManager{
    [self.headerView loginAnimationActionManager];
}

@end

//
//  HomeNormalListGoodsModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "HomeNormalSingleGoodsModel.h"

@interface HomeNormalListGoodsModel : FetchModel

@property (nonatomic,strong)NSArray<HomeNormalSingleGoodsModel> *merchantLists;
@property (nonatomic,strong)NSArray<HomeNormalSingleGoodsModel> *bestGoods;
@property (nonatomic,strong)NSArray<HomeNormalSingleGoodsModel> *adbanner;

@end

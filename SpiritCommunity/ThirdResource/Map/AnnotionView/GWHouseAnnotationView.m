//
//  GWHouseAnnotationView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWHouseAnnotationView.h"
#import "PDCustomCalloutView.h"
#import <objc/runtime.h>

#define kWidth  60.f
#define kHeight 60.f

#define kHoriMargin 5.f
#define kVertMargin 5.f

#define kPortraitWidth  50.f
#define kPortraitHeight 50.f

#define kCalloutWidth   250.0
#define kCalloutHeight  70.0


static  char locationKey;
@interface GWHouseAnnotationView()
@property (nonatomic, strong) UIImageView *portraitImageView;
@property (nonatomic, strong) UILabel *nameLabel;

@end

@implementation GWHouseAnnotationView
@synthesize calloutView;
@synthesize portraitImageView   = _portraitImageView;
@synthesize nameLabel           = _nameLabel;

#pragma mark - Handle Action

- (void)btnAction
{
    CLLocationCoordinate2D coorinate = [self.annotation coordinate];
    
    NSLog(@"coordinate = {%f, %f}", coorinate.latitude, coorinate.longitude);
}

#pragma mark - Override

- (NSString *)name
{
    return self.nameLabel.text;
}

- (void)setName:(NSString *)name
{
    self.nameLabel.text = name;
}

- (UIImage *)portrait
{
    return self.portraitImageView.image;
}

- (void)setPortrait:(UIImage *)portrait
{
    self.portraitImageView.image = portrait;
}

- (void)setSelected:(BOOL)selected
{
    [self setSelected:selected animated:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (self.selected == selected)
    {
        return;
    }
    
    if (selected)
    {
        if (self.calloutView == nil)
        {
            /* Construct custom callout. */
            self.calloutView = [[PDCustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
            self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x,
                                                  -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
            self.calloutView.clipsToBounds = YES;
            
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.text = self.transferSearchSingleModel.nick;
            fixedLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"]boldFont];
            CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:fixedLabel.font])];
            fixedLabel.frame = CGRectMake(LCFloat(12), LCFloat(15), fixedSize.width, [NSString contentofHeightWithFont:fixedLabel.font]);
            [self.calloutView addSubview:fixedLabel];
            
            UILabel *dymicLabel = [[UILabel alloc]init];
            dymicLabel.text = self.transferSearchSingleModel.city;
            dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            dymicLabel.numberOfLines = 0;
            CGSize dymicSize = [dymicLabel.text sizeWithCalcFont:dymicLabel.font constrainedToSize:CGSizeMake(kCalloutWidth - 50 - 2 * LCFloat(12), CGFLOAT_MAX)];
            dymicLabel.frame = CGRectMake(fixedLabel.orgin_x, CGRectGetMaxY(fixedLabel.frame) + LCFloat(10), kCalloutWidth - 50 - 2 * LCFloat(12), dymicSize.height);
            dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            [self.calloutView addSubview:dymicLabel];
            
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btn.frame = CGRectMake(self.calloutView.size_width - 50, 0,50, self.calloutView.size_height - kArrorHeight);
            [btn setTitle:@"导航" forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor colorWithCustomerName:@"绿"]];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            __weak typeof(self)weakSelf = self;
            [btn buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)() = objc_getAssociatedObject(strongSelf, &locationKey);
                if (block){
                    block();
                }
            }];
            [self.calloutView addSubview:btn];
            
            self.calloutView.size_height = LCFloat(15) + [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"正文"] boldFont]] + LCFloat(10) + dymicSize.height + LCFloat(15) + kArrorHeight;
            btn.frame = CGRectMake(self.calloutView.size_width - 50, 0,50, self.calloutView.size_height - kArrorHeight);
            btn.layer.mask = [self setButtoncornerRadius:UIRectCornerBottomRight | UIRectCornerTopRight Btn:btn cornerRadius:kradus];

        }
        
        [self addSubview:self.calloutView];
    }
    else
    {
        [self.calloutView removeFromSuperview];
    }
    
    [super setSelected:selected animated:animated];
}

-(void)locationShow:(void(^)())block{
    objc_setAssociatedObject(self, &locationKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL inside = [super pointInside:point withEvent:event];
    /* Points that lie outside the receiver’s bounds are never reported as hits,
     even if they actually lie within one of the receiver’s subviews.
     This can occur if the current view’s clipsToBounds property is set to NO and the affected subview extends beyond the view’s bounds.
     */
    if (!inside && self.selected)
    {
        inside = [self.calloutView pointInside:[self convertPoint:point toView:self.calloutView] withEvent:event];
    }
    
    return inside;
}

#pragma mark - Life Cycle

- (id)initWithAnnotation:(id<MAAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.bounds = CGRectMake(0, 0, kWidth, kHeight);
        self.backgroundColor = [UIColor clearColor];
        
//        GWImageView *iconImageView = [[GWImageView alloc]init];
//        iconImageView.image = [UIImage imageNamed:@"center_icon_location"];
//        iconImageView.frame = CGRectMake((kWidth - 18 ) / 2., (kHeight - 22) / 2., 18, 22);
//        [self addSubview:iconImageView];
    
    }
    
    return self;
}



-(CAShapeLayer*)setButtoncornerRadius:(UIRectCorner)corners  Btn:(UIButton*)btn  cornerRadius:(float)cornerRadius {
    [btn.layer setMasksToBounds:YES];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:btn.bounds byRoundingCorners: corners cornerRadii: (CGSize){cornerRadius, cornerRadius}].CGPath;
    
    
    return maskLayer;
}


@end

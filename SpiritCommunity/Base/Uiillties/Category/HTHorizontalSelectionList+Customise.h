//
//  HTHorizontalSelectionList+Customise.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/3.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <HTHorizontalSelectionList/HTHorizontalSelectionList.h>
#import "PDSelectionListTitleView.h"

@interface HTHorizontalSelectionList (Customise)<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate>

@property (nonatomic,strong)NSArray *dataSourceArr;
@property (nonatomic,copy)NSString *bridageNumber;
@property (nonatomic,assign)NSInteger bridageIndex;


+(PDSelectionListTitleView *)createSegmentWithDataSource:(NSArray *)dataSource actionClickBlock:(void(^)(HTHorizontalSelectionList *segment, NSInteger index))block;

-(void)cleanSegmentBridge;

@end

//
//  ProductDetailSub2TableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2018/2/9.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "ProductDetailSub2TableViewCell.h"

static char actionBlockHeightKey;
@interface ProductDetailSub2TableViewCell()
@property (nonatomic,strong)PDImageView *productImgView;
@end

@implementation ProductDetailSub2TableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    self.productImgView = [[PDImageView alloc]init];
    [self addSubview:self.productImgView];
}

-(void)setTransferImg:(NSString *)transferImg{
    _transferImg = transferImg;
    __weak typeof(self)weakSelf = self;
    if (!self.productImgView.image){
        self.productImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 100);
        [self.productImgView uploadImageWithURL:transferImg placeholder:nil callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            CGFloat height = image.size.height / (image.size.width / kScreenBounds.size.width);
            strongSelf.cellHeight = height;
            strongSelf.productImgView.size_height = height;
            void(^block)(CGFloat imgHeight) = objc_getAssociatedObject(strongSelf, &actionBlockHeightKey);
            if (block){
                block(height);
            }
        }];
    } else {
        self.productImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.cellHeight);
    }
}

-(void)actionBlockHeight:(void(^)(CGFloat imgHeihgt))block{
    objc_setAssociatedObject(self, &actionBlockHeightKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



@end

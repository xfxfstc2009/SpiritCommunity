//
//  FindRootListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/22.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol FindRootSingleModel <NSObject>

@end

@interface FindRootSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *image;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *desc;
@property (nonatomic,assign)NSTimeInterval datetime;
@property (nonatomic,copy)NSString *offdown;
@property (nonatomic,copy)NSString *direct;

// temp
@property (nonatomic,assign)BOOL hasAnimation;

@end


@interface FindRootListModel : FetchModel

@property (nonatomic,strong)NSArray<FindRootSingleModel> *list;

@end

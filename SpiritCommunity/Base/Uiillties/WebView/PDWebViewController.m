//
//  PDWebViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWebViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>

@interface PDWebViewController()<PDWebViewDelegate>
@end

@implementation PDWebViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createWebView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor clearColor].CGColor;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.f, 0.f);
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)createWebView{
    if (!self.webView){
        self.webView = [[PDWebView alloc]initWithFrame:self.view.bounds];
        self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.webView.delegate = self;
        [self.view addSubview:self.webView];
    }
}

-(void)webDirectedWebUrl:(NSString *)url{
    if (!self.webView){
        self.webView = [[PDWebView alloc]initWithFrame:self.view.bounds];
        self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.webView.delegate = self;
        [self.view addSubview:self.webView];
    }
    
    if ([url hasPrefix:@"/po"]){
        NSString *address = [Tool userDefaultGetWithKey:testNet_address].length?[Tool userDefaultGetWithKey:testNet_address]:JAVA_Host;
        NSString *port = [Tool userDefaultGetWithKey:testNet_port].length?[Tool userDefaultGetWithKey:testNet_port]:JAVA_Port;
        
        if (port.length){
            [self.webView loadURLString:[NSString stringWithFormat:@"http://%@:%@%@",address,port,url]];
        } else {
            [self.webView loadURLString:[NSString stringWithFormat:@"http://%@%@",address,url]];
        }
    } else {
        [self.webView loadURLString:url];
    }
}


- (void)webViewDidStartLoad:(PDWebView *)webview {
    PDLog(@"页面开始加载");
}

- (void)webView:(PDWebView *)webview shouldStartLoadWithURL:(NSURL *)URL {
    PDLog(@"截取到URL：%@",URL);
}

- (void)webView:(PDWebView *)webview didFinishLoadingURL:(NSURL *)URL { // 加载完成
    NSString *title = @"";
    if (webview.uiWebView){
        title = [webview.uiWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
    } else {
        title = webview.wkWebView.title;
    }
    if (!self.barMainTitle.length){
        self.barMainTitle = title;
    }
}

- (void)webView:(PDWebView *)webview didFailToLoadURL:(NSURL *)URL error:(NSError *)error {
    PDLog(@"加载出现错误");
}


@end

//precision highp float;
//varying vec2 canvasCoordinate;
//varying vec2 textureCoordinate;
//uniform sampler2D inputImageTexture;
//uniform sampler2D inputImageTexture2;
//
//void main(void)
//{
//    vec4 texColor = texture2D(inputImageTexture2, textureCoordinate);
//    vec4 canvasColor = texture2D(inputImageTexture, canvasCoordinate);
//    vec3 resultColor = mix(canvasColor.rgb, texColor.rgb, texColor.a);
//    gl_FragColor = vec4(resultColor, 1.0);
//}



//alpha blend, hodxiang modify..
precision highp float;
varying vec2 textureCoordinate;
uniform sampler2D inputImageTexture;
uniform int  alphaType;

void main(void)
{
    vec4 texColor = texture2D(inputImageTexture, textureCoordinate);
    if (alphaType == 1 && texColor.a > 0.0) {
        texColor = texColor / vec4(texColor.a, texColor.a, texColor.a, 1.0);
    }
    gl_FragColor = texColor;
}


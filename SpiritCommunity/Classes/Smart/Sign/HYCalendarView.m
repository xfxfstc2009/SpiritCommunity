//
//  MyCalendarItem.m
//  HYCalendar
//
//  Created by nathan on 14-9-17.
//  Copyright (c) 2014年 nathan. All rights reserved.
//

#import "HYCalendarView.h"
#import "QiandaoSingleView.h"

@implementation HYCalendarView
{
    UIButton  *_selectButton;
    NSMutableArray *_daysArray;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = BACKGROUND_VIEW_COLOR;
        _daysArray = [NSMutableArray arrayWithCapacity:42];
        CGFloat itemW     = self.frame.size.width / 7;
        CGFloat itemH     = self.frame.size.height / 7;
        for (int i = 0; i < 42; i++) {
            QiandaoSingleView *button = [[QiandaoSingleView alloc] initWithFrame:CGRectMake(0, 0, itemW, itemH)];
            [self addSubview:button];
            [_daysArray addObject:button];
        }
    }
    return self;
}

#pragma mark - create View
- (void)setDate:(NSDate *)date{
    _date = date;
    
    [self createCalendarViewWith:date];
}

- (void)createCalendarViewWith:(NSDate *)date{

    CGFloat itemW     = self.frame.size.width / 7;
    CGFloat itemH     = self.frame.size.height / 7;
    
    // 1.year month
    UILabel *headlabel = [[UILabel alloc] init];
    headlabel.backgroundColor = [UIColor whiteColor];
    headlabel.text     = [NSString stringWithFormat:@"%li-%li-%li",(long)[HYCalendarTool year:date],(long)[HYCalendarTool month:date],(long)[HYCalendarTool day:date]];
    headlabel.font     = [UIFont fontWithCustomerSizeName:@"小正文"];
    headlabel.frame           = CGRectMake(0, 0, self.frame.size.width, itemH);
    [self addSubview:headlabel];
    
    // 2.weekday
    NSArray *array = @[@"日", @"一", @"二", @"三", @"四", @"五", @"六"];
    UIView *weekBg = [[UIView alloc] init];
    weekBg.backgroundColor = RGB(221, 228, 240, 1);
    weekBg.frame = CGRectMake(0, CGRectGetMaxY(headlabel.frame), self.frame.size.width, itemH);
    [self addSubview:weekBg];
    
    for (int i = 0; i < 7; i++) {
        UILabel *week = [[UILabel alloc] init];
        week.text     = array[i];
        week.font     = [UIFont systemFontOfSize:14];
        week.frame    = CGRectMake(itemW * i, 0, itemW, 32);
        week.textAlignment   = NSTextAlignmentCenter;
        week.backgroundColor = [UIColor clearColor];
        week.textColor       = [UIColor colorWithCustomerName:@"灰"];
        [weekBg addSubview:week];
    }
    
    //  3.days (1-31)
    for (int i = 0; i < 31; i++) {
        
        int x = (i % 7) * itemW ;
        int y = (i / 7) * itemH + CGRectGetMaxY(weekBg.frame);
        
        QiandaoSingleView *qiandaoView = [[QiandaoSingleView alloc]initWithFrame:CGRectMake(x, y, itemW, itemH)];
        [self addSubview:qiandaoView];
        
        NSInteger daysInLastMonth = [HYCalendarTool totaldaysInMonth:[HYCalendarTool lastMonth:date]];
        NSInteger daysInThisMonth = [HYCalendarTool totaldaysInMonth:date];
        NSInteger firstWeekday    = [HYCalendarTool firstWeekdayInThisMonth:date];
        
        NSInteger day = 0;
        
        if (i < firstWeekday) {
            day = daysInLastMonth - firstWeekday + i + 1;
            
        }else if (i > firstWeekday + daysInThisMonth - 1){
            day = i + 1 - firstWeekday - daysInThisMonth;
            
        }else{
            day = i - firstWeekday + 1;
        }
        
        qiandaoView.date = [NSString stringWithFormat:@"%li",(long)day];
        
        // this month
        if ([HYCalendarTool month:date] == [HYCalendarTool month:[NSDate date]]) {
            
            NSInteger todayIndex = [HYCalendarTool day:date] + firstWeekday - 1;
            
            if (i < todayIndex && i >= firstWeekday) {
                [self setSign:i - (int)firstWeekday + 1 andBtn:qiandaoView];
            }else if(i ==  todayIndex){
                _qiandaoSingleView = qiandaoView;
//                [self setStyle_SignEd1:qiandaoView];
                [self setSign:i - (int)firstWeekday + 1 andBtn:qiandaoView];
            } else {
                [self setStyle_AfterToday:qiandaoView];
            }
        }
    }
}


#pragma mark 设置已经签到
- (void)setSign:(int)i andBtn:(QiandaoSingleView*)dayButton{
    [_signArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        int now = i;
        int now2 = [obj intValue];
        if (now2== now) {
            [self setStyle_SignEd:dayButton];
        }
    }];
}


#pragma mark - output date
-(void)logDate:(UIButton *)dayBtn
{
    _selectButton.selected = NO;
    dayBtn.selected = YES;
    _selectButton = dayBtn;
    
    NSInteger day = [[dayBtn titleForState:UIControlStateNormal] integerValue];
    
    NSDateComponents *comp = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self.date];
    
    if (self.calendarBlock) {
        self.calendarBlock(day, [comp month], [comp year]);
    }
}


#pragma mark - date button style
//设置不是本月的日期字体颜色   ---白色  看不到
- (void)setStyle_BeyondThisMonth:(UIButton *)btn
{
    btn.enabled = NO;
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

//这个月 今日之前的日期style
- (void)setStyle_BeforeToday:(QiandaoSingleView *)btn {
    [btn setCheck:NO];
    [btn setClean:NO];
}


//今日已签到
- (void)setStyle_Today_Signed:(QiandaoSingleView *)btn {
    [btn setCheck:YES];
    [btn setClean:NO];
}

//今日没签到
- (void)setStyle_Today:(QiandaoSingleView *)btn
{
    [btn setCheck:NO];
    [btn setClean:NO];
}

//这个月 今天之后的日期style
- (void)setStyle_AfterToday:(QiandaoSingleView *)btn
{
    [btn setClean:YES];
}


//已经签过的 日期style
- (void)setStyle_SignEd:(QiandaoSingleView *)btn {
    [btn setCheck:YES];
    [btn setClean:NO];
}

- (void)setStyle_SignEd1:(QiandaoSingleView *)btn {
    [btn setCheck:NO];
    [btn setClean:NO];
}
@end

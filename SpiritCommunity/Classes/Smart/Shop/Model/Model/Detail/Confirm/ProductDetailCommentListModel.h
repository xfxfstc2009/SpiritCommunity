//
//  ProductDetailCommentListModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "ProductDetailCommentDataSingleModel.h"
#import "ProductDetailCommentSorceinfoModel.h"
@interface ProductDetailCommentListModel : FetchModel

@property (nonatomic,strong)NSArray<ProductDetailCommentDataSingleModel> *data;
@property (nonatomic,strong)ProductDetailCommentSorceinfoModel *sorceinfo;


@end

//
//  ProductDetailDuobaoInfoCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/15.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "ProductDetailDuobaoInfoCell.h"

@interface ProductDetailDuobaoInfoCell()
@property (nonatomic,strong)UILabel *guizeLabel;        /**< 规则Label*/
@property (nonatomic,strong)UILabel *statusLabel;       /**< 状态label*/
@property (nonatomic,strong)UILabel *qihaoLabel;        /**< 期号label*/
@property (nonatomic,strong)UIView *progressBgView;     /**< 进度条背景*/
@property (nonatomic,strong)UIView *progressDymicView;  /**< 动态*/
@property (nonatomic,strong)UILabel *countLabel;        /**< 总需*/
@property (nonatomic,strong)UILabel *haixuLabel;        /**< 还需多少次*/

@end

@implementation ProductDetailDuobaoInfoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1.
    self.guizeLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"棕"];
    [self addSubview:self.guizeLabel];
    
    // 2. 创建状态label
    self.statusLabel = [GWViewTool createLabelFont:@"提示" textColor:@"白"];
    self.statusLabel.backgroundColor = [UIColor colorWithCustomerName:@"橙"];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.statusLabel];
    
    // 3. 创建期号码
    self.qihaoLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"浅灰"];
    [self addSubview:self.qihaoLabel];
    
    // 4. view
    self.progressBgView = [[UIView alloc]init];
    self.progressBgView.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.progressBgView.clipsToBounds = YES;
    [self addSubview:self.progressBgView];
    
    self.progressDymicView = [[UIView alloc]init];
    self.progressDymicView.backgroundColor = [UIColor colorWithCustomerName:@"棕"];
    self.progressDymicView.frame = CGRectMake(0, 0, 0, self.progressBgView.size_height);
    [self.progressBgView addSubview:self.progressDymicView];
    
    // 5.
    self.countLabel = [GWViewTool createLabelFont:@"提示" textColor:@"浅灰"];
    [self addSubview:self.countLabel];
    
    // 6. 还需要
    self.haixuLabel = [GWViewTool createLabelFont:@"提示" textColor:@"浅灰"];
    [self addSubview:self.haixuLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferProductDetailRootModel:(ProductDetailRootModel *)transferProductDetailRootModel{
    _transferProductDetailRootModel = transferProductDetailRootModel;
    
    // 1.
    if (transferProductDetailRootModel.integralIndiana){
        self.guizeLabel.text = [NSString stringWithFormat:@"1人次 = %li积分",(long)transferProductDetailRootModel.integralIndiana.integral];
        CGSize guizeSize = [self.guizeLabel.text sizeWithCalcFont:self.guizeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.guizeLabel.font])];
        self.guizeLabel.frame = CGRectMake(LCFloat(11), 0, guizeSize.width, [NSString contentofHeightWithFont:self.guizeLabel.font]);
        
        // 2. status
        self.statusLabel.text = transferProductDetailRootModel.integralIndiana.state == 1 ? @"进行中":@"已完成";
        CGSize statusSize = [self.statusLabel.text sizeWithCalcFont:self.statusLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.statusLabel.font])];
        self.statusLabel.frame = CGRectMake(CGRectGetMaxX(self.guizeLabel.frame) + LCFloat(11), self.guizeLabel.orgin_y, statusSize.width + 2 * LCFloat(3), [NSString contentofHeightWithFont:self.statusLabel.font] + 2 * LCFloat(2));
        self.statusLabel.layer.cornerRadius = LCFloat(3);
        self.statusLabel.clipsToBounds = YES;
        
        // 3.期号
        self.qihaoLabel.text = [NSString stringWithFormat:@"%@期",transferProductDetailRootModel.integralIndiana.number];;
        CGSize qihaoSize = [self.qihaoLabel.text sizeWithCalcFont:self.qihaoLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.qihaoLabel.font])];
        self.qihaoLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - qihaoSize.width, 0, qihaoSize.width, [NSString contentofHeightWithFont:self.qihaoLabel.font]);
        self.qihaoLabel.center_y = self.statusLabel.center_y;
        
        // progress
        self.progressBgView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.statusLabel.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(6));
        self.progressBgView.layer.cornerRadius = MIN(self.progressBgView.size_height, self.progressBgView.size_width) / 2.;
        // dymicPro
        self.progressDymicView.frame = CGRectMake(0, 0, 0, LCFloat(6));
        transferProductDetailRootModel.integralIndiana.totalPeople = 10;
        CGFloat progressFloat = (transferProductDetailRootModel.integralIndiana.totalPeople - transferProductDetailRootModel.integralIndiana.needPeople) * 1.0f / transferProductDetailRootModel.integralIndiana.totalPeople;
        [UIView animateWithDuration:.5f animations:^{
            self.progressDymicView.frame = CGRectMake(0, 0, progressFloat * (kScreenBounds.size.width - 2 * LCFloat(11)), LCFloat(6));
        } completion:^(BOOL finished) {
            self.progressDymicView.frame = CGRectMake(0, 0, progressFloat * (kScreenBounds.size.width - 2 * LCFloat(11)), LCFloat(6));
        }];
        
        // 总需
        self.countLabel.text = [NSString stringWithFormat:@"总需:%li人次",(long)transferProductDetailRootModel.integralIndiana.totalPeople];
        CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
        self.countLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.progressBgView.frame) + LCFloat(4), countSize.width, [NSString contentofHeightWithFont:self.countLabel.font]);
        
        // 还需 need
        NSString *needStr = [NSString stringWithFormat:@"还需:100人次"];
        CGSize needSize = [needStr sizeWithCalcFont:self.haixuLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.haixuLabel.font])];
        self.haixuLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - needSize.width, 0, needSize.width, [NSString contentofHeightWithFont:self.haixuLabel.font]);
        self.haixuLabel.attributedText = [Tool rangeLabelWithContent:needStr hltContentArr:@[[NSString stringWithFormat:@"100"]] hltColor:[UIColor colorWithCustomerName:@"橙"] normolColor:[UIColor colorWithCustomerName:@"浅灰"]];
        self.haixuLabel.center_y = self.countLabel.center_y;
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(6);
    cellHeight += LCFloat(4);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}
@end

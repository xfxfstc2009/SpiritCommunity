//
//  LiveUIActionView.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveUIBottomViewSingleModel.h"
#import "HomeLiveSingleModel.h"
#import "LiveMessageView.h"
#import "LiveUIBottomView.h"
#import "LiveUIHouseInfoView.h"
#import "LiveTopUserTableView.h"
#import "OtherSearchListModel.h"
#import "LiveUITopView.h"
@interface LiveUIActionView : UIView

@property (nonatomic,assign)LiveShowType transferShowType;              /**< 上个页面传递过来的当前主播的类型*/
@property (nonatomic,strong)HomeLiveSingleModel *transferLiveSingleModel;   /**< 传递过去的直播Model*/
@property (nonatomic,strong)LiveUITopView *topUIView;                       /**< 顶部的view*/
// 
@property (nonatomic,strong)LiveMessageView *messageView;                   /**< 消息体*/
@property (nonatomic,strong)LiveUIBottomView *bottomUIView;                 /**< 底部的view*/
@property (nonatomic,strong)LiveUIHouseInfoView *houseInfoView;             /**< 房间信息*/
@property (nonatomic,strong)UITableView *userListTableView;
@property (nonatomic,strong)NSMutableArray *userListMutableArr;

-(void)liveUIActionViewActionClickBlock:(void(^)(LiveUIBottomViewSingleModel *liveUIBottomViewSingleModel))block;
-(void)liveUIActionViewActionUITopClickBlock:(void(^)())block;

// 1. 麦克风&美颜&滤镜分辨率等值回复到原先
-(void)liveUIWithReset;



@end

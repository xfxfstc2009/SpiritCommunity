//
//  HomeRootGoodsListMainSingleModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/9.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "HomeRootGoodsListMainSingleModel.h"

@implementation HomeRootGoodsListMainSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end

//
//  GWSettingSliderSingleModel.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/8/22.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol GWSettingSliderSingleModel <NSObject>


@end

@interface GWSettingSliderSingleModel : FetchModel

@property (nonatomic,assign)BOOL show;              /**< 是否显示*/
@property (nonatomic,copy)NSString *sign;           /**< title*/
@property (nonatomic,assign)NSInteger sequence;     /**< 排序*/

@end

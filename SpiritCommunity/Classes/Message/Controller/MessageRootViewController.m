//
//  MessageRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "MessageRootViewController.h"
#import "ShareRootViewController.h"
#import "MessageRootSingleTableViewCell.h"
#import "MessageListModel.h"
#import "AliChatManager.h"
#import "FindRootListModel.h"
#import "ChatSingleModel.h"
#import "MessageChatSingleTableViewCell.h"

@interface MessageRootViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)UIScrollView * mainScrollView;
@property (nonatomic,strong)UITableView *infomationTableView;
@property (nonatomic,strong)UITableView *userTableView;
@property (nonatomic,strong)NSMutableArray *infomationMutableArr;
@property (nonatomic,strong)NSMutableArray *userMutableArr;
@end

@implementation MessageRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSegment];
    [self createMainScrollView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getIMMnager];                     // 获取当前聊天
    [self sendRequestToGetInformation];     // 获取当前通知
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"通知";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.infomationMutableArr = [NSMutableArray array];
    self.userMutableArr = [NSMutableArray array];
}

-(void)createSegment{
    __weak typeof(self)weakSelf = self;
    self.segmentList =[HTHorizontalSelectionList createSegmentWithDataSource:@[@"聊天",@"通知"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0) animated:YES];
    }];
    self.segmentList.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = self.segmentList;
}

#pragma mark - 创建scrollView
-(void)createMainScrollView{
    if (!self.mainScrollView){
        __weak typeof(self)weakSelf = self;
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
            [strongSelf.segmentList setSelectedButtonIndex:page animated:YES];
        }];
        self.mainScrollView.frame = CGRectMake(0,0, kScreenBounds.size.width, kScreenBounds.size.height - 49 - 64);
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, self.mainScrollView.size_height);
        self.mainScrollView.scrollEnabled = NO;
        [self.view addSubview:self.mainScrollView];
    }
    [self createTableView];
}

#pragma mark - 创建tableView
-(void)createTableView{
    if (!self.infomationTableView){
        self.infomationTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.infomationTableView.dataSource = self;
        self.infomationTableView.delegate = self;
        self.infomationTableView.orgin_x = kScreenBounds.size.width;
        [self.mainScrollView addSubview:self.infomationTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.infomationTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInformation];
    }];
    
    [self.infomationTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInformation];
    }];
    
    if (!self.userTableView){
        self.userTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        
        self.userTableView.delegate = self;
        self.userTableView.dataSource = self;
        [self.mainScrollView addSubview:self.userTableView];
    }
    
    [self.userTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getIMMnager];
    }];
    [self.userTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         [strongSelf getIMMnager];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.infomationTableView){
        return self.infomationMutableArr.count;
    } else if (tableView == self.userTableView){
        return self.userMutableArr.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.infomationTableView){
        id messageModel = [self.infomationMutableArr objectAtIndex:indexPath.row];
        if ([messageModel isKindOfClass:[MessageSingleModel class]]){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            MessageRootSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[MessageRootSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowOne.backgroundColor = [UIColor clearColor];
            }
            cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            MessageSingleModel *transferSingleModel = [self.infomationMutableArr objectAtIndex:indexPath.row];
            cellWithRowOne.transferSingleModel = transferSingleModel;
            
            return cellWithRowOne;
        } else if ([messageModel isKindOfClass:[ChatSingleModel class]]){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            MessageChatSingleTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[MessageChatSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowTwo.backgroundColor = [UIColor clearColor];
            }
            cellWithRowTwo.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            ChatSingleModel *transferSingleModel = [self.infomationMutableArr objectAtIndex:indexPath.row];
            cellWithRowTwo.transferSingleModel = transferSingleModel;
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            GWNormalTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            return cellWithRowThr;
        }
        return nil;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        MessageChatSingleTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[MessageChatSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = [UIColor clearColor];
        }
        cellWithRowTwo.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        ChatSingleModel *transferSingleModel = [self.userMutableArr objectAtIndex:indexPath.row];
        cellWithRowTwo.transferSingleModel = transferSingleModel;
        return cellWithRowTwo;

    }
}

#pragma mark- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MessageRootSingleTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.infomationTableView){
        id messageModel = [self.infomationMutableArr objectAtIndex:indexPath.row];
        if ([messageModel isKindOfClass:[MessageSingleModel class]]){
            MessageSingleModel *singleModel = [self.infomationMutableArr objectAtIndex:indexPath.row];
            if (singleModel.has_read == NO){
                NSInteger noReadCount = 0;
                for (MessageSingleModel *messageSingleModel in self.infomationMutableArr){
                    if (!messageSingleModel.has_read){
                        noReadCount++;
                    }
                }
                noReadCount --;
                self.segmentList.bridageNumber = [NSString stringWithFormat:@"%li",(long)noReadCount];
                if(noReadCount <= 0){
                    [self.segmentList cleanSegmentBridge];
                }
            }
            
            if (singleModel.message_type == ActionClickTypeWeb){            // 跳转到网页
                WebViewController *webViewController = [[WebViewController alloc]init];
                [webViewController webViewControllerWithAddress:singleModel.direct];
                [self.navigationController pushViewController:webViewController animated:YES];
                [self sendRequestToReadInfo:singleModel];
            }
        }
    } else {
        id messageModel = [self.userMutableArr objectAtIndex:indexPath.row];
        if ([messageModel isKindOfClass:[ChatSingleModel class]]){
            ChatSingleModel *singleModel = [self.userMutableArr objectAtIndex:indexPath.row];
            YWPerson *person = [[YWPerson alloc]initWithPersonId:singleModel.personId];
            __weak typeof(self)weakSelf = self;
            [[AliChatManager sharedInstance] openConversationWithPerson:person callBack:^(GWCurrentIMViewController *conversationController) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.navigationController pushViewController:conversationController animated:YES];
            }];
        }
    }
}

#pragma mark 删除文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.infomationTableView){
        id messageModel = [self.infomationMutableArr objectAtIndex:indexPath.row];
        if ([messageModel isKindOfClass:[FindRootSingleModel class]]){
            return @"删除";
        } else {
            return @"删除";
        }
    } else {
        return @"删除";
    }
}

// 一点击删除，就会调用这个方法/ 左滑动删除
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.infomationTableView){
        id messageModel = [self.infomationMutableArr objectAtIndex:indexPath.row];
        if ([messageModel isKindOfClass:[MessageSingleModel class]]){
            MessageSingleModel *singleModel = [self.infomationMutableArr objectAtIndex:indexPath.row];
            [self sendRequestToDeleteInfo:singleModel];
        }
    } else {
        ChatSingleModel *singleModel = [self.userMutableArr objectAtIndex:indexPath.row];
        __weak typeof(self)weakSelf = self;
        [[AliChatManager sharedInstance] deleteConversationWithId:singleModel.conversationId block:^(NSError *error) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (!error){
                if ([strongSelf.userMutableArr containsObject:singleModel]){
                    NSInteger index = [self.userMutableArr indexOfObject:singleModel];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                    [strongSelf.userMutableArr removeObject:singleModel];
                    [strongSelf.userTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                    
                    if (strongSelf.userMutableArr.count){
                        [strongSelf.userTableView dismissPrompt];
                    } else {
                        [strongSelf.userTableView showPrompt:@"当前没有消息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
                    }
                }
            }
        }];
    }
}

- (BOOL)tableView: (UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.infomationTableView){
        return YES;
    } else {
        return YES;
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.infomationTableView){
        return YES;
    } else {
        return YES;
    }
}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *infoArr;
    if (tableView == self.infomationTableView){
        infoArr = self.infomationMutableArr;
    } else if (tableView == self.userTableView){
        infoArr = self.userMutableArr;
    }
    
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType  = SeparatorTypeBottom;
    } else if ([indexPath row] == [infoArr count] - 1) {
        separatorType  = SeparatorTypeBottom;
    } else {
        separatorType  = SeparatorTypeMiddle;
    }
    if ([infoArr  count] == 1) {
        separatorType  = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"message"];
}


#pragma mark - SendRequestToGetInfo
-(void)sendRequestToGetInformation{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page":@(self.infomationTableView.currentPage),@"size":@"20"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageList requestParams:params responseObjectClass:[MessageListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            MessageListModel *messageList = (MessageListModel *)responseObject;
            if (strongSelf.infomationTableView.isXiaLa){
                [strongSelf.infomationMutableArr removeAllObjects];
            }

            [strongSelf.infomationMutableArr addObjectsFromArray:messageList.list];
            [strongSelf.infomationTableView reloadData];

//            if (!messageList.list.count){
//                if (!strongSelf.infomationMutableArr.count){
//                    [strongSelf.infomationTableView hiddenBottomTitle];
//                } else {
//                    [strongSelf.infomationTableView showBottomTitle];
//                }
//            } else {
//                [strongSelf.infomationTableView hiddenBottomTitle];
//            }
            
            // 判断有多少条未读消息
            NSInteger noReadCount = 0;
            for (MessageSingleModel *messageSingleModel in strongSelf.infomationMutableArr){
                if (!messageSingleModel.has_read){
                    noReadCount++;
                }
            }
            if (noReadCount != 0){
                strongSelf.segmentList.bridageIndex = 1;
                strongSelf.segmentList.bridageNumber = [NSString stringWithFormat:@"%li",noReadCount];
                [strongSelf.segmentList reloadData];
            } else {
                [strongSelf.segmentList cleanSegmentBridge];
            }
            
            if (strongSelf.infomationMutableArr.count){
                [strongSelf.infomationTableView dismissPrompt];
            } else {
                [strongSelf.infomationTableView showPrompt:@"当前没有通知消息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
            [strongSelf.infomationTableView stopPullToRefresh];
            [strongSelf.infomationTableView stopFinishScrollingRefresh];

        }
    }];
}

-(void)getIMMnager{
    __weak typeof(self)weakSelf = self;
    [[AliChatManager sharedInstance]sendRequestToGetMessageListBlock:^(NSArray *msgArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.userMutableArr.count){
            [strongSelf.userMutableArr removeAllObjects];
        }
        [strongSelf.userMutableArr addObjectsFromArray:msgArr];
        [strongSelf.userTableView reloadData];
        
        if (strongSelf.userMutableArr.count){
            [strongSelf.userTableView dismissPrompt];
        } else {
            [strongSelf.userTableView showPrompt:@"当前没有消息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
        }
        [strongSelf.userTableView stopPullToRefresh];
        [strongSelf.userTableView stopFinishScrollingRefresh];
    }];
}

#pragma mark - 阅读消息
-(void)sendRequestToReadInfo:(MessageSingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"messageId":singleModel.ID};
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageRead requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            singleModel.has_read = YES;
            NSInteger index = [strongSelf.infomationMutableArr indexOfObject:singleModel];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [strongSelf.infomationTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

#pragma mark - 删除消息
-(void)sendRequestToDeleteInfo:(MessageSingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"messageId":singleModel.ID};
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageDelete requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if ([strongSelf.infomationMutableArr containsObject:singleModel]){
                NSInteger index = [self.infomationMutableArr indexOfObject:singleModel];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                [strongSelf.infomationMutableArr removeObject:singleModel];
                [strongSelf.infomationTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
    }];
}


@end

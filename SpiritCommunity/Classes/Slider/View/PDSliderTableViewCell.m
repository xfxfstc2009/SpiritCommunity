//
//  PDSliderTableViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSliderTableViewCell.h"

@interface PDSliderTableViewCell()
@property (nonatomic,strong)PDImageView *iconImageView;             /**< icon*/
@property (nonatomic,strong)UILabel *fixedLabel;                    /**< fixedLaberl*/

@end

@implementation PDSliderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImageView = [[PDImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImageView];
    
    // 2. 创建label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:17];
    self.fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
//    self.fixedLabel.font = [self.fixedLabel.font boldFont];
    [self addSubview:self.fixedLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    // 1. 当前的icon
    self.iconImageView.frame = CGRectMake(LCFloat(13) * 2, (_transferCellHeight - LCFloat(15)) / 2. , LCFloat(15) ,LCFloat(15));
    NSString *imgName = @"";
    if ([self.transferTitle isEqualToString:@"消息"]){
        imgName = @"icon_center_order";
    } else if ([self.transferTitle isEqualToString:@"我的关注"]){
        imgName = @"icon_center_to_tutor";
    } else if ([self.transferTitle isEqualToString:@"我的钱包"]){
        imgName = @"icon_center_wallet";
    } else if ([self.transferTitle isEqualToString:@"发现"]){
        imgName = @"icon_star_full";
    } else if ([self.transferTitle isEqualToString:@"我的撸友"]){
        imgName = @"icon_slider_firend";
    } else if ([self.transferTitle isEqualToString:@"设置"]){
        imgName = @"icon_center_setting";
    } else if ([self.transferTitle isEqualToString:@"我的订单"]){
        imgName = @"icon_center_invite";
    } else if ([self.transferTitle isEqualToString:@"首页"]){
        imgName = @"icon_center_change_identifier";
    } else if ([self.transferTitle isEqualToString:@"购物车"]){
        imgName = @"product_icon_shop_hlt";
    } else if ([self.transferTitle isEqualToString:@"排行榜"]){
        imgName = @"home_paihang_icon";
    }
    
    self.iconImageView.image = [UIImage imageNamed:imgName];
    
    // 2. 创建fixedLabel
    self.fixedLabel.text = self.transferTitle;
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + LCFloat(15), 0, self.size_width - CGRectGetMaxX(self.iconImageView.frame) - LCFloat(11), self.transferCellHeight);
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}



@end

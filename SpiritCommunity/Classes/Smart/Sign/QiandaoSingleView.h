//
//  QiandaoSingleView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/19.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QiandaoSingleView : UIView

-(void)setCheck:(BOOL)checked;
-(void)setClean:(BOOL)clean;
-(void)actionClickClickBlock:(void(^)())block;

@property (nonatomic,copy)NSString *date;



@end

//
//  CenterUserImgsCollectionViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/18.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterUserImgsCollectionViewCell : UICollectionViewCell

@property (nonatomic,copy)NSString *transferCellImg;
@property (nonatomic,strong)PDImageView *imgView;

-(void)actionTapGestureBlock:(void(^)())block;
-(void)actionLongGestureBlock:(void(^)())block;

@end

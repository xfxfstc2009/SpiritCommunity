//
//  CenterMoneyDetailViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CenterMoneyDetailViewController.h"
#import "MoneyFlowListModel.h"
#import "CenterMoneyDetailCell.h"

@interface CenterMoneyDetailViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *moneyDetailTableView;
@property (nonatomic,strong)NSMutableArray *moneyDetailArr;
@property (nonatomic,strong)NSArray *segmentArr;


@end

@implementation CenterMoneyDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"收支明细";
    
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.moneyDetailArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.moneyDetailTableView){
        self.moneyDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.moneyDetailTableView.dataSource = self;
        self.moneyDetailTableView.delegate = self;
        [self.view addSubview:self.moneyDetailTableView];
        __weak typeof(self)weakSelf = self;
        [self.moneyDetailTableView appendingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetList];
        }];
        [self.moneyDetailTableView appendingFiniteScrollingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetList];
        }];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.moneyDetailArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    CenterMoneyDetailCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[CenterMoneyDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferSingleModel = [self.moneyDetailArr objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CenterMoneyDetailCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType  = SeparatorTypeHead;
    } else if ([indexPath row] == [self.moneyDetailArr count] - 1) {
        separatorType  = SeparatorTypeBottom;
    } else {
        separatorType  = SeparatorTypeMiddle;
    }
    if ([self.moneyDetailArr count] == 1) {
        separatorType  = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithType:separatorType];
}



#pragma mark - Interface
// 获取收支明细
-(void)sendRequestToGetList{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page":@(self.moneyDetailTableView.currentPage),@"size":@"20"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:money_Flowing requestParams:params responseObjectClass:[MoneyFlowListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            MoneyFlowListModel *list = (MoneyFlowListModel *)responseObject;
            if (strongSelf.moneyDetailTableView.isXiaLa){
                [self.moneyDetailArr removeAllObjects];
            }
            [strongSelf.moneyDetailArr addObjectsFromArray:list.list];
            [strongSelf.moneyDetailTableView reloadData];
            
            if (strongSelf.moneyDetailArr.count){
                [strongSelf.moneyDetailTableView dismissPrompt];
            } else {
                [strongSelf.moneyDetailTableView showPrompt:@"当前没有收支明细" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
            [strongSelf.moneyDetailTableView stopPullToRefresh];
            [strongSelf.moneyDetailTableView stopFinishScrollingRefresh];
        }
    }];
}


@end

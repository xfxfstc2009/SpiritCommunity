//
//  UserinfoImgsViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/4.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "UserinfoImgsViewController.h"
#import "CenterUserImgsCollectionViewCell.h"
#import "CenterUserImgSingleModel.h"
#import "ImgSelectorViewController.h"

@interface UserinfoImgsViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UICollectionView *imgsCollectionView;
@property (nonatomic,strong)NSMutableArray *imgsMutableArr;

@end

@implementation UserinfoImgsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
    [self sendRequestToGetInfo];
 
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"用户相册";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.imgsMutableArr = [NSMutableArray array];
}

#pragma mark - 用户图片
-(void)createCollectionView{
    if (!self.imgsCollectionView){
        self.imgsCollectionView = [GWViewTool gwCreateCollectionViewRect:self.view.bounds];
        self.imgsCollectionView.dataSource = self;
        self.imgsCollectionView.delegate = self;
        [self.view addSubview:self.imgsCollectionView];
    }
    [self.imgsCollectionView registerClass:[CenterUserImgsCollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentify"];
    

}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imgsMutableArr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CenterUserImgsCollectionViewCell *cell = (CenterUserImgsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentify"  forIndexPath:indexPath];
    cell.transferCellImg = [self.imgsMutableArr objectAtIndex:indexPath.row];
    
    __weak typeof(self)weakSelf = self;
    [cell actionTapGestureBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ImgSelectorViewController *imgSelected = [[ImgSelectorViewController alloc]init];
        CenterUserImgsCollectionViewCell * cell = (CenterUserImgsCollectionViewCell *)[strongSelf.imgsCollectionView cellForItemAtIndexPath:indexPath];
        [imgSelected showInView:strongSelf.parentViewController imgArr:strongSelf.imgsMutableArr currentIndex:indexPath.row cell:cell];
    }];
    
    return cell;

}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat assetMargin = LCFloat(10);
    return CGSizeMake((kScreenBounds.size.width - 4 * assetMargin) / 3., (kScreenBounds.size.width - 4 * assetMargin) / 3.);
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,LCFloat(7),0,LCFloat(7));
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,LCFloat(20)};
    return size;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size = {kScreenBounds.size.width,LCFloat(10)};
    return size;
}

#pragma mark - 获取图片信息
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"userId":self.transferSearchSingleModel.ID};
    [[NetworkAdapter sharedAdapter] fetchWithPath:assetList requestParams:params responseObjectClass:[CenterUserImgSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            CenterUserImgSingleModel *singleModel = (CenterUserImgSingleModel *)responseObject;
            if (strongSelf.imgsCollectionView.isXiaLa){
                [strongSelf.imgsMutableArr removeAllObjects];
            }
            [strongSelf.imgsMutableArr addObjectsFromArray:singleModel.imgs];
            [strongSelf.imgsCollectionView reloadData];
            [strongSelf.imgsCollectionView stopFinishScrollingRefresh];
            [strongSelf.imgsCollectionView stopPullToRefresh];

            if (strongSelf.imgsMutableArr.count){
                [strongSelf.imgsCollectionView dismissPrompt];
            } else {
                [strongSelf.imgsCollectionView showPrompt:@"这家伙很懒，什么照片都没有" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
        }
    }];
}


@end

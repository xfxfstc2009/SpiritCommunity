//
//  MapLiverAnnotationView.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/23.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "OtherSearchSingleModel.h"

#define kCalloutViewMargin          -8

@interface MapLiverAnnotationView : MAAnnotationView

@property (nonatomic, strong) UIView *calloutView;
@property (nonatomic,strong)OtherSearchSingleModel *transferLiverModel;


-(void)locationShow:(void(^)(OtherSearchSingleModel *transferLiverModel))block;
@end

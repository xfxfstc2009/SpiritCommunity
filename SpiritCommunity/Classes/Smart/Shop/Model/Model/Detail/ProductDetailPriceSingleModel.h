//
//  ProductDetailPriceSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol ProductDetailPriceSingleModel <NSObject>


@end

@interface ProductDetailPriceSingleModel : FetchModel

@property (nonatomic,copy)NSString *category_id;
@property (nonatomic,copy)NSString *code;
@property (nonatomic,assign)CGFloat discount_price;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)CGFloat original_price;
@property (nonatomic,assign)NSInteger stock;

@property (nonatomic,copy)NSString *skuStr;
@end

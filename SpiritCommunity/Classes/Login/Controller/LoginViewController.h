//
//  LoginViewController.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "AbstractViewController.h"

@interface LoginViewController : AbstractViewController

-(void)loginSuccess:(void(^)(BOOL success))block;
@end

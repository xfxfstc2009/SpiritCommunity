//
//  UserMoneyTransferViewController.h
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/3/9.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import "AbstractViewController.h"
#import "OtherSearchSingleModel.h"

@interface UserMoneyTransferViewController : AbstractViewController
@property (nonatomic,strong)OtherSearchSingleModel *transferSearchSingleModel;

@end

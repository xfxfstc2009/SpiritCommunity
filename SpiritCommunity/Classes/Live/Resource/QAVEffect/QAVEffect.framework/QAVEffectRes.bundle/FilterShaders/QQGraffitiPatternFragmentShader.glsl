precision highp float;
varying vec2 canvasCoordinate;
varying vec2 textureCoordinate;

uniform sampler2D inputImageTexture;
uniform sampler2D inputImageTexture2;
uniform int blendMode;

 vec4 blendColor(vec4 texColor, vec4 canvasColor)
 {
     if(texColor.a > 0.0){
        texColor.rgb = texColor.rgb / texColor.a;
     }
     vec3 resultFore = texColor.rgb;

     vec4 resultColor = vec4(resultFore * texColor.a, texColor.a);
     return resultColor;
 }

void main(void)
{
    vec4 canvasColor = texture2D(inputImageTexture, canvasCoordinate);
    vec4 texColor = texture2D(inputImageTexture2, textureCoordinate);
    gl_FragColor = blendColor(texColor, canvasColor);
}

//
//  HomeGoodsNormalSingleCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/15.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeNormalSingleGoodsModel.h"

@interface HomeGoodsNormalSingleCell : UITableViewCell

@property (nonatomic,strong)HomeNormalSingleGoodsModel *transferLeftGoodsModel;
@property (nonatomic,strong)HomeNormalSingleGoodsModel *transferRightGoodsModel;

@property (nonatomic,assign)CGFloat transferCellHeight;
+(CGFloat)calculationCellHeight;

-(void)actionClickCellBlock:(void(^)(HomeNormalSingleGoodsModel *transferGoodsModel))block;

@end

//
//  PDCountDownButton.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCountDownButton.h"
#import <objc/runtime.h>
typedef void(^buttonClickBlock)();

static NSString *PDCountDownButtonKey;
@interface PDCountDownButton()
@property (nonatomic,strong)UILabel *dymicLabel;    /**< 动态label*/
@property (nonatomic,strong)UIButton *button;       /**< 倒计时按钮*/
@property (nonatomic,strong)NSTimer *timer;         /**< 倒计时*/
@property (nonatomic,assign)NSInteger timeInteger;          /**< 倒计时*/
@end

@implementation PDCountDownButton

-(instancetype)initWithFrame:(CGRect)frame daojishi:(NSInteger)daojishi withBlock:(void(^)())block{
    self = [super initWithFrame:frame];
    if (self){
        _timeInteger = daojishi;
        objc_setAssociatedObject(self, &PDCountDownButtonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1.  创建label
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    self.dymicLabel.frame = self.bounds;
    self.dymicLabel.layer.cornerRadius = LCFloat(3);
    self.dymicLabel.clipsToBounds = YES;
    [self smsLabelTypeManager:YES];
    [self addSubview:self.dymicLabel];
    
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.frame = self.bounds;
    [self addSubview:self.button];
    self.button.layer.cornerRadius = LCFloat(3);
    self.button.clipsToBounds = YES;
    __weak typeof(self)weakSelf = self;

    [self.button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &PDCountDownButtonKey);
        if(block){
            block();
        }
    }];
}

-(void)startTimer{
    [self smsLabelTypeManager:NO];
    if (self.timeInteger == 0){
        self.timeInteger = 60;
    }
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
    }
}

#pragma mark - 倒计时方法
-(void)daojishiManager{
    --self.timeInteger;
    if (self.timeInteger < 0){
        [self smsLabelTypeManager:YES];
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    } else {
        [self smsLabelTypeManager:NO];
        self.dymicLabel.attributedText = [Tool rangeLabelWithContent:[NSString stringWithFormat:@"验证码%lis",(long)self.timeInteger]  hltContentArr:@[[NSString stringWithFormat:@"%li",(long)self.timeInteger]] hltColor:[UIColor colorWithCustomerName:@"黑"] normolColor:[UIColor colorWithCustomerName:@"灰"]];

        
    }
}

-(void)smsLabelTypeManager:(BOOL)type{
    if (type == YES){
        self.button.enabled = YES;
        self.dymicLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
        self.dymicLabel.text = @"";
        self.dymicLabel.attributedText = [Tool rangeLabelWithContent:[NSString stringWithFormat:@"获取验证码"] hltContentArr:@[] hltColor:[UIColor colorWithCustomerName:@"黑"] normolColor:[UIColor colorWithCustomerName:@"黑"]];
        self.backgroundColor = [UIColor clearColor];
    } else {
        self.button.enabled = NO;
        self.dymicLabel.textColor = [UIColor colorWithCustomerName:@"失效"];
        self.backgroundColor = [UIColor clearColor];
    }
}

@end

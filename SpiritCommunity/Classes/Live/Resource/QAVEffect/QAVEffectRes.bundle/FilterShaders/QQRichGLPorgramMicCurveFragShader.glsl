precision highp float;

varying vec2 textureCoordinate;

uniform sampler2D inputImageTexture;
uniform sampler2D inputImageTexture2;
uniform float quality;
uniform float add_red;

uniform float red_m;
uniform float green_m;
uniform float blue_m;

void main()
{
    lowp vec4 color;
    color = texture2D(inputImageTexture, textureCoordinate);
    color *= quality;
    
    lowp vec4 newColor;
    newColor = color;
    
    if(newColor.r > newColor.g && newColor.r > newColor.b)
    {
        newColor.r *= 1.0 - red_m * add_red;
        newColor.g *= 1.0 - green_m * add_red;
        newColor.b *= 1.0 - blue_m * add_red;
    }
    
    newColor.r = texture2D(inputImageTexture2, vec2(newColor.r,0.5)).r;
    newColor.g = texture2D(inputImageTexture2, vec2(newColor.g,0.5)).g;
    newColor.b = texture2D(inputImageTexture2, vec2(newColor.b,0.5)).b;
    
    
    
    newColor.a = 1.0;
    gl_FragColor = newColor;
}

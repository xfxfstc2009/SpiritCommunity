//
//  BuildDetailViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2018/1/30.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "BuildDetailViewController.h"

@interface BuildDetailViewController ()


@end

@implementation BuildDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = self.transferBuildSingleModel.name;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    
}


@end

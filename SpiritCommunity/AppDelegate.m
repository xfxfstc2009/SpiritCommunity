//
//  AppDelegate.m
//  SpiritCommunity
//
//  Created by 裴烨烽 on 2018/2/21.
//  Copyright © 2018年 Liaoba. All rights reserved.
//

#import "AppDelegate.h"
#import "GWMapViewSetupManager.h"
#import "AliChatManager.h"
#import "PushManager.h"
#import "PDWXPayHandle.h"

@interface AppDelegate ()<QAVLogger,RESideMenuDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self mainSetUp];
    [GWMapViewSetupManager authorizineMap];                                                  // 1.添加地图
    [[ShareSDKManager sharedShareSDKManager] registerShareSDK];                              // 2.分享
    [[AliChatManager sharedInstance] callThisInDidFinishLaunching];                          // 3.即时通讯
    [PDWXPayHandle regiset];                                                                 // 4.注册微信支付
    [[PushManager shareInstance] registWithPUSHWithApplication:application launchOptions:launchOptions callbackBlock:^(NSDictionary *params) {
    }];
    [LiveRootManager registerLiveSDK];                                                       // 6.注册直播SDK
    [self IMManager];
    
    return YES;
}

-(void)IMManager{
    TIMManager *manager = [[ILiveSDK getInstance] getTIMManager];

    NSNumber *evn = [[NSUserDefaults standardUserDefaults] objectForKey:kEnvParam];
    [manager setEnv:[evn intValue]];

    NSNumber *logLevel = [[NSUserDefaults standardUserDefaults] objectForKey:kLogLevel];
    if (!logLevel)//默认debug等级
    {
        [[NSUserDefaults standardUserDefaults] setObject:@(TIM_LOG_DEBUG) forKey:kLogLevel];
        logLevel = @(TIM_LOG_DEBUG);
    }
//    [self disableLogPrint];//禁用日志控制台打印
    [manager setLogLevel:(TIMLogLevel)[logLevel integerValue]];

}

- (void)disableLogPrint {
    TIMManager *manager = [[ILiveSDK getInstance] getTIMManager];
    [manager initLogSettings:NO logPath:[manager getLogPath]];
    [[ILiveSDK getInstance] setConsoleLogPrint:NO];
    [QAVAppChannelMgr setExternalLogger:self];
}


- (NSString *)getLogPath
{
    return [[TIMManager sharedInstance] getLogPath];
}


- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {

}

- (void)applicationWillEnterForeground:(UIApplication *)application {

}

- (void)applicationDidBecomeActive:(UIApplication *)application {

}

- (void)applicationWillTerminate:(UIApplication *)application {

}

#pragma mark - 3. APNS
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken{
    [PushManager application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

#pragma mark 3.1 注册失败回调
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(nonnull NSError *)error{
    [PushManager applicationDidFailToRegisterForRemoteNotificationsWithError:error];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo{
    [PushManager application:application didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}


#pragma mark - 推送end
#pragma mark ----------------------------------------------
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    if ([[NSString stringWithFormat:@"%@",url] hasPrefix:@"wxab8268b11e980213://"]){        //微信支付
        return [PDWXPayHandle handleOpenURL:url delegate:[PDWXPayHandle sharedManager]];
    }
    return NO;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([[NSString stringWithFormat:@"%@",url] hasPrefix:@"wxab8268b11e980213://"]){        //微信支付
        return [PDWXPayHandle handleOpenURL:url delegate:[PDWXPayHandle sharedManager]];
    }
    return NO;
}

#ifdef IS_IOS9_LATER
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    if ([[NSString stringWithFormat:@"%@",url] hasPrefix:@"wxab8268b11e980213://"]){        //微信支付
        return [PDWXPayHandle handleOpenURL:url delegate:[PDWXPayHandle sharedManager]];
    }
    return NO;
}
#else
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([[NSString stringWithFormat:@"%@",url] hasPrefix:@"wxab8268b11e980213://"]){        //微信支付
        return  [PDWXPayHandle handleOpenURL:url delegate:[PDWXPayHandle sharedManager]];
    }
    return NO;
    
}
#endif






#pragma mark - 1.主视图
-(void)mainSetUp{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor clearColor];
    [self.window makeKeyAndVisible];
    [UIApplication sharedApplication].statusBarHidden = NO;
    PDSideMenu *meni = [[PDSideMenu alloc]init];
    self.window.rootViewController = [meni setupSlider];

    PDLaungchViewController *launchVC = [[PDLaungchViewController alloc] init];
    [self.window addSubview:launchVC.view];
}


@end

//
//  OrderRootRowOneTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListSingleModel.h"
#import "CartSingleModel.h"
#import "ProductDetailRootModel.h"

@interface OrderRootRowOneTableViewCell : UITableViewCell

@property (nonatomic,strong)OrderListSingleModel *transferSingleModel;

@property (nonatomic,strong)CartSingleModel *transferCartModel;

@property (nonatomic,strong)ProductDetailRootModel *transferProductDetailModel;

@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;


@end

//
//  UserinfoImgsViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/4.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "OtherSearchSingleModel.h"


@interface UserinfoImgsViewController : AbstractViewController

@property (nonatomic,strong)OtherSearchSingleModel *transferSearchSingleModel;

@end

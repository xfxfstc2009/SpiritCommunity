//
//  ShareRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "ShareRootViewController.h"
#import "UIImage+ImageEffects.h"

#define SheetViewHeight       LCFloat(128 )
@interface ShareRootViewController ()
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)NSArray *shareButtonArray;
@end

static char actionClickWithShareBlockKey;
@implementation ShareRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSheetView];                      // 加载Sheetview
    [self createDismissButton];                  // 创建dismissButton
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.65f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
    
}

-(void)arrayWithInit{
    self.shareButtonArray = @[@[@"微信",@"denglu_img_wx",@"wechat"],@[@"朋友圈",@"share_friendsCircle",@"friendsCircle"],@[@"QQ",@"denglu_img_qq",@"QQ"]];
}

#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = SheetViewHeight;
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor colorWithRed:246/256. green:246/256. blue:246/256. alpha:.9];
    [self.view addSubview:_shareView];
    
    _shareView.size_height = LCFloat(11) + LCFloat(40) + LCFloat(10) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + LCFloat(11) + LCFloat(44);
    
    // 创建图标
    [self createShareButton];
}

#pragma mark 创建分享图标
-(void)createShareButton{
    for (int i = 0 ; i < self.shareButtonArray.count;i++){
        CGFloat margin = LCFloat(10);
        CGFloat width = (kScreenBounds.size.width - 5 * margin) / 4.;
        CGFloat height = LCFloat(40) + LCFloat(10) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
        for (int i = 0 ; i < 3;i++){
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            NSString *title = [[self.shareButtonArray objectAtIndex:i] objectAtIndex:0];
            NSString *img = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:1];
            NSString *type = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:2];
            [button setTitle:title forState:UIControlStateNormal];
            [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
            [button setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
            button.frame = CGRectMake(margin + (width + margin) * i, LCFloat(11), width, height);
            [button layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(10)];
            __weak typeof(self)weakSelf = self;
            [button buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(NSString *type) = objc_getAssociatedObject(strongSelf, &actionClickWithShareBlockKey);
                if (block){
                    block(type);
                }
                [strongSelf dismissFromView:_showViewController];
            }];
            [_shareView addSubview:button];
        }
    }
}

#pragma mark 创建dismissButton
-(void)createDismissButton{
    UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissBtn.frame = CGRectMake(0, _shareView.frame.size.height - LCFloat(44), kScreenBounds.size.width, LCFloat(49));
    [dismissBtn setTitle:@"确定" forState:UIControlStateNormal];
    dismissBtn.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    dismissBtn.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [dismissBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [dismissBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sheetViewDismiss];
    }];
    [_shareView addSubview:dismissBtn];
    
    
    // Gesture
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}


#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak ShareRootViewController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak ShareRootViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.3f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

-(void)actionClickWithShareBlock:(void(^)(NSString *type))block{
    objc_setAssociatedObject(self, &actionClickWithShareBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end

//
//  ZSImageSelecetdTableViewCell.m
//  RegenerationPlan
//
//  Created by SmartMin on 15/11/2.
//  Copyright © 2015年 baimifan. All rights reserved.
//

#import "ZSImageSelecetdTableViewCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
@interface ZSImageSelecetdTableViewCell()
@property (nonatomic,strong)UIButton *addButton;            /**< 添加按钮*/

@end


#define kMarinWidth         LCFloat(25)
#define kTagWidthWithProductRelease (kScreenBounds.size.width - 4 * kMarinWidth) / 3.
#define kTagHeightWithProductRelease (kScreenBounds.size.width - 4 * kMarinWidth) / 3.

@implementation ZSImageSelecetdTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){

        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 300);
    [self addSubview:self.bgView];
    
    //2. 创建
    [self createButton];
    
}


#pragma mark - CreateCustomerItem
-(UIView *)createCustomerItemWithIndex:(NSInteger)index callBlocak:(void (^)(UIView *imageBgView))imgTapCallBack{
    // 1. origin_x
    CGFloat origin_x = kMarinWidth + (index % 3) * kTagWidthWithProductRelease + (index % 3) * kMarinWidth;
    // 2. origin_y
    CGFloat origin_y = LCFloat(5) + ( index / 3 ) * (kTagHeightWithProductRelease + LCFloat(10));
    // 3 .frame
    CGRect tempRect = CGRectMake(origin_x, origin_y, kTagWidthWithProductRelease,kTagHeightWithProductRelease);
    

    UIImage *image = [self.isSelectedImageArr objectAtIndex:index];
//    image = [self getImageByCuttingImage:image Rect:CGRectMake(LCFloat(30), (image.size.height - image.size.width) / 2., image.size.width, image.size.height - image.size.width)];
    image = image;
    
    UIView *imageBgView = [[UIView alloc]init];
    imageBgView.frame = tempRect;
    imageBgView.backgroundColor = [UIColor clearColor];
    imageBgView.clipsToBounds = YES;
    [self.bgView addSubview:imageBgView];
    
    // 2. 创建imageview
    PDImageView *imageView = [[PDImageView alloc]init];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = image ;
    imageView.frame = imageBgView.bounds;
    imageView.stringTag = [NSString stringWithFormat:@"customButton%li",(long)index];
    [Tool setTransferSingleAsset:image imgView:imageView convertView:imageBgView];
    [imageBgView addSubview:imageView];
    
    // 3. 创建按钮
    UIButton *imageTapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    imageTapButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [imageTapButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (imgTapCallBack){
            imgTapCallBack(imageBgView);
        }
    }];
    imageTapButton.frame = imageBgView.bounds;
    [imageBgView addSubview:imageTapButton];
    return imageBgView;
}

-(void)setIsSelectedImageArr:(NSArray *)isSelectedImageArr{
    _isSelectedImageArr = isSelectedImageArr;
    // 1. 删除所有内容
    [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i< isSelectedImageArr.count;i++){
        [weakSelf createCustomerItemWithIndex:i callBlocak:^(UIView *imgView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.delegate imageSelectedDeleteClick:i imageView:imgView andSuperCell:strongSelf];
 
        }];
    }
    [self createButton];
}

#pragma mark - 添加按钮 
-(void)createButton{
    self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addButton setBackgroundColor:[UIColor clearColor]];
    [self.addButton setBackgroundImage:[UIImage imageNamed:@"center_img_add_icon"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [weakSelf.addButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(imageSelectedButtonClick)]){
            [strongSelf.delegate imageSelectedButtonClick];
        }

        
    }];
    [self.bgView addSubview:self.addButton];
    
    // frame
    CGFloat origin_x = kMarinWidth + (self.isSelectedImageArr.count % 3) * kTagWidthWithProductRelease + (self.isSelectedImageArr.count % 3) * kMarinWidth;
    CGFloat origin_y = LCFloat(5) + ( self.isSelectedImageArr.count / 3 ) * (kTagHeightWithProductRelease + LCFloat(10));
    CGRect tempRect = CGRectMake(origin_x + (kTagWidthWithProductRelease - LCFloat(60)) / 2. , origin_y +  (kTagWidthWithProductRelease - LCFloat(60)) / 2., LCFloat(60),LCFloat(60));
    self.addButton.frame = tempRect;

}

+(CGFloat)calculationCellHeightWithImgArr:(NSArray *)isSelectedImageArr{
    CGFloat cellHeight = 0;
    
    NSInteger typeCount = isSelectedImageArr.count + 1;
    NSInteger excessNumber = (isSelectedImageArr.count + 1) % 3 == 0 ? 0 : 1;
    cellHeight += LCFloat(5);
    cellHeight += (typeCount / 3 + excessNumber) * (LCFloat(10) + kTagHeightWithProductRelease);
    cellHeight += LCFloat(5);
    return cellHeight;
}

//- ( UIImage *)getImageByCuttingImage:( UIImage *)image Rect:( CGRect )rect{
//    CGRect myImageRect = rect;
//    UIImage * bigImage= image;
//    CGImageRef imageRef = bigImage. CGImage ;
//    CGImageRef subImageRef = CGImageCreateWithImageInRect (imageRef, myImageRect);
//    CGSize size;
//    size. width = rect. size . width ;
//    size. height = rect. size . height ;
//    UIGraphicsBeginImageContext (size);
//    CGContextRef context = UIGraphicsGetCurrentContext ();
//    CGContextDrawImage (context, myImageRect, subImageRef);
//    UIImage * smallImage = [ UIImage imageWithCGImage :subImageRef];
//    UIGraphicsEndImageContext ();
//    return smallImage;
//}

@end

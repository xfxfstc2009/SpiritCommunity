//
//  GWWeatherTodayOneCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/9.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "GWWeatherTodayOneCell.h"
#import "GWWeatherConditionView.h"

@interface GWWeatherTodayOneCell()
@property (nonatomic,strong)UIVisualEffectView *effectview;
@property (nonatomic,strong)PDImageView *weatherImgView;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *dayLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *wenduLabel;
@property (nonatomic,strong)UIView *lineView2;
@property (nonatomic,strong)UIView *lineView3;
@property (nonatomic,strong)UILabel *locationCityLabel;
@property (nonatomic,strong)UILabel *locationTownLabel;
@property (nonatomic,strong)UIView *lineView4;
@property (nonatomic,strong)UIView *lineView5;
@property (nonatomic,strong)UIView *lineView6;
@property (nonatomic,strong)GWWeatherConditionView *view1;
@property (nonatomic,strong)GWWeatherConditionView *view2;
@property (nonatomic,strong)GWWeatherConditionView *view3;
@property (nonatomic,strong)UIView *lineView7;
@end

@implementation GWWeatherTodayOneCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
     self.effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
    self.effectview.alpha = .8f;
    [self addSubview:self.effectview];
    
    // timeLabel
    self.timeLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    [self addSubview:self.timeLabel];
    
    // 星期天
    self.dayLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    [self addSubview:self.dayLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.lineView];
    
    // 创建温度
    self.wenduLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.wenduLabel.font = [UIFont systemFontOfCustomeSize:50.];
    self.wenduLabel.adjustsFontSizeToFitWidth = YES;
    self.wenduLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.wenduLabel];
    
    // 线条2
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.lineView2];
    
    // 3.地区
    self.locationCityLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.locationCityLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.locationCityLabel];
    
    // 4.
    self.locationTownLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.locationTownLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.locationTownLabel];
    
    self.lineView3 = [[UIView alloc]init];
    self.lineView3.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.lineView3];
    
    // 6.
    self.lineView4 = [[UIView alloc]init];
    self.lineView4.backgroundColor = [UIColor whiteColor];
    self.lineView4.alpha = .7f;
    [self addSubview:self.lineView4];
    
    self.lineView5 = [[UIView alloc]init];
    self.lineView5.backgroundColor = [UIColor whiteColor];
    self.lineView5.alpha = .7f;
    [self addSubview:self.lineView5];
    
    self.lineView6 = [[UIView alloc]init];
    self.lineView6.backgroundColor = [UIColor whiteColor];
    self.lineView6.alpha = .7f;
    [self addSubview:self.lineView6];
    
    self.lineView7 = [[UIView alloc]init];
    self.lineView7.backgroundColor = [UIColor whiteColor];
    self.lineView7.alpha = .7f;
    [self addSubview:self.lineView7];

    self.weatherImgView = [[PDImageView alloc]init];
    self.weatherImgView.backgroundColor = [UIColor clearColor];
    self.weatherImgView.image = [UIImage imageNamed:@"2"];
    [self addSubview:self.weatherImgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferCurrentModel:(GWWeatherCurrentModel *)transferCurrentModel{
    _transferCurrentModel = transferCurrentModel;
    
    self.effectview.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
    
    // 1. 时间
    
    NSArray *timeArr = [transferCurrentModel.date componentsSeparatedByString:@"-"];
    self.timeLabel.text = [NSString stringWithFormat:@"%@/%@",[timeArr objectAtIndex:1],[timeArr objectAtIndex:2]];
    CGSize timeSize = [Tool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(kScreenBounds.size.width - 3 * LCFloat(11) - timeSize.width, LCFloat(11), timeSize.width, [NSString contentofHeightWithFont:self.timeLabel.font]);
    
    // 2. 日期
    self.dayLabel.text = transferCurrentModel.week;
    CGSize daySize = [Tool makeSizeWithLabel:self.dayLabel];
    self.dayLabel.frame = CGRectMake(kScreenBounds.size.width - 3 * LCFloat(11) - daySize.width, CGRectGetMaxY(self.timeLabel.frame) + LCFloat(7), daySize.width, [NSString contentofHeightWithFont:self.dayLabel.font]);

    // 3. line
    self.lineView.frame = CGRectMake(kScreenBounds.size.width / 2., CGRectGetMaxY(self.dayLabel.frame) + LCFloat(7), kScreenBounds.size.width / 2. - 3 * LCFloat(11), .5f);
    self.lineView.alpha = .7f;
    
    // 4. 温度
    self.wenduLabel.text = transferCurrentModel.temperature;
    self.wenduLabel.frame = CGRectMake(kScreenBounds.size.width / 2., CGRectGetMaxY(self.lineView.frame), self.lineView.size_width, LCFloat(70));

    // 5. line2
    self.lineView2.frame = CGRectMake(self.lineView.orgin_x, CGRectGetMaxY(self.wenduLabel.frame), self.lineView.size_width, .5f);
    
    // 6. 城市
    self.locationCityLabel.text = transferCurrentModel.city;
    self.locationCityLabel.frame = CGRectMake(self.lineView.orgin_x, CGRectGetMaxY(self.lineView2.frame) + LCFloat(11), self.lineView2.size_width / 2., [NSString contentofHeightWithFont:self.locationCityLabel.font]);
    self.locationCityLabel.adjustsFontSizeToFitWidth = YES;
    self.locationCityLabel.textAlignment = NSTextAlignmentCenter;
    
    // 7.区县
    if ([transferCurrentModel.distrct isEqualToString:transferCurrentModel.city]){
        self.locationTownLabel.text = transferCurrentModel.province;
    } else {
        self.locationTownLabel.text = transferCurrentModel.distrct;
    }
    
    self.locationTownLabel.frame = CGRectMake(CGRectGetMaxX(self.locationCityLabel.frame),CGRectGetMaxY(self.lineView2.frame) + LCFloat(11), self.locationCityLabel.size_width, [NSString contentofHeightWithFont:self.locationTownLabel.font]);
    self.locationTownLabel.textAlignment = NSTextAlignmentCenter;
    
    self.lineView3.frame = CGRectMake(self.lineView2.orgin_x + self.lineView2.size_width / 2., CGRectGetMaxY(self.lineView2.frame) + LCFloat(3), .5f, 2 * LCFloat(8) + [NSString contentofHeightWithFont:self.locationCityLabel.font]);
    self.lineView3.alpha = .7f;
    
    self.lineView4.frame = CGRectMake(self.lineView2.orgin_x - 5 * LCFloat(11), CGRectGetMaxY(self.lineView3.frame) + LCFloat(3), self.lineView2.size_width + 5 * LCFloat(11), .5f);
    self.lineView4.alpha = .7f;
    
    // view1
    if (!self.view1){
        self.view1 = [[GWWeatherConditionView alloc]initWithFrame:CGRectMake(self.lineView4.orgin_x, CGRectGetMaxY(self.lineView4.frame), self.lineView4.size_width / 3., self.lineView2.size_width / 3.) withFixed:@"空气质量" dymicString:@""];
        [self addSubview:self.view1];
    }
    if (!self.view2){
        self.view2 = [[GWWeatherConditionView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.view1.frame), self.view1.orgin_y, self.view1.size_width, self.view1.size_height) withFixed:@"湿度" dymicString:@""];
        [self addSubview:self.view2];
    }
    if (!self.view3){
        self.view3 = [[GWWeatherConditionView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.view2.frame), self.view2.orgin_y, self.view2.size_width, self.view2.size_height) withFixed:@"风向" dymicString:@""];
        [self addSubview:self.view3];
    }
    self.view1.transferDymicStr = transferCurrentModel.airCondition;
    self.view2.transferDymicStr = transferCurrentModel.humidity;
    self.view3.transferDymicStr = transferCurrentModel.wind;
    
    self.lineView5.frame = CGRectMake(CGRectGetMaxX(self.view1.frame),CGRectGetMaxY(self.lineView4.frame) + LCFloat(3), .5f, self.view1.size_height - 2 * LCFloat(3));
    self.lineView6.frame = CGRectMake(CGRectGetMaxX(self.view2.frame), self.lineView5.orgin_y, .5f, self.lineView5.size_height);
    
    self.lineView7.frame = CGRectMake(0, CGRectGetMaxY(self.view2.frame) + 1, kScreenBounds.size.width, .5f);
    
    self.weatherImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(160), LCFloat(160));
    self.weatherImgView.backgroundColor = [UIColor clearColor];
    
    CGFloat margin = 2 * LCFloat(11);
    CGFloat max_w = self.lineView.orgin_x - 2 * margin;
    CGFloat max_h = self.lineView4.orgin_y - 2 * margin;
    self.weatherImgView.frame = CGRectMake(margin, margin, MIN(max_h, max_w), MIN(max_h, max_w));
    self.weatherImgView.center_x = self.lineView.orgin_x / 2.;
}

+(CGFloat)calculationCellheight:(GWWeatherCurrentModel *)singleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(7);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(7);
    cellHeight += LCFloat(70);
    cellHeight += .5f;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += LCFloat(11);
    cellHeight += (kScreenBounds.size.width / 2. - 3 * LCFloat(11)) / 3.;
    return cellHeight;
}

@end

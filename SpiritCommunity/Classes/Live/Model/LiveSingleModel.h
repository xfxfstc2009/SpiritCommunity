//
//  LiveSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/27.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "LiveShowUserSingleModel.h"

@interface LiveSingleModel : FetchModel

@property (nonatomic,copy)NSString *house_id;               /**< 直播房间编号*/
@property (nonatomic,assign)BOOL show_location;             /**< 是否显示地址*/
@property (nonatomic,assign)BOOL show_status;               /**< 直播状态*/
@property (nonatomic,assign)BOOL show_type;                 /**< 直播类型 1 直播 2 一对一*/
@property (nonatomic,assign)NSTimeInterval time;            /**< 时间*/
@property (nonatomic,copy)NSString *title;                  /**< 标题*/
@property (nonatomic,strong)LiveShowUserSingleModel *show_user;/**< 直播用户信息*/



@end

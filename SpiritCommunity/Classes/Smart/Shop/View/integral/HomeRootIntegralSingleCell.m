//
//  HomeRootIntegralSingleCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/11.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "HomeRootIntegralSingleCell.h"

@interface HomeRootIntegralSingleCell()
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *integralLabel;
@property (nonatomic,strong)UIView *duihuanButton;

@end

@implementation HomeRootIntegralSingleCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark -createView
-(void)createView{
    // 1. convert
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.clipsToBounds = YES;
    [self addSubview:self.convertView];
    
    // 2. img
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.imgView];
    
    // 3. 创建标题
    self.nameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"灰"];
    [self addSubview:self.nameLabel];
    
    // 4. 创建积分
    self.integralLabel = [GWViewTool createLabelFont:@"标题" textColor:@"棕"];
    [self.integralLabel.font boldFont];
    [self addSubview:self.integralLabel];

    // 5. 按钮
    self.duihuanButton = [GWViewTool goButtonWithTitle:@"立即兑换" block:NULL];
    self.duihuanButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - self.duihuanButton.size_width, 0, self.duihuanButton.size_width, self.duihuanButton.size_height);
    [self addSubview:self.duihuanButton];
}


-(void)setTransferCellheight:(CGFloat)transferCellheight{
    _transferCellheight = transferCellheight;
}

-(void)setTransferSingleModel:(HomeIntegralSingleModel *)transferSingleModel{
    // 1. convert
//    self.convertView.frame = CGRectMake(LCFloat(11), LCFloat(11), (self.transferCellheight - 2 * LCFloat(11)), (self.transferCellheight - 2 * LCFloat(11)));
//
//    // 2. img
//    __weak typeof(self)weakSelf = self;
//    [self.imgView uploadImageWithURL:transferSingleModel.img_path placeholder:nil callback:^(UIImage *image) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [GWTool setTransferSingleAsset:image imgView:strongSelf.imgView convertView:strongSelf.convertView];
//    }];
//
//    // 2. 标题
//    CGFloat width = kScreenBounds.size.width - self.transferCellheight - LCFloat(11);
//    self.nameLabel.text = transferSingleModel.title;
//    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
//    if (nameSize.height >= 2 * [NSString contentofHeight:self.nameLabel.font]){
//        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, width, 2 * [NSString contentofHeight:self.nameLabel.font]);
//        self.nameLabel.numberOfLines = 2;
//    } else {
//        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, width, nameSize.height);
//        self.nameLabel.numberOfLines = 0;
//    }
//
//    // 3. 积分
//    self.integralLabel.text = [NSString stringWithFormat:@"%li积分",transferSingleModel.integral];
//    self.integralLabel.adjustsFontSizeToFitWidth = YES;
//    CGSize integralSize = [@"15000积分" sizeWithCalcFont:self.integralLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.integralLabel.font])];
//    self.integralLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), CGRectGetMaxY(self.convertView.frame) - [NSString contentofHeight:self.integralLabel.font], integralSize.width, [NSString contentofHeight:self.integralLabel.font]);
//
//    // 4.
//    self.duihuanButton.center_y = self.integralLabel.center_y;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(125);
}


@end

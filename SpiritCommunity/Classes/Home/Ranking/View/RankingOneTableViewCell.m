//
//  RankingOneTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "RankingOneTableViewCell.h"
#import <pop/POP.h>
#import "StringAttributeHelper.h"
#import "GWPulseLayer.h"

@interface RankingOneTableViewCell()
@property (nonatomic,strong)UIView *avatarBgView;
@property (nonatomic,strong)PDImageView *avatarbImgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *levelLabel;
@property (nonatomic,strong)UILabel *moneyLabel;
@property (nonatomic,strong)UIButton *guanzhuButton;
@property (nonatomic,strong)PDImageView *wangguanImgView;
@property (nonatomic,strong)GWPulseLayer *pulseLayer;
@property (nonatomic,strong)PDImageView *lineImgView;

@end

static char guanzhuButtonClickKey;
@implementation RankingOneTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.avatarBgView = [[UIView alloc]init];
    [self.avatarBgView.layer addSublayer:[self haloLayer]];
    
    [self addSubview:self.avatarBgView];
    
    // avatar_bg
    self.avatarbImgView = [[PDImageView alloc]init];
    self.avatarbImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarbImgView];

    // 1. avatar
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self.avatarbImgView addSubview:self.avatarImgView];


    
    // 2. name
    self.nameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    [self addSubview:self.nameLabel];
    
    // 3. 创建money
    self.moneyLabel = [GWViewTool createLabelFont:@"标题" textColor:@"红"];
    [self addSubview:self.moneyLabel];
    
    // 4. 创建关注按钮
    self.guanzhuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.guanzhuButton.hidden = YES;
    [self addSubview:self.guanzhuButton];
    
    self.wangguanImgView = [[PDImageView alloc]init];
    self.wangguanImgView.image = [UIImage imageNamed:@"home_ranking_top1"];
    self.wangguanImgView.frame = CGRectMake(0, 0, LCFloat(20), LCFloat(20));
    [self addSubview:self.wangguanImgView];
    self.wangguanImgView.hidden = YES;

    self.lineImgView = [[PDImageView alloc]init];
    self.lineImgView.image = [UIImage imageNamed:@"ig_lv_bg_17"];
    [self addSubview:self.lineImgView];
}

-(void)setTransferSingleModel:(RankingSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;

    self.avatarbImgView.image = [UIImage imageNamed:@"ig_leaderboard_on_air_circle"];
    self.avatarbImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(78)) / 2., LCFloat(7), LCFloat(78), LCFloat(78));

    self.avatarImgView.frame = CGRectMake(0,0,LCFloat(70),LCFloat(70));
    self.avatarImgView.center = CGPointMake(self.avatarbImgView.size_width / 2., self.avatarbImgView.size_height / 2.);
    self.avatarBgView.frame = self.avatarbImgView.frame;
    
    [self.avatarImgView uploadImageWithAvatarURL:transferSingleModel.userModel.avatar placeholder:nil callback:NULL];
    [self changeLayer];

    // line
    self.lineImgView.frame = CGRectMake(0, 0, 424 * .3f, 110 * .3f);
    self.lineImgView.orgin_y = CGRectGetMaxY(self.avatarImgView.frame) - self.lineImgView.size_height / 2. + 10;
    self.lineImgView.center_x = kScreenBounds.size.width / 2.;

    // 网管
    self.wangguanImgView.frame = CGRectMake(self.avatarImgView.center_x - self.avatarImgView.size_width / 4. - LCFloat(10), self.avatarImgView.center_y - self.avatarImgView.size_height / 4. - LCFloat(20), LCFloat(20), LCFloat(20));
    
    // 2. 创建名字
    self.nameLabel.text = transferSingleModel.userModel.nick;
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.lineImgView.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // 3. 创建价格
    self.moneyLabel.text = [NSString stringWithFormat:@"%li",transferSingleModel.count];
    self.moneyLabel.textAlignment = NSTextAlignmentCenter;
    self.moneyLabel.frame= CGRectMake(0, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.moneyLabel.font]);
    
    // 4. 创建关注按钮
    if (transferSingleModel.userModel.link){
        [self.guanzhuButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu_hlt"] forState:UIControlStateNormal];
    } else {
        [self.guanzhuButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu_nor"] forState:UIControlStateNormal];
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf.guanzhuButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(RankingSingleModel *transferSingleModel) = objc_getAssociatedObject(strongSelf, &guanzhuButtonClickKey);
        if(block){
            block(transferSingleModel);
        }
    }];
    
    self.guanzhuButton.frame = CGRectMake(0, CGRectGetMaxY(self.moneyLabel.frame) + LCFloat(11), LCFloat(20), LCFloat(20));
    self.guanzhuButton.center_x = kScreenBounds.size.width / 2.;


}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(50);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"标题"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(30);
    cellHeight += LCFloat(11);
    return cellHeight;
}

-(void)loadContentWithIndex:(NSInteger)index{
    NSString *fullStirng = [NSString stringWithFormat:@"%02ld", (long)index];
    NSMutableAttributedString *richString = [[NSMutableAttributedString alloc] initWithString:fullStirng];
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont HeitiSCWithFontSize:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, richString.length);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont fontWithName:@"GillSans-Italic" size:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, 3);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [[UIColor blackColor] colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, richString.length);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [UUBlue colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, 3);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    self.nameLabel.attributedText = richString;
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (self.highlighted){
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration           = 0.1f;
        scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
        [self.moneyLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    } else {
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        scaleAnimation.springBounciness    = 20.f;
        [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        [self.moneyLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    }
}


#pragma mark 飘动过去Animation
- (void)startAnimationWithDelay:(CGFloat)delayTime {
    self.nameLabel.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    self.moneyLabel.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.nameLabel.transform = CGAffineTransformIdentity;
        self.moneyLabel.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
}

-(void)guanzhuButtonClick:(void(^)(RankingSingleModel *transferSingleModel))block{
    objc_setAssociatedObject(self, &guanzhuButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



- (CALayer *)haloLayer {
    if(!_pulseLayer) {
        _pulseLayer = [GWPulseLayer layer];
    }
    return _pulseLayer;
}

-(void)changeLayer{
    _pulseLayer.bounds = CGRectMake(0, 0, LCFloat(120), LCFloat(120));
    _pulseLayer.position = CGPointMake(self.avatarbImgView.size_width / 2., self.avatarbImgView.size_height / 2.);
    _pulseLayer.contentsScale = [UIScreen mainScreen].scale;
    _pulseLayer.radius = LCFloat(70);
//    [self.avatarImgView.layer addSublayer:_pulseLayer];

}

@end

//
//  GWDropListView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWDropListView.h"
#import "DropListSingleCell.h"
#import <objc/runtime.h>


static char chooseKey;
@interface GWDropListView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSArray *listArr;
@property (nonatomic,strong)NSMutableArray *selectedMutableArr;             /**< 选中的数据源*/
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation GWDropListView

-(instancetype)init{
    self = [super init];
    if (self){
        self.backgroundColor = [UIColor colorWithWhite:0. alpha:.1f];
    }
    return self;
}

-(void)showInViewController:(UIViewController *)controller frame:(CGRect)listFrame listArr:(NSArray *)listArr block:(void(^)(NSString *chooseInfo))block{
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    objc_setAssociatedObject(self, &chooseKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    _listArr = listArr;
    self.frame = keywindow.bounds;
    
    CGFloat height = (listArr.count >= 5) ? 5 * 44 : _listArr.count * 44;
    CGFloat startY = listFrame.origin.y >= (keywindow.frame.size.height / 2) ? listFrame.origin.y : listFrame.origin.y + listFrame.size.height;
    CGFloat endY = listFrame.origin.y >= (keywindow.frame.size.height / 2) ? listFrame.origin.y - height : listFrame.origin.y + listFrame.size.height;
    
     self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(listFrame.origin.x, startY, listFrame.size.width, 0)  style:UITableViewStylePlain];
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.tableView.frame = CGRectMake(listFrame.origin.x, endY, listFrame.size.width, height);
    } completion:nil];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.scrollEnabled = YES;

    [self addSubview:self.tableView];
    
    self.selectedMutableArr = [NSMutableArray array];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _listArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    DropListSingleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[DropListSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    cellWithRowOne.transferFixedStr = [self.listArr objectAtIndex:indexPath.row];
    
    if ([self.selectedMutableArr containsObject:[self.listArr objectAtIndex:indexPath.row]]){
        [cellWithRowOne setChecked: YES];
    } else {
        [cellWithRowOne setChecked: NO];
    }
    
    
    return cellWithRowOne;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(50);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.selectedMutableArr.count){
        [self.selectedMutableArr removeAllObjects];
    }
    NSString *info = [self.listArr objectAtIndex:indexPath.row];
    if ([self.selectedMutableArr containsObject:info]){
        [self.selectedMutableArr addObject:info];
    }
    for (DropListSingleCell *cell in self.tableView.visibleCells){
        [cell setChecked:NO];
    }
    DropListSingleCell *cell = (DropListSingleCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    [cell setChecked:YES];
    
    
    void(^block)(NSString *chooseInfo) = objc_getAssociatedObject(self, &chooseKey);
    if (block){
        block([self.listArr objectAtIndex:indexPath.row]);
    }
    
    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(clickCell:) userInfo:nil repeats:NO];
}


-(void)clickCell:(NSTimer*) timer {
    [self removeFromSuperview];
    [timer invalidate];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self removeFromSuperview];
}


@end

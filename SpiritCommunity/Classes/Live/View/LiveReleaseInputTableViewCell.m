//
//  LiveReleaseInputTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/18.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveReleaseInputTableViewCell.h"

static char actionInputWithTextKey;
@interface LiveReleaseInputTableViewCell()<UITextFieldDelegate>
@property (nonatomic,strong)UILabel *placeholderLabel;
@property (nonatomic,strong)UIView *lineView;

@end

@implementation LiveReleaseInputTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"标题"];
    self.inputTextField.delegate = self;
    [self.inputTextField addTarget:self action:@selector(inputTextFieldDidInput) forControlEvents:UIControlEventEditingChanged];
    self.inputTextField.textAlignment = NSTextAlignmentCenter;
    self.inputTextField.textColor = [UIColor whiteColor];
    [self addSubview:self.inputTextField];
    
    // placeholder
    self.placeholderLabel = [GWViewTool createLabelFont:@"标题" textColor:@"白"];
    self.placeholderLabel.text = @"起个好标题人气多更多";
    self.placeholderLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.placeholderLabel];
    
    // line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.lineView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    self.placeholderLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(44));
    self.inputTextField.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(44));
    self.lineView.frame = CGRectMake(LCFloat(11) * 3, CGRectGetMaxY(self.inputTextField.frame), kScreenBounds.size.width - 6 * LCFloat(11), 1);
}

-(void)inputTextFieldDidInput{
    if (self.inputTextField.text.length){
        self.placeholderLabel.hidden = YES;
    } else {
        self.placeholderLabel.hidden = NO;
    }
    void(^block)(NSString *str) = objc_getAssociatedObject(self, &actionInputWithTextKey);
    if(block){
        block(self.inputTextField.text);
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(51);
}

-(void)actionInputWithText:(void(^)(NSString *str))block{
    objc_setAssociatedObject(self, &actionInputWithTextKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferInfo:(NSString *)transferInfo{
    _transferInfo = transferInfo;
    self.inputTextField.text = transferInfo;
    if(transferInfo.length){
        self.placeholderLabel.hidden = YES;
    } else {
        self.placeholderLabel.hidden = NO;
    }
    
}

@end

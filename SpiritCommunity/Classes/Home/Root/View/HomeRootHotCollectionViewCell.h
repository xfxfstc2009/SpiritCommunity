//
//  HomeRootHotCollectionViewCell.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeLiveSingleModel.h"

@interface HomeRootHotCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)HomeLiveSingleModel *transferOnebyoneSingleModel;

@end

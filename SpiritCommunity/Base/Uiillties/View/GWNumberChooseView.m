//
//  GWNumberChooseView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWNumberChooseView.h"

static char numberKey;
@interface GWNumberChooseView()
@property (nonatomic,strong)UIView *backgroundView;          /**< 背景view*/
@property (nonatomic,strong)UIButton *leftButton;           /**< 左侧的按钮*/
@property (nonatomic,strong)UIButton *rightButton;          /**< 右侧的按钮*/
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UIView *line1;
@property (nonatomic,strong)UIView *line2;

@end

@implementation GWNumberChooseView

-(instancetype)initWithFrame:(CGRect)frame numberBlock:(void(^)(NSInteger number))block{
    self = [super initWithFrame:frame];
    if (self){
        objc_setAssociatedObject(self, &numberKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        self.currentNumber = 1;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建背景view
    self.backgroundView = [[UIView alloc]init];
    self.backgroundView.backgroundColor = [UIColor clearColor];
    self.backgroundView.frame = self.bounds;
    self.backgroundView.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
    self.backgroundView.layer.borderWidth = .5f;
    self.backgroundView.clipsToBounds = YES;
    self.backgroundView.layer.cornerRadius = LCFloat(3);
    [self addSubview:self.backgroundView];
    
    // 2. 计算宽度
    CGFloat margin_width = self.backgroundView.size_width / 3.;
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    self.leftButton.frame = CGRectMake(0, 0, margin_width, self.size_height);
    __weak typeof(self)weakSelf = self;
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.currentNumber -- ;
        if (strongSelf.currentNumber <= 1){
            strongSelf.currentNumber = 1;
            [strongSelf buttonStatusBtn:strongSelf.leftButton enable:NO];
        }
        void(^block)(NSInteger number) = objc_getAssociatedObject(strongSelf, &numberKey);
        if (block){
            block(strongSelf.currentNumber);
        }
    }];
    [self.leftButton setTitle:@"-" forState:UIControlStateNormal];
    [self buttonStatusBtn:self.leftButton enable:NO];
    [self.backgroundView addSubview:self.leftButton];
    
    // 3. 创建右侧的按钮
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.backgroundColor = [UIColor clearColor];
    [self.rightButton setTitle:@"+" forState:UIControlStateNormal];
    self.rightButton.frame = CGRectMake(self.size_width - margin_width, 0, margin_width, self.size_height);
    [self.rightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.currentNumber ++ ;
        void(^block)(NSInteger number) = objc_getAssociatedObject(strongSelf, &numberKey);
        if (block){
            block(strongSelf.currentNumber);
        }
        [strongSelf buttonStatusBtn:self.leftButton enable:YES];
    }];
    [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    [self.backgroundView addSubview:self.rightButton];
    
    // 4. 创建数字
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.frame = CGRectMake(margin_width, 0, margin_width, self.size_height);
    self.numberLabel.adjustsFontSizeToFitWidth = YES;
    self.numberLabel.text = [NSString stringWithFormat:@"%li",(long)self.currentNumber];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    [self.backgroundView addSubview:self.numberLabel];
    
    self.line1 = [[UIView alloc]init];
    self.line1.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
    [self.backgroundView addSubview:self.line1];
    self.line1.frame = CGRectMake(margin_width, 0, .5f, self.size_height);
    
    self.line2 = [[UIView alloc]init];
    self.line2.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
    [self.backgroundView addSubview:self.line2];
    self.line2.frame = CGRectMake(2 * margin_width, 0, .5f, self.size_height);
    
}


-(void)setCurrentNumber:(NSInteger)currentNumber{
    _currentNumber = currentNumber;
    self.numberLabel.text = [NSString stringWithFormat:@"%li",(long)self.currentNumber];
    
}

-(void)buttonStatusBtn:(UIButton *)button enable:(BOOL)enable{
    if(enable){
        button.userInteractionEnabled =YES;
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    } else {
        button.userInteractionEnabled = NO;
        [button setTitleColor:[UIColor colorWithCustomerName:@"白灰"] forState:UIControlStateNormal];
    }
}

@end

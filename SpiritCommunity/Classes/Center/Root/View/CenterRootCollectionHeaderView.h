//
//  CenterRootCollectionHeaderView.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/17.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterCustomerSingleModel.h"
@interface CenterRootCollectionHeaderView : UICollectionReusableView

@property (nonatomic,strong)AccountModel *transferAccountModel;

-(void)starAnimation;

-(void)actionClickBlock:(void(^)(CenterCustomerSingleModel *recordSingleModel))block;
-(void)actionClickWithHeader:(void(^)())block;                      // 点击头像
-(void)actionCLickWithPaihangbang:(void(^)())block;                 // 点击直播按钮
-(void)headerStatusWithLogined;

-(void)loginAnimationActionManager;             // 登录之后的动画

+(CGFloat)calculationCellHeight;

@end

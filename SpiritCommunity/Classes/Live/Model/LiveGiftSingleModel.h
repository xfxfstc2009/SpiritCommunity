//
//  LiveGiftSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/3.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol LiveGiftSingleModel <NSObject>

@end

@interface LiveGiftSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)NSInteger price;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,assign)NSInteger count;

@end

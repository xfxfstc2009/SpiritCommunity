//
//  GWMapViewSetupManager.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMapViewSetupManager.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>

@implementation GWMapViewSetupManager

+(void)authorizineMap{
    if ([[Tool getBundleID] isEqualToString:@"com.ppwhale.beetv"]){         // 蜜蜂
        [AMapServices sharedServices].apiKey = (NSString *)beeMapIdentify;
    } else if ([[Tool getBundleID] isEqualToString:@"com.liaoba.SpiritCommunity"]){ // 精灵社区
        [AMapServices sharedServices].apiKey = (NSString *)@"8ced77786f3ece5c298e996c79dfdddb";
    } else {
        [AMapServices sharedServices].apiKey = (NSString *)mapIdentify;
    }

    [AMapServices sharedServices].enableHTTPS = YES;

}


@end

//
//  OrderListSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "OrderListGoodsSingleModel.h"

@protocol OrderListSingleModel <NSObject>

@end

@interface OrderListSingleModel : FetchModel

@property (nonatomic,strong)NSArray<OrderListGoodsSingleModel> *goodSet;


@property (nonatomic,copy)NSString *address;
@property (nonatomic,copy)NSString *area;
@property (nonatomic,copy)NSString *city;
@property (nonatomic,copy)NSString *country;
@property (nonatomic,assign)NSTimeInterval createTime;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *logisticsId;
@property (nonatomic,copy)NSString *orderSn;
@property (nonatomic,assign)NSInteger orderStatus;
@property (nonatomic,copy)NSString *payJifen;
@property (nonatomic,assign)CGFloat payMoney;
@property (nonatomic,copy)NSString *payUb;
@property (nonatomic,copy)NSString *phoneNum;
@property (nonatomic,copy)NSString *province;
@property (nonatomic,copy)NSString *receiveName;
@property (nonatomic,copy)NSString *shippingFee;
@property (nonatomic,copy)NSString *shippingType;
@property (nonatomic,copy)NSString *supplyName;


@end

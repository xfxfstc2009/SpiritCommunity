//
//  CenterRootCollectionHeaderView.m
//  17Live
//
//  Created by 裴烨烽 on 2018/1/17.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "CenterRootCollectionHeaderView.h"
#import "CenterRootCustomerSingleView.h"
#import "CenterRootTopHeaderView.h"

static char actionClickBlockKey;
static char actionClickWithHeaderKey;
static char actionCLickWithPaihangbangKey;

@interface CenterRootCollectionHeaderView()
@property(nonatomic,strong)UILabel *nameLabel;                  /**< 名字*/
@property (nonatomic,strong)UILabel *descLabel;                 /**< desc*/
@property (nonatomic,strong)UIButton *button;
@property (nonatomic,strong)PDImageView *leftImgView;
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic,strong)NSMutableArray *bottomMutableArr;
@property (nonatomic,strong)UIButton *paihangbangButton;
@property (nonatomic,strong)PDImageView *liwuImgView;
@property (nonatomic,strong)UILabel *liwuLabel;
@end

@implementation CenterRootCollectionHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{

    // 2. 创建名字
    self.nameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.nameLabel];

    // 3. 创建desc
    self.descLabel = [GWViewTool createLabelFont:@"提示" textColor:@"黑"];
    [self addSubview:self.descLabel];

    // 4. 创建星星
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [self.button buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithHeaderKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.button];

    // 5. 创建desc

    self.leftImgView = [[PDImageView alloc]init];
    self.leftImgView.frame = CGRectMake(0 - LCFloat(20), LCFloat(21), LCFloat(40), LCFloat(40));
    self.leftImgView.image = [Tool stretchImageWithName:@"center_header_left_btn"];
    [self addSubview:self.leftImgView];

    // 6.底部的view
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.bottomView.frame = CGRectMake(0, self.size_height - LCFloat(50), kScreenBounds.size.width, LCFloat(50));
    [self addSubview:self.bottomView];

    self.bottomMutableArr = [NSMutableArray array];

    // 7. 创建礼物按钮
    self.liwuImgView = [[PDImageView alloc]init];
    self.liwuImgView.image = [UIImage imageNamed:@"center_header_zhibo_btn"];
    self.liwuImgView.frame = CGRectMake(LCFloat(20) + LCFloat(7), 0, LCFloat(20), LCFloat(20));
    self.liwuImgView.center_y = LCFloat(20);
    [self.leftImgView addSubview:self.liwuImgView];

    // 8. 排行榜
    self.liwuLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.liwuLabel.text = @"我要直播";
    CGSize liwuSize = [NSString makeSizeWithLabel:self.liwuLabel];
    self.liwuLabel.frame = CGRectMake(CGRectGetMaxX(self.liwuImgView.frame) + LCFloat(5), 0, liwuSize.width, liwuSize.height);
    self.liwuLabel.center_y = self.liwuImgView.center_y;
    [self.leftImgView addSubview:self.liwuLabel];

    self.liwuImgView.alpha = 0;
    self.liwuLabel.alpha = 0;

    // 9 .创建按钮
    self.paihangbangButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.paihangbangButton.frame = CGRectMake(0, 0, LCFloat(100), LCFloat(50));
    self.paihangbangButton.center_y = self.liwuImgView.center_y;
    [self.paihangbangButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionCLickWithPaihangbangKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.paihangbangButton];
}


-(void)setTransferAccountModel:(AccountModel *)transferAccountModel{
    _transferAccountModel = transferAccountModel;

//    // avatar;
//    self.avatarImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(60)) / 2., LCFloat(20), LCFloat(60), LCFloat(60));
//    if (transferAccountModel.loginModel.avatar.length){
//        [self.avatarImgView uploadImageWithAvatarURL:transferAccountModel.loginModel.avatar placeholder:nil callback:NULL];
//    } else {
//        self.avatarImgView.image = [UIImage imageNamed:@"center_header_nor_icon"];
//    }


    // 2. 名字
    if (transferAccountModel.loginModel.nick.length){
        self.nameLabel.text = transferAccountModel.loginModel.nick;
    } else {
        if ([AccountModel sharedAccountModel].loginModel.ID.length && [AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeNor){
            if ([AccountModel sharedAccountModel].loginModel.phone.length){
                self.nameLabel.text = [NSString stringWithFormat:@"精灵%@",[AccountModel sharedAccountModel].loginModel.phone];
            } else if ([AccountModel sharedAccountModel].loginModel.ID.length){
                self.nameLabel.text = [NSString stringWithFormat:@"精灵%@",[AccountModel sharedAccountModel].loginModel.ID];
            }
        } else {
            self.nameLabel.text = @"注册/登录";
        }
    }

//    // 3. 头像
//    self.button.frame = CGRectMake(self.avatarImgView.orgin_x, self.avatarImgView.orgin_y, self.avatarImgView.size_width, self.avatarImgView.size_height + LCFloat(11) + LCFloat(20));
//
//
    self.nameLabel.frame = CGRectMake(0, kCenterTopHeaderAvatar / 2. + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.nameLabel.font]);

    // 3. 创建desc
    self.descLabel.text = @"";
    CGSize descSize = [NSString makeSizeWithLabel:self.descLabel];
    if (descSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        self.descLabel.frame = CGRectMake(3 * LCFloat(11), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), kScreenBounds.size.width - 6 * LCFloat(11), descSize.height);
        self.descLabel.numberOfLines = 0;
        self.descLabel.textAlignment = NSTextAlignmentLeft;
    } else {
        self.descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), descSize.height);
        self.descLabel.numberOfLines = 1;
        self.descLabel.textAlignment = NSTextAlignmentCenter;
    }

    if (self.bottomMutableArr.count){
        [self.bottomMutableArr removeAllObjects];
    }

    NSInteger count = 0;
    if ([AccountModel sharedAccountModel].isShenhe){
        count = 2;
    } else {
        count = 3;
    }
    for (int i = 0 ; i < count; i ++ ){
        CenterCustomerSingleModel *recordSingleModel = [[CenterCustomerSingleModel alloc]init];
        if (i == 0){
            recordSingleModel.fixedStr = @"我的粉丝";
            recordSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.fans_count];
        } else if (i == 1){
            recordSingleModel.fixedStr = @"关注中";
            recordSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.link_count];
        } else if (i == 2){
            recordSingleModel.fixedStr = @"我的金币";
            recordSingleModel.dymicStr = [NSString stringWithFormat:@"%li",(long)[AccountModel sharedAccountModel].loginModel.money];
        }
        [self.bottomMutableArr addObject:recordSingleModel];
    }


    // 4. 创建底部的View
    if (self.bottomView){
        [self.bottomView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

        // 1. 计算宽度
        CGFloat kmargin_size = (kScreenBounds.size.width - (self.bottomMutableArr.count - 1) * .5f) / self.bottomMutableArr.count;

        for (int i = 0 ;i < self.bottomMutableArr.count;i++){
            CenterCustomerSingleModel *recordSingleModel = [self.bottomMutableArr objectAtIndex:i];

            // origin_x
            CGFloat origin_x = i * (kmargin_size + .5f);
            // 1.
            CenterRootCustomerSingleView *recordSingleView = [[CenterRootCustomerSingleView alloc]initWithFrame:CGRectMake(origin_x, 0, kmargin_size, LCFloat(50))];
            recordSingleView.transferCustomerSingleMode = recordSingleModel;
            recordSingleView.backgroundColor = [UIColor clearColor];
            [self.bottomView addSubview:recordSingleView];

            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = recordSingleView.bounds;
            __weak typeof(self)weakSelf = self;
            [button buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(CenterCustomerSingleModel *recordSingleModel) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
                if (block){
                    block(recordSingleModel);
                }
            }];
            [recordSingleView addSubview:button];

            // 2. 创建竖线
            CGFloat origin_line_x = i * kmargin_size;

            UIView *lineView = [[UIView alloc]init];
            lineView.backgroundColor = [UIColor grayColor];
            lineView.frame = CGRectMake(origin_line_x, LCFloat(5), .5f, (LCFloat(50) - 2 * LCFloat(5)));
            [self.bottomView addSubview:lineView];
        }
    }
}


-(void)starAnimation{
    if (![AccountModel sharedAccountModel].isShenhe){
        [UIView animateWithDuration:.7f animations:^{
            self.leftImgView.frame = CGRectMake(0 - LCFloat(20), LCFloat(21), LCFloat(120), LCFloat(40));
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:.3f animations:^{
                self.liwuLabel.alpha = 1;
                self.liwuImgView.alpha = 1;
            } completion:^(BOOL finished) {
                self.liwuLabel.alpha = 1;
                self.liwuImgView.alpha = 1;
            }];
        }];
    } else {
        self.liwuImgView.hidden = YES;
        self.liwuLabel.hidden = YES;
        self.paihangbangButton.hidden = YES;
        self.leftImgView.hidden = YES;
    }
}

-(void)actionClickBlock:(void(^)(CenterCustomerSingleModel *recordSingleModel))block{
    objc_setAssociatedObject(self, &actionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithHeader:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithHeaderKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionCLickWithPaihangbang:(void(^)())block{
    objc_setAssociatedObject(self, &actionCLickWithPaihangbangKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)headerStatusWithLogined{

}

-(void)guanzhuStatus:(BOOL)status{

}

-(void)loginAnimationActionManager{
    self.transferAccountModel = [AccountModel sharedAccountModel];
}



+(CGFloat)calculationCellHeight{
    return LCFloat(200);
}

@end

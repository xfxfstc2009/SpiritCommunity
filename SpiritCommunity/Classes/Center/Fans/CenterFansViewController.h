//
//  CenterFansViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

// 粉丝
#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,CenterFansViewControllerType) {
    CenterFansViewControllerTypeFans,               /**< 我的粉丝*/
    CenterFansViewControllerTypeLink,               /**< 我的关注*/
};

@interface CenterFansViewController : AbstractViewController

@property (nonatomic,assign)CenterFansViewControllerType transferType;

@end

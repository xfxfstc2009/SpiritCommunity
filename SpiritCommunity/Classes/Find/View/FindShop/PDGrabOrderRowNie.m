//
//  PDGrabOrderRowNie.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDGrabOrderRowNie.h"

@interface PDGrabOrderRowNie()
@property (nonatomic,strong)PDImageView *backgroundImgView;
@property (nonatomic,strong)PDImageView *inputView;
@property (nonatomic,strong)UILabel *inputLabel;

@end

@implementation PDGrabOrderRowNie

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backgroundImgView = [[PDImageView alloc]init];
    self.backgroundImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.backgroundImgView];

    // 2. 创建输入背景
    self.inputView = [[PDImageView alloc]init];
    self.inputView.backgroundColor = [UIColor clearColor];
    self.inputView.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * (LCFloat(3) + LCFloat(8)), 10);
    self.inputView.image = [Tool stretchImageWithName:@"icon_orderDetail_bg"];
    [self addSubview:self.inputView];

    // 3. 创建输入信息
    self.inputLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.inputLabel.numberOfLines = 0;
    self.inputLabel.text = @"";
    [self addSubview:self.inputLabel];
}

-(void)setTransferBeizhuInfo:(NSString *)transferBeizhuInfo{
    _transferBeizhuInfo = transferBeizhuInfo;

    self.inputLabel.text = transferBeizhuInfo;
    CGFloat width = kScreenBounds.size.width - 2 * (LCFloat(3) + LCFloat(8) + LCFloat(11));
    CGSize inputLabelSize = [self.inputLabel.text sizeWithCalcFont:self.inputLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.inputLabel.frame = CGRectMake(LCFloat(8) + LCFloat(3) + LCFloat(11), LCFloat(11), width, inputLabelSize.height);

    // 2. input
    self.inputView.frame = CGRectMake(LCFloat(8) + LCFloat(3), 0, kScreenBounds.size.width - 2 * LCFloat(3 + 8), inputLabelSize.height + 2 * LCFloat(11));

    self.backgroundImgView.frame = CGRectMake(LCFloat(8), 0, kScreenBounds.size.width - 2 * LCFloat(8), self.inputView.size_height + LCFloat(11));
    self.backgroundImgView.image = [Tool stretchImageWithName:@"grabOrder_background_3"];

}


+(CGFloat)calculationCellHeightWithInfo:(NSString *)info{

    CGFloat width = kScreenBounds.size.width - 2 * (LCFloat(3) + LCFloat(8) + LCFloat(11));
    CGSize inputLabelSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];

    CGFloat cellHeight = 0;
    cellHeight += inputLabelSize.height + 3 * LCFloat(11);
    return cellHeight;
}
@end

//
//  FindRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FindRootViewController.h"
#import "FindRootTableViewCell.h"
#import "FindRootListModel.h"

@interface FindRootViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *findTableView;
@property (nonatomic,strong)NSMutableArray *findArr;

@end

@implementation FindRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestTogetMenuList];
}

#pragma mark - 发现
-(void)pageSetting{
    self.barMainTitle = @"发现";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.findArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.findTableView){
        self.findTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.findTableView.dataSource = self;
        self.findTableView.delegate = self;
        [self.view addSubview:self.findTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.findTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestTogetMenuList];
    }];

    [self.findTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestTogetMenuList];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.findArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    FindRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[FindRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    FindRootSingleModel *findRootSingleModel = [self.findArr objectAtIndex:indexPath.section];
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferIndexPath = indexPath.section;
    cellWithRowOne.transferSingleModel = findRootSingleModel;
    return cellWithRowOne;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    static CGFloat initialDelay = 0.09f;
    static CGFloat stutter = 0.1f;

    FindRootTableViewCell *cardCell = (FindRootTableViewCell *)cell;
    FindRootSingleModel *findRootSingleModel = [self.findArr objectAtIndex:indexPath.section];
    if (findRootSingleModel.hasAnimation == NO){
        [cardCell startAnimationWithDelay:initialDelay + ((indexPath.section) * stutter)];
        findRootSingleModel.hasAnimation = YES;
    }
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FindRootSingleModel *findRootSingleModel = [self.findArr objectAtIndex:indexPath.section];
    WebViewController *webView = [[WebViewController alloc]init];
    webView.transferFindRootSingleModel = findRootSingleModel;
    [webView webViewControllerWithAddress:findRootSingleModel.direct];
    [self.navigationController pushViewController:webView animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    FindRootSingleModel *findRootSingleModel = [self.findArr objectAtIndex:indexPath.section];
    return [FindRootTableViewCell calculationCellHeight:findRootSingleModel];
}

#pragma mark - 接口
-(void)sendRequestTogetMenuList{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"page":@(self.findTableView.currentPage),@"size":@"20"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:activityList requestParams:params responseObjectClass:[FindRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            FindRootListModel *findListModel = (FindRootListModel *)responseObject;
            if (strongSelf.findTableView.isXiaLa){
                [strongSelf.findArr removeAllObjects];
            }
            
            [strongSelf.findArr addObjectsFromArray:findListModel.list];
            [strongSelf.findTableView reloadData];
            if (!strongSelf.findArr.count){
                [strongSelf.findTableView showPrompt:@"没有数据哦~点击重新获取" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
                    [strongSelf sendRequestTogetMenuList];
                }];
            } else {
                [strongSelf.findTableView dismissPrompt];
            }
            if (!findListModel.list.count){
                if (!strongSelf.findArr.count){
                    [strongSelf.findTableView hiddenBottomTitle];
                } else {
                    [strongSelf.findTableView showBottomTitle];
                }
            } else {
                [strongSelf.findTableView hiddenBottomTitle];
            }
            
            [strongSelf.findTableView stopPullToRefresh];
            [strongSelf.findTableView stopFinishScrollingRefresh];
        }
    }];
}

//#pragma mark - 获取当前的商品列表
//-(void)sendRequestToGetProductInfo{
//    __weak typeof(self)weakSelf = self;
//    NSDictionary *params = @{@"page":@(self.findTableView.currentPage),@"size":@"10"};
//    [[NetworkAdapter sharedAdapter] fetchWithPath:product_list requestParams:params responseObjectClass:[GWShopShopListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSucceeded){
//            GWShopShopListModel *listModel = (GWShopShopListModel *)responseObject;
//            if (strongSelf.findTableView.isXiaLa){
//                [strongSelf.findArr removeAllObjects];
//            }
//
//            [strongSelf.findArr addObjectsFromArray:listModel.list];
//            [strongSelf.findTableView reloadData];
//            if (!strongSelf.findArr.count){
//                [strongSelf.findTableView showPrompt:@"没有数据哦~点击重新获取" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
//                    [strongSelf sendRequestTogetMenuList];
//                }];
//            } else {
//                [strongSelf.findTableView dismissPrompt];
//            }
//            if (!listModel.list.count){
//                if (!strongSelf.findArr.count){
//                    [strongSelf.findTableView hiddenBottomTitle];
//                } else {
//                    [strongSelf.findTableView showBottomTitle];
//                }
//            } else {
//                [strongSelf.findTableView hiddenBottomTitle];
//            }
//
//            [strongSelf.findTableView stopPullToRefresh];
//            [strongSelf.findTableView stopFinishScrollingRefresh];
//        }
//    }];
//}

@end

//
//  OrderRootViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/8.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,orderType) {
    orderTypeYiwancheng = 5,                /**< 已完成*/
    orderTypeDaifukuan = 0,                 /**< 待付款*/
    orderTypeDaifahuo = 1,                  /**< 待发货*/
    orderTypeDaiShouhuo = 2,                /**< 待收货*/
    orderTypeDaipingjia = 3,                /**< 待评价*/
    orderTypeTuikuan = 4,                   /**< 退款退货*/
    orderTypeALL = 999,                     /**< 全部*/

};

@interface OrderRootViewController : AbstractViewController

@end

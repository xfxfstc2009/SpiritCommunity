//
//  LiveActionViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "LiveUIBottomViewSingleModel.h"
#import "HomeLiveSingleModel.h"


@interface LiveActionViewController : AbstractViewController

@property (nonatomic,assign)LiveUIBottomViewType transferActionType;

- (void)showInView:(UIViewController *)viewController withBlock:(void(^)(LiveUIBottomViewSingleModel *actionSingleModel))block;                          /**< block*/
-(void)sheetViewDismissWithBlcok:(void(^)())block;
- (void)dismissFromView:(UIViewController *)viewController withBlcok:(void(^)())block;                     /**< block*/

@property (nonatomic,strong)HomeLiveSingleModel *transferLiveSingleModel;   /**< 传递过去的直播Model*/

-(void)changeCurrentMoney;
-(void)chongzhiBtnClickManagerBlock:(void(^)())block;


@end

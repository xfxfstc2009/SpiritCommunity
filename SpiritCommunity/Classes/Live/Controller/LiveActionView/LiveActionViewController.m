//
//  LiveActionViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveActionViewController.h"
#import "LiveBottomShowView.h"
#import "GWSlider.h"
#import "LiveGiftListModel.h"
#import "LiveUIBottomSingleView.h"
#import "LiveTopMoneyView.h"
#import "TXLiveManager.h"

static char LiveActionViewControllerShowKey;
static char chongzhiBtnClickManagerBlockKey;
@interface LiveActionViewController ()<GWSliderDataSource>
@property (nonatomic,strong)UIViewController *showViewController;               /**< 展示view*/
@property (nonatomic,strong)UIView *backgrondView;                              /**< 底部的view 用来做背景的变色*/
@property (nonatomic,strong)UIScrollView *actionBackgroundView;                 /**< 点击产生的view*/
@property (nonatomic,strong)PDImageView *actionImgView;
@property (nonatomic,strong)NSMutableArray *infoMutableArr;
@property (nonatomic,strong)GWSlider *beautySlider;                             // 美颜度
@property (nonatomic,strong)GWSlider *whiteSlider;                              // 美白度
@property (nonatomic,strong)LiveUIBottomViewSingleModel *beautySingleModel;
@property (nonatomic,strong)LiveTopMoneyView *topAccountView;

@end

@implementation LiveActionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    if (self.transferActionType == LiveUIBottomViewTypeActionGift){
        
    } else{
        [self arrayWithInit];
        [self createSheetViewWithGift:NO];
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    if (!self.infoMutableArr){
        self.infoMutableArr = [NSMutableArray array];
    }
    if (self.infoMutableArr.count){
        [self.infoMutableArr removeAllObjects];
    }
    if (self.transferActionType == LiveUIBottomViewTypeActionRoot){                         // 【主页】
        [self arrayInitWithHome];
    } else if (self.transferActionType == LiveUIBottomViewTypeActionMengyantexiao){         // 【萌颜特效】
        [self arrayWithSpecil];
    } else if (self.transferActionType == LiveUIBottomViewTypeActionFilter){                // 【滤镜】
        [self arrayWithFilter];
    } else if (self.transferActionType == LiveActionTypeResolving){                         // 【分辨率】
        [self arrayInitWithResolving];
    } else if (self.transferActionType == LiveUIBottomViewTypeActionSpecialSounds){         // 【声音】
        [self arrayInitWithAudio];
    }
}

#pragma mark - 首页
-(void)arrayInitWithHome{
    for (int i = 1 ; i < 8 ; i++){
        LiveUIBottomViewSingleModel *actionSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
        if (i == 0){                // 【萌颜特效】
            actionSingleModel.buttonImg = @"live_detail_mengyan";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionMengyantexiao;
            actionSingleModel.title = @"萌颜特效";
        } else if (i == 1){         // 【美颜】
            actionSingleModel.buttonImg = @"live_detail_beauty-1";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionBeauty;
            actionSingleModel.title = @"美颜";
        } else if (i == 2){         // 【滤镜】
            actionSingleModel.buttonImg = @"live_detail_lvjing";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter;
            actionSingleModel.title = @"滤镜";
        } else if (i == 3){         // 【声音】
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSounds;
            actionSingleModel.title = @"声音";
        } else if (i == 4){         // 分辨率
            actionSingleModel.buttonImg = @"live_detail_fenbianlv";
            actionSingleModel.transferUIBottomType = LiveActionTypeResolving;
            actionSingleModel.title = @"分辨率";
        } else if (i == 5){         // 开关静音
            if ([TXLiveManager sharedAccountModel].transferMicStatus){
                actionSingleModel.buttonImg = @"live_detail_mic_nor";
            } else {
                actionSingleModel.buttonImg = @"live_detail_mic_hlt";
            }
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSounds;
            actionSingleModel.title = @"开关静音";
        } else if (i == 6){         // 开关闪光
            if ([TXLiveManager sharedAccountModel].lightStatus){
                actionSingleModel.buttonImg = @"live_detail_lights_off";
            } else {
                actionSingleModel.buttonImg = @"live_detail_lights_on";
            }
            
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionLighting;
            actionSingleModel.title = @"开关闪光";
        } else if (i == 7){         // 镜头翻转
            actionSingleModel.buttonImg = @"live_detail_camera_change";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionLensFlip;
            actionSingleModel.title = @"镜头翻转";
        }
        [self.infoMutableArr addObject:actionSingleModel];
    }
}

#pragma mark 激萌特效
-(void)arrayWithSpecil{
    for (int i = 0 ; i < 3 ; i++){
        LiveUIBottomViewSingleModel *actionSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
        if (i == 0){                            // 取消激萌特效
            actionSingleModel.buttonImg = @"live_detail_texiao_none";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionMengyantexiaoNone;
            actionSingleModel.title = @"取消特效";
        } else if (i == 1){                     // 兔子
            actionSingleModel.buttonImg = @"live_detail_texiao_tuzi";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionMengyantexiaoTuzi;
            actionSingleModel.title = @"兔子";
        } else if (i == 2){                     // 白雪公主
            actionSingleModel.buttonImg = @"live_detail_texiao_meinv";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionMengyantexiaoBaixuegongzhu;
            actionSingleModel.title = @"白雪公主";
        }
        
        [self.infoMutableArr addObject:actionSingleModel];
    }
}

#pragma mark  滤镜
-(void)arrayWithFilter{
    for (int i = 0 ; i < 10 ; i++){
        LiveUIBottomViewSingleModel *actionSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
        if (i == 0){                            // 【清空滤镜】
            actionSingleModel.buttonImg = @"live_lvjing_301";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilterNone;
            actionSingleModel.title = @"清空滤镜";
        } else if (i == 1){                     // 【清新】
            actionSingleModel.buttonImg = @"live_lvjing_302";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter1;
            actionSingleModel.title = @"美颜美白";
        } else if (i == 2){                     // 【唯美】
            actionSingleModel.buttonImg = @"live_lvjing_303";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter2;
            actionSingleModel.title = @"清新";
        } else if (i == 3){                     // 【粉嫩】
            actionSingleModel.buttonImg = @"live_lvjing_304";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter3;
            actionSingleModel.title = @"浪漫";
        } else if (i == 4){                     // 【美颜】
            actionSingleModel.buttonImg = @"live_lvjing_305";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter4;
            actionSingleModel.title = @"唯美";
        } else if (i == 5){                     // 【怀旧】
            actionSingleModel.buttonImg = @"live_lvjing_306";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter5;
            actionSingleModel.title = @"粉嫩";
        } else if (i ==6){                      // 【浪漫】
            actionSingleModel.buttonImg = @"live_lvjing_307";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter6;
            actionSingleModel.title = @"怀旧";
        } else if (i == 7){                     // 【清凉】
            actionSingleModel.buttonImg = @"live_lvjing_308";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter7;
            actionSingleModel.title = @"蓝调";
        } else if (i == 8){                     // 【日系】
            actionSingleModel.buttonImg = @"live_lvjing_309";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter8;
            actionSingleModel.title = @"清亮";
        } else if (i == 9){                     // 【蓝调】
            actionSingleModel.buttonImg = @"live_lvjing_310";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionFilter9;
            actionSingleModel.title = @"日系";
        }
        
        [self.infoMutableArr addObject:actionSingleModel];
    }
}

#pragma mark 分辨率
-(void)arrayInitWithResolving{
    for (int i = 0 ; i < 3 ; i++){
        LiveUIBottomViewSingleModel *actionSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
        if (i == 0){                           // 高清
            actionSingleModel.buttonImg = @"live_detail_gaoqing";
            actionSingleModel.transferUIBottomType = LiveActionTypeResolving1;
            actionSingleModel.title = @"高清";
        } else if (i == 1){                     // 标清
            actionSingleModel.buttonImg = @"live_detail_biaoqing";
            actionSingleModel.transferUIBottomType = LiveActionTypeResolving2;
            actionSingleModel.title = @"标清";
        } else if (i == 2){                     // 流畅
            actionSingleModel.buttonImg = @"live_detail_liuchang";
            actionSingleModel.transferUIBottomType = LiveActionTypeResolving3;
            actionSingleModel.title = @"流畅";
        }
        
        [self.infoMutableArr addObject:actionSingleModel];
    }
}

#pragma mark - 声音
-(void)arrayInitWithAudio{
    for (int i = 0 ; i < 11;i++){
        LiveUIBottomViewSingleModel *actionSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
        if (i == 0){                           // 原生
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsNone;
            actionSingleModel.title = @"原声";
        } else if (i == 1){                     // 萝莉
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsLolita;
            actionSingleModel.title = @"萝莉";
        } else if (i == 2){                     // 大叔
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsUncle;
            actionSingleModel.title = @"大叔";
        } else if (i == 3){                     // 空灵
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsIntangible;
            actionSingleModel.title = @"空灵";
        } else if (i == 4){                     // 幼稚园
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsKinder;
            actionSingleModel.title = @"幼稚园";
        } else if (i == 5){                     // 重机器
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsHeavy;
            actionSingleModel.title = @"重机器";
        } else if (i == 6){                     // 擎天柱
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsPrime;
            actionSingleModel.title = @"擎天柱";
        } else if (i == 7){                     // 困兽
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsAnimal;
            actionSingleModel.title = @"困兽";
        } else if (i == 8){                     // 方言
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsDialect;
            actionSingleModel.title = @"方言";
        } else if (i == 9){                     // 机器人
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsRobot;
            actionSingleModel.title = @"机器人";
        } else if (i == 10){                     // 肥仔
            actionSingleModel.buttonImg = @"live_detail_shengyin";
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionSpecialSoundsFatboy;
            actionSingleModel.title = @"肥仔";
        }
        [self.infoMutableArr addObject:actionSingleModel];
    }
}

-(void)setTransferLiveSingleModel:(HomeLiveSingleModel *)transferLiveSingleModel{
    _transferLiveSingleModel = transferLiveSingleModel;

    self.topAccountView.transferName = self.transferLiveSingleModel.userInfo.nick;
}

#pragma mark - 创建sheetView
-(void)createSheetViewWithGift:(BOOL)hasGift{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = YES;
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.1f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.f];
    
    self.actionImgView = [[PDImageView alloc]init];
    self.actionImgView.frame = CGRectMake(0, kScreenBounds.size.height, kScreenBounds.size.width, 100);
    self.actionImgView.userInteractionEnabled = YES;
    self.actionImgView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.actionImgView];
    

    // 创建view
    self.actionBackgroundView = [GWViewTool gwCreateScrollViewWithRect:self.actionImgView.bounds];
    self.actionBackgroundView.backgroundColor = [UIColor clearColor];
    self.actionBackgroundView.alpha = 1;
    self.actionBackgroundView.userInteractionEnabled = YES;
    [self.actionImgView addSubview:self.actionBackgroundView];
    if (self.actionBackgroundView.subviews.count){
        for (int i = 0 ; i < self.actionBackgroundView.subviews.count;i++){
            id view = [self.actionBackgroundView.subviews objectAtIndex:i];
            if ([view isKindOfClass:[LiveBottomShowView class]]){
                [self.actionBackgroundView removeFromSuperview];
            }
        }
    }
    
    // 创建一个scrollview
    // 判断actionbackgroundView 的高度
    CGFloat margin_height =  LCFloat(11);
    CGFloat margin_width =  LCFloat(11);
    CGFloat size_width = (kScreenBounds.size.width - 5 * margin_width) / 4.;
    CGFloat size_height = size_width;
    CGFloat height_count = margin_height + (margin_height + size_height) * 2 ;


    // 添加价格
    if (hasGift == YES){            // 添加礼物
        if (!self.topAccountView){
            self.topAccountView = [[LiveTopMoneyView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(40))];
            self.topAccountView.transferName = self.transferLiveSingleModel.userInfo.nick;
            self.topAccountView.myAccountMoney = [AccountModel sharedAccountModel].loginModel.money;
            [self.actionImgView addSubview:self.topAccountView];
            __weak typeof(self)weakSelf = self;
            [self.topAccountView.chongzhiButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void (^block)() = objc_getAssociatedObject(strongSelf, &chongzhiBtnClickManagerBlockKey);
                if (block){
                    block();
                }
            }];
        }

        height_count += self.topAccountView.size_height;
    }
    self.actionImgView.size_height = height_count;

    if (hasGift){
        self.actionBackgroundView.frame = self.actionImgView.bounds;
        self.actionBackgroundView.orgin_y = self.topAccountView.size_height;
    } else {
        self.actionBackgroundView.frame = self.actionImgView.bounds;
    }
    

    if (self.transferActionType == LiveUIBottomViewTypeActionBeauty){
        [self createBeautySliderView];
    } else {
        // 创建图标
        __weak typeof(self)weakSelf = self;
        NSInteger viewPage = self.infoMutableArr.count % 8 == 0 ?(self.infoMutableArr.count / 8) :self.infoMutableArr.count / 8 + 1;
        self.actionBackgroundView.contentSize = CGSizeMake(kScreenBounds.size.width * viewPage, self.actionBackgroundView.size_height);
        
        for (int j = 0 ; j < viewPage;j++){
            LiveBottomShowView *bottomShowView = [[LiveBottomShowView alloc]init];
            bottomShowView.backgroundColor = [UIColor clearColor];
            bottomShowView.frame = CGRectMake(j * self.actionBackgroundView.size_width, 0, self.actionBackgroundView.size_width, self.actionBackgroundView.size_height);
            [self.actionBackgroundView addSubview:bottomShowView];
            
            
            // 1. 获取当前的总数
            NSInteger currentCount = (j + 1) * 8 <= self.infoMutableArr.count?8:self.infoMutableArr.count % 8;
            for (int i = 0 ; i < currentCount;i++){
                // 1. origin_x
                CGFloat origin_x = margin_width + (i % 4) * size_width + (i % 4) * margin_width;
                // 2. origin_y
                CGFloat origin_y = margin_height + ( i / 4 ) * (size_width + margin_height);
                
                LiveUIBottomViewSingleModel *actionSingleModel = [self.infoMutableArr objectAtIndex:(j * 8 + i)];
                
                LiveUIBottomSingleView *actionButton = [[LiveUIBottomSingleView alloc]initWithFrame:CGRectMake(origin_x, origin_y, size_width, size_height) model:actionSingleModel actionBlcok:^(LiveUIBottomViewSingleModel *model) {
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    if (strongSelf.transferActionType != LiveUIBottomViewTypeActionGift){
                        [strongSelf sheetViewDismissWithBlcok:^{
                            void(^block)(LiveUIBottomViewSingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &LiveActionViewControllerShowKey);
                            if (block){
                                block(actionSingleModel);
                            }
                        }];
                    } else {
                        void(^block)(LiveUIBottomViewSingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &LiveActionViewControllerShowKey);
                        if (block){
                            block(actionSingleModel);
                        }
                    }
                }];
                [bottomShowView addSubview:actionButton];
            }
        }
    }
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = .3f;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

-(void)sheetViewDismiss{
    [self dismissFromView:self.showViewController withBlcok:^{
        void(^block)(LiveUIBottomViewSingleModel *singleModel) = objc_getAssociatedObject(self, &LiveActionViewControllerShowKey);
        if (block){
            LiveUIBottomViewSingleModel *actionSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
            actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionDismiss;
            block(actionSingleModel);
        }
    }];
}

-(void)sheetViewDismissWithBlcok:(void(^)())block{
    [self dismissFromView:self.showViewController withBlcok:^{
        if (block){
            block();
        }
    }];
}

- (void)showInView:(UIViewController *)viewController withBlock:(void(^)(LiveUIBottomViewSingleModel *actionSingleModel))block{
    if (self.transferActionType == LiveUIBottomViewTypeActionGift){
        [StatusBarManager statusBarHidenWithText:@"正在获取礼物信息"];
        [self sendRequestToGetInfoWithBlock:^(BOOL successed) {
            if (successed){
                [StatusBarManager statusBarHidenWithText:@"已获取金币信息"];
                [self createSheetViewWithGift:YES];
                __weak LiveActionViewController *weakVC = self;
                _showViewController = viewController;
                [viewController.view.window addSubview:self.view];
                [viewController addChildViewController:self];
                [self didMoveToParentViewController:viewController];
                
                NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
                
                
                [UIView animateWithDuration:.3f animations:^{
                    weakVC.actionImgView.frame = CGRectMake(weakVC.actionImgView.orgin_x, screenHeight-weakVC.actionImgView.size_height -(IS_IOS7_LATER ? 0 : 20), weakVC.actionImgView.size_width, weakVC.actionImgView.size_height);
                } completion:^(BOOL finished) {
                    objc_setAssociatedObject(self, &LiveActionViewControllerShowKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
                }];
            } else {
                [StatusBarManager statusBarHidenWithText:@"获取礼物信息失败"];
            }
        }];
    } else {
        __weak LiveActionViewController *weakVC = self;
        _showViewController = viewController;
        [viewController.view.window addSubview:self.view];
        [viewController addChildViewController:self];
        [self didMoveToParentViewController:viewController];
        
        NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
        
        
        [UIView animateWithDuration:.3f animations:^{
            weakVC.actionImgView.frame = CGRectMake(weakVC.actionImgView.orgin_x, screenHeight-weakVC.actionImgView.size_height -(IS_IOS7_LATER ? 0 : 20), weakVC.actionImgView.size_width, weakVC.actionImgView.size_height);
        } completion:^(BOOL finished) {
            objc_setAssociatedObject(self, &LiveActionViewControllerShowKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        }];
    }
    
    
    
}

- (void)dismissFromView:(UIViewController *)viewController withBlcok:(void(^)())block{
    __weak LiveActionViewController *weakVC = self;
    
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
    
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.actionImgView.frame = CGRectMake(0, kScreenBounds.size.height, weakVC.actionImgView.bounds.size.width, weakVC.actionImgView.bounds.size.height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
        if (block){
            block();
        }
    }];
}






#pragma mark - 创建sliderView
-(void)createBeautySliderView{  // 美颜
    UIView *sliderView = [[UIView alloc]init];
    
    // 1. 创建美颜
    UILabel *beautyLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    beautyLabel.text = @"美颜";
    beautyLabel.font = [beautyLabel.font boldFont];
    CGSize beautySize = [Tool makeSizeWithLabel:beautyLabel];
    beautyLabel.frame = CGRectMake(LCFloat(11), 0, beautySize.width, beautySize.height);
    [sliderView addSubview:beautyLabel];
    
    CGFloat width = kScreenBounds.size.width - LCFloat(11) - beautySize.width - LCFloat(11) - LCFloat(11);
    self.beautySlider = [[GWSlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(beautyLabel.frame) + LCFloat(11), 0, width, beautySize.height)];
    self.beautySlider.popUpViewCornerRadius = 0.6f;
    [self.beautySlider setMaxFractionDigitsDisplayed:0];
    self.beautySlider.dataSource = self;
    self.beautySlider.popUpViewColor = [UIColor colorWithCustomerName:@"黑"];
    self.beautySlider.font = [UIFont fontWithName:@"GillSans-Bold" size:18];
    self.beautySlider.textColor = [UIColor whiteColor];
    [self.beautySlider addTarget:self action:@selector(playProgressChangeEnd:) forControlEvents:UIControlEventTouchUpInside];
    self.beautySlider.value = [TXLiveManager sharedAccountModel].transferBeautyValue;
    [sliderView addSubview:self.beautySlider];
    
    // 2. 创建美白
    UILabel *whiteLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    whiteLabel.text = @"美白";
    whiteLabel.font = [whiteLabel.font boldFont];
    CGSize whiteSize = [Tool makeSizeWithLabel:whiteLabel];
    whiteLabel.frame = CGRectMake(LCFloat(11), 0, whiteSize.width, whiteSize.height);
    [sliderView addSubview:whiteLabel];
    
    self.whiteSlider = [[GWSlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(beautyLabel.frame) + LCFloat(11), 0, width, beautySize.height)];
    self.whiteSlider.popUpViewCornerRadius = 0.6f;
    [self.whiteSlider setMaxFractionDigitsDisplayed:0];
    self.whiteSlider.dataSource = self;
    self.whiteSlider.popUpViewColor = [UIColor colorWithCustomerName:@"黑"];
    self.whiteSlider.font = [UIFont fontWithName:@"GillSans-Bold" size:18];
    self.whiteSlider.textColor = [UIColor whiteColor];
    [self.whiteSlider addTarget:self action:@selector(playProgressChangeEnd:) forControlEvents:UIControlEventTouchUpInside];
    self.whiteSlider.value = [TXLiveManager sharedAccountModel].transferWhiteValue;
    [sliderView addSubview:self.whiteSlider];
    
    [self.actionBackgroundView addSubview:sliderView];
    sliderView.frame = self.actionBackgroundView.bounds;
    
    CGFloat avg_height = self.actionBackgroundView.size_height / 2.;
    beautyLabel.center_y = avg_height / 2. + LCFloat(20);
    self.beautySlider.center_y = beautyLabel.center_y;
    
    whiteLabel.center_y = avg_height + avg_height / 2.;
    self.whiteSlider.center_y = whiteLabel.center_y;
    
    self.beautySingleModel = [[LiveUIBottomViewSingleModel alloc]init];
}

- (NSString *)slider:(GWSlider *)slider stringForValue:(float)value{
    if (slider == self.beautySlider){
        return [NSString stringWithFormat:@"美颜度%li",(long)(value * 100)];
    } else {
        return [NSString stringWithFormat:@"美白度%li",(long)(value * 100)];
    }
}
-(void)playProgressChangeEnd:(GWSlider *)slider{
    if (slider == self.beautySlider){
        [TXLiveManager sharedAccountModel].transferBeautyValue = slider.value;
        self.beautySingleModel.transferUIBottomType = LiveUIBottomViewTypeActionBeautyColor;
    } else if (slider == self.whiteSlider){
        [TXLiveManager sharedAccountModel].transferWhiteValue = slider.value;
        self.beautySingleModel.transferUIBottomType = LiveUIBottomViewTypeActionBeautyWhite;
    }
    
    void(^block)(LiveUIBottomViewSingleModel *singleModel) = objc_getAssociatedObject(self, &LiveActionViewControllerShowKey);
    if (block){
        block(self.beautySingleModel);
    }
}





#pragma mark - gift
#pragma mark - sendRequestToGetInfo
-(void)sendRequestToGetInfoWithBlock:(void(^)(BOOL successed))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:live_gift requestParams:nil responseObjectClass:[LiveGiftListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            LiveGiftListModel *giftListModel = (LiveGiftListModel *)responseObject;
            if (!strongSelf.infoMutableArr){
                strongSelf.infoMutableArr = [NSMutableArray array];
            }
            if (strongSelf.infoMutableArr.count){
                [strongSelf.infoMutableArr removeAllObjects];
            }
            
            for (int i = 0 ; i < giftListModel.list.count;i++){
                LiveGiftSingleModel *singleModel = [giftListModel.list objectAtIndex:i];
                 LiveUIBottomViewSingleModel *actionSingleModel = [[LiveUIBottomViewSingleModel alloc]init];
                actionSingleModel.giftSingleModel = singleModel;
                actionSingleModel.transferUIBottomType = LiveUIBottomViewTypeActionGiftOther;
                [self.infoMutableArr addObject:actionSingleModel];
            }
            
            if (block){
                block(YES);
            }
        }
    }];
}

#pragma mark - 创建礼物顶部的view
-(void)changeCurrentMoney{
    self.topAccountView.myAccountMoney = [AccountModel sharedAccountModel].loginModel.money;
}

-(void)chongzhiBtnClickManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &chongzhiBtnClickManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end

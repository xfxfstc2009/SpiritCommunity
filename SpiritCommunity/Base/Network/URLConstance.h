//
//  URLConstance.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#ifndef URLConstance_h
#define URLConstance_h

// 【接口环境】
#ifdef DEBUG        // 测试
#define JAVA_Host @"103.45.59.11"
#define JAVA_Port @"80"
#else               // 线上
#define JAVA_Host @"103.45.59.11"
#define JAVA_Port @"80"
#endif

#ifdef DEBUG        // 测试
#define socket_Host @"103.45.59.11"
#define socket_port @"3003"
#else               // 线上
#define socket_Host @"103.45.59.11"
#define socket_port @"3003"
#endif


// 【资讯接口环境】
#ifdef DEBUG        // 测试
#define TX_Host @"console.tim.qq.com"
#define TX_Port @""
#else               // 线上
#define TX_Host @"console.tim.qq.com"
#define TX_Port @""
#endif



// 【配置】
static NSString *const ErrorDomain = @"ErrorDomain";
static BOOL NetLogAlert = YES;                                                  /**< 网络输出*/
static NSInteger TimeoutInterval = 15;                                          /**< 短链接失效时间*/

// 【长链接】
static BOOL isHeartbeatPacket = NO;                                            /**< 是否心跳包*/
static BOOL isSocketLogAlert = NO;                                             /**< 是否socket*/

#ifdef DEBUG
#define PDSocketDebug
#endif

#ifdef PDSocketDebug
#define PDSocketLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define PDSocketLog(format, ...)
#endif




///////////////////////////////////////////////////////////////////////接口链接//////////////////////////////////////////////////////////////////////
// 【1.审核接口】
static NSString *const appShenhe = @"/Classes/Other/Controller/Other_Shenhe.ashx";

// 【2.首页】
static NSString *const home_banner = @"Classes/Home/Controller/Home_Banner.ashx";
static NSString *const home_onebyone = @"Classes/Home/Controller/Home_Onebyone.ashx";
static NSString *const home_live = @"Classes/Home/Controller/Home_Live.ashx";
static NSString *const home_search = @"Classes/Home/Controller/Home_Search.ashx";                 // 进行搜索
static NSString *const home_search_hot = @"Classes/Home/Controller/Home_Search_Fans_Tuijian.ashx";                 // 进行搜索
static NSString *const home_link = @"Classes/Center/Controller/Center_ToLink.ashx";                 // 进行搜索
static NSString *const home_ranking = @"Classes/Home/Controller/Home_Ranking.ashx";                 // 排行榜
static NSString *const home_search_single = @"Classes/home/Controller/Home_Search_UserInfo.ashx";   // 搜索个人

// 【3.登录】
static NSString *const login = @"Classes/Login/user_login.ashx";
static NSString *const userLocation = @"Classes/Login/User_Upload_Location.ashx";

// 腾讯登录
static NSString *const txLogin = @"v4/im_open_login_svc/account_import";

// 【4.个人中心】
static NSString *const assetList = @"Classes/Center/Controller/Center_AssetList.ashx";
static NSString *const center_uploadImg = @"Classes/Center/Controller/Center_Asset_Insert.ashx";               // 个人中心分享
static NSString *const center_deleteImg = @"Classes/Center/Controller/Center_Asset_Delete.ashx";               // 删除图片
static NSString *const suggestion = @"Classes/Center/Controller/Center_Suggestion.ashx";            //
static NSString *const user_eidt = @"Classes/Center/Controller/Center_UserEdit.ashx";               // 用户编辑信息
static NSString *const my_fans = @"Classes/Center/Controller/Center_MyFansList.ashx";               // 我的粉丝
static NSString *const my_link = @"Classes/Center/Controller/Center_MyLinkList.ashx";               // 我关注的
static NSString *const setting_share = @"Classes/Other/Controller/Other_Share.ashx";               // 个人中心分享
static NSString *const center_uploadLive = @"Classes/Center/Controller/Center_User_Live_Edit.ashx";               // 个人中心分享
static NSString *const chongzhi = @"Classes/Center/Controller/Center_Chongzhi_Add.ashx";
static NSString *const infoOrder = @"Classes/Center/Pay/Center_Pay_Create_Order.ashx";                  // 生成预生成单
static NSString *const money_Flowing = @"Classes/Center/Controller/Center_Money_Flowing.ashx";
static NSString *const center_zhifuPay = @"Classes/Center/Controller/Center_Binding_ZhifuPay.ashx";     // 绑定支付宝
static NSString *const pay_list = @"Classes/Center/Controller/Center_Pay_List.ashx";                // 支付列表
static NSString *const paySuccess = @"Classes/Center/Pay/Center_Pay_SuccessManager.ashx";                // 支付列表
static NSString *const center_moneyTransfer = @"Classes/Center/Controller/Center_Money_Transfer.ashx";


// 【5.消息】
static NSString *const messageList = @"Classes/Message/Controller/Message_List.ashx";
static NSString *const messageRead = @"Classes/Message/Controller/Message_Read.ashx";                // 已读消息
static NSString *const messageDelete = @"Classes/Message/Controller/Message_Delete.ashx";           // 删除消息

// 【6.发现】
static NSString *const activityList = @"Classes/Find/Controller/Activity_List.ashx";
// 【7.直播】
static NSString *const live_create = @"Classes/Live/Controller/Live_CreateHouse.ashx";
static NSString *const live_gift = @"Classes/Live/Controller/gift/Live_Gift_List.ashx";
static NSString *const live_send_gift = @"Classes/Live/Controller/Home_Live_Send_Gift.ashx";
static NSString *const live_out_house = @"Classes/Live/Controller/Live_OutHome.ashx";
static NSString *const live_join_house = @"Classes/Live/Controller/Live_JoinHome.ashx";
static NSString *const live_root_out_house = @"Classes/Live/Controller/Live_House_Release.ashx";            //  主播退出房间
static NSString *const live_onebyone_upload = @"Classes/Live/Controller/Live_OneByOne_Upload_Identify.ashx";
static NSString *const live_onebyone_getIdentify = @"Classes/Live/Controller/Live_OneByOne_GetIdentify.ashx";   // 获取用户identify
static NSString *const live_out_house_root = @"Classes/Live/Controller/Live_Error_Release.ashx";              // 退出直播
static NSString *const live_err_status = @"Classes/Live/Controller/Live_Error_Status.ashx";
static NSString *const live_has_in = @"Classes/Live/Controller/Live_Has_In.ashx";
static NSString *const live_zan = @"Classes/Live/Controller/Live_Add_Zan.ashx";                         // 点赞
static NSString *const live_users = @"Classes/live/Controller/Live_Current_House_Users.ashx";           // 当前房间里面的用户
// 【8.审核】
static NSString *const shenhe_live = @"Classes/Shenhe/Shenhe_Live_Home.ashx";
static NSString *const product_list = @"Classes/product/Controller/Product_List.ashx";                  // 商品列表
static NSString *const product_add = @"Classes/product/Controller/Product_Add.ashx";                    // 添加商品

// 【9.map】
static NSString *const map_build_list = @"Classes/Map/Controller/Map_Build_List.ashx";
// 【获取天气】
static NSString *const weather_get = @"http://apicloud.mob.com/v1/weather/query";


///////////////////////////////////////////////////////////////////////阿里OSS//////////////////////////////////////////////////////////////////////

static NSString *const aliOSS_Avatar = @"";




// shop
static NSString *const _goods_001 = @"_goods_001";              /**< 商品列表*/
static NSString *const shop_login = @"_index_menu_001";


static NSString *const _oauth = @"ymgInterface/mobile/oauth/token";
static NSString *const _login = @"ymgInterface/mobile/user/login";
static NSString *const _orderList = @"ymgInterface/mobile/order/queryOrder";
static NSString *const _goodsList = @"ymgInterface/mobile/shop/unionShop";
static NSString *const _goodsInfo = @"ymgInterface/mobile/merchant/getGoodInfo";
static NSString *const _addCart = @"ymgInterface/mobile/shop/item/addCart";
static NSString *const _cartList = @"ymgInterface/mobile/shop/item/cartList";
static NSString *const _cartDelete = @"ymgInterface/mobile/shop/item/removeCart";
#endif /* URLConstance_h */

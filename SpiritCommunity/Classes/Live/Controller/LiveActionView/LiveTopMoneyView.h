//
//  LiveTopMoneyView.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveUIBottomViewSingleModel.h"

@interface LiveTopMoneyView : UIView

@property (nonatomic,strong)UIButton *chongzhiButton;

@property (nonatomic,strong)LiveUIBottomViewSingleModel *transferSingleModel;
@property (nonatomic,copy)NSString *transferName;               /**< 传递主播名字*/

@property (nonatomic,assign)NSInteger myAccountMoney;           /**< 我的账户*/

@end

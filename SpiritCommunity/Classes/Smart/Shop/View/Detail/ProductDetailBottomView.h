//
//  ProductDetailBottomView.h
//  17Live
//
//  Created by 裴烨烽 on 2018/2/1.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailRootModel.h"
#import "ShopDetailViewController.h"
@interface ProductDetailBottomView : UIView

@property (nonatomic,strong)ProductDetailRootModel *productDetailRootModel;

-(void)actionClickWithAddCard:(void(^)())block buyNowBlock:(void(^)())block1 cart:(void(^)())block2 collection:(void(^)())block3 customer:(void(^)())block4;

-(void)collection;
-(void)unCollection;

@end

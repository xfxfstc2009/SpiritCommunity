//
//  HomeRootGoodsListSuperModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/9.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "HomeRootGoodsListMainModel.h"
@interface HomeRootGoodsListSuperModel : FetchModel

@property (nonatomic,strong)NSArray<HomeRootGoodsListMainModel> *infoList;

@end

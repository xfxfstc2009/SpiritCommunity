precision highp float;
uniform sampler2D tex;

varying vec2 vs_tex_coord;

void main(void)
{
    vec4 color = texture2D(tex, vs_tex_coord);
    gl_FragColor = color;
}

//
//  LiveNotifyRootModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/7.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "LiveNotifySingleModel.h"
#import "ZanModel.h"
@interface LiveNotifyRootModel : FetchModel

@property (nonatomic,strong)LiveNotifySingleModel *giftModel;
@property (nonatomic,copy)NSString *houseId;
@property (nonatomic,assign)NSInteger type;
@property (nonatomic,strong)ZanModel *zanModel;
@end



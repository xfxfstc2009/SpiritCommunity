//
//  FindShenheProductDetailViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/31.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FindShenheProductDetailViewController.h"
#import "PDGrabOrderRowNie.h"
#import "PDGrabOrderRowTen.h"
#import "PDGrabOrderRowEig.h"
#import "PDGrabOrderAuthSingleCell.h"
#import "UserInfoViewController.h"

@interface FindShenheProductDetailViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)NSArray *productDetailArr;
@property (nonatomic,strong)UITableView *productDetailTableView;

@end

@implementation FindShenheProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self creatTableView];

}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"商品详情";
}

#pragma mark - arrayWithinit
-(void)arrayWithInit{
    self.productDetailArr = @[@[@"商品图片"],@[@"商品名称",@"商品单价",@"新旧程度"],@[@"商品详情"],@[@"商品发布者"],@[@"规则"]];
}

#pragma mark - UITableView
-(void)creatTableView{
    if (!self.productDetailTableView){
        self.productDetailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.productDetailTableView.dataSource = self;
        self.productDetailTableView.delegate = self;
        [self.view addSubview:self.productDetailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.productDetailArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.productDetailArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWBannerTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWBannerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferImgArr = self.transferSingleModel.imgArr;
        return cellWithRowOne;
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = [UIColor clearColor];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = [[self.productDetailArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if (indexPath.row == 0){
            cellWithRowTwo.transferDesc = self.transferSingleModel.name;
            cellWithRowTwo.transferIcon = [UIImage imageNamed:@"product_guarantee"];
        } else if (indexPath.row == 1){
            cellWithRowTwo.transferDesc = self.transferSingleModel.price;
            cellWithRowTwo.transferIcon = [UIImage imageNamed:@"product_guarantee"];
        } else if (indexPath.row == 2){
            cellWithRowTwo.transferDesc = [NSString stringWithFormat:@"%@成新",self.transferSingleModel.olenew];
            cellWithRowTwo.transferIcon = [UIImage imageNamed:@"product_guarantee"];
        }

        return cellWithRowTwo;
    } else if (indexPath.section == 2){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDGrabOrderRowNie *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDGrabOrderRowNie alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferBeizhuInfo = self.transferSingleModel.desc;
        return cellWithRowThr;
    } else if (indexPath.section == 3){             // 商品发布者
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        PDGrabOrderAuthSingleCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[PDGrabOrderAuthSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferAuthModel = self.transferSingleModel;
        return cellWithRowFour;
    } else if (indexPath.section == 4){         // 规则
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        PDGrabOrderRowTen *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[PDGrabOrderRowTen alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        return cellWithRowFiv;
    } else if (indexPath.section == 5){     // 购买
        static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
        GWButtonTableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
        if (!cellWithRowSex){
            cellWithRowSex = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
            cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowSex.transferCellHeight = cellHeight;
        [cellWithRowSex setButtonStatus:YES];
        cellWithRowSex.transferTitle = @"立即购买";
        __weak typeof(self)weakSelf = self;
        [cellWithRowSex buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            [[UIAlertView alertViewWithTitle:@"添加成功" message:@"由于商品的稀缺性与珍贵性，24小时内，平台客服将联系买卖双方，以促成最终交易" buttonTitles:@[@"确定"] callBlock:NULL]show];
        }];
        return cellWithRowSex;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 3){
        UserInfoViewController *vc = [[UserInfoViewController alloc]init];
        vc.transferSearchSingleModel = self.transferSingleModel.userInfo;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [GWBannerTableViewCell calculationCellHeight];
    } else  if (indexPath.section == 2){
        return [PDGrabOrderRowNie calculationCellHeightWithInfo:self.transferSingleModel.desc];
    } else if (indexPath.section == 1){
        return [GWNormalTableViewCell calculationCellHeight];
    } else if (indexPath.section == 4){
        return [PDGrabOrderRowTen calculationCellHeight];
    } else if (indexPath.section == 3){
        return [PDGrabOrderAuthSingleCell calculationCellHeight];
    }else {
        return LCFloat(44);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 5){
        return LCFloat(100);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footView = [[UIView alloc]init];
    footView.backgroundColor = [UIColor clearColor];
    return footView;

}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return LCFloat(20);
    } else if (section == 2){
        return LCFloat(20);
    } else if (section == 3){
        return LCFloat(11);
    } else {
        return 20;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;

}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1){
        SeparatorType separatorType = SeparatorTypeBottom;
        [cell addSeparatorLineWithType:separatorType];
    }
}

@end

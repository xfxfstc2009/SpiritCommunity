//
//  LiveReleaseShowViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/19.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "HomeLiveSingleModel.h"
#import "LiveUIActionView.h"
#import "ShareRootViewController.h"

@interface LiveReleaseShowViewController : AbstractViewController

@property (nonatomic,strong)LiveUIActionView *actionView;                   /**< 房间view*/
@property (nonatomic, strong) UILabel  *noCameraDatatalabel;                //对方没有打开相机时的提示
@property (nonatomic, assign) BOOL  isCameraEvent;                          //noCameraDatatalabel需要延迟显示，isNoCameraEvent用来判断是否收到了camera事件
@property (nonatomic, strong) TILFilter *tilFilter;
@property (nonatomic, strong) TIMUserProfile *selfProfile;                  //自己的信息

////////////////////////
@property (nonatomic,assign)LiveShowType transferLiveShowType;              /**< 当前直播的用户类型*/
@property (nonatomic,strong)HomeLiveSingleModel *transferLiveSingleModel;   /**< 传递过去的直播Model*/



/////消息
@property (nonatomic,strong)NSMutableArray *msgMutableArr;
-(void)sendMsg:(ILVLiveMessage *)msg;

@end

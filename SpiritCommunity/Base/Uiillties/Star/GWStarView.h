//
//  GWStarView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/26.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>


@class GWStarView;
@protocol GWStarViewDelegate <NSObject>
@optional
- (void)starRateView:(GWStarView *)starRateView scroePercentDidChange:(CGFloat)newScorePercent;
@end

@interface GWStarView : UIView

@property (nonatomic, assign) CGFloat scorePercent;//得分值，范围为0--1，默认为1
@property (nonatomic, assign) BOOL hasAnimation;//是否允许动画，默认为NO
@property (nonatomic, assign) BOOL allowIncompleteStar;//评分时是否允许不是整星，默认为NO

@property (nonatomic, weak) id<GWStarViewDelegate>delegate;

- (instancetype)initWithFrame:(CGRect)frame numberOfStars:(NSInteger)numberOfStars;

@end

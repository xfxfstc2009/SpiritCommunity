//
//  GWAssetsSnapshotStackView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/18.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef struct {
    CGFloat angleRotation;
    CGSize boundingRectSize;
} SnapshotPosition_t;

@interface GWAssetsSnapshotStackView : UIView{
    #pragma mark Constants
    #define SWSnapshotStackViewSnapshotsPerStack 3
    
    BOOL m_displayStack;    // property (displayAsStack)
    UIImage *m_image;       // property (image)
    CGFloat m_imageAspect;
    SnapshotPosition_t m_snapshotPositions[SWSnapshotStackViewSnapshotsPerStack];
    NSInteger m_minScaleShotIdx;
    BOOL m_scaledUsingWidth;
    CGFloat m_shadowDirSign;
}
@property (nonatomic, assign) BOOL displayAsStack;
@property (nonatomic, retain) UIImage *image;


@end

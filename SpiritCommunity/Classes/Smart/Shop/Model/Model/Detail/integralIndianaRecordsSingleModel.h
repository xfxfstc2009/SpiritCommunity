//
//  integralIndianaRecordsSingleModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/15.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "FetchModel.h"

@protocol integralIndianaRecordsSingleModel <NSObject>

@end

@interface integralIndianaRecordsSingleModel : FetchModel

@property (nonatomic,copy)NSString *number;
@property (nonatomic,copy)NSString *time;
@end

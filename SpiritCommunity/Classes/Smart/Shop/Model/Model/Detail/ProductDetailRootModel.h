//
//  ProductDetailRootModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "FetchModel.h"
#import "ProductDetailGoodsInfoModel.h"
#import "ProductDetailCategorySingleModel.h"
#import "ProductDetailPriceSingleModel.h"
#import "IntegralIndianaSingleModel.h"
#import "ProductDetailGoodBannerImgsModel.h"

@interface ProductDetailRootModel : FetchModel

@property (nonatomic,strong)NSArray<ProductDetailGoodBannerImgsModel> *goodBannerImgs;
@property (nonatomic,strong)ProductDetailGoodsInfoModel *goodBaseInfo;


@property (nonatomic,strong)NSArray<ProductDetailCategorySingleModel> *categorylist;

@property (nonatomic,strong)NSArray<ProductDetailPriceSingleModel> *pricelist;
@property (nonatomic,strong)IntegralIndianaSingleModel *integralIndiana;                /**< 积分详情*/
@end


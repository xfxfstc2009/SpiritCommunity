//
//  NickNameViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/20.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "NickNameViewController.h"

static char updateKey;
@interface NickNameViewController ()<UITextFieldDelegate>{
    UITextField *newTextField;              // 创建新的textField
}
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UILabel *numberLabel;

@end

@implementation NickNameViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (newTextField){
        [newTextField becomeFirstResponder];
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createNewTextField];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的昵称";
    
    __weak typeof(self)weakSelf = self;
    self.rightButton = [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_asset_nor"] barHltImage:[UIImage imageNamed:@"icon_main_asset_nor"] action:^{
       if (!weakSelf){
           return ;
       }
       __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToUpdateInfoWithType:strongSelf.transferEditType info:strongSelf->newTextField.text];
   }];
}

#pragma mark - createTextField
-(void)createNewTextField {
    newTextField = [[UITextField alloc]init];
    newTextField.frame = CGRectMake(LCFloat(11), 20, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(44));
    newTextField.delegate = self;

    if (self.transferEditType == UserEditTypeNickName){
        newTextField.text = [AccountModel sharedAccountModel].loginModel.nick;
    } else if (self.transferEditType == UserEditTypeAge){
        newTextField.text = [NSString stringWithFormat:@"%li",[AccountModel sharedAccountModel].loginModel.age];
    }
    
    newTextField.textColor = [UIColor blackColor];
    newTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    newTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    newTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    newTextField.returnKeyType = UIReturnKeyDone;
    newTextField.borderStyle = UITextBorderStyleRoundedRect;
    [newTextField setKeyboardType:UIKeyboardTypeNamePhonePad];
    newTextField.keyboardType = UIKeyboardTypeDefault;
    // 添加监听器观察是否改变
    [newTextField addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview:newTextField];
    
}

-(void)textFieldDidChange{
    if (newTextField.text.length){
        self.navigationItem.rightBarButtonItem.enabled = YES;
        [self.rightButton setImage:[UIImage imageNamed:@"icon_main_asset_hlt"] forState:UIControlStateNormal];
    } else {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        [self.rightButton setImage:[UIImage imageNamed:@"icon_main_asset_nor"] forState:UIControlStateNormal];
    }
}

#pragma mark - sendRequestToUpdateInfo
-(void)sendRequestToUpdateInfoWithType:(UserEditType)type info:(NSString *)info{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if(type == UserEditTypeAvatar){             // 修改头像
        [params setValue:info forKey:@"avatar"];
    } else if (type == UserEditTypeAge){        // 年龄
        [params setValue:info forKey:@"age"];
    } else if(type == UserEditTypeSex){         // 性别
        [params setValue:info forKey:@"gender"];
    } else if (type == UserEditTypeNickName){   // 昵称
        [params setValue:info forKey:@"nick"];
    }
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:user_eidt requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (type == UserEditTypeAge){        // 年龄
                [StatusBarManager statusBarHidenWithText:@"年龄修改成功"];
                [AccountModel sharedAccountModel].loginModel.age = [info integerValue];
            } else if (type == UserEditTypeNickName){   // 昵称
                [StatusBarManager statusBarHidenWithText:@"昵称修改成功"];
                [AccountModel sharedAccountModel].loginModel.nick = info;
            }
            void(^block)(UserEditType transferEditType) = objc_getAssociatedObject(strongSelf, &updateKey);
            if (block){
                block(strongSelf.transferEditType);
            }
            [[PDMainTabbarViewController sharedController].centerViewController uploadUserInfo];
            [strongSelf.navigationController popViewControllerAnimated:YES];

        }
    }];
}

-(void)actionBlock:(void(^)(UserEditType transferEditType))block{
    objc_setAssociatedObject(self, &updateKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end

//
//  PDThirdLoginModel.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/8.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface PDThirdLoginModel : FetchModel

@property (nonatomic,copy)NSString *city;
@property (nonatomic,copy)NSString *country;
@property (nonatomic,copy)NSString *headimgurl;
@property (nonatomic,copy)NSString *language;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,copy)NSString *openid;
@property (nonatomic,copy)NSString *province;
@property (nonatomic,assign)NSInteger sex;
@property (nonatomic,copy)NSString *unionid;

@end

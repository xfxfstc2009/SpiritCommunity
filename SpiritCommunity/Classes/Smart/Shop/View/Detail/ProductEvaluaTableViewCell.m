//
//  ProductEvaluaTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "ProductEvaluaTableViewCell.h"
#import "GWStarView.h"

@interface ProductEvaluaTableViewCell()
@property (nonatomic,strong)GWStarView *starView;
@property (nonatomic,strong)UILabel*nameLabel;
@property (nonatomic,strong)UILabel*descLabel;

@end

@implementation ProductEvaluaTableViewCell

-(instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 2.创建名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.nameLabel.textAlignment = NSTextAlignmentRight;
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.nameLabel];
    
// 3. 创建评论
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.backgroundColor = [UIColor clearColor];
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.descLabel.textColor =[UIColor colorWithCustomerName:@"黑"];
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
}

-(void)setTransferCommentDataSingleModel:(ProductDetailCommentDataSingleModel *)transferCommentDataSingleModel{
    _transferCommentDataSingleModel = transferCommentDataSingleModel;
    
    if (!self.starView){
        self.starView = [[GWStarView alloc]initWithFrame:CGRectMake(LCFloat(11), LCFloat(11), LCFloat(100), LCFloat(20)) numberOfStars:5];
        self.starView.hasAnimation = YES;
        self.starView.userInteractionEnabled = NO;
        self.starView.scorePercent = 1./ 5 * transferCommentDataSingleModel.star;
        [self addSubview:self.starView];
    }
    
    // name
    self.nameLabel.text = transferCommentDataSingleModel.username;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - nameSize.width, LCFloat(11), nameSize.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // desc
    self.descLabel.text = transferCommentDataSingleModel.content;
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), descSize.height);
    
}

+(CGFloat)calculationCellHeight:(ProductDetailCommentDataSingleModel *)transferCommentDataSingleModel{
    CGFloat cellheight = 0;
    cellheight += LCFloat(11) ;
    cellheight += LCFloat(20);
    cellheight += LCFloat(11);
    
    CGSize descSize = [transferCommentDataSingleModel.content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellheight += descSize.height;
    cellheight += LCFloat(11);
    return cellheight;
    
    
}
@end

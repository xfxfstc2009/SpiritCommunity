//
//  RankingTwoTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/13.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "RankingTwoTableViewCell.h"
#import <pop/POP.h>
#import "StringAttributeHelper.h"

@interface RankingTwoTableViewCell()
@property (nonatomic,strong)UILabel *mingciLabel;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)PDImageView *coinImgView;
@property (nonatomic,strong)UILabel *coinNameLabel;
@property (nonatomic,strong)UIButton *guanzhuButton;
@property (nonatomic,strong)PDImageView *avatarbgView;

@end
static char guanzhuButtonClickKey;
@implementation RankingTwoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.mingciLabel = [GWViewTool createLabelFont:@"标题" textColor:@"黑"];
    [self addSubview:self.mingciLabel];

    self.avatarbgView = [[PDImageView alloc]init];
    self.avatarbgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarbgView];


    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.nameLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    [self addSubview:self.nameLabel];
    
    self.coinImgView = [[PDImageView alloc]init];
    self.coinImgView.backgroundColor = [UIColor clearColor];
    self.coinImgView.image = [UIImage imageNamed:@"home_ranking_coin2"];
    [self addSubview:self.coinImgView];
    
    self.coinNameLabel = [GWViewTool createLabelFont:@"提示" textColor:@"黑"];
    self.coinNameLabel.font = [self.coinNameLabel.font boldFont];
    [self addSubview:self.coinNameLabel];
    
    self.guanzhuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:self.guanzhuButton];
}

-(void)setTranscerCellHeight:(CGFloat)transcerCellHeight{
    _transcerCellHeight = transcerCellHeight;
}

-(void)setTransferIndex:(NSInteger)transferIndex{
    _transferIndex = transferIndex;
}

-(void)setTransferSingleModel:(RankingSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // 1. 名词
    self.mingciLabel.text = [NSString stringWithFormat:@"%li",(long)self.transferIndex];
    CGSize mingciSize = [NSString makeSizeWithLabel:self.mingciLabel];
    self.mingciLabel.frame = CGRectMake(LCFloat(11), (self.transcerCellHeight - mingciSize.height) / 2., LCFloat(20), mingciSize.height);
    
    // 2. 创建头像
    [self.avatarImgView uploadImageWithAvatarURL:transferSingleModel.userModel.avatar placeholder:nil callback:NULL];
    self.avatarbgView.frame = CGRectMake(CGRectGetMaxX(self.mingciLabel.frame) + LCFloat(11),(self.transcerCellHeight - LCFloat(38)) / 2. , LCFloat(30), LCFloat(38));
    if (self.transferIndex == 1){
        self.avatarbgView.image = [UIImage imageNamed:@"ig_level_second_place"];
    } else if (self.transferIndex == 2){
        self.avatarbgView.image = [UIImage imageNamed:@"ig_level_third_place"];
    } else {
        self.avatarbgView.image = [UIImage imageNamed:@"ig_level_first_place"];
    }

    self.avatarImgView.frame = CGRectMake(self.avatarbgView.orgin_x + LCFloat(2), self.avatarbgView.orgin_y + LCFloat(2) , LCFloat(30) - 2 * LCFloat(2), LCFloat(30) - 2 * LCFloat(2));

    // 3. 创建名字
    self.nameLabel.text = transferSingleModel.userModel.nick;
    CGSize nameSize = [NSString makeSizeWithLabel:self.nameLabel];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), self.avatarImgView.orgin_y, nameSize.width, nameSize.height);
    
    // 4. 创建金币
    self.coinImgView.frame = CGRectMake(self.nameLabel.orgin_x, 0, LCFloat(10), LCFloat(10));
    
    if (self.transferIndex == 1){
        self.coinImgView.image = [UIImage imageNamed:@"home_ranking_coin2"];
        self.coinNameLabel.textColor = RGB(189, 52, 167, 1);
    } else if (self.transferIndex == 2){
        self.coinImgView.image = [UIImage imageNamed:@"home_ranking_coin3"];
        self.coinNameLabel.textColor = RGB(0, 144, 222, 1);
    } else {
        self.coinImgView.image = [UIImage imageNamed:@"home_ranking_coin4"];
        self.coinNameLabel.textColor = RGB(255, 0, 186, 1);
    }
    
    // 5. 金币label
    self.coinNameLabel.text = [NSString stringWithFormat:@"%li",transferSingleModel.count];
    CGSize coinSize = [NSString makeSizeWithLabel:self.coinNameLabel];
    self.coinNameLabel.frame = CGRectMake(CGRectGetMaxX(self.coinImgView.frame) + LCFloat(3), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), coinSize.width, coinSize.height);
    self.coinImgView.center_y = self.coinNameLabel.center_y;
    
    // 6.按钮
    if (transferSingleModel.userModel.link){
        [self.guanzhuButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_hlt"] forState:UIControlStateNormal];
    } else {
        [self.guanzhuButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_nor"] forState:UIControlStateNormal];
    }
    self.guanzhuButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(30), (self.transcerCellHeight - LCFloat(30)) / 2., LCFloat(30), LCFloat(30));
    __weak typeof(self)weakSelf = self;
    [self.guanzhuButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        void(^block)(RankingSingleModel *transferSingleModel) = objc_getAssociatedObject(strongSelf, &guanzhuButtonClickKey);
        if(block){
            block(transferSingleModel);
        }
    }];
    
    // 调整
    CGFloat margin = (self.transcerCellHeight - [NSString contentofHeightWithFont:self.nameLabel.font] - [NSString contentofHeightWithFont:self.coinNameLabel.font]) / 3.;
    self.nameLabel.orgin_y = margin;
    self.coinNameLabel.orgin_y = CGRectGetMaxY(self.nameLabel.frame) + margin;
    self.coinImgView.center_y = self.coinNameLabel.center_y;
    
    
    [self loadContentWithIndex:self.transferIndex + 1];
}




-(void)loadContentWithIndex:(NSInteger)index{
    NSString *fullStirng = [NSString stringWithFormat:@"%02li. ", (long)index ];
    NSMutableAttributedString *richString = [[NSMutableAttributedString alloc] initWithString:fullStirng];
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont HeitiSCWithFontSize:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, richString.length);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont fontWithName:@"GillSans-Italic" size:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, 3);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [[UIColor blackColor] colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, richString.length);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [UUBlue colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, 3);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    self.mingciLabel.attributedText = richString;
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (self.highlighted){
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration           = 0.1f;
        scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
        [self.coinNameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    } else {
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        scaleAnimation.springBounciness    = 20.f;
        [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
        [self.coinNameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    }
}


#pragma mark 飘动过去Animation
- (void)startAnimationWithDelay:(CGFloat)delayTime {
    self.nameLabel.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    self.coinNameLabel.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    self.coinImgView.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.nameLabel.transform = CGAffineTransformIdentity;
        self.coinNameLabel.transform = CGAffineTransformIdentity;
        self.coinImgView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)guanzhuButtonClick:(void(^)(RankingSingleModel *transferSingleModel))block{
     objc_setAssociatedObject(self, &guanzhuButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)guanzhuBtnClickManagerStatus:(BOOL)status{
    __weak typeof(self)weakSelf = self;
    
    [Tool clickZanWithView:self.guanzhuButton block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (status){
            [strongSelf.guanzhuButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_hlt"] forState:UIControlStateNormal];

        } else {
            [strongSelf.guanzhuButton setImage:[UIImage imageNamed:@"home_ranking_guanzhu2_nor"] forState:UIControlStateNormal];
        }
    }];
    

}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(30);
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end

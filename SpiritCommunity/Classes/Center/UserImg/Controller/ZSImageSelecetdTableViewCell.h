//
//  ZSImageSelecetdTableViewCell.h
//  RegenerationPlan
//
//  Created by SmartMin on 15/11/2.
//  Copyright © 2015年 baimifan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ZSImageSelecetdTableViewCell;

@protocol ZSImageSelectedDelegate <NSObject>

-(void)imageSelectedButtonClick;
-(void)imageSelectedDeleteClick:(NSInteger)imgIndex imageView:(UIView *)imgView andSuperCell:(ZSImageSelecetdTableViewCell *)cell;

@end

@interface ZSImageSelecetdTableViewCell : UITableViewCell

@property (nonatomic,strong)NSArray *isSelectedImageArr;
@property (nonatomic,weak)id<ZSImageSelectedDelegate> delegate;
@property (nonatomic,strong)UIView *bgView;

+(CGFloat)calculationCellHeightWithImgArr:(NSArray *)isSelectedImageArr;
@end

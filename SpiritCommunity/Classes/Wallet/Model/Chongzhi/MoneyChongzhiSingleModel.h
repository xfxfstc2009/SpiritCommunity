//
//  MoneyChongzhiSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/7.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
#import "ChongZhiSingleModelSimple.h"


@class MoneyChongzhiSingleModel;

@protocol MoneyChongzhiSingleModel <NSObject>

@end

@interface MoneyChongzhiSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)NSInteger cost;
@property (nonatomic,assign)NSInteger price;

@end

@interface MoneyChongzhiListModel:FetchModel

@property (nonatomic,strong)NSArray<MoneyChongzhiSingleModel> *list;

@end


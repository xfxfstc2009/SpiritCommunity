//
//  LiveReleaseViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import "HomeLiveSingleModel.h"

@interface LiveReleaseViewController : AbstractViewController



-(void)liveReleaseBtnClickManager:(void(^)(HomeLiveSingleModel *liveModel))block;

@end

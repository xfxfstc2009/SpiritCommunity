//
//  CartBottomView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "CartBottomView.h"

static char allKey;
static char buyKey;

@interface CartBottomView()
@property (nonatomic,strong)UIButton *buyButton;
@property (nonatomic,strong)UILabel *countFixedLabel;
@property (nonatomic,strong)UILabel *priceLabel;

@end

@implementation CartBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        self.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        self.layer.shadowOpacity = .2f;
        self.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1.创建全选按钮
    self.allButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.allButton.backgroundColor = [UIColor clearColor];
    [self.allButton setImage:[UIImage imageNamed:@"dizhi_xuanzhong"] forState:UIControlStateNormal];
    [self.allButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.allButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &allKey);
        if (block){
            block();
        }
    }];
    [self.allButton setTitle:@"全选" forState:UIControlStateNormal];
    self.allButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    [self addSubview:self.allButton];
    
    CGSize allSize = [@"全选" sizeWithCalcFont:self.allButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.allButton.titleLabel.font])];
    self.allButton.frame = CGRectMake(LCFloat(11), 0, allSize.width +LCFloat(20), self.size_height);
    
    // 2. 创建购买按钮
    self.buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.buyButton.backgroundColor = [UIColor colorWithCustomerName:@"蓝"];
    [self.buyButton setTitle:@"立即购买" forState:UIControlStateNormal];
    [self.buyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.buyButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.buyButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &buyKey);
        if (block){
            block();
        }
    }];
    self.buyButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(110), 0, LCFloat(110), self.size_height);
    [self addSubview:self.buyButton];
    
    // 3. 创建价格
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.priceLabel.textColor = [UIColor colorWithCustomerName:@"蓝"];
    self.priceLabel.textAlignment = NSTextAlignmentCenter;
    self.priceLabel.adjustsFontSizeToFitWidth = YES;
    self.priceLabel.text = [NSString stringWithFormat:@"%@0.00",@"￥"];
    [self addSubview:self.priceLabel];
    NSString *sizeStr = [NSString stringWithFormat:@"%@10000.00",@"￥"];
    CGSize priceSize = [sizeStr sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    self.priceLabel.frame = CGRectMake(self.buyButton.orgin_x - priceSize.width, 0, priceSize.width, self.size_height);
    

    // 4. 创建合计
    self.countFixedLabel = [[UILabel alloc]init];
    self.countFixedLabel.backgroundColor = [UIColor clearColor];
    self.countFixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.countFixedLabel.text = @"合计";
    self.countFixedLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    
    CGSize hejiSize = [self.countFixedLabel.text sizeWithCalcFont:self.countFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countFixedLabel.font])];
    self.countFixedLabel.frame = CGRectMake(self.priceLabel.orgin_x - hejiSize.width, 0, hejiSize.width, self.size_height);
    [self addSubview:self.countFixedLabel];
}

-(void)actionClickWithChooseAll:(void(^)())block{
    objc_setAssociatedObject(self, &allKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionClickWithBuyNow:(void (^)())block{
    objc_setAssociatedObject(self, &buyKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setPrice:(CGFloat)price{
    self.priceLabel.text = [NSString stringWithFormat:@"%@%.2f",@"￥",price];
}

@end

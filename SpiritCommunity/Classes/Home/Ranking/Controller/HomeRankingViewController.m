//
//  HomeRankingViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//
// 排行榜
#import "HomeRankingViewController.h"

#import "RankingOneTableViewCell.h"             // 第一名
#import "RankingTwoTableViewCell.h"
#import "RankingListModel.h"
#import "ToLinkSingleModel.h"
#import "UserInfoViewController.h"

@interface HomeRankingViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)NSMutableArray *shouliMutableArr;
@property (nonatomic,strong)NSMutableArray *songliMutableArr;
@property (nonatomic,strong)UITableView *shouliTableView;
@property (nonatomic,strong)UITableView *songliTableView;
@end

@implementation HomeRankingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSegment];
    [self createMainScrollView];
    [self sendRequestToGetInfo:1];
    [self sendRequestToGetInfo:2];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"排行榜";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.shouliMutableArr = [NSMutableArray array];
    self.songliMutableArr = [NSMutableArray array];
}

#pragma mark - segment
-(void)createSegment{
    __weak typeof(self)weakSelf = self;
   self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"收礼",@"送礼"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
       if (!weakSelf){
           return ;
       }
       __strong typeof(weakSelf)strongSelf = weakSelf;
       [strongSelf.mainScrollView setContentOffset:CGPointMake(strongSelf.mainScrollView.size_width * index, 0) animated:YES];
   }];
    self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44));
    [self.view addSubview:self.segmentList];
}

#pragma mark - 创建ScrollView
-(void)createMainScrollView{
    __weak typeof(self)weakSelf = self;
    self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
        [strongSelf.segmentList setSelectedButtonIndex:page animated:YES];
    }];
    self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width,kScreenBounds.size.height - 64 - self.segmentList.size_height);
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, self.mainScrollView.size_height);
    [self.view addSubview:self.mainScrollView];
    [self createTableView];
}

-(void)createTableView{
    if (!self.shouliTableView){
        self.shouliTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.shouliTableView.dataSource = self;
        self.shouliTableView.delegate = self;
        [self.mainScrollView addSubview:self.shouliTableView];
    }
    
    if (!self.songliTableView){
        self.songliTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.songliTableView.orgin_x = kScreenBounds.size.width;
        self.songliTableView.dataSource = self;
        self.songliTableView.delegate = self;
        [self.mainScrollView addSubview:self.songliTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.songliTableView){
        return self.songliMutableArr.count;
    } else if (tableView == self.shouliTableView){
        return self.shouliMutableArr.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        RankingOneTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[RankingOneTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        
        if (tableView == self.songliTableView){
            cellWithRowOne.transferSingleModel = [self.songliMutableArr objectAtIndex:indexPath.row];
        } else if (tableView == self.shouliTableView){
            cellWithRowOne.transferSingleModel = [self.shouliMutableArr objectAtIndex:indexPath.row];
        }
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        RankingTwoTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[RankingTwoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = [UIColor clearColor];
        }
        cellWithRowTwo.transcerCellHeight = cellHeight;
        cellWithRowTwo.transferIndex = indexPath.row;
        if (tableView == self.songliTableView){
            cellWithRowTwo.transferSingleModel = [self.songliMutableArr objectAtIndex:indexPath.row];
        } else if (tableView == self.shouliTableView){
            cellWithRowTwo.transferSingleModel = [self.shouliMutableArr objectAtIndex:indexPath.row];
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo guanzhuButtonClick:^(RankingSingleModel *transferSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            transferSingleModel.userModel.link = !transferSingleModel.userModel.link;
            [strongSelf sendRequestToLinkWithModel:transferSingleModel cell:cellWithRowTwo];
        }];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UserInfoViewController *userInfoController = [[UserInfoViewController alloc]init];
    RankingSingleModel *singleModel;
    if (tableView == self.songliTableView){
        singleModel = [self.songliMutableArr objectAtIndex:indexPath.row];
    } else if (tableView == self.shouliTableView){
        singleModel = [self.shouliMutableArr objectAtIndex:indexPath.row];
    }
    userInfoController.transferSearchSingleModel = singleModel.userModel;
    [self.navigationController pushViewController:userInfoController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return [RankingOneTableViewCell calculationCellHeight];
    } else {
        return [RankingTwoTableViewCell calculationCellHeight];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.shouliTableView) {
        if (indexPath.row >= 1){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if (indexPath.row == 1){
                separatorType = SeparatorTypeMiddle;
            } else if (indexPath.row == self.shouliMutableArr.count){
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"ranking"];
        }
    }
    
    // 动画
    static CGFloat initialDelay = 0.2f;
    static CGFloat stutter = 0.06f;
    if (indexPath.row >= 1){
        RankingTwoTableViewCell *rankingOtherCell = (RankingTwoTableViewCell *)cell;
        RankingSingleModel *singleModel;
        if (tableView == self.shouliTableView){
            singleModel = [self.shouliMutableArr objectAtIndex:indexPath.row];
        } else {
            singleModel = [self.songliMutableArr objectAtIndex:indexPath.row];
        }
//        if(singleModel.hasAnimation == NO){
            [rankingOtherCell startAnimationWithDelay:initialDelay + ((indexPath.row) * stutter)];
//            singleModel.hasAnimation = YES;
//        }
    }
}



#pragma mark - Interface
-(void)sendRequestToGetInfo:(NSInteger)status{
    NSDictionary *params = @{@"status":@(status)};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_ranking requestParams:params responseObjectClass:[RankingListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            RankingListModel *singleModel = (RankingListModel *)responseObject;
            
            if (status == 1){
                [strongSelf.shouliMutableArr removeAllObjects];
                [strongSelf.shouliMutableArr addObjectsFromArray:singleModel.list];
                [strongSelf.shouliTableView reloadData];
                if (strongSelf.shouliMutableArr.count){
                    [strongSelf.shouliTableView dismissPrompt];
                } else {
                    [strongSelf.shouliTableView showPrompt:@"当前没有排行榜信息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
                        [strongSelf sendRequestToGetInfo:1];
                    }];
                }
            } else if (status == 2){
                [strongSelf.songliMutableArr removeAllObjects];
                [strongSelf.songliMutableArr addObjectsFromArray:singleModel.list];
                [strongSelf.songliTableView reloadData];
                
                if (strongSelf.songliMutableArr.count){
                    [strongSelf.songliTableView dismissPrompt];
                } else {
                    [strongSelf.songliTableView showPrompt:@"当前没有排行榜信息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:^{
                        [strongSelf sendRequestToGetInfo:2];
                    }];
                }
            }
            
        }
    }];
}

#pragma mark - toLink
-(void)sendRequestToLinkWithModel:(RankingSingleModel *)singleModel cell:(RankingTwoTableViewCell *)cell{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"toUser":singleModel.userModel.ID};
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_link requestParams:params responseObjectClass:[ToLinkSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ToLinkSingleModel *linkSingleModel = (ToLinkSingleModel *)responseObject;
            
            if (linkSingleModel.haslink){
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"已关注%@",singleModel.userModel.nick]];
            } else {
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"已取消关注%@",singleModel.userModel.nick]];
            }
            [[PDMainTabbarViewController sharedController].centerViewController guanzhuManagerStatus:linkSingleModel.haslink];
            [cell guanzhuBtnClickManagerStatus:linkSingleModel.haslink];
        }
    }];
}


@end

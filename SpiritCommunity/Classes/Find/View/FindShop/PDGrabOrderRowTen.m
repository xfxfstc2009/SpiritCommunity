//
//  PDGrabOrderRowTen.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/12/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDGrabOrderRowTen.h"

@interface PDGrabOrderRowTen()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UIView *errorBgView;
@property (nonatomic,strong)NSArray *errArr;

@end

@implementation PDGrabOrderRowTen

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.errArr = @[@"该商品仅供参考。"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.fixedLabel.text = @"提醒";
    self.fixedLabel.font = [self.fixedLabel.font boldFont];
    CGSize fixedSize =[Tool makeSizeWithLabel:self.fixedLabel];
    self.fixedLabel.frame = CGRectMake(LCFloat(20), LCFloat(28), fixedSize.width, fixedSize.height);
    [self addSubview:self.fixedLabel];

    // 2. 创建error
    self.errorBgView = [[UIView alloc]init];
    [self addSubview:self.errorBgView];
    [self setArrInfo];
}

-(void)setArrInfo{
    if (self.errorBgView.subviews.count){
        [self.errorBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    CGFloat origin_y = CGRectGetMaxY(self.fixedLabel.frame);
    origin_y += LCFloat(18);
    for (int i = 0 ; i < self.self.errArr.count;i++){
        UILabel *fixedLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
        fixedLabel.numberOfLines = 0;
        fixedLabel.text = [self.errArr objectAtIndex:i];
        CGFloat width = kScreenBounds.size.width - 2 * LCFloat(20);
        CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        fixedLabel.frame = CGRectMake(LCFloat(20), origin_y, kScreenBounds.size.width - 2 * LCFloat(20),fixedSize.height);
        [self.errorBgView addSubview:fixedLabel];
        origin_y += fixedSize.height ;
        origin_y += LCFloat(3);
    }

}

+(CGFloat)calculationCellHeight{
    NSArray *infoArr = @[@"1、由于二手商品的不确定性，最好进行线下交易",@"2.由于线上交易双方的未知性，买家进行下单后，平台客服将会在下一个工作日联系买卖双方以促成双方交易",@"3、商品提交仅供参考，本平台不予以追责",@"4.请与卖家进行沟通后，再进行购买"];
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(28);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"正文"] boldFont]];
    cellHeight += LCFloat(18);

    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(20);
    for (int i = 0 ; i < infoArr.count;i++){
        NSString *title = [infoArr objectAtIndex:i];
        CGSize titleSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(width,CGFLOAT_MAX )];
        cellHeight += titleSize.height;
        cellHeight += LCFloat(3);
    }
    cellHeight += LCFloat(20);
    return cellHeight;
}

@end

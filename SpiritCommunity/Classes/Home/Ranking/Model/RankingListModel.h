//
//  RankingListModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/5.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

#import "RankingSingleModel.h"

@interface RankingListModel : FetchModel

@property (nonatomic,strong)NSArray<RankingSingleModel> *list;

@end

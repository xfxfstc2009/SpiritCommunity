//
//  ProductDetailProductNameTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "ProductDetailProductNameTableViewCell.h"

@interface ProductDetailProductNameTableViewCell()
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *descLabel;

@end

@implementation ProductDetailProductNameTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.nameLabel];
    
    // 2. 创建详情
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.backgroundColor = [UIColor clearColor];
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferProductDetailModel:(ProductDetailRootModel *)transferProductDetailModel{
    _transferProductDetailModel = transferProductDetailModel;
    
    // 1.
    self.nameLabel.text = transferProductDetailModel.goodBaseInfo.goodsName;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width- 2 * LCFloat(11), CGFLOAT_MAX)];
    self.nameLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), nameSize.height);
    
    // 2. desc
    self.descLabel.text = transferProductDetailModel.goodBaseInfo.goodsSummary;
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), descSize.height);
}

+(CGFloat)calculationCellHeight:(ProductDetailRootModel *)transferProductDetailModel{
    CGFloat cellHeihgt = 0;
    cellHeihgt += LCFloat(11);
    CGFloat width = kScreenBounds.size.width - 2* LCFloat(11);
    CGSize size = [transferProductDetailModel.goodBaseInfo.goodsName sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeihgt += size.height;
    cellHeihgt += LCFloat(11);
    CGSize descSize = [transferProductDetailModel.goodBaseInfo.goodsSummary sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"提示"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeihgt += descSize.height;
    cellHeihgt += LCFloat(11);
    return cellHeihgt;
    
}

@end

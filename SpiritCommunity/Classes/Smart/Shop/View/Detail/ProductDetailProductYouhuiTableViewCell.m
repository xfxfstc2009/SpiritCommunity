//
//  ProductDetailProductYouhuiTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "ProductDetailProductYouhuiTableViewCell.h"

@interface ProductDetailProductYouhuiTableViewCell()
@property (nonatomic,strong)UILabel *iconLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UIButton *moreButton;

@end

@implementation ProductDetailProductYouhuiTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建iconview
    self.iconLabel = [[UILabel alloc]init];
    self.iconLabel.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    self.iconLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.iconLabel.text = @"优惠";
    self.iconLabel.textAlignment = NSTextAlignmentCenter;
    self.iconLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.iconLabel];
    CGSize iconSize = [self.iconLabel.text sizeWithCalcFont:self.iconLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.iconLabel.font])];
    self.iconLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), iconSize.width + 2 * LCFloat(7), [NSString contentofHeightWithFont:self.iconLabel.font]);
    
    // 2. 创建文字
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.backgroundColor = [UIColor clearColor];
    self.descLabel.text = @"新人福利，首次购买五折优惠";
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.descLabel];
    
    // 3. 创建more
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.moreButton.backgroundColor = [UIColor clearColor];
    [self.moreButton setImage:[UIImage imageNamed:@"xqy_zhankai"] forState:UIControlStateNormal];
    self.moreButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(20), 0, LCFloat(20), LCFloat(4));
    [self addSubview:self.moreButton];
    
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(11) - self.iconLabel.size_width - LCFloat(11) - self.moreButton.size_width;
    CGSize moreSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(width,CGFLOAT_MAX)];
    self.descLabel.frame = CGRectMake(CGRectGetMaxX(self.iconLabel.frame) + LCFloat(11), LCFloat(11), width, moreSize.height);
    
    self.moreButton.center_y = self.iconLabel.center_y;
    
}

+(CGFloat)calculationCellheight{
    return LCFloat(44);
}


@end

//
//  CenterOrderRootSingleView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,CenterOrderRootSingleViewType) {
    CenterOrderRootSingleViewTypeHome = 1,
    CenterOrderRootSingleViewTypeCenter = 2,
};

@interface CenterOrderRootSingleView : UIView


@property (nonatomic,strong)UIImage *image;
@property (nonatomic,copy)NSString *title;

@property (nonatomic,assign)NSInteger number;

@property (nonatomic,assign)CenterOrderRootSingleViewType tranferType;

-(void)actionClickBlock:(void(^)())block;

@end

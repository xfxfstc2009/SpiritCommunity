//
//  HomeGoodsNormalSingleView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/1/15.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "HomeGoodsNormalSingleView.h"


static char actionClickBlockKey;
@interface HomeGoodsNormalSingleView()
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *productImgView;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *oldPriceLabel;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation HomeGoodsNormalSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.frame = CGRectMake(LCFloat(3), LCFloat(3), self.size_width - 2 * LCFloat(3), self.size_width - 2 * LCFloat(3));
    self.convertView.clipsToBounds = YES;
    [self addSubview:self.convertView];
    
    self.productImgView = [[PDImageView alloc]init];
    self.productImgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.productImgView];
    
    // 3.
    self.priceLabel = [GWViewTool createLabelFont:@"标题" textColor:@"棕黑"];
    [self addSubview:self.priceLabel];
    
    // 4.
    self.oldPriceLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self addSubview:self.oldPriceLabel];
    
    // 5. 创建名字
    self.nameLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    [self addSubview:self.nameLabel];
    
    // btn
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(HomeNormalSingleGoodsModel *transferHomeNormalGoodsModel) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(strongSelf.transferHomeNormalGoodsModel);
        }
    }];
    [self addSubview:self.actionButton];
}

-(void) setTransferHomeNormalGoodsModel:(HomeNormalSingleGoodsModel *)transferHomeNormalGoodsModel{
    _transferHomeNormalGoodsModel = transferHomeNormalGoodsModel;
    
    // 1.
    __weak typeof(self)weakSelf = self;
    [self.productImgView uploadImageWithURL:transferHomeNormalGoodsModel.goodsImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [Tool setTransferSingleAsset:image imgView:strongSelf.productImgView convertView:strongSelf.convertView];
    }];
    
    // 2.价格
    self.priceLabel.text = [NSString stringWithFormat:@"￥%.2f",transferHomeNormalGoodsModel.salePriceRmb];
    CGSize priceSize = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    self.priceLabel.frame = CGRectMake(self.convertView.orgin_x, CGRectGetMaxY(self.convertView.frame) + LCFloat(6), priceSize.width, [NSString contentofHeightWithFont:self.priceLabel.font]);
    
    // 3.
    NSString *oldPrice = [NSString stringWithFormat:@"￥%.2f",transferHomeNormalGoodsModel.marketPrice];
    self.oldPriceLabel.attributedText = [Tool addLineWithStr:[NSString stringWithFormat:@"%.2f",transferHomeNormalGoodsModel.marketPrice]];
    CGSize oldSize = [oldPrice sizeWithCalcFont:self.oldPriceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.oldPriceLabel.font])];
    self.oldPriceLabel.frame = CGRectMake(CGRectGetMaxX(self.priceLabel.frame) + LCFloat(11), CGRectGetMaxY(self.priceLabel.frame) - [NSString contentofHeightWithFont:self.oldPriceLabel.font], oldSize.width, [NSString contentofHeightWithFont:self.oldPriceLabel.font]);


    
    // 4. name
    self.nameLabel.text = transferHomeNormalGoodsModel.goodsName;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(self.convertView.size_width, CGFLOAT_MAX)];
    if (nameSize.height >= 2 * [NSString contentofHeightWithFont:self.nameLabel.font]){
        self.nameLabel.frame = CGRectMake(self.convertView.orgin_x, CGRectGetMaxY(self.priceLabel.frame) + LCFloat(11), self.convertView.size_width, 2 * [NSString contentofHeightWithFont:self.nameLabel.font]);
        self.nameLabel.numberOfLines = 2;
    } else {
        self.nameLabel.frame = CGRectMake(self.convertView.orgin_x, CGRectGetMaxY(self.priceLabel.frame) + LCFloat(11), self.convertView.size_width, 1 * [NSString contentofHeightWithFont:self.nameLabel.font]);
        self.nameLabel.numberOfLines = 1;
    }
    
    // btn
    self.actionButton.frame = self.bounds;
    
}


+(CGSize)calculationSize{
    CGFloat width = (kScreenBounds.size.width - 3 * LCFloat(6)) / 2.;
    CGFloat height = 0;
    height += LCFloat(width);
    height += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"标题"]];
    height += LCFloat(11);
    height += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    height += LCFloat(5);
    return CGSizeMake(width, height);
}

-(void)actionClickBlock:(void(^)(HomeNormalSingleGoodsModel *transferHomeNormalGoodsModel))block{
    objc_setAssociatedObject(self, &actionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end

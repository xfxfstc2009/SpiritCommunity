//
//  CenterFansViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "CenterFansViewController.h"
#import "OtherSearchListModel.h"
#import "SearchSingleTableViewCell.h"
#import "ToLinkSingleModel.h"
#import "UserInfoViewController.h"

@interface CenterFansViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)NSMutableArray *fansMutableArr;
@property (nonatomic,strong)UITableView *fansTableView;
@property (nonatomic,strong)NSMutableArray *linkMutableArr;
@property (nonatomic,strong)UITableView *linkTableView;
@end

@implementation CenterFansViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createMainScrollView];
    [self sendRequestToGetInfoWithType:CenterFansViewControllerTypeLink];
    [self sendRequestToGetInfoWithType:CenterFansViewControllerTypeFans];
}



#pragma mark - pageSetting
-(void)pageSetting{
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    // segment
    __weak typeof(self)weakSelf = self;
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"关注",@"粉丝"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
    }];
    self.segmentList.size_width = 200;
    self.navigationItem.titleView = self.segmentList;
}

-(void)arrayWithInit{
    self.fansMutableArr = [NSMutableArray array];
    self.linkMutableArr = [NSMutableArray array];
}

#pragma mark - createMainScrollView
-(void)createMainScrollView{
    __weak typeof(self)weakSelf = self;
    self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger index = scrollView.contentOffset.x / kScreenBounds.size.width;
        [strongSelf.segmentList setSelectedButtonIndex:index animated:YES];
    }];
    if (IS_iPhoneX){
        self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 160);
    } else {
        self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64);
    }
    self.mainScrollView.contentSize = CGSizeMake(2 * kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.view addSubview:self.mainScrollView];
    [self createTableView];
}

-(void)createTableView{
    if (!self.linkTableView){
        self.linkTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.linkTableView.dataSource = self;
        self.linkTableView.delegate = self;
        [self.mainScrollView addSubview:self.linkTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.linkTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithType:CenterFansViewControllerTypeLink];
    }];
    [self.linkTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithType:CenterFansViewControllerTypeLink];
    }];
    

    if (!self.fansTableView){
        self.fansTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.fansTableView.orgin_x = kScreenBounds.size.width;
        self.fansTableView.dataSource = self;
        self.fansTableView.delegate = self;
        [self.mainScrollView addSubview:self.fansTableView];
    }
    [self.fansTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithType:CenterFansViewControllerTypeFans];
    }];
    [self.fansTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithType:CenterFansViewControllerTypeFans];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.fansTableView){
        return self.fansMutableArr.count;
    } else if (tableView == self.linkTableView){
        return self.linkMutableArr.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    SearchSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[SearchSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    cellWithRowOne.transferType = SearchControllerTypeFans;
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (tableView == self.fansTableView){
        cellWithRowOne.transferSingleModel = [self.fansMutableArr objectAtIndex:indexPath.row];
    } else if (tableView == self.linkTableView){
        cellWithRowOne.transferSingleModel = [self.linkMutableArr objectAtIndex:indexPath.row];
    }

    __weak typeof(self)weakSelf = self;
    [cellWithRowOne actionClickWithLinkWithBlock:^(BOOL status) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToLinkManagerModel:cellWithRowOne link:status];
    }];
    
    return cellWithRowOne;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

        SeparatorType separatorType = SeparatorTypeMiddle;
        if (indexPath.row == 0){
            separatorType = SeparatorTypeHead;
        } else if (indexPath.row == self.fansMutableArr.count){
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"ranking"];

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [SearchSingleTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UserInfoViewController *userInfoViewController = [[UserInfoViewController alloc]init];
    OtherSearchSingleModel *singleModel;
    if (tableView == self.fansTableView){
        singleModel = [self.fansMutableArr  objectAtIndex:indexPath.row];
    } else if (tableView == self.linkTableView){
        singleModel = [self.linkMutableArr  objectAtIndex:indexPath.row];
    }
    userInfoViewController.transferSearchSingleModel = singleModel;
    [self.navigationController pushViewController:userInfoViewController animated:YES];
}


#pragma mark - 关注
-(void)sendRequestToLinkManagerModel:(SearchSingleTableViewCell *)cell link:(BOOL)link{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"toUser":cell.transferSingleModel.ID,@"action":@(link)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:home_link requestParams:params responseObjectClass:[ToLinkSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        if (isSucceeded){
            ToLinkSingleModel *singleModel = (ToLinkSingleModel *)responseObject;
            if (singleModel.haslink){
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"已关注%@",cell.transferSingleModel.nick]];
            } else {
                [StatusBarManager statusBarHidenWithText:[NSString stringWithFormat:@"已取消关注%@",cell.transferSingleModel.nick]];
            }
            [[PDMainTabbarViewController sharedController].centerViewController guanzhuManagerStatus:singleModel.haslink];
            [cell actionClickZan:singleModel.haslink];
        }
    }];
}




#pragma mark - sendRequestToGetInfo
-(void)sendRequestToGetInfoWithType:(CenterFansViewControllerType)type{
    __weak typeof(self)weakSelf = self;
    NSString *path = @"";
    NSDictionary *params;
    if (type == CenterFansViewControllerTypeFans){
        path = my_fans;
        params = @{@"page":@(self.fansTableView.currentPage),@"size":@"20"};
    } else if (type == CenterFansViewControllerTypeLink){
        path = my_link;
        params = @{@"page":@(self.linkTableView.currentPage),@"size":@"20"};
    }
    
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:params responseObjectClass:[OtherSearchListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            OtherSearchListModel *searchListModel = (OtherSearchListModel *)responseObject;
            if (type == CenterFansViewControllerTypeFans){              // 粉丝
                if(strongSelf.fansTableView.isXiaLa){
                    [strongSelf.fansMutableArr removeAllObjects];
                }
                [strongSelf.fansMutableArr addObjectsFromArray:searchListModel.list];
                [strongSelf.fansTableView reloadData];

                if(strongSelf.fansMutableArr.count){
                    [strongSelf.fansTableView dismissPrompt];
                } else {
                    [strongSelf.fansTableView showPrompt:@"当前没有粉丝哦，快去直播吧" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
                }
                [strongSelf.fansTableView stopPullToRefresh];
                [strongSelf.fansTableView stopFinishScrollingRefresh];
                
            } else if (type == CenterFansViewControllerTypeLink){
                if(strongSelf.linkTableView.isXiaLa){
                    [strongSelf.linkMutableArr removeAllObjects];
                }
                [strongSelf.linkMutableArr addObjectsFromArray:searchListModel.list];
                [strongSelf.linkTableView reloadData];

                if(strongSelf.linkMutableArr.count){
                    [strongSelf.linkTableView dismissPrompt];
                } else {
                     [strongSelf.linkTableView showPrompt:@"当前没有关注哦，快去关注吧" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
                }
                
                [strongSelf.linkTableView stopPullToRefresh];
                [strongSelf.linkTableView stopFinishScrollingRefresh];
            }
        }
    }];
}
@end

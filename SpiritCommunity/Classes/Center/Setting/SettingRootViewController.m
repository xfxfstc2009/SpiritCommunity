//
//  SettingRootViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "SettingRootViewController.h"
#import "GWSuggestionViewController.h"
#import "ShareRootViewController.h"                     // 分享
#import "CenterShareSingleModel.h"

@interface SettingRootViewController ()<UITableViewDataSource,UITableViewDelegate>{
    CenterShareSingleModel *centerSingleModel;
}
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSArray *settingArr;
@end

@implementation SettingRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetShareInfoWithBlock:NULL];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"设置";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"center_share_icon"] barHltImage:[UIImage imageNamed:@"center_share_icon"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ShareRootViewController *shareViewController = [[ShareRootViewController alloc]init];
        shareViewController.isHasGesture = YES;
        [shareViewController actionClickWithShareBlock:^(NSString *type) {
            if (!weakSelf){
                return ;
            }
            SSDKPlatformType mainType = SSDKPlatformSubTypeQZone;
            if ([type isEqualToString:@"QQ"]){
                mainType = SSDKPlatformTypeQQ;
            } else if ([type isEqualToString:@"wechat"]){
                mainType = SSDKPlatformTypeWechat;
            } else if ([type isEqualToString:@"friendsCircle"]){
                mainType = SSDKPlatformSubTypeWechatTimeline;
            }
            
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf->centerSingleModel){
                NSString *img = @"";
                if (centerSingleModel.imgs.length){
                    img =  [NSString stringWithFormat:@"%@%@",aliyunOSS_BaseURL,strongSelf->centerSingleModel.imgs];
                }
                [[ShareSDKManager sharedShareSDKManager] shareManagerWithType:mainType title:strongSelf->centerSingleModel.title desc:strongSelf->centerSingleModel.desc img:img url:strongSelf->centerSingleModel.url callBack:^(BOOL success) {
                    [[UIAlertView alertViewWithTitle:@"分享成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
                }];
            }
        }];
        [shareViewController showInView:strongSelf.parentViewController];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.settingArr = @[@[@"清除缓存",@"问题反馈"],@[@"退出登录"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.settingTableView.dataSource = self;
        self.settingTableView.delegate = self;
        [self.view addSubview:self.settingTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.settingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.settingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = [[self.settingArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if (indexPath.row == 0){
            cellWithRowOne.transferDesc = [PDImageView diskCount];
        } else {
            cellWithRowOne.transferDesc = @"";
        }
        cellWithRowOne.transferHasArrow = YES;
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if ([AccountModel sharedAccountModel].loginModel.ID.length){
            cellWithRowTwo.transferTitle = @"退出登录";
        } else {
            cellWithRowTwo.transferTitle = @"登录";
        }
        
        [cellWithRowTwo setButtonStatus:YES];
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if ([AccountModel sharedAccountModel].loginModel.ID.length){
                [strongSelf logoutManager];
            } else {
                [strongSelf loginManager];
            }
        }];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0){
        if (indexPath.row == 1){
            GWSuggestionViewController*suggestiongViewController = [[GWSuggestionViewController alloc]init];
            [self.navigationController pushViewController:suggestiongViewController animated:YES];
        } else if (indexPath.row == 0){
             [StatusBarManager statusBarHidenWithText:@"正在清除缓存中……"];
            __weak typeof(self)weakSelf = self;
            [PDImageView cleanImgCacheWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [StatusBarManager statusBarHidenWithText:@"缓存清除成功"];
                [strongSelf.settingTableView reloadData];
            }];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return LCFloat(44);
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.settingTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.settingArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.settingArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(void)logoutManager{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] logoutManagerWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [StatusBarManager statusBarHidenWithText:@"退出登录成功"];
            [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
//            [[CenterRootViewController sharedAccountModel] logoutManager];
        });
    }];
}

-(void)loginManager{
    __strong typeof(self)weakSelf = self;
    LoginViewController *loginViewController = [[LoginViewController alloc]init];
    [loginViewController loginSuccess:^(BOOL success) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (success){
            [StatusBarManager statusBarHidenWithText:@"登录成功"];
            [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
//            [[CenterRootViewController sharedAccountModel] logoutManager];
        }
    }];
    UINavigationController *loginNav = [[UINavigationController alloc]initWithRootViewController:loginViewController];
    [self.navigationController presentViewController:loginNav animated:YES completion:NULL];

}



#pragma mark - 获取当前的分享内容
-(void)sendRequestToGetShareInfoWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:setting_share requestParams:nil responseObjectClass:[CenterShareSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->centerSingleModel = (CenterShareSingleModel *)responseObject;
            if (block){
                block();
            }
        }
    }];
}


@end

//
//  LiveUITopView.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeLiveSingleModel.h"

@interface LiveUITopView : UIView

@property (nonatomic,strong)HomeLiveSingleModel *transferLiveSingleModel;   /**< 传递过去的直播Model*/

-(void)changeCustomerCount:(NSInteger)count;

-(void)actionClickWithLiveTopBlock:(void(^)())block;

@end

//
//  LiveShowUserSingleModel.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/27.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LiveShowUserSingleModel.h"

@implementation LiveShowUserSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end

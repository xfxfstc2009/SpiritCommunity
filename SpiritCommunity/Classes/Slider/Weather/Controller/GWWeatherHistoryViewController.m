//
//  GWWeatherHistoryViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2017/5/8.
//  Copyright © 2017年 Gigantic. All rights reserved.
//

#import "GWWeatherHistoryViewController.h"
#import "GWWeatherFeatureTableViewCell.h"

@interface GWWeatherHistoryViewController()<UITableViewDataSource,UITableViewDelegate>


@end

@implementation GWWeatherHistoryViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"标题";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.historyMutableArr = [NSMutableArray array];
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.historyTableView){
        self.historyTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.historyTableView.delegate = self;
        self.historyTableView.dataSource = self;
        [self.view addSubview:self.historyTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.historyMutableArr.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowTow = @"cellIdengifyWithRowTow";
    GWWeatherFeatureTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTow];
    if (!cellWithRowTwo){
        cellWithRowTwo = [[GWWeatherFeatureTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTow];
        cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowTwo.cellheight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    GWWeatherFutureModel *featureSingleModel = [self.historyMutableArr objectAtIndex:indexPath.row];
    cellWithRowTwo.transferFutureModel = featureSingleModel;
    return cellWithRowTwo;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWWeatherFeatureTableViewCell calculationCellHeight];
}


@end

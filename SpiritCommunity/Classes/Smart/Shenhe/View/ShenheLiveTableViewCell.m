//
//  ShenheLiveTableViewCell.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/10.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "ShenheLiveTableViewCell.h"
#import "ZFPlayer.h"
#import "ZFPlayerSingleton.h"
#import <objc/runtime.h>
#import "GWStudyItem.h"
#import <pop/POP.h>
#import <AVFoundation/AVFoundation.h>

static char playerButtonClickKey;

@interface ShenheLiveTableViewCell()
@property (nonatomic,strong)UIButton *playButton;           /**< 播放按钮*/
@property (nonatomic,strong)UILabel *playerNameLabel;
@property (nonatomic,strong)UILabel *createTimeLabel;       /**< 创建时间label*/

@end

@implementation ShenheLiveTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView;
-(void)createView{
    UIImageView *bgView = [[UIImageView alloc]init];
    self.backgroundView.userInteractionEnabled = YES;
    bgView.image = [Tool stretchImageWithName:@"common_card_background"];
    self.backgroundView = bgView;

    // 1. 创建头部图片
    self.playerImageView = [[PDImageView alloc]init];
    self.playerImageView.userInteractionEnabled = YES;
    self.playerImageView.backgroundColor = [UIColor clearColor];
    self.playerImageView.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200));
    self.playerImageView.tag = 102;
    self.playerImageView.userInteractionEnabled = YES;
    [self addSubview:self.playerImageView];

    // 2. 创建按钮
    self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.playButton.backgroundColor = [UIColor clearColor];
    self.playButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [weakSelf.playButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(ShenheLiveSIngleModel *transferPlayerSingleModel) = objc_getAssociatedObject(strongSelf, &playerButtonClickKey);
        if (block){
            block(self.transferLiveSingleModel);
        }
    }];
    self.playButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(50)) / 2., (self.playerImageView.size_height - LCFloat(50)) / 2., LCFloat(50), LCFloat(50));
    [self.playButton setImage:[UIImage imageNamed:@"icon_player_play"] forState:UIControlStateNormal];
    [self.playerImageView addSubview:self.playButton];

    // 3. 创建名字label
    self.playerNameLabel = [[UILabel alloc]init];
    self.playerNameLabel.backgroundColor = [UIColor clearColor];
    self.playerNameLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.playerNameLabel.numberOfLines = 0;
    [self.backgroundView addSubview:self.playerNameLabel];

    // 4. 创建createTimeLabel
    self.createTimeLabel = [[UILabel alloc]init];
    self.createTimeLabel.backgroundColor = [UIColor clearColor];
    self.createTimeLabel.font = [UIFont AvenirLightWithFontSize:10.f];
    self.createTimeLabel.textAlignment = NSTextAlignmentRight;
    self.createTimeLabel.textColor = [UIColor grayColor];
    [self.backgroundView addSubview:self.createTimeLabel];
}

-(void)setTransferIndexPath:(NSIndexPath *)transferIndexPath{
    _transferIndexPath = transferIndexPath;
}

-(void)setTransferLiveSingleModel:(ShenheLiveSIngleModel *)transferLiveSingleModel{
    _transferLiveSingleModel = transferLiveSingleModel;
    //1. 设置图片
    if(transferLiveSingleModel.img.length){
        [self.playerImageView uploadImageWithURL:transferLiveSingleModel.img placeholder:nil callback:NULL];
    } else {
        self.playerImageView.image = [Tool getPlayerThumbnailImageWithURL:transferLiveSingleModel.url];
    }

    // 2. 创建label
    self.playerNameLabel.text = transferLiveSingleModel.title;
    CGSize contentOfSize = [transferLiveSingleModel.title sizeWithCalcFont:[UIFont systemFontOfCustomeSize:12.] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.playerNameLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.playerImageView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), contentOfSize.height);

    GWStudyItem *item = [GWStudyItem itemWithName:transferLiveSingleModel.title object:@"123"];
    item.index = self.transferIndexPath.section + 1;
    [item createAttributedString];
    self.playerNameLabel.attributedText = item.nameString;

    // 3.获取创建时间
    self.createTimeLabel.text = transferLiveSingleModel.des;
    self.createTimeLabel.frame = CGRectMake(self.playerNameLabel.orgin_x, CGRectGetMaxY(self.playerNameLabel.frame) + LCFloat(5), self.playerNameLabel.size_width, [NSString contentofHeightWithFont:self.createTimeLabel.font]);
}


-(void)playerViewClickManagerWithBlock:(void(^)(ShenheLiveSIngleModel *singleModel))block{
    objc_setAssociatedObject(self, &playerButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeightWithPlayerSingleModel:(ShenheLiveSIngleModel *)transferPlayerSingleModel{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(200);
    cellHeight += LCFloat(11);
    CGSize contentOfSize = [transferPlayerSingleModel.title sizeWithCalcFont:[UIFont systemFontOfCustomeSize:12.] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeightWithFont:[UIFont AvenirLightWithFontSize:9.f]];
    cellHeight += LCFloat(11);
    return cellHeight;
}


- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];

    if (self.highlighted) {
        [self showBlunesManager1];

    } else {
        [self showBlunesManager];
    }
}

-(void)showBlunesManager{
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.duration           = 0.2f;
    scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
    [self.playerNameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.playButton pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

-(void)showBlunesManager1{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(6, 6)];
    scaleAnimation.springBounciness    = 1.f;
    [self.playerNameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.playButton pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)startAnimationWithDelay:(CGFloat)delayTime {
    self.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
             [self showBlunesManager];
    }];
}



@end

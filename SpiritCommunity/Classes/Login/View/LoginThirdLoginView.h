//
//  LoginThirdLoginView.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginThirdLoginView : UIView

@property (nonatomic,strong)NSArray *transferArr;

-(void)actionButtonClick:(void(^)(NSString *key))block;

@end

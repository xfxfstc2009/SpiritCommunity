//
//  MessageSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/15.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol MessageSingleModel <NSObject>

@end

@interface MessageSingleModel : FetchModel


@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *desc;
@property (nonatomic,assign)NSTimeInterval time;
@property (nonatomic,assign)BOOL has_read;
@property (nonatomic,copy)NSString *direct;
@property (nonatomic,assign)ActionClickType message_type;
@end
